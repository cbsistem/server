var rateObject = {
	urlRate : '/rate/rate.php',
	rate : function(obj) {
		obj.on('click', function(e) {
			var thisObj = $(this);
			var thisType = thisObj.hasClass('rateUp') ? 'up' : 'down';
			var thisItem = thisObj.attr('data-item');
			var thisValue = thisObj.children('span').text();
			jQuery.getJSON(rateObject.urlRate, { type : thisType, item : thisItem }, function(data) {
				if (!data.error) {
					thisObj.children('span').html(parseInt(thisValue, 10) + 1);
					thisObj.parent('.rateWrapper').find('.rate').addClass('rateDone').removeClass('rate');
					thisObj.addClass('active');
				}
			});
			e.preventDefault();
		});
	},
};
$(function() {
	jQuery.ajaxSetup({ cache:false });
	rateObject.rate($('.rate'));
});

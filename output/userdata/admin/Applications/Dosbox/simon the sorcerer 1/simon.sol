open drawer
get scissors
get magnet
leave
right
get rope
get clapper
use rope on magnet (get magnet on rope)
upper left
open door
enter pub
use scissors on dwarf (get his beard)
get matches (on fruit machine)
right
talk (tell them you do anything to be a wizard)
  they tell you you need a magic staff
leave pub
left
doorway
left
if that guy speaks to you talk or abort talk (doesn't matter)
left
get ladder
enter house
get cold remedy potion
get specimen jar
leave
right
right
right
left
right
right
talk (remove thorn, receive whistle)
left
down
right
do something with stump until woodworms talk to you
  talk until you promise to bring them mahogany
down
left
move handle
get bucket
right
right
right
right
talk to troll 
  talk until he takes the whistle
  the barbarian comes and gets rid of the troll
right
upper right
right
talk to oaf
  tell him he needs to water the beans,
  offer him water, you drown the beans and leave
reenter and get beans from puddle
right
lower right
use clapper on bell
move bell
use hair
  you kiss her and she becomes a pig
leave the tower
use map (troll bridge)
upper left
  so you need something to stop that guy
use map (village)
doorway
left
left
use pig with chocolate truffel door
enter
get hat
get smokebox
leave
use matches with smokebox
get wax from beehive
goto tavern
talk to barkeep, order Wet Wizard
  when the mouse reappears use the wax on the barrel
  he brings the barrel outside because he thinks it is empty
  he gives you a voucher valid from tomorrow
leave
get beer
go behind cottage (left,doorway,doorway,left,middle)
use beans on compost
get melon (for guy with tuba)
use map (troll bridge)
upper left
use melon on sausaphone
left
left
left
talk
get feather
right
right
upper right
right
get rock (center of screen)
look at rock (password for the cave)
wear beard (so dwarves think you are one)
enter mine
say beer
give beer to left one (bribe the guard);   he leaves to the right
follow him
use feather on the sleeping snoring fat guy
get key
up
left (the guard is gone...)
get hook
use key with door
enter
offer voucher, let him give you a gem as reward
leave mine
lower right
right
talk until you get the metal detector
left
left
upper right
right
right
enter house
  tell him you came to see him
  eat that stuff
when he gives you more and the mouse reappears use specimen jar on stew
eat another one, he runs out of stew and leaves to get ingredients
  Optional: move the box and go down, try to go right.
Leave the house
right
right
right
use metal detector
right
use Sausaphone on giant
right
right
enter
use cold remedy on dragon
use hook on boulder
walk boulder
use magnet on hole
use magnet on hole (you need the money for your wizard approval)
go back down
enter
get extinguisher
leave
upper right
get rock (it contains a fossil)
use map (village)
doorway
talk to the "merchant", offer the gem and get 20 bucks for it
doorway
use rock with anvil; the blacksmith frees the fossil
right
right
enter shoppe
buy hammer
buy white spirit (you need to remove paint later)
use map (cross roads)
upper right
vines (lower left of the screen)
talk and offer stew
use map (centre of forest)
upper left
get paper
right
lower left
give fossil to hole (Dr. Von Jones digs there)
  tell him you found it in the mountains so he digs free the mithriel
use map (village)
enter shoppe
give shopping list
use map (giant)
left
look dirt
get ore
use map (village)
open box; goblins take you to their hideaway
open box
get rat bone
look boxes (you notice your spellbook)
get spellbook
look spellbook (you find a scrap of paper)
use paper with door, so a key falling down on the other side lands on it
use rat bone with lock
get paper (with key)
use key on lock
open door
get bucket
down
get mints
get flaming brand
talk (he believes you are a demon)
remove ring
talk; he becomes a frog when he sees full moon (or thinks he does)
use bucket on druid
use flaming brand on druid; he polymorphs
  he escapes, the ring has no more power, so hide
open iron maiden
enter iron maiden (ouch!); days later the frog arrives with a saw
get saw
use saw on bars
use map (village)
doorway
doorway
use ore with anvil; get axe head
map (centre of the forest)
lower right
right
give axe head to woodcutter
enter
get climbing pin
use extinguisher on fireplace
move hook
get mahogany (you are allergic to the rest and the worms want it)
use map (witch)
right
right
talk to stump; they enter the mahogany
use map (crossroad)
lower right
use hair
use woorworm on floor
use ladder on hole
down
open tomb (flee outside)
enter
use ladder
open tomb
move loose bandage (the mummy crumbles to dust)
get staff
use map to go to the wizards in the tavern
give staff to them; pay them; become a wizard
use map (dragon)
upper right
right
ice ledge
right
right
use white spirit on stain; talk to tree; get magic words
use map (witch)
enter
get broom
fight and win, may take a few tries.
  The word you say determines what you become :
    Alcazam - snake
    Hocus Pocus - cat
    Sausages - dog
    Abracadabra - mouse
  when you got the broom, the witch appears as dragon
  use Abracadabra and escape through the mouse hole in the wall
use map (centre of the forest)
upper right
right
right
enter
down
use hammer on plank
right
get frogsbane
use map (village)
doorway
left
enter
talk until he becomes a frog
give frogsbane to frog; receive the potion
use map (dragon)
upper right
right
use pin on hole
consume mints to melt the dangerous snowman
right
tower
use broom when the bridge falls down
drink potion
get leaf
look bucket (get firestick)
get stone
left
use hair with top
get lily leaf
use stick on lily leaf
use leaf on stick
seeds
get seeds
use seeds on stone (get oil)
shore
use oil with tap
move hair
puddle's centre
look water (notice tadpole)
get tadpole
talk frog (threaten tadpole)
get mushroom
eat mushroom
get branch
enter (the chest becomes a monster, you flee outside)
reenter
use branch on chest
get spear
get shield
down
use spear on skull
get skull
get chest
move lever
use chest on block
move lever (crush chest)
move lever
get candles
up
up
get socks (they smell like cheese, you want the mouse)
get pouch
put socks into pouch
use pouch on hole (catch the mouse)
get book
get wand
up
get chemicals
use chemicals on shield (to make it mirror-like)
use shield on hook
down
look mirror (it's a spying device which can see through any mirror-like
  thing, like the shield you prepared)
  you want to teleport away, the demos know how to, but they won't tell
  until you send them to the fiery pits. You need the candles, a mouse,
  a human skull and their true names, which they won't reveal, so you
  need the mirror to spy on them
look at both books
up
talk to demos until you convinced them you are a mighty wizard and
  you will send them back
use teleporter; go to fiery pits
  the mage is there (the demos said is is bathing, there is lava, and
  you have to destroy the wand there)
get pebble
get sapling
talk; he won't let you in, but he will give you brochures
look brochures; receive elastic band
use band on sapling to make catapult
use catapult on bell; he flees
get matches
enter
get wax
right
use wand on Sordid; he petrifies
use matches on pits
use wand on lava
  the wand is gone, all victims unpetrify, so does Sordid
  he zaps you
right; he begins to zap you again
when the mouse appears, use wax on Sordid
  he slips and falls into the lava
  
Congrats for following this walkthrough ;)


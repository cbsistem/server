
var os = require("os");
var dgram = require("dgram");
var socket = dgram.createSocket({type: 'udp4', reuseAddr: true, toString: function () { return 'udp4' }});

var MULTICAST_HOST = "224.0.0.236";
var BROADCAST_HOST = "255.255.255.255";
var ALL_PORT = 60540;
var MULTICAST_TTL = 1; // Local network

module.exports = function(options){

	var instanceId = guid();

	var exports = {};
	var serviceInfos = {};
	var events = {};

	var options = options || {};

	var broadcast = !!options.broadcast;

	var multicastHost = options.host || MULTICAST_HOST;
	var port = options.port || ALL_PORT;
	var ttl = options.ttl || MULTICAST_TTL;
	var sendHost = (broadcast ? BROADCAST_HOST : multicastHost);

	// Services is a map (service.host+":"+service.port+":"+service.name) => Object serviceInfo
	// where serviceInfo is an object like
	// { isOurService : Boolean, service: Object }

	// =====
	// Set up UDP Broadcast/Multicast connection
	// =====

	socket.bind(port);
	socket.on('listening', function() {
		socket.setMulticastLoopback(true);
		socket.setMulticastTTL(ttl);
		socket.addMembership(multicastHost); // Tell the OS to listen for messages on the specified host and treat them as if they were meant for this host
		if(broadcast) {
			socket.setBroadcast(true);
		}
		queryForServices();
	});
	socket.on('message', parseMessage);

	// =====
	// Function to parse incoming messages
	// =====

	function parseMessage(message, rinfo) {
		try {
			var messageObject = JSON.parse(message);
			var eventType = messageObject.eventType;
			var fromDiontId = messageObject.fromDiontInstance;
			if (fromDiontId == instanceId) {
				return;
			}
			if (eventType == "query") {
				var serviceInfosToAnnounce = [];
				for(var index in serviceInfos) {
					serviceInfosToAnnounce.push(serviceInfos[index]);
				}
				sendAnnouncement(serviceInfosToAnnounce);
			} else {
				var receivedServiceInfos = messageObject.serviceInfos;
				for(var serviceInfoIndex in receivedServiceInfos) {
					var serviceInfo = receivedServiceInfos[serviceInfoIndex];
					if(!serviceInfo.service) {
						continue;
					}
					var service = serviceInfo.service;
					if (!service.host || !service.port || !service.name) {
						continue;
					}
					if (eventType == "announce") {
						var id = service.host + ":" + service.port + ":" + service.name;
						if(!serviceInfos[id]) {
							var serviceInfo = serviceInfos[id] = {
								isOurService: false,
								service: service
							}
							if (events["serviceAnnounced"]) {
								for(var callbackId in events["serviceAnnounced"]) {
									var callback = events["serviceAnnounced"][callbackId];
									callback(serviceInfo);
								}
							}
						}
					} else if (eventType == "renounce") {
						var id = service.host + ":" + service.port + ":" + service.name;
						if(serviceInfos[id]) {
							var serviceInfo = serviceInfos[id];
							delete serviceInfos[id];
							if (events["serviceRenounced"]) {
								for(var callbackId in events["serviceRenounced"]) {
									var callback = events["serviceRenounced"][callbackId];
									callback(serviceInfo);
								}
							}
						}
					}
				}
			}
		} catch(e) {
			// ignore...
		}
	};

	// =====
	// Exported functions
	// =====

	exports.announceService = function(service) {
		if (!service.host) {
			service.host = getNetworkIPAddress();
		}
		if (!service.host || !service.port || !service.name) {
			return false;
		}
		var id = service.host + ":" + service.port + ":" + service.name;
		if(!serviceInfos[id]) {
			var serviceInfo = serviceInfos[id] = {
				isOurService: true,
				service: service
			}
			sendAnnouncement(serviceInfo);
		}
		return id;
	}

	exports.renounceService = function(service) {
		var id;
		if (typeof service == 'string') {
			id = service;
		} else {
			if (!service.host || !service.port || !service.name) {
				return false;
			}
			id = service.host + ":" + service.port + ":" + service.name;
		}
		if(serviceInfos[id] && serviceInfos[id].isOurService) {
			sendRenouncement(serviceInfos[id]);
			delete serviceInfos[id];
		}
	}

	exports.repeatAnnouncements = function() {
		for(var id in serviceInfos) {
			var serviceInfo = serviceInfos[id];
			sendAnnouncement(serviceInfo);
		}
	}

	exports.queryForServices = function() {
		queryForServices();
	}

	exports.on = function(eventName, callback) {
		if(!events[eventName]) {
			events[eventName] = {};
		}
		var callbackId = guid();
		events[eventName][callbackId] = callback;
		return callbackId;
	}

	exports.off = function(eventName, callbackId) {
		if(!events[eventName]) {
			return false;
		}
		delete events[eventName][callbackId];
		return true;
	}

	exports.getServiceInfos = function() {
		return JSON.parse(JSON.stringify(serviceInfos));
	}

	// =====
	// Helper functions
	// =====

	function sendAnnouncement(serviceInfo) {
		var serviceInfosToAnnounce = [];
		if (serviceInfo instanceof Array) {
			serviceInfosToAnnounce = serviceInfo;
		} else {
			serviceInfosToAnnounce = [serviceInfo];
		}
		var messageObject = {
			eventType: "announce",
			fromDiontInstance: instanceId,
			serviceInfos: serviceInfosToAnnounce
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function sendRenouncement(serviceInfo) {
		var serviceInfosToRenounce = [];
		if (serviceInfo instanceof Array) {
			serviceInfosToRenounce = serviceInfo;
		} else {
			serviceInfosToRenounce = [serviceInfo];
		}
		var messageObject = {
			eventType: "renounce",
			fromDiontInstance: instanceId,
			serviceInfos: serviceInfosToRenounce
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function queryForServices() {
		var messageObject = {
			eventType: "query",
			fromDiontInstance: instanceId
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	function getNetworkIPAddress() {
		var ifaces = os.networkInterfaces();
		var addresses = [];
		var localAddress;
		for (var dev in ifaces) {
			ifaces[dev].forEach(function(details){
				if (details.family=='IPv4' && details.internal === false) {
					addresses.push(details.address);
					if (details.address.indexOf('192.168.') === 0) {
						localAddress = details.address;
					}
				}
			});
		}
		// Return a 192.168.x.x address if possible, otherwise return the first address found
		if (localAddress) {
			return localAddress;
		}
		return addresses[0];
	}

	// =====
	// Export
	// =====

	return exports;
}
var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"File operation [%s] failed, system threw exception: %s",
	"Structure %s error, method %s.%s threw exception [%s] error",
	"Structure storage failed, structure contains function reference error",
	"Structure storage failed, structure contains symbol reference error",
	"Structure storage failed, structure contains uknown datatype error",
	"Failed to read structure, method %s.%s threw exception [%s] error",
	"Failed to write structure, method %s.%s threw exception [%s] error",
	"Structure data contains invalid or damaged data signature error",
	"Invalid dictionary key",
	"Invalid array of dictionary keys",
	"Options cannot be altered while a logfile is active error",
	"Failed to delete existing logfile (%s): %s",
	"Failed to create logfile (%s): %s",
	"Failed to close logfile (%s): %s",
	"Failed to emit data, no logfile is open",
	"Failed to emit data: %s",
	"Seek failed, stream is empty error",
	"Read operation failed, %s bytes exceeds storage medium error",
	"Read operation failed, invalid signature error [%s]",
	"Bookmarks not supported by medium error",
	"No bookmarks to roll back error",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid datasize [%d] error",
	"'Invalid handle [%s], reference was rejected error",
	"Invalid bit index, expected 0..31",
	"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",
	"Invalid datatype, failed to identify number [integer] type error",
	"Invalid datatype, byte conversion failed error",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"'Invalid handle for object (%s), reference rejected error",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"0123456789",
	"0123456789ABCDEF"];
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function Now() { var d=new Date(); var utcnow=d.getTime()/864e5+25569; var dt = new Date(); var n = dt.getTimezoneOffset(); return utcnow-n/1440 }
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event2(i,f) {
	var li=i,lf=f;
	return function(a,b) {
		return lf.call(li,li,a,b)
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $Div(a,b) { var r=a/b; return (r>=0)?Math.floor(r):Math.ceil(r) }
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
/// TQTXHandleObject = class (TObject)
///  [line: 285, column: 3, file: qtx.classes]
var TQTXHandleObject = {
   $ClassName:"TQTXHandleObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHandle = undefined;
   }
   /// procedure TQTXHandleObject.ObjHandleChanged(const OldHandle: THandle; const NewHandle: THandle)
   ///  [line: 497, column: 28, file: qtx.classes]
   ,ObjHandleChanged:function(Self, OldHandle, NewHandle) {
      /* null */
   }
   /// function TQTXHandleObject.AcceptObjHandle(const NewHandle: THandle) : Boolean
   ///  [line: 476, column: 27, file: qtx.classes]
   ,AcceptObjHandle:function(Self, NewHandle$1) {
      return (NewHandle$1?true:false);
   }
   /// procedure TQTXHandleObject.SetObjHandle(const NewHandle: THandle)
   ///  [line: 487, column: 28, file: qtx.classes]
   ,SetObjHandle:function(Self, NewHandle$2) {
      var LTemp = undefined;
      if (!TQTXHandleObject.AcceptObjHandle(Self,NewHandle$2)) {
         throw EException.CreateFmt$1($New(EQTXHandleError),$R[38],["TQTXHandleObject.SetObjHandle"]);
      }
      LTemp = Self.FHandle;
      Self.FHandle = NewHandle$2;
      TQTXHandleObject.ObjHandleChanged(Self,LTemp,Self.FHandle);
   }
   /// function TQTXHandleObject.GetObjHandle() : THandle
   ///  [line: 482, column: 27, file: qtx.classes]
   ,GetObjHandle:function(Self) {
      return Self.FHandle;
   }
   ,Destroy:TObject.Destroy
};
TQTXHandleObject.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TQTXLogHandleObject = class (TQTXHandleObject)
///  [line: 99, column: 3, file: qtx.logfile]
var TQTXLogHandleObject = {
   $ClassName:"TQTXLogHandleObject",$Parent:TQTXHandleObject
   ,$Init:function ($) {
      TQTXHandleObject.$Init($);
      $.FEmitter = null;
   }
   /// constructor TQTXLogHandleObject.Create()
   ///  [line: 129, column: 33, file: qtx.logfile]
   ,Create$3:function(Self) {
      TObject.Create(Self);
      return Self
   }
   /// destructor TQTXLogHandleObject.Destroy()
   ///  [line: 134, column: 32, file: qtx.logfile]
   ,Destroy:function(Self) {
      Self.FEmitter = null;
      TObject.Destroy(Self);
   }
   /// procedure TQTXLogHandleObject.WriteToLog(Text: String)
   ///  [line: 150, column: 31, file: qtx.logfile]
   ,WriteToLog$1:function(Self, Text$1) {
      if (Self.FEmitter===null) {
         WriteLn$1(Text$1);
      } else {
         Self.FEmitter[1](Text$1);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
};
TQTXLogHandleObject.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TQTXNetworkService = class (TQTXLogHandleObject)
///  [line: 22, column: 3, file: qtx.network.service]
var TQTXNetworkService = {
   $ClassName:"TQTXNetworkService",$Parent:TQTXLogHandleObject
   ,$Init:function ($) {
      TQTXLogHandleObject.$Init($);
      $.OnAfterStopped = null;
      $.OnBeforeStopped = null;
      $.OnAfterStarted = null;
      $.OnBeforeStarted = null;
      $.FActive = false;
      $.FHost = "";
      $.FPort = 0;
   }
   /// procedure TQTXNetworkService.AfterStart()
   ///  [line: 150, column: 30, file: qtx.network.service]
   ,AfterStart:function(Self) {
      if (Self.OnAfterStarted) {
         Self.OnAfterStarted(Self);
      }
   }
   /// procedure TQTXNetworkService.AfterStop()
   ///  [line: 162, column: 30, file: qtx.network.service]
   ,AfterStop:function(Self) {
      if (Self.OnAfterStopped) {
         Self.OnAfterStopped(Self);
      }
   }
   /// procedure TQTXNetworkService.BeforeStart()
   ///  [line: 144, column: 30, file: qtx.network.service]
   ,BeforeStart:function(Self) {
      if (Self.OnBeforeStarted) {
         Self.OnBeforeStarted(Self);
      }
   }
   /// procedure TQTXNetworkService.BeforeStop()
   ///  [line: 156, column: 30, file: qtx.network.service]
   ,BeforeStop:function(Self) {
      if (Self.OnBeforeStopped) {
         Self.OnBeforeStopped(Self);
      }
   }
   /// destructor TQTXNetworkService.Destroy()
   ///  [line: 127, column: 31, file: qtx.network.service]
   ,Destroy:function(Self) {
      if (TQTXNetworkService.GetActive(Self)) {
         try {
            try {
               TQTXNetworkService.FinalizeService$(Self);
            } catch ($e) {
               var e = $W($e);
               /* null */
            }
         } finally {
            TQTXHandleObject.SetObjHandle(Self,undefined);
         }
      }
      TQTXLogHandleObject.Destroy(Self);
   }
   /// function TQTXNetworkService.GetActive() : Boolean
   ///  [line: 194, column: 29, file: qtx.network.service]
   ,GetActive:function(Self) {
      return Self.FActive;
   }
   /// function TQTXNetworkService.GetAddress() : String
   ///  [line: 168, column: 29, file: qtx.network.service]
   ,GetAddress:function(Self) {
      return Self.FHost;
   }
   /// function TQTXNetworkService.GetPort() : Integer
   ///  [line: 181, column: 29, file: qtx.network.service]
   ,GetPort:function(Self) {
      return Self.FPort;
   }
   /// procedure TQTXNetworkService.SetActive(const NewValue: Boolean)
   ///  [line: 204, column: 30, file: qtx.network.service]
   ,SetActive:function(Self, NewValue) {
      if (NewValue!=TQTXNetworkService.GetActive(Self)) {
         if (TQTXNetworkService.GetActive(Self)) {
            TQTXNetworkService.BeforeStop(Self);
            TQTXNetworkService.FinalizeService$(Self);
         } else {
            TQTXNetworkService.BeforeStart(Self);
            TQTXNetworkService.InitializeService$(Self);
         }
      }
   }
   /// procedure TQTXNetworkService.SetAddress(const NewHost: String)
   ///  [line: 173, column: 30, file: qtx.network.service]
   ,SetAddress:function(Self, NewHost) {
      if (TQTXNetworkService.GetActive(Self)) {
         throw Exception.Create($New(EQTXNetworkError),"Address cannot be altered while object is active error");
      } else {
         Self.FHost = Trim$_String_(NewHost);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TQTXLogHandleObject.Create$3
   ,FinalizeService$:function($){return $.ClassType.FinalizeService($)}
   ,InitializeService$:function($){return $.ClassType.InitializeService($)}
};
TQTXNetworkService.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TQTXBoundNetworkService = class (TQTXNetworkService)
///  [line: 62, column: 3, file: qtx.network.service]
var TQTXBoundNetworkService = {
   $ClassName:"TQTXBoundNetworkService",$Parent:TQTXNetworkService
   ,$Init:function ($) {
      TQTXNetworkService.$Init($);
      $.FBindings = null;
   }
   /// constructor TQTXBoundNetworkService.Create()
   ///  [line: 92, column: 37, file: qtx.network.service]
   ,Create$3:function(Self) {
      TQTXLogHandleObject.Create$3(Self);
      Self.FBindings = TObject.Create($New(TQTXHostBindings));
      return Self
   }
   /// destructor TQTXBoundNetworkService.Destroy()
   ///  [line: 98, column: 36, file: qtx.network.service]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBindings);
      TQTXNetworkService.Destroy(Self);
   }
   /// procedure TQTXBoundNetworkService.FinalizeService()
   ///  [line: 116, column: 35, file: qtx.network.service]
   ,FinalizeService:function(Self) {
      var LAccess = null;
      LAccess = $AsIntf(Self.FBindings,"IQTXLockObject");
      LAccess[1]();
   }
   /// function TQTXBoundNetworkService.GetBindings() : IQTXHostBindings
   ///  [line: 104, column: 34, file: qtx.network.service]
   ,GetBindings:function(Self) {
      return $AsIntf(Self.FBindings,"IQTXHostBindings");
   }
   /// procedure TQTXBoundNetworkService.InitializeService()
   ///  [line: 109, column: 35, file: qtx.network.service]
   ,InitializeService:function(Self) {
      var LAccess$1 = null;
      LAccess$1 = $AsIntf(Self.FBindings,"IQTXLockObject");
      LAccess$1[0]();
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,FinalizeService$:function($){return $.ClassType.FinalizeService($)}
   ,InitializeService$:function($){return $.ClassType.InitializeService($)}
};
TQTXBoundNetworkService.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TQTXNetworkServer = class (TQTXBoundNetworkService)
///  [line: 75, column: 3, file: qtx.network.service]
var TQTXNetworkServer = {
   $ClassName:"TQTXNetworkServer",$Parent:TQTXBoundNetworkService
   ,$Init:function ($) {
      TQTXBoundNetworkService.$Init($);
   }
   ,Destroy:TQTXBoundNetworkService.Destroy
   ,Create$3:TQTXBoundNetworkService.Create$3
   ,FinalizeService:TQTXBoundNetworkService.FinalizeService
   ,InitializeService:TQTXBoundNetworkService.InitializeService
};
TQTXNetworkServer.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TQTXUDPServer = class (TQTXNetworkServer)
///  [line: 35, column: 3, file: Unit1]
var TQTXUDPServer = {
   $ClassName:"TQTXUDPServer",$Parent:TQTXNetworkServer
   ,$Init:function ($) {
      TQTXNetworkServer.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 41, column: 44, file: Unit1]
   ,a$6:function(Self) {
      return TQTXHandleObject.GetObjHandle(Self);
   }
   /// procedure TQTXUDPServer.ApplyBindings()
   ///  [line: 112, column: 25, file: Unit1]
   ,ApplyBindings:function(Self) {
      var LCount = 0,
         x$1 = 0;
      var LBinding = null,
         LInfo = null;
      LCount = TQTXBoundNetworkService.GetBindings(Self)[0]();
      var $temp1;
      for(x$1=0,$temp1=LCount;x$1<$temp1;x$1++) {
         LBinding = TQTXBoundNetworkService.GetBindings(Self)[1](x$1);
         LInfo = new dgram_bindinfo();
         LInfo.port = LBinding.FPort$4;
         LInfo.address = LBinding.FHost$2;
         TQTXUDPServer.a$6(Self).bind(LInfo,function () {
            var LMCount = 0,
               y = 0;
            var LMembership = null;
            WriteLn("Binding OK, now adding memberships");
            LMCount = TQTXHostBinding.GetMemberships(LBinding)[0]();
            var $temp2;
            for(y=0,$temp2=LMCount;y<$temp2;y++) {
               LMembership = TQTXHostBinding.GetMemberships(LBinding)[1](y);
               TQTXUDPServer.a$6(Self).addMembership(LMembership.FHost$3);
            }
         });
      }
   }
   /// constructor TQTXUDPServer.Create()
   ///  [line: 62, column: 27, file: Unit1]
   ,Create$3:function(Self) {
      TQTXBoundNetworkService.Create$3(Self);
      return Self
   }
   /// destructor TQTXUDPServer.Destroy()
   ///  [line: 67, column: 26, file: Unit1]
   ,Destroy:function(Self) {
      TQTXBoundNetworkService.Destroy(Self);
   }
   /// procedure TQTXUDPServer.FinalizeService()
   ///  [line: 134, column: 25, file: Unit1]
   ,FinalizeService:function(Self) {
      TQTXUDPServer.a$6(Self).close(function () {
         TQTXNetworkService.AfterStop(Self);
      });
      TQTXBoundNetworkService.FinalizeService(Self);
   }
   /// procedure TQTXUDPServer.InitializeService()
   ///  [line: 72, column: 25, file: Unit1]
   ,InitializeService:function(Self) {
      var LServer = null;
      var LHandle = undefined;
      TQTXBoundNetworkService.InitializeService(Self);
      try {
         LHandle = UdpAPI().createSocket("udp4");
      } catch ($e) {
         var e$1 = $W($e);
         WriteLn("Failed to create socket");
         TQTXLogHandleObject.WriteToLog$1(Self,e$1.FMessage);
      }
      TQTXHandleObject.SetObjHandle(Self,LHandle);
      TQTXUDPServer.ApplyBindings(Self);
      LServer = LHandle;
      LServer.bind(TQTXNetworkService.GetPort(Self),TQTXNetworkService.GetAddress(Self));
      LServer.on("listening",function () {
         WriteLn("UDP-server is now in listen mode");
         TQTXNetworkService.AfterStart(Self);
      });
      LServer.on("message",function (msg, rinfo) {
         WriteLn("message:"+msg);
      });
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,FinalizeService$:function($){return $.ClassType.FinalizeService($)}
   ,InitializeService$:function($){return $.ClassType.InitializeService($)}
};
TQTXUDPServer.$Intf={
   IQTXHandleObject:[TQTXHandleObject.ObjHandleChanged,TQTXHandleObject.AcceptObjHandle,TQTXHandleObject.SetObjHandle,TQTXHandleObject.GetObjHandle]
}
/// TNodeProgram = class (TObject)
///  [line: 47, column: 3, file: Unit1]
var TNodeProgram = {
   $ClassName:"TNodeProgram",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TNodeProgram.Execute()
   ///  [line: 158, column: 24, file: Unit1]
   ,Execute:function(Self) {
      var LServer$1 = null,
         LBinding$1 = null,
         NetAdapters,
         Adapter = "",
         netIntf = "",
         address$9;
      LServer$1 = TQTXLogHandleObject.Create$3$($New(TQTXUDPServer));
      LBinding$1 = TQTXBoundNetworkService.GetBindings(LServer$1)[2]();
      TQTXHostBinding.SetHost(LBinding$1,"192.168.17.146");
      TQTXHostBinding.SetPort$5(LBinding$1,2109);
      TQTXNetworkService.SetActive(LServer$1,true);
      NetAdapters = TApplication.GetOSApi(Application()).networkInterfaces();
      WriteLn("Adapters:");
      for (Adapter in NetAdapters) {
         if (((Adapter).indexOf("Loopback")>=0)) {
            continue;
         } else {
            WriteLn(Adapter);
         }
         for (netIntf in NetAdapters[Adapter]) {
            address$9 = NetAdapters[Adapter][netIntf];
            if (!address$9.internal) {
               if (address$9.address) {
                  WriteLn(address$9.address);
               }
            }
         }
      }
      WriteLn("All adapters listed");
   }
   /// constructor TNodeProgram.Create()
   ///  [line: 148, column: 26, file: Unit1]
   ,Create$6:function(Self) {
      TObject.Create(Self);
      return Self
   }
   /// destructor TNodeProgram.Destroy()
   ///  [line: 153, column: 25, file: Unit1]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// function TW3VariantHelper.DataType(const Self: Variant) : TW3VariantDataType
///  [line: 1944, column: 27, file: System.Types]
function TW3VariantHelper$DataType(Self$1) {
   var Result = 1;
   var LType = "";
   if (TW3VariantHelper$Valid$2(Self$1)) {
      LType = typeof(Self$1);
      {var $temp3 = (LType).toLocaleLowerCase();
         if ($temp3=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp3=="function") {
            Result = 7;
         }
          else if ($temp3=="symbol") {
            Result = 6;
         }
          else if ($temp3=="boolean") {
            Result = 2;
         }
          else if ($temp3=="string") {
            Result = 5;
         }
          else if ($temp3=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp3=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
/// function TW3VariantHelper.IsObject(const Self: Variant) : Boolean
///  [line: 1991, column: 27, file: System.Types]
function TW3VariantHelper$IsObject(Self$2) {
   var Result = false;
   Result = ((Self$2) !== undefined)
      && (Self$2 !== null)
      && (typeof Self$2  === "object")
      && ((Self$2).length === undefined);
   return Result
}
/// function TW3VariantHelper.IsUInt8Array(const Self: Variant) : Boolean
///  [line: 1930, column: 27, file: System.Types]
function TW3VariantHelper$IsUInt8Array(Self$3) {
   var Result = false;
   var LTypeName = "";
   Result = false;
   if (TW3VariantHelper$Valid$2(Self$3)) {
      LTypeName = Object.prototype.toString.call(Self$3);
      Result = LTypeName=="[object Uint8Array]";
   }
   return Result
}
/// function TW3VariantHelper.Valid(const Self: Variant) : Boolean
///  [line: 1902, column: 27, file: System.Types]
function TW3VariantHelper$Valid$2(Self$4) {
   var Result = false;
   Result = !( (Self$4 == undefined) || (Self$4 == null) );
   return Result
}
/// TW3VariantDataType enumeration
///  [line: 574, column: 3, file: System.Types]
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TW3OwnedObject = class (TObject)
///  [line: 373, column: 3, file: System.Types]
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   /// function TW3OwnedObject.GetOwner() : TObject
   ///  [line: 1306, column: 26, file: System.Types]
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   /// procedure TW3OwnedObject.SetOwner(const NewOwner: TObject)
   ///  [line: 1316, column: 26, file: System.Types]
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   /// function TW3OwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1311, column: 25, file: System.Types]
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   /// constructor TW3OwnedObject.Create(const AOwner: TObject)
   ///  [line: 1300, column: 28, file: System.Types]
   ,Create$21:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$21$:function($){return $.ClassType.Create$21.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedObject = class (TW3OwnedObject)
///  [line: 387, column: 3, file: System.Types]
var TW3OwnedLockedObject = {
   $ClassName:"TW3OwnedLockedObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked = 0;
   }
   /// procedure TW3OwnedLockedObject.DisableAlteration()
   ///  [line: 1262, column: 32, file: System.Types]
   ,DisableAlteration:function(Self) {
      ++Self.FLocked;
      if (Self.FLocked==1) {
         TW3OwnedLockedObject.ObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.EnableAlteration()
   ///  [line: 1269, column: 32, file: System.Types]
   ,EnableAlteration:function(Self) {
      if (Self.FLocked>0) {
         --Self.FLocked;
         if (!Self.FLocked) {
            TW3OwnedLockedObject.ObjectUnLocked(Self);
         }
      }
   }
   /// function TW3OwnedLockedObject.GetLockState() : Boolean
   ///  [line: 1279, column: 31, file: System.Types]
   ,GetLockState:function(Self) {
      return Self.FLocked>0;
   }
   /// procedure TW3OwnedLockedObject.ObjectLocked()
   ///  [line: 1284, column: 32, file: System.Types]
   ,ObjectLocked:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.ObjectUnLocked()
   ///  [line: 1290, column: 32, file: System.Types]
   ,ObjectUnLocked:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$21:TW3OwnedObject.Create$21
};
TW3OwnedLockedObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3LockedObject = class (TObject)
///  [line: 405, column: 3, file: System.Types]
var TW3LockedObject = {
   $ClassName:"TW3LockedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$1 = 0;
   }
   /// procedure TW3LockedObject.DisableAlteration()
   ///  [line: 1223, column: 27, file: System.Types]
   ,DisableAlteration$1:function(Self) {
      ++Self.FLocked$1;
      if (Self.FLocked$1==1) {
         TW3LockedObject.ObjectLocked$1$(Self);
      }
   }
   /// procedure TW3LockedObject.EnableAlteration()
   ///  [line: 1230, column: 27, file: System.Types]
   ,EnableAlteration$1:function(Self) {
      if (Self.FLocked$1>0) {
         --Self.FLocked$1;
         if (!Self.FLocked$1) {
            TW3LockedObject.ObjectUnLocked$1$(Self);
         }
      }
   }
   /// function TW3LockedObject.GetLockState() : Boolean
   ///  [line: 1240, column: 26, file: System.Types]
   ,GetLockState$1:function(Self) {
      return Self.FLocked$1>0;
   }
   /// procedure TW3LockedObject.ObjectLocked()
   ///  [line: 1245, column: 27, file: System.Types]
   ,ObjectLocked$1:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3LockedObject.ObjectUnLocked()
   ///  [line: 1251, column: 27, file: System.Types]
   ,ObjectUnLocked$1:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TW3LockedObject.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TW3Identifiers = class (TObject)
///  [line: 294, column: 3, file: System.Types]
var TW3Identifiers = {
   $ClassName:"TW3Identifiers",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Identifiers.GenerateUniqueObjectId() : String
   ///  [line: 1830, column: 31, file: System.Types]
   ,GenerateUniqueObjectId:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "OBJ"+__UNIQUE.toString();
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3FilePermissionMask enumeration
///  [line: 80, column: 3, file: System.Types]
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
/// TVariant = class (TObject)
///  [line: 525, column: 3, file: System.Types]
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.AsBool(const aValue: Variant) : Boolean
   ///  [line: 2350, column: 25, file: System.Types]
   ,AsBool:function(aValue) {
      var Result = false;
      if (aValue!=undefined&&aValue!=null) {
         Result = (aValue?true:false);
      }
      return Result
   }
   /// function TVariant.AsFloat(const aValue: Variant) : Float
   ///  [line: 2333, column: 25, file: System.Types]
   ,AsFloat:function(aValue$1) {
      var Result = 0;
      if (aValue$1!=undefined&&aValue$1!=null) {
         Result = Number(aValue$1);
      }
      return Result
   }
   /// function TVariant.AsInteger(const aValue: Variant) : Integer
   ///  [line: 2319, column: 25, file: System.Types]
   ,AsInteger:function(aValue$2) {
      var Result = 0;
      if (aValue$2!=undefined&&aValue$2!=null) {
         Result = parseInt(aValue$2,10);
      }
      return Result
   }
   /// function TVariant.AsObject(const aValue: Variant) : TObject
   ///  [line: 2340, column: 25, file: System.Types]
   ,AsObject:function(aValue$3) {
      var Result = null;
      if (aValue$3!=undefined&&aValue$3!=null) {
         Result = aValue$3;
      }
      return Result
   }
   /// function TVariant.AsString(const aValue: Variant) : String
   ///  [line: 2326, column: 25, file: System.Types]
   ,AsString:function(aValue$4) {
      var Result = "";
      if (aValue$4!=undefined&&aValue$4!=null) {
         Result = String(aValue$4);
      }
      return Result
   }
   /// function TVariant.CreateObject() : Variant
   ///  [line: 2371, column: 25, file: System.Types]
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// procedure TVariant.ForEachProperty(const Data: Variant; const CallBack: TW3ObjectKeyCallback)
   ///  [line: 2454, column: 26, file: System.Types]
   ,ForEachProperty:function(Data$1, CallBack) {
      var LObj,
         Keys$1 = [],
         a$283 = 0;
      var LName = "";
      if (CallBack) {
         Keys$1 = TVariant.Properties(Data$1);
         var $temp4;
         for(a$283=0,$temp4=Keys$1.length;a$283<$temp4;a$283++) {
            LName = Keys$1[a$283];
            LObj = Keys$1[LName];
            if ((~CallBack(LName,LObj))==1) {
               break;
            }
         }
      }
   }
   /// function TVariant.Properties(const Data: Variant) : TStrArray
   ///  [line: 2473, column: 25, file: System.Types]
   ,Properties:function(Data$2) {
      var Result = [];
      if (Data$2) {
         if (!(Object.keys === undefined)) {
        Result = Object.keys(Data$2);
        return Result;
      }
         if (!(Object.getOwnPropertyNames === undefined)) {
          Result = Object.getOwnPropertyNames(Data$2);
          return Result;
      }
         for (var qtxenum in Data$2) {
        if ( (Data$2).hasOwnProperty(qtxenum) == true )
          (Result).push(qtxenum);
      }
      return Result;
      }
      return Result
   }
   /// function TVariant.ValidRef(const aValue: Variant) : Boolean
   ///  [line: 2314, column: 25, file: System.Types]
   ,ValidRef:function(aValue$5) {
      return aValue$5!=undefined&&aValue$5!=null;
   }
   ,Destroy:TObject.Destroy
};
/// TTextFormation enumeration
///  [line: 216, column: 3, file: System.Types]
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
/// function TStringHelper.ContainsHex(const Self: String) : Boolean
///  [line: 1769, column: 24, file: System.Types]
function TStringHelper$ContainsHex(Self$5) {
   var Result = false;
   var x$2 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen = 0;
   Result = false;
   LLen = Self$5.length;
   if (LLen>=1) {
      LStart = 1;
      if (Self$5.charAt(0)=="$") {
         ++LStart;
         --LLen;
      } else {
         LItem = (Self$5.substr(0,1)).toLocaleUpperCase();
         Result = ($R[50].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen>=1) {
         var $temp5;
         for(x$2=LStart,$temp5=Self$5.length;x$2<=$temp5;x$2++) {
            LItem = (Self$5.charAt(x$2-1)).toLocaleUpperCase();
            Result = ($R[50].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsOrdinal(const Self: String) : Boolean
///  [line: 1753, column: 24, file: System.Types]
function TStringHelper$ContainsOrdinal(Self$6) {
   var Result = false;
   var LLen$1 = 0,
      x$3 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$1 = Self$6.length;
   if (LLen$1>=1) {
      var $temp6;
      for(x$3=1,$temp6=LLen$1;x$3<=$temp6;x$3++) {
         LItem$1 = Self$6.charAt(x$3-1);
         Result = ($R[49].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsFloat(const Self: String) : Boolean
///  [line: 1698, column: 24, file: System.Types]
function TStringHelper$ContainsFloat(Self$7) {
   var Result = false;
   var x$4 = 0;
   var LItem$2 = "";
   var LLen$2 = 0;
   var LLine = false;
   Result = false;
   LLen$2 = Self$7.length;
   if (LLen$2>=1) {
      LLine = false;
      var $temp7;
      for(x$4=1,$temp7=LLen$2;x$4<=$temp7;x$4++) {
         LItem$2 = Self$7.charAt(x$4-1);
         if (LItem$2==".") {
            if (x$4==1&&LLen$2==1) {
               break;
            }
            if (x$4==1&&LLen$2>1) {
               LLine = true;
               continue;
            }
            if (x$4>1&&x$4<LLen$2) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsQuote(const Self: String) : Boolean
///  [line: 1617, column: 24, file: System.Types]
function TStringHelper$ContainsQuote(Self$8) {
   var Result = false;
   var LLen$3 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$3 = Self$8.length;
   if (LLen$3>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$3) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$3) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
/// TString = class (TObject)
///  [line: 235, column: 3, file: System.Types.Convert]
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.CharCodeFor(const Character: Char) : Integer
   ///  [line: 1369, column: 24, file: System.Types]
   ,CharCodeFor:function(Self, Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   /// function TString.CreateGUID() : String
   ///  [line: 1961, column: 24, file: System.Types.Convert]
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   /// function TString.DecodeBase64(TextToDecode: String) : String
   ///  [line: 1984, column: 24, file: System.Types.Convert]
   ,DecodeBase64:function(Self, TextToDecode) {
      return TBase64EncDec.Base64ToString(TBase64EncDec,TextToDecode);
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TByteArray) : String
   ///  [line: 1428, column: 24, file: System.Types]
   ,DecodeUTF8:function(Self, BytesToDecode) {
      var Result = "";
      var LCodec = null;
      LCodec = TCustomCodec.Create$33($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Decode(LCodec,BytesToDecode);
      } finally {
         TObject.Free(LCodec);
      }
      return Result
   }
   /// function TString.EncodeBase64(TextToEncode: String) : String
   ///  [line: 1979, column: 24, file: System.Types.Convert]
   ,EncodeBase64:function(Self, TextToEncode) {
      return TBase64EncDec.StringToBase64(TBase64EncDec,TextToEncode);
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TByteArray
   ///  [line: 1418, column: 24, file: System.Types]
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = {v:[]};
      try {
         var LCodec$1 = null;
         LCodec$1 = TCustomCodec.Create$33($New(TUTF8Codec));
         try {
            Result.v = TUTF8Codec.Encode(LCodec$1,TextToEncode$1);
         } finally {
            TObject.Free(LCodec$1);
         }
      } finally {return Result.v}
   }
   /// function TString.FromCharCode(const CharCode: Byte) : Char
   ///  [line: 1392, column: 24, file: System.Types]
   ,FromCharCode:function(Self, CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TInteger = class (TObject)
///  [line: 474, column: 3, file: System.Types]
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ///  [line: 2203, column: 25, file: System.Types]
   ,Diff:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const aValue: Integer; const aMin: Integer; const aMax: Integer) : Integer
   ///  [line: 2157, column: 25, file: System.Types]
   ,EnsureRange:function(aValue$6, aMin, aMax) {
      return ClampInt(aValue$6,aMin,aMax);
   }
   /// procedure TInteger.SetBit(index: Integer; aValue: Boolean; var buffer: Integer)
   ///  [line: 2072, column: 26, file: System.Types]
   ,SetBit:function(index, aValue$7, buffer$1) {
      if (index>=0&&index<=31) {
         if (aValue$7) {
            buffer$1.v = buffer$1.v|(1<<index);
         } else {
            buffer$1.v = buffer$1.v&(~(1<<index));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   /// function TInteger.SubtractSmallest(const First: Integer; const Second: Integer) : Integer
   ///  [line: 2129, column: 25, file: System.Types]
   ,SubtractSmallest:function(First, Second) {
      var Result = 0;
      if (First<Second) {
         Result = Second-First;
      } else {
         Result = First-Second;
      }
      return Result
   }
   /// function TInteger.ToNearest(const Value: Integer; const Factor: Integer) : Integer
   ///  [line: 2188, column: 25, file: System.Types]
   ,ToNearest:function(Value, Factor) {
      var Result = 0;
      var FTemp = 0;
      Result = Value;
      FTemp = Value%Factor;
      if (FTemp>0) {
         (Result+= (Factor-FTemp));
      }
      return Result
   }
   /// function TInteger.WrapRange(const aValue: Integer; const aLowRange: Integer; const aHighRange: Integer) : Integer
   ///  [line: 2171, column: 25, file: System.Types]
   ,WrapRange:function(aValue$8, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$8>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$8-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$8<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$8+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$8;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function THandleHelper.Defined(const Self: THandle) : Boolean
///  [line: 1866, column: 24, file: System.Types]
function THandleHelper$Defined(Self$9) {
   var Result = false;
   Result = !(Self$9 == undefined);
   return Result
}
/// TFileAccessMode enumeration
///  [line: 135, column: 3, file: System.Types]
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
/// TEnumState enumeration
///  [line: 129, column: 3, file: System.Types]
var TEnumState = { 1:"esContinue", 0:"esAbort" };
/// TEnumResult enumeration
///  [line: 110, column: 3, file: System.Types]
var TEnumResult = { 160:"erContinue", 16:"erBreak" };
/// TDataTypeMap = record
///  [line: 515, column: 3, file: System.Types]
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
/// EW3Exception = class (Exception)
///  [line: 245, column: 3, file: System.Types]
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EW3Exception.CreateFmt(aText: String; const aValues: array of const)
   ///  [line: 1884, column: 26, file: System.Types]
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   /// constructor EW3Exception.Create(const MethodName: String; const Instance: TObject; const ErrorText: String)
   ///  [line: 1889, column: 26, file: System.Types]
   ,Create$32:function(Self, MethodName, Instance$3, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance$3)?TObject.ClassName(Instance$3.ClassType):"Anonymous";
      EW3Exception.CreateFmt(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
};
/// EW3OwnedObject = class (EW3Exception)
///  [line: 364, column: 3, file: System.Types]
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3LockError = class (EW3Exception)
///  [line: 356, column: 3, file: System.Types]
var EW3LockError = {
   $ClassName:"EW3LockError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
/// TValuePrefixType enumeration
///  [line: 52, column: 3, file: System.Types.Convert]
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TSystemEndianType enumeration
///  [line: 66, column: 3, file: System.Types.Convert]
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
/// TRTLDatatype enumeration
///  [line: 37, column: 3, file: System.Types.Convert]
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
/// TDataTypeConverter = class (TObject)
///  [line: 73, column: 3, file: System.Types.Convert]
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TByteArray
   ///  [line: 1048, column: 35, file: System.Types.Convert]
   ,BooleanToBytes:function(Self, Value$1) {
      var Result = [];
      Result.push((Value$1)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1276, column: 35, file: System.Types.Convert]
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TByteArray) : Boolean
   ///  [line: 1281, column: 29, file: System.Types.Convert]
   ,BytesToBoolean:function(Self, Data$3) {
      return Data$3[0]>0;
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TByteArray) : Float
   ///  [line: 1226, column: 29, file: System.Types.Convert]
   ,BytesToFloat32:function(Self, Data$4) {
      var Result = 0;
      Self.FView.setUint8(0,Data$4[0]);
      Self.FView.setUint8(1,Data$4[1]);
      Self.FView.setUint8(2,Data$4[2]);
      Self.FView.setUint8(3,Data$4[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TByteArray) : Float
   ///  [line: 997, column: 29, file: System.Types.Convert]
   ,BytesToFloat64:function(Self, Data$5) {
      var Result = 0;
      Self.FView.setUint8(0,Data$5[0]);
      Self.FView.setUint8(1,Data$5[1]);
      Self.FView.setUint8(2,Data$5[2]);
      Self.FView.setUint8(3,Data$5[3]);
      Self.FView.setUint8(4,Data$5[4]);
      Self.FView.setUint8(5,Data$5[5]);
      Self.FView.setUint8(6,Data$5[6]);
      Self.FView.setUint8(7,Data$5[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TByteArray) : SmallInt
   ///  [line: 1015, column: 29, file: System.Types.Convert]
   ,BytesToInt16:function(Self, Data$6) {
      var Result = 0;
      Self.FView.setUint8(0,Data$6[0]);
      Self.FView.setUint8(1,Data$6[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TByteArray) : Integer
   ///  [line: 953, column: 29, file: System.Types.Convert]
   ,BytesToInt32:function(Self, Data$7) {
      var Result = 0;
      Self.FView.setUint8(0,Data$7[0]);
      Self.FView.setUint8(1,Data$7[1]);
      Self.FView.setUint8(2,Data$7[2]);
      Self.FView.setUint8(3,Data$7[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TByteArray) : String
   ///  [line: 1314, column: 29, file: System.Types.Convert]
   ,BytesToString:function(Self, Data$8) {
      var Result = "";
      Result = String.fromCharCode.apply(String, Data$8);
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1027, column: 29, file: System.Types.Convert]
   ,BytesToTypedArray:function(Self, Values$6) {
      var Result = undefined;
      var LLen$4 = 0;
      LLen$4 = Values$6.length;
      Result = new Uint8Array(LLen$4);
      (Result).set(Values$6, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TByteArray) : Word
   ///  [line: 1097, column: 29, file: System.Types.Convert]
   ,BytesToUInt16:function(Self, Data$9) {
      var Result = 0;
      Self.FView.setUint8(0,Data$9[0]);
      Self.FView.setUint8(1,Data$9[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TByteArray) : Longword
   ///  [line: 899, column: 29, file: System.Types.Convert]
   ,BytesToUInt32:function(Self, Data$10) {
      var Result = 0;
      Self.FView.setUint8(0,Data$10[0]);
      Self.FView.setUint8(1,Data$10[1]);
      Self.FView.setUint8(2,Data$10[2]);
      Self.FView.setUint8(3,Data$10[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TByteArray) : Variant
   ///  [line: 1122, column: 29, file: System.Types.Convert]
   ,BytesToVariant:function(Self, Data$11) {
      var Result = undefined;
      var LType$1 = 0;
      LType$1 = Data$11[0];
      Data$11.shift();
      switch (LType$1) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$11);
            break;
         case 162 :
            Result = Data$11[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$11);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$11);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$11);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$11);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$11);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$11);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$11);
            break;
         default :
            throw EW3Exception.CreateFmt($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$1)]);
      }
      return Result
   }
   /// function TDataTypeConverter.ByteToChar(const Value: Byte) : Char
   ///  [line: 1321, column: 35, file: System.Types.Convert]
   ,ByteToChar:function(Self, Value$2) {
      var Result = "";
      Result = String.fromCharCode(Value$2);
      return Result
   }
   /// function TDataTypeConverter.CharToByte(const Value: Char) : Word
   ///  [line: 1071, column: 35, file: System.Types.Convert]
   ,CharToByte:function(Self, Value$3) {
      var Result = 0;
      Result = (Value$3).charCodeAt(0);
      return Result
   }
   /// constructor TDataTypeConverter.Create()
   ///  [line: 637, column: 32, file: System.Types.Convert]
   ,Create$7:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ///  [line: 647, column: 31, file: System.Types.Convert]
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TByteArray
   ///  [line: 1109, column: 29, file: System.Types.Convert]
   ,Float32ToBytes:function(Self, Value$4) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$4);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$4,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$4,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TByteArray
   ///  [line: 925, column: 29, file: System.Types.Convert]
   ,Float64ToBytes:function(Self, Value$5) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$5));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$5),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$5),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   /// function TDataTypeConverter.InitFloat32(const Value: float32) : float32
   ///  [line: 721, column: 35, file: System.Types.Convert]
   ,InitFloat32:function(Value$6) {
      var Result = 0;
      var temp = null;
      temp = new Float32Array(1);
      temp[0]=Value$6;
      Result = temp[0];
      temp = null;
      return Result
   }
   /// function TDataTypeConverter.InitFloat64(const Value: float64) : float64
   ///  [line: 729, column: 35, file: System.Types.Convert]
   ,InitFloat64:function(Value$7) {
      var Result = undefined;
      var temp$1 = null;
      temp$1 = new Float64Array(1);
      temp$1[0]=(Number(Value$7));
      Result = temp$1[0];
      temp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: Byte) : Byte
   ///  [line: 753, column: 35, file: System.Types.Convert]
   ,InitInt08:function(Value$8) {
      var Result = 0;
      var temp$2 = null;
      temp$2 = new Int8Array(1);
      temp$2[0]=((Value$8<-128)?-128:(Value$8>127)?127:Value$8);
      Result = temp$2[0];
      temp$2 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: SmallInt) : SmallInt
   ///  [line: 745, column: 35, file: System.Types.Convert]
   ,InitInt16:function(Value$9) {
      var Result = 0;
      var temp$3 = null;
      temp$3 = new Int16Array(1);
      temp$3[0]=((Value$9<-32768)?-32768:(Value$9>32767)?32767:Value$9);
      Result = temp$3[0];
      temp$3 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: Integer) : Integer
   ///  [line: 737, column: 35, file: System.Types.Convert]
   ,InitInt32:function(Value$10) {
      var Result = 0;
      var temp$4 = null;
      temp$4 = new Int32Array(1);
      temp$4[0]=((Value$10<-2147483648)?-2147483648:(Value$10>2147483647)?2147483647:Value$10);
      Result = temp$4[0];
      temp$4 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: Byte) : Byte
   ///  [line: 777, column: 35, file: System.Types.Convert]
   ,InitUint08:function(Value$11) {
      var Result = 0;
      var LTemp$1 = null;
      LTemp$1 = new Uint8Array(1);
      LTemp$1[0]=((Value$11<0)?0:(Value$11>255)?255:Value$11);
      Result = LTemp$1[0];
      LTemp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: Word) : Word
   ///  [line: 769, column: 35, file: System.Types.Convert]
   ,InitUint16:function(Value$12) {
      var Result = 0;
      var temp$5 = null;
      temp$5 = new Uint16Array(1);
      temp$5[0]=((Value$12<0)?0:(Value$12>65536)?65536:Value$12);
      Result = temp$5[0];
      temp$5 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: Longword) : Longword
   ///  [line: 761, column: 35, file: System.Types.Convert]
   ,InitUint32:function(Value$13) {
      var Result = 0;
      var temp$6 = null;
      temp$6 = new Uint32Array(1);
      temp$6[0]=((Value$13<0)?0:(Value$13>4294967295)?4294967295:Value$13);
      Result = temp$6[0];
      temp$6 = null;
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: SmallInt) : TByteArray
   ///  [line: 1252, column: 29, file: System.Types.Convert]
   ,Int16ToBytes:function(Self, Value$14) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$14);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$14,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$14,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: Integer) : TByteArray
   ///  [line: 874, column: 29, file: System.Types.Convert]
   ,Int32ToBytes:function(Self, Value$15) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$15);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$15,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$15,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TSystemEndianType)
   ///  [line: 655, column: 30, file: System.Types.Convert]
   ,SetEndian:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian) {
         Self.FEndian = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TRTLDatatype) : Integer
   ///  [line: 691, column: 35, file: System.Types.Convert]
   ,SizeOfType:function(Self, Kind) {
      return __SIZES[Kind];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1305, column: 29, file: System.Types.Convert]
   ,StringToBytes:function(Self, Value$16) {
      var Result = [];
      Result = (Value$16).split("").map( function( val ) {
        return val.charCodeAt( 0 );
    } );
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TByteArray
   ///  [line: 913, column: 29, file: System.Types.Convert]
   ,TypedArrayToBytes:function(Self, Value$17) {
      var Result = [];
      if (!TVariant.ValidRef(Value$17)) {
         throw EW3Exception.Create$32($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[48]);
      }
      Result = Array.prototype.slice.call(Value$17);
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle) : String
   ///  [line: 937, column: 29, file: System.Types.Convert]
   ,TypedArrayToStr:function(Self, Value$18) {
      var Result = "";
      var x$5 = 0;
      if (TVariant.ValidRef(Value$18)) {
         if (Value$18.length>0) {
            var $temp8;
            for(x$5=0,$temp8=Value$18.length-1;x$5<=$temp8;x$5++) {
               Result += String.fromCharCode((Value$18)[x$5]);
            }
         }
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle) : Longword
   ///  [line: 838, column: 29, file: System.Types.Convert]
   ,TypedArrayToUInt32:function(Self, Value$19) {
      var Result = 0;
      var LBuffer = null,
         LBytes = 0,
         LTypeSize = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$19)) {
         throw EW3Exception.Create$32($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[48]);
      }
      LBuffer = Value$19.buffer;
      LBytes = LBuffer.byteLength;
      LTypeSize = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes<LTypeSize) {
         throw EW3Exception.Create$32($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[47],["Int32", LTypeSize, LBytes]));
      }
      if (LBytes>LTypeSize) {
         LBytes = LTypeSize;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getUint32(0);
            break;
         case 1 :
            Result = LView.getUint32(0,true);
            break;
         case 2 :
            Result = LView.getUint32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: Word) : TByteArray
   ///  [line: 1264, column: 29, file: System.Types.Convert]
   ,UInt16ToBytes:function(Self, Value$20) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$20);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$20,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$20,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: Longword) : TByteArray
   ///  [line: 887, column: 29, file: System.Types.Convert]
   ,UInt32ToBytes:function(Self, Value$21) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$21);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$21,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$21,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(Value: Variant) : TByteArray
   ///  [line: 1145, column: 29, file: System.Types.Convert]
   ,VariantToBytes:function(Self, Value$22) {
      var Result = [];
      var LType$2 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$22<=255) {
            return 162;
         }
         if (Value$22<=65536) {
            return 168;
         }
         if (Value$22<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$22>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$22>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32(x$6) {
         var Result = false;
         Result = isFinite(x$6) && x$6 == Math.fround(x$6);
         return Result
      };
      switch (TW3VariantHelper$DataType(Value$22)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$22?true:false)));
            break;
         case 3 :
            if (Value$22<0) {
               LType$2 = GetSignedIntType();
            } else {
               LType$2 = GetUnSignedIntType();
            }
            if (LType$2) {
               Result = [LType$2].slice();
               switch (LType$2) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$22,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$22,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$22,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$22,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$22,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32(Value$22)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$22))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$22))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$22)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$7$:function($){return $.ClassType.Create$7($)}
};
/// TDatatype = class (TObject)
///  [line: 158, column: 3, file: System.Types.Convert]
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TDatatype.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1439, column: 26, file: System.Types.Convert]
   ,BytesToBase64$1:function(Self, Bytes$2) {
      return TDataTypeConverter.BytesToBase64(__Def_Converter.ClassType,Bytes$2);
   }
   /// function TDatatype.BytesToString(const Data: TByteArray) : String
   ///  [line: 1459, column: 26, file: System.Types.Convert]
   ,BytesToString$1:function(Self, Data$12) {
      return TDataTypeConverter.BytesToString(__Def_Converter,Data$12);
   }
   /// function TDatatype.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1449, column: 26, file: System.Types.Convert]
   ,BytesToTypedArray$1:function(Self, Values$7) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$7);
   }
   /// function TDatatype.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1454, column: 26, file: System.Types.Convert]
   ,StringToBytes$1:function(Self, Value$23) {
      return TDataTypeConverter.StringToBytes(__Def_Converter,Value$23);
   }
   /// function TDatatype.TypedArrayToBytes(const Value: TDefaultBufferType) : TByteArray
   ///  [line: 1347, column: 26, file: System.Types.Convert]
   ,TypedArrayToBytes$1:function(Self, Value$24) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$24);
   }
   /// function TDatatype.TypedArrayToUInt32(const Value: TDefaultBufferType) : Longword
   ///  [line: 1352, column: 26, file: System.Types.Convert]
   ,TypedArrayToUInt32$1:function(Self, Value$25) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$25);
   }
   ,Destroy:TObject.Destroy
};
/// TBase64EncDec = class (TObject)
///  [line: 291, column: 3, file: System.Types.Convert]
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBase64EncDec.Base64ToBytes(const b64: String) : TByteArray
   ///  [line: 461, column: 30, file: System.Types.Convert]
   ,Base64ToBytes$2:function(Self, b64) {
      var Result = [];
      var ASeg = 0;
      var BSeg = 0;
      var CSeg = 0;
      var DSeg = 0;
      var LTextLen = 0,
         LPlaceholderCount = 0,
         BufferSize = 0,
         xpos = 0,
         idx = 0,
         temp$7 = 0,
         temp$8 = 0,
         temp$9 = 0;
      LTextLen = b64.length;
      if (LTextLen>0) {
         LPlaceholderCount = 0;
         if (LTextLen%4<1) {
            LPlaceholderCount = (b64.charAt((LTextLen-1)-1)=="=")?2:(b64.charAt(LTextLen-1)=="=")?1:0;
         }
         BufferSize = ($Div(LTextLen*3,4))-LPlaceholderCount;
         $ArraySetLenC(Result,BufferSize,function (){return 0});
         if (LPlaceholderCount>0) {
            (LTextLen-= LPlaceholderCount);
         }
         xpos = 1;
         idx = 0;
         while (xpos<LTextLen) {
            ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<18;
            BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<12;
            CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]<<6;
            DSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+2))];
            temp$7 = ((ASeg|BSeg)|CSeg)|DSeg;
            Result[idx]=(temp$7>>>16)&255;
            ++idx;
            Result[idx]=(temp$7>>>8)&255;
            ++idx;
            Result[idx]=temp$7&255;
            ++idx;
            (xpos+= 4);
         }
         switch (LPlaceholderCount) {
            case 1 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<2;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]>>>4;
               temp$8 = ASeg|BSeg;
               Result[idx]=temp$8&255;
               break;
            case 2 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<10;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<4;
               CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]>>>2;
               temp$9 = (ASeg|BSeg)|CSeg;
               Result[idx]=(temp$9>>>8)&255;
               ++idx;
               Result[idx]=temp$9&255;
               break;
         }
      }
      return Result
   }
   /// function TBase64EncDec.Base64ToString(const b64: String) : String
   ///  [line: 409, column: 30, file: System.Types.Convert]
   ,Base64ToString:function(Self, b64$1) {
      var Result = "";
      Result = atob(b64$1);
      return Result
   }
   /// function TBase64EncDec.BytesToBase64(const Data: TByteArray) : String
   ///  [line: 525, column: 30, file: System.Types.Convert]
   ,BytesToBase64$2:function(Self, Data$13) {
      var Result = "";
      var LLen$5 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i = 0,
         Ahead = 0,
         SegSize = 0,
         output = "",
         LTemp$2 = 0,
         LTemp$3 = 0;
      LLen$5 = Data$13.length;
      if (LLen$5>0) {
         LExtra = Data$13.length%3;
         LStrideLen = LLen$5-LExtra;
         LMaxChunkLength = 16383;
         i = 0;
         while (i<LStrideLen) {
            Ahead = i+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$13,i,SegSize);
            (i+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$5;
         }
         output = "";
         switch (LExtra) {
            case 1 :
               LTemp$2 = Data$13[LLen$5];
               output+=__B64_Lookup[LTemp$2>>>2];
               output+=__B64_Lookup[(LTemp$2<<4)&63];
               output+="==";
               break;
            case 2 :
               LTemp$3 = (Data$13[LLen$5-1]<<8)+Data$13[LLen$5];
               output+=__B64_Lookup[LTemp$3>>>10];
               output+=__B64_Lookup[(LTemp$3>>>4)&63];
               output+=__B64_Lookup[(LTemp$3<<2)&63];
               output+="=";
               break;
         }
         Result+=output;
      }
      return Result
   }
   /// function TBase64EncDec.EncodeChunk(const Data: TByteArray; startpos: Integer; endpos: Integer) : String
   ///  [line: 424, column: 30, file: System.Types.Convert]
   ,EncodeChunk:function(Self, Data$14, startpos, endpos) {
      var Result = "";
      var temp$10 = 0;
      while (startpos<endpos) {
         temp$10 = (Data$14[startpos]<<16)+(Data$14[startpos+1]<<8)+Data$14[startpos+2];
         Result+=__B64_Lookup[(temp$10>>>18)&63]+__B64_Lookup[(temp$10>>>12)&63]+__B64_Lookup[(temp$10>>>6)&63]+__B64_Lookup[temp$10&63];
         (startpos+= 3);
      }
      return Result
   }
   /// function TBase64EncDec.StringToBase64(const Text: String) : String
   ///  [line: 402, column: 30, file: System.Types.Convert]
   ,StringToBase64:function(Self, Text$2) {
      var Result = "";
      Result = btoa(Text$2);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EDatatype = class (EW3Exception)
///  [line: 22, column: 3, file: System.Types.Convert]
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EConvertError = class (EW3Exception)
///  [line: 23, column: 3, file: System.Types.Convert]
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e$2 = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$1 = 0;
   var $temp9;
   for(i$1=1,$temp9=CNT_B64_CHARSET.length;i$1<=$temp9;i$1++) {
      __B64_Lookup[i$1-1] = CNT_B64_CHARSET.charAt(i$1-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$1-1))] = i$1-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
/// TUnManaged = class (TObject)
///  [line: 106, column: 3, file: System.Memory]
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TUnManaged.AllocMemA(const Size: Integer) : TMemoryHandle
   ///  [line: 245, column: 27, file: System.Memory]
   ,AllocMemA:function(Self, Size$9) {
      var Result = undefined;
      if (Size$9>0) {
         Result = new Uint8Array(Size$9);
      } else {
         Result = null;
      }
      return Result
   }
   /// function TUnManaged.ReAllocMemA(const Memory: TMemoryHandle; Size: Integer) : TMemoryHandle
   ///  [line: 264, column: 27, file: System.Memory]
   ,ReAllocMemA:function(Self, Memory$1, Size$10) {
      var Result = undefined;
      if (Memory$1) {
         if (Size$10>0) {
            Result = new Uint8Array(Size$10);
            TMarshal.Move$1(TMarshal,Memory$1,0,Result,0,Size$10);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$10);
      }
      return Result
   }
   /// function TUnManaged.ReadMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer) : TMemoryHandle
   ///  [line: 347, column: 27, file: System.Memory]
   ,ReadMemoryA:function(Self, Memory$2, Offset, Size$11) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$2&&Offset>=0) {
         LTotal = Offset+Size$11;
         if (LTotal>Memory$2.length) {
            Size$11 = parseInt((Memory$2.length-LTotal),10);
         }
         if (Size$11>0) {
            Result = new Uint8Array(Memory$2.buffer.slice(Offset,Size$11));
         }
      }
      return Result
   }
   /// function TUnManaged.WriteMemoryA(const Memory: TMemoryHandle; const Offset: Integer; const Data: TMemoryHandle) : Integer
   ///  [line: 313, column: 27, file: System.Memory]
   ,WriteMemoryA:function(Self, Memory$3, Offset$1, Data$15) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$3) {
         if (Data$15) {
            mTotal = Offset$1+Data$15.length;
            if (mTotal>Memory$3.length) {
               Result = parseInt((Memory$3.length-mTotal),10);
            } else {
               Result = parseInt(Data$15.length,10);
            }
            if (Result>0) {
               if (Offset$1+Data$15.length<=Memory$3.length) {
                  Memory$3.set(Data$15,Offset$1);
               } else {
                  mChunk = Data$15.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$3.set(mTemp,Offset$1);
               }
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function TMemoryHandleHelper.Valid(const Self: TMemoryHandle) : Boolean
///  [line: 212, column: 30, file: System.Memory]
function TMemoryHandleHelper$Valid$3(Self$10) {
   var Result = false;
   Result = !( (Self$10 == undefined) || (Self$10 == null) );
   return Result
}
/// function TMemoryHandleHelper.Defined(const Self: TMemoryHandle) : Boolean
///  [line: 219, column: 30, file: System.Memory]
function TMemoryHandleHelper$Defined$1(Self$11) {
   var Result = false;
   Result = !(Self$11 == undefined);
   return Result
}
/// TMarshal = class (TObject)
///  [line: 133, column: 3, file: System.Memory]
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TMarshal.Move(const Source: TMemoryHandle; const SourceStart: Integer; const Target: TMemoryHandle; const TargetStart: Integer; const Size: Integer)
   ///  [line: 553, column: 26, file: System.Memory]
   ,Move$1:function(Self, Source, SourceStart, Target, TargetStart, Size$12) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined$1(Source)&&TMemoryHandleHelper$Valid$3(Source)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined$1(Target)&&TMemoryHandleHelper$Valid$3(Target)&&TargetStart>=0) {
            LRef = Source.subarray(SourceStart,SourceStart+Size$12);
            if (LRef.length>Size$12) {
               console.log("Move fucked up");
            }
            Target.set(LRef,TargetStart);
         }
      }
   }
   /// procedure TMarshal.Move(const Source: TAddress; const Target: TAddress; const Size: Integer)
   ///  [line: 577, column: 26, file: System.Memory]
   ,Move:function(Self, Source$1, Target$1, Size$13) {
      if (Source$1!==null) {
         if (Target$1!==null) {
            if (Size$13>0) {
               TMarshal.Move$1(Self,Source$1.FBuffer$2,Source$1.FOffset$2,Target$1.FBuffer$2,Target$1.FOffset$2,Size$13);
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TBufferHandleHelper.Valid(const Self: TBufferHandle) : Boolean
///  [line: 180, column: 30, file: System.Memory]
function TBufferHandleHelper$Valid$4(Self$12) {
   var Result = false;
   Result = !( (Self$12 == undefined) || (Self$12 == null) );
   return Result
}
/// TAddress = class (TObject)
///  [line: 71, column: 3, file: System.Streams]
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$2 = undefined;
      $.FOffset$2 = 0;
   }
   /// constructor TAddress.Create(const Memory: TBinaryData)
   ///  [line: 259, column: 22, file: System.Memory.Buffer]
   ,Create$40:function(Self, Memory$4) {
      if (Memory$4!==null) {
         if (TAllocation.GetSize$3(Memory$4)>0) {
            TAddress.Create$38(Self,TAllocation.GetHandle(Memory$4),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   /// constructor TAddress.Create(const Stream: TStream)
   ///  [line: 228, column: 22, file: System.Streams]
   ,Create$39:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle($As(Stream,TMemoryStream).FBuffer$1);
         if (LRef$1) {
            TAddress.Create$38(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   /// constructor TAddress.Create(const Segment: TBufferHandle; const Offset: Integer)
   ///  [line: 628, column: 22, file: System.Memory]
   ,Create$38:function(Self, Segment$1, Offset$2) {
      TObject.Create(Self);
      if (Segment$1&&TBufferHandleHelper$Valid$4(Segment$1)) {
         Self.FBuffer$2 = Segment$1;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$2>=0) {
         Self.FOffset$2 = Offset$2;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   /// destructor TAddress.Destroy()
   ///  [line: 642, column: 21, file: System.Memory]
   ,Destroy:function(Self) {
      Self.FBuffer$2 = null;
      Self.FOffset$2 = 0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EAddress = class (EW3Exception)
///  [line: 83, column: 3, file: System.Memory]
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3OwnedErrorObject = class (TW3OwnedObject)
///  [line: 78, column: 3, file: System.Objects]
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError = "";
      $.FOptions$3 = null;
   }
   /// procedure TW3OwnedErrorObject.ClearLastError()
   ///  [line: 279, column: 31, file: System.Objects]
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   /// constructor TW3OwnedErrorObject.Create(const AOwner: TObject)
   ///  [line: 227, column: 33, file: System.Objects]
   ,Create$21:function(Self, AOwner$1) {
      TW3OwnedObject.Create$21(Self,AOwner$1);
      Self.FOptions$3 = TW3ErrorObjectOptions.Create$47($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3OwnedErrorObject.Destroy()
   ///  [line: 233, column: 32, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$3);
      TObject.Destroy(Self);
   }
   /// function TW3OwnedErrorObject.GetFailed() : Boolean
   ///  [line: 244, column: 30, file: System.Objects]
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   /// procedure TW3OwnedErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 249, column: 31, file: System.Objects]
   ,SetLastError:function(Self, ErrorText$1) {
      Self.FLastError = Trim$_String_(ErrorText$1);
      if (Self.FLastError.length>0) {
         if (Self.FOptions$3.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
        }
         }
         if (Self.FOptions$3.ThrowExceptions) {
            throw Exception.Create($New(EW3ErrorObject),Self.FLastError);
         }
      }
   }
   /// procedure TW3OwnedErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 273, column: 31, file: System.Objects]
   ,SetLastErrorF:function(Self, ErrorText$2, Values$8) {
      Self.FLastError = Format(ErrorText$2,Values$8.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$21$:function($){return $.ClassType.Create$21.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedErrorObject = class (TW3OwnedErrorObject)
///  [line: 96, column: 3, file: System.Objects]
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   /// procedure TW3OwnedLockedErrorObject.DisableAlteration()
   ///  [line: 189, column: 37, file: System.Objects]
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.EnableAlteration()
   ///  [line: 196, column: 37, file: System.Objects]
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2(Self);
         }
      }
   }
   /// function TW3OwnedLockedErrorObject.GetLockState() : Boolean
   ///  [line: 206, column: 36, file: System.Objects]
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectLocked()
   ///  [line: 211, column: 37, file: System.Objects]
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectUnLocked()
   ///  [line: 217, column: 37, file: System.Objects]
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$21:TW3OwnedErrorObject.Create$21
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3HandleBasedObject = class (TObject)
///  [line: 115, column: 3, file: System.Objects]
var TW3HandleBasedObject = {
   $ClassName:"TW3HandleBasedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHandle$2 = undefined;
   }
   /// procedure TW3HandleBasedObject.ObjectHandleChanged(const PreviousHandle: THandle; const NewHandle: THandle)
   ///  [line: 160, column: 32, file: System.Objects]
   ,ObjectHandleChanged:function(Self, PreviousHandle, NewHandle$3) {
      /* null */
   }
   /// function TW3HandleBasedObject.AcceptObjectHandle(const CandidateHandle: THandle) : Boolean
   ///  [line: 154, column: 31, file: System.Objects]
   ,AcceptObjectHandle:function(Self, CandidateHandle) {
      return true;
   }
   /// procedure TW3HandleBasedObject.SetObjectHandle(const NewHandle: THandle)
   ///  [line: 173, column: 32, file: System.Objects]
   ,SetObjectHandle:function(Self, NewHandle$4) {
      var LTemp$4 = undefined;
      if (TW3HandleBasedObject.AcceptObjectHandle(Self,NewHandle$4)) {
         LTemp$4 = Self.FHandle$2;
         Self.FHandle$2 = NewHandle$4;
         TW3HandleBasedObject.ObjectHandleChanged(Self,LTemp$4,Self.FHandle$2);
      } else {
         throw EW3Exception.CreateFmt($New(EW3HandleBasedObject),$R[46],["TW3HandleBasedObject.SetObjectHandle"]);
      }
   }
   /// function TW3HandleBasedObject.GetObjectHandle() : THandle
   ///  [line: 168, column: 31, file: System.Objects]
   ,GetObjectHandle:function(Self) {
      return Self.FHandle$2;
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObjectOptions = class (TObject)
///  [line: 27, column: 3, file: System.Objects]
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   /// constructor TW3ErrorObjectOptions.Create()
   ///  [line: 139, column: 35, file: System.Objects]
   ,Create$47:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObject = class (TObject)
///  [line: 56, column: 3, file: System.Objects]
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$4 = null;
   }
   /// procedure TW3ErrorObject.ClearLastError()
   ///  [line: 351, column: 26, file: System.Objects]
   ,ClearLastError$1:function(Self) {
      Self.FLastError$1 = "";
   }
   /// constructor TW3ErrorObject.Create()
   ///  [line: 288, column: 28, file: System.Objects]
   ,Create$48:function(Self) {
      TObject.Create(Self);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$47($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3ErrorObject.Destroy()
   ///  [line: 294, column: 27, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   /// function TW3ErrorObject.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 356, column: 25, file: System.Objects]
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   /// function TW3ErrorObject.GetFailed() : Boolean
   ///  [line: 305, column: 25, file: System.Objects]
   ,GetFailed$1:function(Self) {
      return Self.FLastError$1.length>0;
   }
   /// function TW3ErrorObject.GetLastError() : String
   ///  [line: 300, column: 25, file: System.Objects]
   ,GetLastError$1:function(Self) {
      return Self.FLastError$1;
   }
   /// procedure TW3ErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 310, column: 26, file: System.Objects]
   ,SetLastError$1:function(Self, ErrorText$3) {
      var ErrClass = null;
      Self.FLastError$1 = Trim$_String_(ErrorText$3);
      if (Self.FLastError$1.length>0) {
         if (Self.FOptions$4.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError$1) );
       }
         }
         if (Self.FOptions$4.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass$(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass,""),Self.FLastError$1);
         }
      }
   }
   /// procedure TW3ErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 345, column: 26, file: System.Objects]
   ,SetLastErrorF$1:function(Self, ErrorText$4, Values$9) {
      TW3ErrorObject.SetLastError$1(Self,Format(ErrorText$4,Values$9.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$48$:function($){return $.ClassType.Create$48($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TW3ErrorObject.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// EW3HandleBasedObject = class (EW3Exception)
///  [line: 113, column: 3, file: System.Objects]
var EW3HandleBasedObject = {
   $ClassName:"EW3HandleBasedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3ErrorObject = class (EW3Exception)
///  [line: 21, column: 3, file: System.Objects]
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomReader = class (TDataTypeConverter)
///  [line: 38, column: 3, file: System.Reader]
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$1 = null;
      $.FOffset$1 = $.FTotalSize$1 = 0;
      $.FOptions$1 = [0];
   }
   /// constructor TW3CustomReader.Create(const Access: IBinaryTransport)
   ///  [line: 113, column: 29, file: System.Reader]
   ,Create$34:function(Self, Access$2) {
      TDataTypeConverter.Create$7(Self);
      Self.FOptions$1 = [1];
      Self.FAccess$1 = Access$2;
      Self.FOffset$1 = Self.FAccess$1[0]();
      Self.FTotalSize$1 = Self.FAccess$1[1]();
      return Self
   }
   /// function TW3CustomReader.GetReadOffset() : Integer
   ///  [line: 156, column: 26, file: System.Reader]
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$1,0,0,1)) {
         Result = Self.FOffset$1;
      } else {
         Result = Self.FAccess$1[0]();
      }
      return Result
   }
   /// function TW3CustomReader.GetTotalSize() : Integer
   ///  [line: 148, column: 26, file: System.Reader]
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$1,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$1[1]();
      }
      return Result
   }
   /// function TW3CustomReader.Read(const BytesToRead: Integer) : TByteArray
   ///  [line: 169, column: 26, file: System.Reader]
   ,Read:function(Self, BytesToRead) {
      var Result = [];
      if (BytesToRead>0) {
         Result = Self.FAccess$1[2](TW3CustomReader.GetReadOffset(Self),BytesToRead);
         if ($SetIn(Self.FOptions$1,0,0,1)) {
            (Self.FOffset$1+= Result.length);
         }
      } else {
         throw EW3Exception.Create$32($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead.toString()+")"));
      }
      return Result
   }
   /// procedure TW3CustomReader.SetUpdateOption(const NewUpdateMode: TW3ReaderProviderOptions)
   ///  [line: 122, column: 27, file: System.Reader]
   ,SetUpdateOption:function(Self, NewUpdateMode) {
      Self.FOptions$1 = NewUpdateMode.slice(0);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// TReader = class (TW3CustomReader)
///  [line: 82, column: 3, file: System.Reader]
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// TStreamReader = class (TReader)
///  [line: 86, column: 3, file: System.Reader]
var TStreamReader = {
   $ClassName:"TStreamReader",$Parent:TReader
   ,$Init:function ($) {
      TReader.$Init($);
   }
   /// constructor TStreamReader.Create(const Stream: TStream)
   ///  [line: 99, column: 27, file: System.Reader]
   ,Create$35:function(Self, Stream$1) {
      TW3CustomReader.Create$34(Self,$AsIntf(Stream$1,"IBinaryTransport"));
      if (!$SetIn(Self.FOptions$1,0,0,1)) {
         TW3CustomReader.SetUpdateOption(Self,[1]);
      }
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// EW3ReadError = class (EW3Exception)
///  [line: 34, column: 3, file: System.Reader]
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TStream = class (TDataTypeConverter)
///  [line: 80, column: 3, file: System.Streams]
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   /// function TStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 473, column: 18, file: System.Streams]
   ,CopyFrom:function(Self, Source$2, Count$7) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.DataGetSize() : Integer
   ///  [line: 439, column: 18, file: System.Streams]
   ,DataGetSize:function(Self) {
      return TStream.GetSize$(Self);
   }
   /// function TStream.DataOffset() : Integer
   ///  [line: 433, column: 18, file: System.Streams]
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   /// function TStream.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 445, column: 19, file: System.Streams]
   ,DataRead:function(Self, Offset$3, ByteCount) {
      return TStream.ReadBuffer$(Self,Offset$3,ByteCount);
   }
   /// procedure TStream.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 451, column: 19, file: System.Streams]
   ,DataWrite:function(Self, Offset$4, Bytes$3) {
      TStream.WriteBuffer$(Self,Bytes$3,Offset$4);
   }
   /// function TStream.GetPosition() : Integer
   ///  [line: 478, column: 18, file: System.Streams]
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.GetSize() : Integer
   ///  [line: 498, column: 18, file: System.Streams]
   ,GetSize:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Read(const Count: Integer) : TByteArray
   ///  [line: 456, column: 18, file: System.Streams]
   ,Read$1:function(Self, Count$8) {
      return TStream.ReadBuffer$(Self,TStream.GetPosition$(Self),Count$8);
   }
   /// function TStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 508, column: 18, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$5, Count$9) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 493, column: 18, file: System.Streams]
   ,Seek:function(Self, Offset$6, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// procedure TStream.SetPosition(NewPosition: Integer)
   ///  [line: 483, column: 19, file: System.Streams]
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// procedure TStream.SetSize(NewSize: Integer)
   ///  [line: 488, column: 19, file: System.Streams]
   ,SetSize:function(Self, NewSize) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// function TStream.Skip(Amount: Integer) : Integer
   ///  [line: 503, column: 18, file: System.Streams]
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Write(const Buffer: TByteArray) : Integer
   ///  [line: 461, column: 18, file: System.Streams]
   ,Write$1:function(Self, Buffer$2) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$2,TStream.GetPosition$(Self));
      Result = Buffer$2.length;
      return Result
   }
   /// procedure TStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 513, column: 19, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$3, Offset$7) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TMemoryStream = class (TStream)
///  [line: 122, column: 3, file: System.Streams]
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$1 = null;
      $.FPos = 0;
   }
   /// function TMemoryStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 258, column: 24, file: System.Streams]
   ,CopyFrom:function(Self, Source$3, Count$10) {
      var Result = 0;
      var LData = [];
      LData = TStream.ReadBuffer$(Source$3,TStream.GetPosition$(Source$3),Count$10);
      TStream.WriteBuffer$(Self,LData,TStream.GetPosition$(Self));
      Result = LData.length;
      return Result
   }
   /// constructor TMemoryStream.Create()
   ///  [line: 246, column: 27, file: System.Streams]
   ,Create$7:function(Self) {
      TDataTypeConverter.Create$7(Self);
      Self.FBuffer$1 = TDataTypeConverter.Create$7$($New(TAllocation));
      return Self
   }
   /// destructor TMemoryStream.Destroy()
   ///  [line: 252, column: 26, file: System.Streams]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TMemoryStream.GetPosition() : Integer
   ///  [line: 265, column: 24, file: System.Streams]
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   /// function TMemoryStream.GetSize() : Integer
   ///  [line: 340, column: 24, file: System.Streams]
   ,GetSize:function(Self) {
      return TAllocation.GetSize$3(Self.FBuffer$1);
   }
   /// function TMemoryStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 358, column: 24, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$8, Count$11) {
      var Result = [];
      var LLen$6 = 0,
         LBytesToMove = 0,
         LTemp$5 = null;
      LLen$6 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$(Self)) {
         LLen$6 = TStream.GetSize$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$6>0) {
         try {
            LBytesToMove = Count$11;
            if (LBytesToMove>LLen$6) {
               LBytesToMove = LLen$6;
            }
            LTemp$5 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Self.FBuffer$1),Offset$8,LTemp$5,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$5);
            TStream.SetPosition$(Self,Offset$8+Result.length);
         } catch ($e) {
            var e$3 = $W($e);
            throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$3.FMessage]);
         }
      }
      return Result
   }
   /// function TMemoryStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 312, column: 24, file: System.Streams]
   ,Seek:function(Self, Offset$9, Origin$1) {
      var Result = 0;
      var LSize = 0;
      LSize = TStream.GetSize$(Self);
      if (LSize>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$9>-1) {
                  TStream.SetPosition$(Self,Offset$9);
                  Result = Offset$9;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$9),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$9)),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   /// procedure TMemoryStream.SetPosition(NewPosition: Integer)
   ///  [line: 270, column: 25, file: System.Streams]
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$1 = 0;
      LSize$1 = TStream.GetSize$(Self);
      if (LSize$1>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$1);
      }
   }
   /// procedure TMemoryStream.SetSize(NewSize: Integer)
   ///  [line: 277, column: 25, file: System.Streams]
   ,SetSize:function(Self, NewSize$1) {
      var LSize$2 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$2 = TStream.GetSize$(Self);
      if (NewSize$1>0) {
         if (NewSize$1==LSize$2) {
            return;
         }
         if (NewSize$1>LSize$2) {
            LDiff = NewSize$1-LSize$2;
            if (TAllocation.GetSize$3(Self.FBuffer$1)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$1,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         } else {
            LDiff$1 = LSize$2-NewSize$1;
            if (TAllocation.GetSize$3(Self.FBuffer$1)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$1,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$1);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$(Self));
   }
   /// function TMemoryStream.Skip(Amount: Integer) : Integer
   ///  [line: 345, column: 24, file: System.Streams]
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$3 = 0,
         LTotal$1 = 0;
      LSize$3 = TStream.GetSize$(Self);
      if (LSize$3>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$3) {
            LTotal$1 = LSize$3-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   /// procedure TMemoryStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 386, column: 25, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$4, Offset$10) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$36(Self.FBuffer$1)&&Offset$10<1) {
            TAllocation.Allocate(Self.FBuffer$1,Buffer$4.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$4);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle(Self.FBuffer$1),0,Buffer$4.length);
            TStream.SetPosition$(Self,Buffer$4.length);
            return;
         }
         if (TStream.GetPosition$(Self)==TStream.GetSize$(Self)) {
            TAllocation.Grow(Self.FBuffer$1,Buffer$4.length);
            mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$4);
            TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle(Self.FBuffer$1),Offset$10,Buffer$4.length);
         } else {
            TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$4),0,TAllocation.GetHandle(Self.FBuffer$1),Offset$10,Buffer$4.length);
         }
         TStream.SetPosition$(Self,Offset$10+Buffer$4.length);
      } catch ($e) {
         var e$4 = $W($e);
         throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$4.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$7$:function($){return $.ClassType.Create$7($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStreamSeekOrigin enumeration
///  [line: 48, column: 3, file: System.Streams]
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
/// TCustomFileStream = class (TStream)
///  [line: 151, column: 3, file: System.Streams]
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$2 = 0;
      $.FFilename = "";
   }
   /// procedure TCustomFileStream.SetAccessMode(const Value: TFileAccessMode)
   ///  [line: 214, column: 29, file: System.Streams]
   ,SetAccessMode:function(Self, Value$26) {
      Self.FAccess$2 = Value$26;
   }
   /// procedure TCustomFileStream.SetFilename(const Value: String)
   ///  [line: 219, column: 29, file: System.Streams]
   ,SetFilename:function(Self, Value$27) {
      Self.FFilename = Value$27;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
   ,CopyFrom:TStream.CopyFrom
   ,GetPosition:TStream.GetPosition
   ,GetSize:TStream.GetSize
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$37$:function($){return $.ClassType.Create$37.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// EStream = class (EW3Exception)
///  [line: 56, column: 3, file: System.Streams]
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamWriteError = class (EStream)
///  [line: 58, column: 3, file: System.Streams]
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamReadError = class (EStream)
///  [line: 57, column: 3, file: System.Streams]
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamNotImplemented = class (EStream)
///  [line: 59, column: 3, file: System.Streams]
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TAllocationOptions = class (TW3OwnedObject)
///  [line: 100, column: 3, file: System.Memory.Allocation]
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 113, column: 41, file: System.Memory.Allocation]
   ,a$35:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TAllocation);
   }
   /// function TAllocationOptions.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 134, column: 29, file: System.Memory.Allocation]
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   /// constructor TAllocationOptions.Create(const AOwner: TAllocation)
   ///  [line: 127, column: 32, file: System.Memory.Allocation]
   ,Create$41:function(Self, AOwner$2) {
      TW3OwnedObject.Create$21(Self,AOwner$2);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   /// function TAllocationOptions.GetCacheFree() : Integer
   ///  [line: 140, column: 29, file: System.Memory.Allocation]
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed(Self);
   }
   /// function TAllocationOptions.GetCacheUsed() : Integer
   ///  [line: 145, column: 29, file: System.Memory.Allocation]
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle(TAllocationOptions.a$35(Self)).length-TAllocation.GetSize$3(TAllocationOptions.a$35(Self)))),10);
      }
      return Result
   }
   /// procedure TAllocationOptions.SetCacheSize(const NewCacheSize: Integer)
   ///  [line: 156, column: 30, file: System.Memory.Allocation]
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   /// procedure TAllocationOptions.SetUseCache(const NewUseValue: Boolean)
   ///  [line: 151, column: 30, file: System.Memory.Allocation]
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$21:TW3OwnedObject.Create$21
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TAllocation = class (TDataTypeConverter)
///  [line: 51, column: 3, file: System.Memory.Allocation]
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle$1 = undefined;
      $.FOptions$2 = null;
      $.FSize = 0;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 73, column: 37, file: System.Memory.Allocation]
   ,a$36:function(Self) {
      return ((!Self.FHandle$1)?true:false);
   }
   /// procedure TAllocation.Allocate(const NumberOfBytes: Integer)
   ///  [line: 258, column: 23, file: System.Memory.Allocation]
   ,Allocate:function(Self, NumberOfBytes) {
      var NewSize$2 = 0;
      if (Self.FHandle$1) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes>0) {
         NewSize$2 = 0;
         if (Self.FOptions$2.FUseCache) {
            NewSize$2 = TInteger.ToNearest(NumberOfBytes,Self.FOptions$2.FCacheSize);
         } else {
            NewSize$2 = NumberOfBytes;
         }
         Self.FHandle$1 = TUnManaged.AllocMemA(TUnManaged,NewSize$2);
         Self.FSize = NumberOfBytes;
         TAllocation.HandleAllocated$(Self);
      }
   }
   /// constructor TAllocation.Create(const ByteSize: Integer)
   ///  [line: 179, column: 25, file: System.Memory.Allocation]
   ,Create$43:function(Self, ByteSize) {
      TDataTypeConverter.Create$7$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   /// constructor TAllocation.Create()
   ///  [line: 173, column: 25, file: System.Memory.Allocation]
   ,Create$7:function(Self) {
      TDataTypeConverter.Create$7(Self);
      Self.FOptions$2 = TAllocationOptions.Create$41($New(TAllocationOptions),Self);
      return Self
   }
   /// function TAllocation.DataGetSize() : Integer
   ///  [line: 230, column: 22, file: System.Memory.Allocation]
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self);
   }
   /// function TAllocation.DataOffset() : Integer
   ///  [line: 224, column: 22, file: System.Memory.Allocation]
   ,DataOffset$1:function(Self) {
      return 0;
   }
   /// function TAllocation.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 236, column: 22, file: System.Memory.Allocation]
   ,DataRead$1:function(Self, Offset$11, ByteCount$1) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$11,ByteCount$1));
   }
   /// procedure TAllocation.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 243, column: 23, file: System.Memory.Allocation]
   ,DataWrite$1:function(Self, Offset$12, Bytes$4) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$12,TDataTypeConverter.BytesToTypedArray(Self,Bytes$4));
   }
   /// destructor TAllocation.Destroy()
   ///  [line: 186, column: 24, file: System.Memory.Allocation]
   ,Destroy:function(Self) {
      if (Self.FHandle$1) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$2);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TAllocation.GetBufferHandle() : TBufferHandle
   ///  [line: 430, column: 22, file: System.Memory.Allocation]
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle$1) {
         Result = Self.FHandle$1.buffer;
      }
      return Result
   }
   /// function TAllocation.GetHandle() : TMemoryHandle
   ///  [line: 425, column: 22, file: System.Memory.Allocation]
   ,GetHandle:function(Self) {
      return Self.FHandle$1;
   }
   /// function TAllocation.GetSize() : Integer
   ///  [line: 420, column: 22, file: System.Memory.Allocation]
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   /// function TAllocation.GetTotalSize() : Integer
   ///  [line: 414, column: 22, file: System.Memory.Allocation]
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if (Self.FHandle$1) {
         Result = parseInt(Self.FHandle$1.length,10);
      }
      return Result
   }
   /// function TAllocation.GetTransport() : IBinaryTransport
   ///  [line: 195, column: 22, file: System.Memory.Allocation]
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   /// procedure TAllocation.Grow(const NumberOfBytes: Integer)
   ///  [line: 300, column: 23, file: System.Memory.Allocation]
   ,Grow:function(Self, NumberOfBytes$1) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle$1) {
         ExactNewSize = Self.FSize+NumberOfBytes$1;
         if (Self.FOptions$2.FUseCache) {
            if (NumberOfBytes$1<TAllocationOptions.GetCacheFree(Self.FOptions$2)) {
               (Self.FSize+= NumberOfBytes$1);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$2.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$1);
      }
   }
   /// procedure TAllocation.HandleAllocated()
   ///  [line: 248, column: 23, file: System.Memory.Allocation]
   ,HandleAllocated:function(Self) {
      /* null */
   }
   /// procedure TAllocation.HandleReleased()
   ///  [line: 253, column: 23, file: System.Memory.Allocation]
   ,HandleReleased:function(Self) {
      /* null */
   }
   /// procedure TAllocation.ReAllocate(const NewSize: Integer)
   ///  [line: 379, column: 23, file: System.Memory.Allocation]
   ,ReAllocate:function(Self, NewSize$3) {
      var LSizeToSet = 0;
      if (NewSize$3>0) {
         if (Self.FHandle$1) {
            if (NewSize$3!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$2.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,Self.FOptions$2.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,16);
               }
               Self.FHandle$1 = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle$1,LSizeToSet);
               Self.FSize = NewSize$3;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$3);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   /// procedure TAllocation.Release()
   ///  [line: 287, column: 23, file: System.Memory.Allocation]
   ,Release$1:function(Self) {
      if (Self.FHandle$1) {
         try {
            Self.FHandle$1 = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   /// procedure TAllocation.Shrink(const NumberOfBytes: Integer)
   ///  [line: 332, column: 23, file: System.Memory.Allocation]
   ,Shrink:function(Self, NumberOfBytes$2) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle$1) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$2),0,2147483647);
         if (Self.FOptions$2.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$2.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$2.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   /// procedure TAllocation.Transport(const Target: IBinaryTransport)
   ///  [line: 200, column: 23, file: System.Memory.Allocation]
   ,Transport:function(Self, Target$2) {
      var Data$16 = [];
      if (Target$2===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$36(Self)) {
            try {
               Data$16 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle(Self));
               Target$2[3](Target$2[0](),Data$16);
            } catch ($e) {
               var e$5 = $W($e);
               throw EW3Exception.CreateFmt($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$5.ClassType), e$5.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$7$:function($){return $.ClassType.Create$7($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$2,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
function a$284(Self) {
   return ((!Self[0]())?true:false);
}/// EAllocation = class (EW3Exception)
///  [line: 22, column: 3, file: System.Memory.Allocation]
var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomWriter = class (TDataTypeConverter)
///  [line: 39, column: 3, file: System.Writer]
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions = [0];
   }
   /// constructor TW3CustomWriter.Create(const Access: IBinaryTransport)
   ///  [line: 102, column: 29, file: System.Writer]
   ,Create$8:function(Self, Access$3) {
      TDataTypeConverter.Create$7(Self);
      Self.FAccess = Access$3;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions = [3];
      return Self
   }
   /// function TW3CustomWriter.GetOffset() : Integer
   ///  [line: 125, column: 26, file: System.Writer]
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   /// function TW3CustomWriter.GetTotalFree() : Integer
   ///  [line: 146, column: 26, file: System.Writer]
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   /// function TW3CustomWriter.GetTotalSize() : Integer
   ///  [line: 116, column: 26, file: System.Writer]
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   /// function TW3CustomWriter.QueryBreachOfBoundary(const BytesToFit: Integer) : Boolean
   ///  [line: 133, column: 26, file: System.Writer]
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   /// procedure TW3CustomWriter.SetAccessOptions(const NewOptions: TW3WriterOptions)
   ///  [line: 111, column: 27, file: System.Writer]
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions = NewOptions.slice(0);
   }
   /// function TW3CustomWriter.Write(const Data: TByteArray) : Integer
   ///  [line: 151, column: 26, file: System.Writer]
   ,Write:function(Self, Data$17) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$17.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$17);
            if ($SetIn(Self.FOptions,0,0,2)) {
               (Self.FOffset+= Data$17.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$17,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$17);
               if ($SetIn(Self.FOptions,0,0,2)) {
                  (Self.FOffset+= Data$17.length);
               }
            } else {
               throw EW3Exception.Create$32($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[12],[Data$17.length]));
            }
         }
         Result = Data$17.length;
      } else {
         throw EW3Exception.Create$32($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[14],[LBytesToWrite]));
      }
      return Result
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// TWriter = class (TW3CustomWriter)
///  [line: 76, column: 3, file: System.Writer]
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// TStreamWriter = class (TW3CustomWriter)
///  [line: 79, column: 3, file: System.Writer]
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   /// constructor TStreamWriter.Create(const Stream: TStream)
   ///  [line: 92, column: 27, file: System.Writer]
   ,Create$9:function(Self, Stream$2) {
      TW3CustomWriter.Create$8(Self,$AsIntf(Stream$2,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$7:TDataTypeConverter.Create$7
};
/// EW3WriteError = class (EW3Exception)
///  [line: 31, column: 3, file: System.Writer]
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONObject = class (TObject)
///  [line: 37, column: 3, file: System.JSON]
var TJSONObject = {
   $ClassName:"TJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance = undefined;
      $.FOptions$5 = [0];
   }
   /// function TJSONObject.AddOrSet(const PropertyName: String; const Data: Variant) : TJSONObject
   ///  [line: 415, column: 22, file: System.JSON]
   ,AddOrSet:function(Self, PropertyName, Data$18) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists(Self,PropertyName)) {
         if ($SetIn(Self.FOptions$5,3,0,4)) {
            Self.FInstance[PropertyName] = Data$18;
         } else {
            throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName]);
         }
      } else if ($SetIn(Self.FOptions$5,1,0,4)) {
         Self.FInstance[PropertyName] = Data$18;
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to add value [%s], instance does not allow new properties",[PropertyName]);
      }
      return Result
   }
   /// procedure TJSONObject.Clear()
   ///  [line: 346, column: 23, file: System.JSON]
   ,Clear$1:function(Self) {
      Self.FInstance = TVariant.CreateObject();
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions; Clone: Boolean)
   ///  [line: 185, column: 25, file: System.JSON]
   ,Create$52:function(Self, Instance$4, Options$3, Clone$3) {
      TObject.Create(Self);
      Self.FOptions$5 = Options$3.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$4)) {
         if (TW3VariantHelper$IsObject(Instance$4)) {
            if (Clone$3) {
               Self.FInstance = TVariant.CreateObject();
               TVariant.ForEachProperty(Instance$4,function (Name$7, Data$19) {
                  var Result = 1;
                  TJSONObject.AddOrSet(Self,Name$7,Data$19);
                  Result = 1;
                  return Result
               });
            } else {
               Self.FInstance = Instance$4;
            }
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to clone instance, reference is not an object");
         }
      } else {
         if ($SetIn(Self.FOptions$5,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions)
   ///  [line: 161, column: 25, file: System.JSON]
   ,Create$51:function(Self, Instance$5, Options$4) {
      TObject.Create(Self);
      Self.FOptions$5 = Options$4.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$5)&&TW3VariantHelper$IsObject(Instance$5)) {
         Self.FInstance = Instance$5;
      } else {
         if ($SetIn(Self.FOptions$5,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance)
   ///  [line: 142, column: 25, file: System.JSON]
   ,Create$50:function(Self, Instance$6) {
      TObject.Create(Self);
      Self.FOptions$5 = [15];
      if (Instance$6) {
         if (TW3VariantHelper$IsObject(Instance$6)) {
            Self.FInstance = Instance$6;
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         Self.FInstance = TVariant.CreateObject();
      }
      return Self
   }
   /// constructor TJSONObject.Create()
   ///  [line: 135, column: 25, file: System.JSON]
   ,Create$49:function(Self) {
      TObject.Create(Self);
      Self.FOptions$5 = [15];
      Self.FInstance = TVariant.CreateObject();
      return Self
   }
   /// destructor TJSONObject.Destroy()
   ,Destroy$17:function(Self) {
      Self.FInstance = null;
      TObject.Destroy(Self);
   }
   /// function TJSONObject.Exists(const PropertyName: String) : Boolean
   ///  [line: 434, column: 22, file: System.JSON]
   ,Exists:function(Self, PropertyName$1) {
      return (Object.hasOwnProperty.call(Self.FInstance,PropertyName$1)?true:false);
   }
   /// procedure TJSONObject.FromJSON(const Text: String)
   ///  [line: 308, column: 23, file: System.JSON]
   ,FromJSON:function(Self, Text$3) {
      Self.FInstance = JSON.parse(Text$3);
   }
   /// procedure TJSONObject.LoadFromBuffer(const Buffer: TBinaryData)
   ///  [line: 320, column: 23, file: System.JSON]
   ,LoadFromBuffer:function(Self, Buffer$5) {
      var LBytes$1 = [],
         LText = "";
      LBytes$1 = TBinaryData.ToBytes(Buffer$5);
      LText = TString.DecodeUTF8(TString,LBytes$1);
      Self.FInstance = JSON.parse(LText);
   }
   /// procedure TJSONObject.LoadFromStream(const Stream: TStream)
   ///  [line: 334, column: 23, file: System.JSON]
   ,LoadFromStream$1:function(Self, Stream$3) {
      var LBytes$2 = [],
         LText$1 = "";
      LBytes$2 = TStream.Read$1(Stream$3,TStream.GetSize$(Stream$3));
      LText$1 = TString.DecodeUTF8(TString,LBytes$2);
      Self.FInstance = JSON.parse(LText$1);
   }
   /// function TJSONObject.Read(const PropertyName: String; var Data: Variant) : TJSONObject
   ///  [line: 391, column: 22, file: System.JSON]
   ,Read$2:function(Self, PropertyName$2, Data$20) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists(Self,PropertyName$2)) {
         Data$20.v = Self.FInstance[PropertyName$2];
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$2]);
      }
      return Result
   }
   /// procedure TJSONObject.SaveToBuffer(const Buffer: TBinaryData)
   ///  [line: 313, column: 23, file: System.JSON]
   ,SaveToBuffer:function(Self, Buffer$6) {
      var LText$2 = "",
         LBytes$3 = [];
      LText$2 = JSON.stringify(Self.FInstance)+"   ";
      LBytes$3 = TString.EncodeUTF8(TString,LText$2);
      TBinaryData.AppendBytes(Buffer$6,LBytes$3);
   }
   /// procedure TJSONObject.SaveToStream(const Stream: TStream)
   ///  [line: 327, column: 23, file: System.JSON]
   ,SaveToStream$1:function(Self, Stream$4) {
      var LText$3 = "",
         LBytes$4 = [];
      LText$3 = JSON.stringify(Self.FInstance,null,2)+"   ";
      LBytes$4 = TString.EncodeUTF8(TString,LText$3);
      TStream.Write$1(Stream$4,LBytes$4);
   }
   /// function TJSONObject.ToJSON() : String
   ///  [line: 303, column: 22, file: System.JSON]
   ,ToJSON:function(Self) {
      return JSON.stringify(Self.FInstance,null,2);
   }
   ,Destroy:TObject.Destroy
};
/// EJSONObject = class (EW3Exception)
///  [line: 35, column: 3, file: System.JSON]
var EJSONObject = {
   $ClassName:"EJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TBinaryData = class (TAllocation)
///  [line: 158, column: 3, file: System.Memory.Buffer]
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   /// procedure TBinaryData.AppendBuffer(const Raw: TMemoryHandle)
   ///  [line: 1200, column: 23, file: System.Memory.Buffer]
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   /// procedure TBinaryData.AppendBytes(const Bytes: TByteArray)
   ///  [line: 1256, column: 23, file: System.Memory.Buffer]
   ,AppendBytes:function(Self, Bytes$5) {
      var LLen$7 = 0,
         LOffset$1 = 0;
      LLen$7 = Bytes$5.length;
      if (LLen$7>0) {
         LOffset$1 = TAllocation.GetSize$3(Self);
         TAllocation.Grow(Self,LLen$7);
         TAllocation.GetHandle(Self).set(Bytes$5,LOffset$1);
      }
   }
   /// procedure TBinaryData.AppendFloat32(const Value: float32)
   ///  [line: 1186, column: 23, file: System.Memory.Buffer]
   ,AppendFloat32:function(Self, Value$28) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$28);
   }
   /// procedure TBinaryData.AppendFloat64(const Value: float64)
   ///  [line: 1193, column: 23, file: System.Memory.Buffer]
   ,AppendFloat64:function(Self, Value$29) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$29);
   }
   /// procedure TBinaryData.AppendMemory(const Buffer: TBinaryData; const ReleaseBufferOnExit: Boolean)
   ///  [line: 1214, column: 23, file: System.Memory.Buffer]
   ,AppendMemory:function(Self, Buffer$7, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$7!==null) {
         try {
            if (TAllocation.GetSize$3(Buffer$7)>0) {
               LOffset$4 = TAllocation.GetSize$3(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3(Buffer$7));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$7);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$7);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   /// procedure TBinaryData.AppendStr(const Text: String)
   ///  [line: 1234, column: 23, file: System.Memory.Buffer]
   ,AppendStr:function(Self, Text$4) {
      var LLen$8 = 0,
         LOffset$5 = 0,
         LTemp$6 = [],
         x$7 = 0;
      LLen$8 = Text$4.length;
      if (LLen$8>0) {
         LOffset$5 = TAllocation.GetSize$3(Self);
         LTemp$6 = TString.EncodeUTF8(TString,Text$4);
         TAllocation.Grow(Self,LTemp$6.length);
         var $temp10;
         for(x$7=0,$temp10=LTemp$6.length;x$7<$temp10;x$7++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$6[x$7]);
            ++LOffset$5;
         }
      }
   }
   /// function TBinaryData.Clone() : TBinaryData
   ///  [line: 1159, column: 22, file: System.Memory.Buffer]
   ,Clone:function(Self) {
      return TBinaryData.Create$45($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   /// function TBinaryData.Copy(const Offset: Integer; const ByteLen: Integer) : TByteArray
   ///  [line: 480, column: 22, file: System.Memory.Buffer]
   ,Copy$1:function(Self, Offset$13, ByteLen) {
      var Result = [];
      var LSize$4 = 0,
         LTemp$7 = null;
      LSize$4 = TAllocation.GetSize$3(Self);
      if (LSize$4>0) {
         if (Offset$13>=0&&Offset$13<LSize$4) {
            if (Offset$13+ByteLen<=LSize$4) {
               LTemp$7 = new Uint8Array(Self.FDataView.buffer,Offset$13,ByteLen);
               Result = Array.prototype.slice.call(LTemp$7);
               LTemp$7.buffer = null;
               LTemp$7 = null;
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.CopyFrom(const Buffer: TBinaryData; const Offset: Integer; const ByteLen: Integer)
   ///  [line: 1164, column: 23, file: System.Memory.Buffer]
   ,CopyFrom$2:function(Self, Buffer$8, Offset$14, ByteLen$1) {
      if (Buffer$8!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle(Buffer$8),Offset$14,ByteLen$1);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   /// procedure TBinaryData.CopyFromMemory(const Raw: TMemoryHandle; Offset: Integer; ByteLen: Integer)
   ///  [line: 1172, column: 23, file: System.Memory.Buffer]
   ,CopyFromMemory:function(Self, Raw$1, Offset$15, ByteLen$2) {
      if (TMemoryHandleHelper$Valid$3(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$15)) {
            if (ByteLen$2>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle(Self),Offset$15,ByteLen$2);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$15]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   /// constructor TBinaryData.Create(aHandle: TMemoryHandle)
   ///  [line: 283, column: 25, file: System.Memory.Buffer]
   ,Create$45:function(Self, aHandle) {
      var LSignature;
      TDataTypeConverter.Create$7$(Self);
      if (TMemoryHandleHelper$Defined$1(aHandle)&&TMemoryHandleHelper$Valid$3(aHandle)) {
         if (aHandle.toString) {
            LSignature = aHandle.toString();
            if (SameText(String(LSignature),"[object Uint8Array]")||SameText(String(LSignature),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   /// function TBinaryData.CutBinaryData(const Offset: Integer; const ByteLen: Integer) : TBinaryData
   ///  [line: 1138, column: 22, file: System.Memory.Buffer]
   ,CutBinaryData:function(Self, Offset$16, ByteLen$3) {
      var Result = null;
      var LSize$5 = 0,
         LNewBuffer = null;
      if (ByteLen$3>0) {
         LSize$5 = TAllocation.GetSize$3(Self);
         if (LSize$5>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$16)) {
               LNewBuffer = TAllocation.GetHandle(Self).subarray(Offset$16,Offset$16+ByteLen$3-1);
               Result = TBinaryData.Create$45($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$16]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$45($New(TBinaryData),null);
      }
      return Result
   }
   /// function TBinaryData.CutStream(const Offset: Integer; const ByteLen: Integer) : TStream
   ///  [line: 1116, column: 22, file: System.Memory.Buffer]
   ,CutStream:function(Self, Offset$17, ByteLen$4) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$17,ByteLen$4));
   }
   /// function TBinaryData.CutTypedArray(Offset: Integer; ByteLen: Integer) : TMemoryHandle
   ///  [line: 1121, column: 22, file: System.Memory.Buffer]
   ,CutTypedArray:function(Self, Offset$18, ByteLen$5) {
      var Result = undefined;
      var LTemp$8 = null;
      if (ByteLen$5>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$18)) {
            if (TAllocation.GetSize$3(Self)-Offset$18>0) {
               LTemp$8 = Self.FDataView.buffer.slice(Offset$18,Offset$18+ByteLen$5);
               Result = new Uint8Array(LTemp$8);
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.FromBase64(FileData: String)
   ///  [line: 639, column: 23, file: System.Memory.Buffer]
   ,FromBase64:function(Self, FileData) {
      var LBytes$5 = [];
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LBytes$5 = TBase64EncDec.Base64ToBytes$2(TBase64EncDec,FileData);
         if (LBytes$5.length>0) {
            TBinaryData.AppendBytes(Self,LBytes$5);
         }
      }
   }
   /// procedure TBinaryData.FromNodeBuffer(const NodeBuffer: JNodeBuffer)
   ///  [line: 93, column: 23, file: SmartNJ.Streams]
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$36(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   /// function TBinaryData.GetBit(const BitIndex: Integer) : Boolean
   ///  [line: 357, column: 22, file: System.Memory.Buffer]
   ,GetBit$1:function(Self, BitIndex) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex]);
      }
      return Result
   }
   /// function TBinaryData.GetBitCount() : Integer
   ///  [line: 317, column: 22, file: System.Memory.Buffer]
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3(Self)<<3;
   }
   /// function TBinaryData.GetByte(const Index: Integer) : Byte
   ///  [line: 676, column: 22, file: System.Memory.Buffer]
   ,GetByte:function(Self, Index) {
      var Result = 0;
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index)) {
            Result = Self.FDataView.getUint8(Index);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index]);
         }
      }
      return Result
   }
   /// procedure TBinaryData.HandleAllocated()
   ///  [line: 309, column: 23, file: System.Memory.Buffer]
   ,HandleAllocated:function(Self) {
      var LRef$2 = undefined;
      LRef$2 = TAllocation.GetBufferHandle(Self);
      (Self.FDataView) = new DataView(LRef$2);
   }
   /// procedure TBinaryData.HandleReleased()
   ///  [line: 322, column: 23, file: System.Memory.Buffer]
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   /// procedure TBinaryData.LoadFromStream(const Stream: TStream)
   ///  [line: 561, column: 23, file: System.Memory.Buffer]
   ,LoadFromStream:function(Self, Stream$5) {
      var BytesToRead$1 = 0;
      TAllocation.Release$1(Self);
      if (Stream$5!==null) {
         if (TStream.GetSize$(Stream$5)>0) {
            BytesToRead$1 = TStream.GetSize$(Stream$5)-TStream.GetPosition$(Stream$5);
            if (BytesToRead$1>0) {
               TBinaryData.AppendBytes(Self,TStream.ReadBuffer$(Stream$5,TStream.GetPosition$(Stream$5),BytesToRead$1));
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),$R[45]);
      }
   }
   /// function TBinaryData.OffsetInRange(const Offset: Integer) : Boolean
   ///  [line: 849, column: 22, file: System.Memory.Buffer]
   ,OffsetInRange:function(Self, Offset$19) {
      var Result = false;
      var LSize$6 = 0;
      LSize$6 = TAllocation.GetSize$3(Self);
      if (LSize$6>0) {
         Result = Offset$19>=0&&Offset$19<=LSize$6;
      } else {
         Result = (Offset$19==0);
      }
      return Result
   }
   /// function TBinaryData.ReadBool(Offset: Integer) : Boolean
   ///  [line: 841, column: 22, file: System.Memory.Buffer]
   ,ReadBool:function(Self, Offset$20) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         Result = Self.FDataView.getUint8(Offset$20)>0;
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$20, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadBytes(Offset: Integer; ByteLen: Integer) : TByteArray
   ///  [line: 824, column: 22, file: System.Memory.Buffer]
   ,ReadBytes:function(Self, Offset$21, ByteLen$6) {
      var Result = [];
      var LSize$7 = 0,
         x$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$21)) {
         LSize$7 = TAllocation.GetSize$3(Self);
         if (Offset$21+ByteLen$6<=LSize$7) {
            var $temp11;
            for(x$8=0,$temp11=ByteLen$6;x$8<$temp11;x$8++) {
               Result.push(Self.FDataView.getUint8(Offset$21+x$8));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$21, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat32(Offset: Integer) : float32
   ///  [line: 718, column: 22, file: System.Memory.Buffer]
   ,ReadFloat32:function(Self, Offset$22) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$22)) {
         if (Offset$22+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$22);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$22,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$22,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$22, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat64(Offset: Integer) : float64
   ///  [line: 700, column: 22, file: System.Memory.Buffer]
   ,ReadFloat64:function(Self, Offset$23) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$23)) {
         if (Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$23);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$23,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$23,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$23, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt(Offset: Integer) : Integer
   ///  [line: 786, column: 22, file: System.Memory.Buffer]
   ,ReadInt:function(Self, Offset$24) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$24)) {
         if (Offset$24+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$24);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$24,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$24,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$24, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt16(Offset: Integer) : SmallInt
   ///  [line: 735, column: 22, file: System.Memory.Buffer]
   ,ReadInt16:function(Self, Offset$25) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         if (Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$25);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$25,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$25,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$25, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadLong(Offset: Integer) : Longword
   ///  [line: 769, column: 22, file: System.Memory.Buffer]
   ,ReadLong$1:function(Self, Offset$26) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$26)) {
         if (Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$26);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$26,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$26,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$26, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadStr(Offset: Integer; ByteLen: Integer) : String
   ///  [line: 803, column: 22, file: System.Memory.Buffer]
   ,ReadStr$1:function(Self, Offset$27, ByteLen$7) {
      var Result = "";
      var LSize$8 = 0,
         LFetch = [],
         x$9 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         LSize$8 = TAllocation.GetSize$3(Self);
         if (Offset$27+ByteLen$7<=LSize$8) {
            var $temp12;
            for(x$9=0,$temp12=ByteLen$7;x$9<$temp12;x$9++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$27+x$9)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$27, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadWord(Offset: Integer) : Word
   ///  [line: 752, column: 22, file: System.Memory.Buffer]
   ,ReadWord$1:function(Self, Offset$28) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$28);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$28,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$28,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[43],[Offset$28, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// procedure TBinaryData.SetBit(const BitIndex: Integer; const value: Boolean)
   ///  [line: 352, column: 23, file: System.Memory.Buffer]
   ,SetBit$1:function(Self, BitIndex$1, value) {
      TBinaryData.SetByte(Self,(BitIndex$1>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$1%8),TBinaryData.GetByte(Self,(BitIndex$1>>>3)),value));
   }
   /// procedure TBinaryData.SetByte(const Index: Integer; const Value: Byte)
   ///  [line: 688, column: 23, file: System.Memory.Buffer]
   ,SetByte:function(Self, Index$1, Value$30) {
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$1)) {
            Self.FDataView.setUint8(Index$1,Value$30);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$1]);
         }
      }
   }
   /// function TBinaryData.ToBase64() : String
   ///  [line: 654, column: 22, file: System.Memory.Buffer]
   ,ToBase64:function(Self) {
      return TDataTypeConverter.BytesToBase64(Self.ClassType,TBinaryData.ToBytes(Self));
   }
   /// function TBinaryData.ToBytes() : TByteArray
   ///  [line: 510, column: 22, file: System.Memory.Buffer]
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$9 = 0,
         LTemp$9 = null;
      LSize$9 = TAllocation.GetSize$3(Self);
      if (LSize$9>0) {
         LTemp$9 = new Uint8Array(Self.FDataView.buffer,0,LSize$9);
         Result = Array.prototype.slice.call(LTemp$9);
         LTemp$9.buffer = null;
         LTemp$9 = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TBinaryData.ToHexDump(BytesPerRow: Integer; Options: TBufferHexDumpOptions) : String
   ///  [line: 367, column: 22, file: System.Memory.Buffer]
   ,ToHexDump:function(Self, BytesPerRow, Options$5) {
      var Result = "";
      var mDump = [],
         mCount = 0,
         x$10 = 0;
      var LByte = 0,
         mPad = 0,
         z = 0;
      var mPad$1 = 0,
         z$1 = 0;
      function SliceToText(DataSlice) {
         var Result = "";
         var y$1 = 0;
         var LChar = "",
            LOff = 0;
         if (DataSlice.length>0) {
            var $temp13;
            for(y$1=0,$temp13=DataSlice.length;y$1<$temp13;y$1++) {
               LChar = TDataTypeConverter.ByteToChar(TDataTypeConverter,DataSlice[y$1]);
               if ((((LChar>="A")&&(LChar<="Z"))||((LChar>="a")&&(LChar<="z"))||((LChar>="0")&&(LChar<="9"))||(LChar==",")||(LChar==";")||(LChar=="<")||(LChar==">")||(LChar=="{")||(LChar=="}")||(LChar=="[")||(LChar=="]")||(LChar=="-")||(LChar=="_")||(LChar=="#")||(LChar=="$")||(LChar=="%")||(LChar=="&")||(LChar=="\/")||(LChar=="(")||(LChar==")")||(LChar=="!")||(LChar=="§")||(LChar=="^")||(LChar==":")||(LChar==",")||(LChar=="?"))) {
                  Result+=LChar;
               } else {
                  Result+="_";
               }
            }
         }
         LOff = BytesPerRow-DataSlice.length;
         while (LOff>0) {
            Result+="_";
            --LOff;
         }
         Result+="\r\n";
         return Result
      };
      if (TAllocation.GetHandle(Self)) {
         mCount = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp14;
         for(x$10=0,$temp14=TAllocation.GetSize$3(Self);x$10<$temp14;x$10++) {
            LByte = TBinaryData.GetByte(Self,x$10);
            mDump.push(LByte);
            if ($SetIn(Options$5,0,0,2)) {
               Result+="$"+(IntToHex2(LByte)).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(LByte)).toLocaleUpperCase();
            }
            ++mCount;
            if (mCount>=BytesPerRow) {
               if ($SetIn(Options$5,1,0,2)&&mCount>0) {
                  Result+=" ";
                  mPad = BytesPerRow-mCount;
                  var $temp15;
                  for(z=1,$temp15=mPad;z<=$temp15;z++) {
                     if ($SetIn(Options$5,0,0,2)) {
                        Result+="$";
                     }
                     Result+="00";
                     ++mCount;
                     if (mCount>=BytesPerRow) {
                        mCount = 0;
                        break;
                     } else {
                        Result+=" ";
                     }
                  }
               }
               if (mDump.length>0) {
                  Result+=SliceToText(mDump);
                  mDump.length=0;
                  mCount = 0;
               }
            } else {
               Result+=" ";
            }
         }
         if (mDump.length>0) {
            if ($SetIn(Options$5,1,0,2)&&mCount>0) {
               mPad$1 = BytesPerRow-mCount;
               var $temp16;
               for(z$1=1,$temp16=mPad$1;z$1<=$temp16;z$1++) {
                  if ($SetIn(Options$5,0,0,2)) {
                     Result+="$";
                  }
                  Result+="00";
                  ++mCount;
                  if (mCount>=BytesPerRow) {
                     break;
                  } else {
                     Result+=" ";
                  }
               }
            }
            Result+=" "+SliceToText(mDump);
            mCount = 0;
            mDump.length=0;
         }
      }
      return Result
   }
   /// function TBinaryData.ToNodeBuffer() : JNodeBuffer
   ///  [line: 83, column: 22, file: SmartNJ.Streams]
   ,ToNodeBuffer:function(Self) {
      var Result = null;
      if (TAllocation.a$36(Self)) {
         Result = new Buffer();
      } else {
         Result = new Buffer(TBinaryData.ToTypedArray(Self));
      }
      return Result
   }
   /// function TBinaryData.ToStream() : TStream
   ///  [line: 583, column: 22, file: System.Memory.Buffer]
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$10 = 0,
         LCache = [],
         LTemp$10 = null;
      Result = TDataTypeConverter.Create$7$($New(TMemoryStream));
      LSize$10 = TAllocation.GetSize$3(Self);
      if (LSize$10>0) {
         LTemp$10 = new Uint8Array(Self.FDataView.buffer,0,LSize$10);
         LCache = Array.prototype.slice.call(LTemp$10);
         LTemp$10 = null;
         if (LCache.length>0) {
            try {
               TStream.Write$1(Result,LCache);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$6 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToString() : String
   ///  [line: 659, column: 22, file: System.Memory.Buffer]
   ,ToString$2:function(Self) {
      var Result = "";
      var CHUNK_SIZE = 32768;
      var LRef$3 = undefined;
      if (TAllocation.GetHandle(Self)) {
         LRef$3 = TAllocation.GetHandle(Self);
         var c = [];
    for (var i=0; i < (LRef$3).length; i += CHUNK_SIZE) {
      c.push(String.fromCharCode.apply(null, (LRef$3).subarray(i, i + CHUNK_SIZE)));
    }
    Result = c.join("");
      }
      return Result
   }
   /// function TBinaryData.ToTypedArray() : TMemoryHandle
   ///  [line: 619, column: 22, file: System.Memory.Buffer]
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$9 = 0,
         LTemp$11 = null;
      LLen$9 = TAllocation.GetSize$3(Self);
      if (LLen$9>0) {
         LTemp$11 = Self.FDataView.buffer.slice(0,LLen$9);
         Result = new Uint8Array(LTemp$11);
      }
      return Result
   }
   /// procedure TBinaryData.WriteBinaryData(const Offset: Integer; const Data: TBinaryData)
   ///  [line: 881, column: 23, file: System.Memory.Buffer]
   ,WriteBinaryData:function(Self, Offset$29, Data$21) {
      var LGrowth = 0;
      if (Data$21!==null) {
         if (TAllocation.GetSize$3(Data$21)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$29)) {
               LGrowth = 0;
               if (Offset$29+TAllocation.GetSize$3(Data$21)>TAllocation.GetSize$3(Self)-1) {
                  LGrowth = Offset$29+TAllocation.GetSize$3(Data$21)-TAllocation.GetSize$3(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Data$21),0,TAllocation.GetHandle(Self),0,TAllocation.GetSize$3(Data$21));
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$29]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   /// procedure TBinaryData.WriteBool(const Offset: Integer; const Data: Boolean)
   ///  [line: 968, column: 23, file: System.Memory.Buffer]
   ,WriteBool:function(Self, Offset$30, Data$22) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$30)) {
         LGrowth$1 = 0;
         if (Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$1 = Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$22) {
            Self.FDataView.setUint8(Offset$30,1);
         } else {
            Self.FDataView.setUint8(Offset$30,0);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$30]);
      }
   }
   /// procedure TBinaryData.WriteBuffer(const Offset: Integer; const Data: TMemoryHandle)
   ///  [line: 905, column: 23, file: System.Memory.Buffer]
   ,WriteBuffer$2:function(Self, Offset$31, Data$23) {
      var LBuffer$1 = null,
         LGrowth$2 = 0;
      if (Data$23) {
         if (Data$23.buffer) {
            LBuffer$1 = Data$23.buffer;
            if (LBuffer$1.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$31)) {
                  LGrowth$2 = 0;
                  if (Offset$31+Data$23.length>TAllocation.GetSize$3(Self)-1) {
                     LGrowth$2 = Offset$31+Data$23.length-TAllocation.GetSize$3(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$23,0,TAllocation.GetHandle(Self),Offset$31,parseInt(TAllocation.GetHandle(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$31]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   /// procedure TBinaryData.WriteBytes(const Offset: Integer; const Data: TByteArray)
   ///  [line: 862, column: 23, file: System.Memory.Buffer]
   ,WriteBytes:function(Self, Offset$32, Data$24) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$32)) {
         if (Data$24.length>0) {
            LGrowth$3 = 0;
            if (Offset$32+Data$24.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$3 = Offset$32+Data$24.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle(Self).set(Data$24,Offset$32);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$32]);
      }
   }
   /// procedure TBinaryData.WriteFloat32(const Offset: Integer; const Data: float32)
   ///  [line: 988, column: 23, file: System.Memory.Buffer]
   ,WriteFloat32:function(Self, Offset$33, Data$25) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$33)) {
         LGrowth$4 = 0;
         if (Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$4 = Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$33,Data$25);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$33,Data$25,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$33,Data$25,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$33]);
      }
   }
   /// procedure TBinaryData.WriteFloat64(const Offset: Integer; const Data: float64)
   ///  [line: 1011, column: 23, file: System.Memory.Buffer]
   ,WriteFloat64:function(Self, Offset$34, Data$26) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$34)) {
         LGrowth$5 = 0;
         if (Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$5 = Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$26));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$26),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$26),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$34]);
      }
   }
   /// procedure TBinaryData.WriteInt16(const Offset: Integer; const Data: SmallInt)
   ///  [line: 1032, column: 23, file: System.Memory.Buffer]
   ,WriteInt16:function(Self, Offset$35, Data$27) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$35)) {
         LGrowth$6 = 0;
         if (Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$6 = Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$35,Data$27);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$35,Data$27,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$35,Data$27,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$35]);
      }
   }
   /// procedure TBinaryData.WriteInt32(const Offset: Integer; const Data: Integer)
   ///  [line: 1095, column: 23, file: System.Memory.Buffer]
   ,WriteInt32:function(Self, Offset$36, Data$28) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$36)) {
         LGrowth$7 = 0;
         if (Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$7 = Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$36,Data$28);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$36,Data$28,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$36,Data$28,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$36]);
      }
   }
   /// procedure TBinaryData.WriteLong(const Offset: Integer; const Data: Longword)
   ///  [line: 1074, column: 23, file: System.Memory.Buffer]
   ,WriteLong:function(Self, Offset$37, Data$29) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$37)) {
         LGrowth$8 = 0;
         if (Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$8 = Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$37,Data$29);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$37,Data$29,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$37,Data$29,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$37]);
      }
   }
   /// procedure TBinaryData.WriteStr(const Offset: Integer; const Data: String)
   ///  [line: 942, column: 23, file: System.Memory.Buffer]
   ,WriteStr$1:function(Self, Offset$38, Data$30) {
      var LTemp$12 = [],
         LGrowth$9 = 0,
         x$11 = 0;
      if (Data$30.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$38)) {
            LTemp$12 = TString.EncodeUTF8(TString,Data$30);
            LGrowth$9 = 0;
            if (Offset$38+LTemp$12.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$9 = Offset$38+LTemp$12.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp17;
            for(x$11=0,$temp17=LTemp$12.length;x$11<$temp17;x$11++) {
               Self.FDataView.setUint8(Offset$38+x$11,LTemp$12[x$11]);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$38]);
         }
      }
   }
   /// procedure TBinaryData.WriteWord(const Offset: Integer; const Data: Word)
   ///  [line: 1053, column: 23, file: System.Memory.Buffer]
   ,WriteWord$1:function(Self, Offset$39, Data$31) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$39)) {
         LGrowth$10 = 0;
         if (Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$10 = Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$39,Data$31);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$39,Data$31,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$39,Data$31,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$39]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$7:TAllocation.Create$7
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TBinaryData.$Intf={
   IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$2,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$2,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
/// EBinaryData = class (EW3Exception)
///  [line: 157, column: 3, file: System.Memory.Buffer]
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TBitAccess = class (TObject)
///  [line: 20, column: 3, file: System.Types.Bits]
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBitAccess.Get(const index: Integer; const Value: Byte) : Boolean
   ///  [line: 114, column: 27, file: System.Types.Bits]
   ,Get:function(Self, index$1, Value$31) {
      var Result = false;
      var mMask = 0;
      if (index$1>=0&&index$1<8) {
         mMask = 1<<index$1;
         Result = ((Value$31&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$1]);
      }
      return Result
   }
   /// function TBitAccess.Set(const Index: Integer; const Value: Byte; const Data: Boolean) : Byte
   ///  [line: 127, column: 27, file: System.Types.Bits]
   ,Set$4:function(Self, Index$2, Value$32, Data$32) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$32;
      if (Index$2>=0&&Index$2<8) {
         mMask$1 = 1<<Index$2;
         mSet = ((Value$32&mMask$1)!=0);
         if (mSet!=Data$32) {
            if (Data$32) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
/// TW3DirectoryParser = class (TW3ErrorObject)
///  [line: 49, column: 3, file: System.IOUtils]
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   /// function TW3DirectoryParser.GetErrorObject() : IW3ErrorAccess
   ///  [line: 176, column: 29, file: System.IOUtils]
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   /// function TW3DirectoryParser.IsPathRooted(FilePath: String) : Boolean
   ///  [line: 181, column: 29, file: System.IOUtils]
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   /// function TW3DirectoryParser.IsRelativePath(FilePath: String) : Boolean
   ///  [line: 194, column: 29, file: System.IOUtils]
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3UnixDirectoryParser = class (TW3DirectoryParser)
///  [line: 81, column: 3, file: System.IOUtils]
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3UnixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 670, column: 33, file: System.IOUtils]
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator = "",
         x$12 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Separator = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$12=FilePath$2.length;x$12>=1;x$12--) {
         {var $temp18 = FilePath$2.charAt(x$12-1);
            if ($temp18==".") {
               Result = FilePath$2.substr(0,(x$12-1))+NewExt.v;
               break;
            }
             else if ($temp18==Separator) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 723, column: 33, file: System.IOUtils]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 739, column: 33, file: System.IOUtils]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 517, column: 33, file: System.IOUtils]
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 604, column: 33, file: System.IOUtils]
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$1 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$6.length>0) {
         Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$1)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$1);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$1)+Separator$1;
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 627, column: 33, file: System.IOUtils]
   ,GetExtension:function(Self, Filename$1) {
      var Result = "";
      var Separator$2 = "",
         x$13 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$1.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$1,Separator$2)) {
            TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$13=Filename$1.length;x$13>=1;x$13--) {
               {var $temp19 = Filename$1.charAt(x$13-1);
                  if ($temp19==".") {
                     dx = Filename$1.length;
                     (dx-= x$13);
                     ++dx;
                     Result = RightStr(Filename$1,dx);
                     break;
                  }
                   else if ($temp19==Separator$2) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 585, column: 33, file: System.IOUtils]
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$3 = "",
         Parts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$3)) {
            TW3ErrorObject.SetLastError$1(Self,"No filename part in path error");
         } else {
            Parts = (Temp$1).split(Separator$3);
            Result = Parts[(Parts.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 575, column: 33, file: System.IOUtils]
   ,GetFileNameWithoutExtension:function(Self, Filename$2) {
      var Result = "";
      var temp$11 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      temp$11 = TW3DirectoryParser.GetFileName$(Self,Filename$2);
      if (!TW3ErrorObject.GetFailed$1(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$11,"");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 532, column: 33, file: System.IOUtils]
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$1 = [],
         Separator$4 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$4)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$1 = (Temp$2).split(Separator$4);
            Result = Parts$1[(Parts$1.length-1)];
            return Result;
         }
         Parts$1 = (Temp$2).split(Separator$4);
         if (Parts$1.length>1) {
            Result = Parts$1[(Parts$1.length-1)-1];
         } else {
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 398, column: 33, file: System.IOUtils]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3UnixDirectoryParser.GetRootMoniker() : String
   ///  [line: 403, column: 33, file: System.IOUtils]
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   /// function TW3UnixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 408, column: 33, file: System.IOUtils]
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp20=0;$temp20<FileName.length;$temp20++) {
               el=$uniCharAt(FileName,$temp20);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 437, column: 33, file: System.IOUtils]
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp21=0;$temp21<FolderName.length;$temp21++) {
               el$1=$uniCharAt(FolderName,$temp21);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 714, column: 33, file: System.IOUtils]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$5 = "";
      Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$5)) {
         Result = FilePath$9;
      } else {
         Result = Separator$5+FilePath$9;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 731, column: 33, file: System.IOUtils]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 466, column: 33, file: System.IOUtils]
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$6 = "",
         PathParts = [],
         Index$3 = 0,
         a$285 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$6);
            Index$3 = 0;
            var $temp22;
            for(a$285=0,$temp22=PathParts.length;a$285<$temp22;a$285++) {
               part = PathParts[a$285];
               {var $temp23 = part;
                  if ($temp23=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp23=="~") {
                     if (Index$3>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index$3==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index$3+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TPath = class (TObject)
///  [line: 107, column: 3, file: System.IOUtils]
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TPath.GetDevice(const FilePath: String) : String
   ///  [line: 268, column: 22, file: System.IOUtils]
   ,GetDevice$2:function(FilePath$12) {
      var Result = "";
      var Access$4 = null,
         Error$1 = null;
      Access$4 = GetDirectoryParser();
      Error$1 = Access$4[2]();
      Result = Access$4[10](FilePath$12);
      if (Error$1[0]()) {
         throw Exception.Create($New(EPathError),Error$1[1]());
      }
      return Result
   }
   /// function TPath.IsPathRooted(const FilePath: String) : Boolean
   ///  [line: 257, column: 22, file: System.IOUtils]
   ,IsPathRooted$1:function(FilePath$13) {
      var Result = false;
      var Access$5 = null,
         Error$2 = null;
      Access$5 = GetDirectoryParser();
      Error$2 = Access$5[2]();
      Result = Access$5[7](FilePath$13);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$48$($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
/// EPathError = class (EW3Exception)
///  [line: 105, column: 3, file: System.IOUtils]
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3StorageObjectType enumeration
///  [line: 36, column: 3, file: System.Device.Storage]
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
/// TW3Component = class (TW3OwnedLockedErrorObject)
///  [line: 19, column: 3, file: system.widget]
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.FInitialized = false;
   }
   /// constructor TW3Component.Create(AOwner: TW3Component)
   ///  [line: 73, column: 26, file: system.widget]
   ,Create$53:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$21(Self,AOwner$3);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   /// constructor TW3Component.CreateEx(AOwner: TObject)
   ///  [line: 88, column: 26, file: system.widget]
   ,CreateEx:function(Self, AOwner$4) {
      TW3OwnedErrorObject.Create$21(Self,AOwner$4);
      Self.FInitialized = false;
      return Self
   }
   /// destructor TW3Component.Destroy()
   ///  [line: 99, column: 25, file: system.widget]
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TW3Component.FinalizeObject()
   ///  [line: 129, column: 24, file: system.widget]
   ,FinalizeObject:function(Self) {
      /* null */
   }
   /// procedure TW3Component.InitializeObject()
   ///  [line: 119, column: 24, file: system.widget]
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$21:TW3OwnedErrorObject.Create$21
   ,Create$53$:function($){return $.ClassType.Create$53.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3StorageDevice = class (TW3Component)
///  [line: 145, column: 3, file: System.Device.Storage]
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FActive$1 = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$6 = [0];
   }
   /// procedure TW3StorageDevice.FinalizeObject()
   ///  [line: 211, column: 28, file: System.Device.Storage]
   ,FinalizeObject:function(Self) {
      if (Self.FActive$1) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3StorageDevice.GetActive() : Boolean
   ///  [line: 307, column: 27, file: System.Device.Storage]
   ,GetActive$1:function(Self) {
      return Self.FActive$1;
   }
   /// procedure TW3StorageDevice.GetDeviceId(CB: TW3DeviceIdCallback)
   ///  [line: 261, column: 28, file: System.Device.Storage]
   ,GetDeviceId:function(Self, CB) {
      if (CB) {
         CB(Self.FIdentifier,true);
      }
   }
   /// procedure TW3StorageDevice.GetDeviceOptions(CB: TW3DeviceOptionsCallback)
   ///  [line: 218, column: 28, file: System.Device.Storage]
   ,GetDeviceOptions:function(Self, CB$1) {
      if (CB$1) {
         CB$1(Self.FOptions$6.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.GetName(CB: TW3DeviceNameCallback)
   ///  [line: 255, column: 28, file: System.Device.Storage]
   ,GetName:function(Self, CB$2) {
      if (CB$2) {
         CB$2(Self.FName,true);
      }
   }
   /// procedure TW3StorageDevice.InitializeObject()
   ///  [line: 203, column: 28, file: System.Device.Storage]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$6 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   /// procedure TW3StorageDevice.SetActive(const NewActiveState: Boolean)
   ///  [line: 312, column: 28, file: System.Device.Storage]
   ,SetActive$1:function(Self, NewActiveState) {
      Self.FActive$1 = NewActiveState;
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$21:TW3OwnedErrorObject.Create$21
   ,Create$53:TW3Component.Create$53
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$:function($){return $.ClassType.Examine.apply($.ClassType, arguments)}
   ,FileExists$:function($){return $.ClassType.FileExists.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetPath$:function($){return $.ClassType.GetPath.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive$1,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DeviceAuthenticationData = class (TObject)
///  [line: 67, column: 3, file: System.Device.Storage]
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJFileItemType enumeration
///  [line: 47, column: 3, file: System.Device.Storage]
var TNJFileItemType = [ "wtFile", "wtFolder" ];
/// TNJFileItemList = class (JObject)
///  [line: 62, column: 3, file: System.Device.Storage]
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

/// TNJFileItem = class (JObject)
///  [line: 52, column: 3, file: System.Device.Storage]
function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

function NodeJSOsAPI() {
   return require("os");
};
function UdpAPI() {
   return require("dgram");
};
/// dgram_bindinfo = class (JObject)
///  [line: 24, column: 3, file: NodeJS.dgram]
function dgram_bindinfo() {
}
$Extend(Object,dgram_bindinfo,
   {
      "reuseAddr" : false,
      "port" : 0,
      "address" : "",
      "exclusive" : false
   });

/// TDataTypeConverter = class (TObject)
///  [line: 151, column: 3, file: qtx.sysutils]
var TDataTypeConverter$1 = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer$3 = $.FView$1 = null;
      $.FEndian$1 = 0;
      $.FTyped$1 = null;
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TUInt8Array
   ///  [line: 2060, column: 35, file: qtx.sysutils]
   ,BooleanToBytes$2:function(Value$33) {
      var Result = [];
      Result.push((Value$33)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TUInt8Array) : Boolean
   ///  [line: 2269, column: 29, file: qtx.sysutils]
   ,BytesToBoolean$2:function(Self, Data$33) {
      return Data$33[0]>0;
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TUInt8Array) : Float
   ///  [line: 2214, column: 29, file: qtx.sysutils]
   ,BytesToFloat32$2:function(Self, Data$34) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$34[0]);
      Self.FView$1.setUint8(1,Data$34[1]);
      Self.FView$1.setUint8(2,Data$34[2]);
      Self.FView$1.setUint8(3,Data$34[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView$1.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TUInt8Array) : Float
   ///  [line: 2015, column: 29, file: qtx.sysutils]
   ,BytesToFloat64$2:function(Self, Data$35) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$35[0]);
      Self.FView$1.setUint8(1,Data$35[1]);
      Self.FView$1.setUint8(2,Data$35[2]);
      Self.FView$1.setUint8(3,Data$35[3]);
      Self.FView$1.setUint8(4,Data$35[4]);
      Self.FView$1.setUint8(5,Data$35[5]);
      Self.FView$1.setUint8(6,Data$35[6]);
      Self.FView$1.setUint8(7,Data$35[7]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView$1.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TUInt8Array) : smallint
   ///  [line: 2033, column: 29, file: qtx.sysutils]
   ,BytesToInt16$2:function(Self, Data$36) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$36[0]);
      Self.FView$1.setUint8(1,Data$36[1]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getInt16(0);
            break;
         case 1 :
            Result = Self.FView$1.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TUInt8Array) : Integer
   ///  [line: 1973, column: 29, file: qtx.sysutils]
   ,BytesToInt32$2:function(Self, Data$37) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$37[0]);
      Self.FView$1.setUint8(1,Data$37[1]);
      Self.FView$1.setUint8(2,Data$37[2]);
      Self.FView$1.setUint8(3,Data$37[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getInt32(0);
            break;
         case 1 :
            Result = Self.FView$1.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TUInt8Array) : String
   ///  [line: 1958, column: 29, file: qtx.sysutils]
   ,BytesToString$2:function(Self, Data$38) {
      var Result = "";
      var LTemp$13 = null,
         Codec__ = null;
      if (Data$38.length>0) {
         LTemp$13 = new Uint8Array(Data$38.length);
         (LTemp$13).set(Data$38, 0);
         Codec__ = new TextDecoder("utf8");
         Result = Codec__.decode(LTemp$13);
         Codec__ = null;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TUInt8Array) : TMemoryHandle
   ///  [line: 2045, column: 29, file: qtx.sysutils]
   ,BytesToTypedArray$2:function(Self, Values$10) {
      var Result = undefined;
      var LLen$10 = 0;
      LLen$10 = Values$10.length;
      Result = new Uint8Array(LLen$10);
      (Result).set(Values$10, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TUInt8Array) : word
   ///  [line: 2089, column: 29, file: qtx.sysutils]
   ,BytesToUInt16$2:function(Self, Data$39) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$39[0]);
      Self.FView$1.setUint8(1,Data$39[1]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getUint16(0);
            break;
         case 1 :
            Result = Self.FView$1.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TUInt8Array) : longword
   ///  [line: 1878, column: 29, file: qtx.sysutils]
   ,BytesToUInt32$2:function(Self, Data$40) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$40[0]);
      Self.FView$1.setUint8(1,Data$40[1]);
      Self.FView$1.setUint8(2,Data$40[2]);
      Self.FView$1.setUint8(3,Data$40[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getUint32(0);
            break;
         case 1 :
            Result = Self.FView$1.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TUInt8Array) : Variant
   ///  [line: 2113, column: 29, file: qtx.sysutils]
   ,BytesToVariant$2:function(Self, Data$41) {
      var Result = {v:undefined};
      try {
         var LType$3 = 0;
         LType$3 = Data$41[0];
         Data$41.shift();
         switch (LType$3) {
            case 17 :
               Result.v = TDataTypeConverter$1.BytesToBoolean$2(Self,Data$41);
               break;
            case 18 :
               Result.v = Data$41[0];
               break;
            case 24 :
               Result.v = TDataTypeConverter$1.BytesToUInt16$2(Self,Data$41);
               break;
            case 25 :
               Result.v = TDataTypeConverter$1.BytesToUInt32$2(Self,Data$41);
               break;
            case 19 :
               Result.v = TDataTypeConverter$1.BytesToInt16$2(Self,Data$41);
               break;
            case 20 :
               Result.v = TDataTypeConverter$1.BytesToInt32$2(Self,Data$41);
               break;
            case 21 :
               Result.v = TDataTypeConverter$1.BytesToFloat32$2(Self,Data$41);
               break;
            case 22 :
               Result.v = TDataTypeConverter$1.BytesToFloat64$2(Self,Data$41);
               break;
            case 23 :
               Result.v = TString$1.DecodeUTF8$1(Data$41);
               break;
            default :
               throw EException.CreateFmt$1($New(EConvertError$1),$R[40],[IntToHex2(LType$3)]);
         }
      } finally {return Result.v}
   }
   /// function TDataTypeConverter.CharToByte(const Value: char) : word
   ///  [line: 2071, column: 35, file: qtx.sysutils]
   ,CharToByte$2:function(Value$34) {
      var Result = 0;
      Result = (Value$34).charCodeAt(0);
      return Result
   }
   /// constructor TDataTypeConverter.Create()
   ///  [line: 1635, column: 32, file: qtx.sysutils]
   ,Create$83:function(Self) {
      TObject.Create(Self);
      Self.FBuffer$3 = new ArrayBuffer(16);
    Self.FView$1   = new DataView(Self.FBuffer$3);
      Self.FTyped$1 = new Uint8Array(Self.FBuffer$3,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ///  [line: 1645, column: 31, file: qtx.sysutils]
   ,Destroy:function(Self) {
      Self.FTyped$1 = null;
      Self.FView$1 = null;
      Self.FBuffer$3 = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TUInt8Array
   ///  [line: 2101, column: 29, file: qtx.sysutils]
   ,Float32ToBytes$2:function(Self, Value$35) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setFloat32(0,Value$35);
            break;
         case 1 :
            Self.FView$1.setFloat32(0,Value$35,true);
            break;
         case 2 :
            Self.FView$1.setFloat32(0,Value$35,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TUInt8Array
   ///  [line: 1903, column: 29, file: qtx.sysutils]
   ,Float64ToBytes$2:function(Self, Value$36) {
      var Result = [];
      var LTypeSize$1 = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setFloat64(0,Number(Value$36));
            break;
         case 1 :
            Self.FView$1.setFloat64(0,Number(Value$36),true);
            break;
         case 2 :
            Self.FView$1.setFloat64(0,Number(Value$36),false);
            break;
      }
      LTypeSize$1 = __SIZES$1[9];
      --LTypeSize$1;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize$1 );
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: int8) : int8
   ///  [line: 1746, column: 35, file: qtx.sysutils]
   ,InitInt08$2:function(Value$37) {
      var Result = 0;
      var temp$12 = null;
      temp$12 = new Int8Array(1);
      temp$12[0]=((Value$37<-128)?-128:(Value$37>127)?127:Value$37);
      Result = temp$12[0];
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: int16) : int16
   ///  [line: 1739, column: 35, file: qtx.sysutils]
   ,InitInt16$2:function(Value$38) {
      var Result = 0;
      var temp$13 = null;
      temp$13 = new Int16Array(1);
      temp$13[0]=((Value$38<-32768)?-32768:(Value$38>32767)?32767:Value$38);
      Result = temp$13[0];
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: int32) : int32
   ///  [line: 1732, column: 35, file: qtx.sysutils]
   ,InitInt32$2:function(Value$39) {
      var Result = 0;
      var temp$14 = null;
      temp$14 = new Int32Array(1);
      temp$14[0]=((Value$39<-2147483648)?-2147483648:(Value$39>2147483647)?2147483647:Value$39);
      Result = temp$14[0];
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: uint8) : uint8
   ///  [line: 1767, column: 35, file: qtx.sysutils]
   ,InitUint08$2:function(Value$40) {
      var Result = 0;
      var LTemp$14 = null;
      LTemp$14 = new Uint8Array(1);
      LTemp$14[0]=((Value$40<0)?0:(Value$40>255)?255:Value$40);
      Result = LTemp$14[0];
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: uint16) : uint16
   ///  [line: 1760, column: 35, file: qtx.sysutils]
   ,InitUint16$2:function(Value$41) {
      var Result = 0;
      var temp$15 = null;
      temp$15 = new Uint16Array(1);
      temp$15[0]=((Value$41<0)?0:(Value$41>65536)?65536:Value$41);
      Result = temp$15[0];
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: uint32) : uint32
   ///  [line: 1753, column: 35, file: qtx.sysutils]
   ,InitUint32$2:function(Value$42) {
      var Result = 0;
      var temp$16 = null;
      temp$16 = new Uint32Array(1);
      temp$16[0]=((Value$42<0)?0:(Value$42>4294967295)?4294967295:Value$42);
      Result = temp$16[0];
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: int16) : TUInt8Array
   ///  [line: 2240, column: 29, file: qtx.sysutils]
   ,Int16ToBytes$2:function(Self, Value$43) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setInt16(0,Value$43);
            break;
         case 1 :
            Self.FView$1.setInt16(0,Value$43,true);
            break;
         case 2 :
            Self.FView$1.setInt16(0,Value$43,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: int32) : TUInt8Array
   ///  [line: 1848, column: 29, file: qtx.sysutils]
   ,Int32ToBytes$2:function(Self, Value$44) {
      var Result = [];
      var LTypeSize$2 = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setInt32(0,Value$44);
            break;
         case 1 :
            Self.FView$1.setInt32(0,Value$44,true);
            break;
         case 2 :
            Self.FView$1.setInt32(0,Value$44,false);
            break;
      }
      LTypeSize$2 = __SIZES$1[7];
      --LTypeSize$2;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize$2 );
      return Result
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TJSVMEndianType)
   ///  [line: 1653, column: 30, file: qtx.sysutils]
   ,SetEndian$2:function(Self, NewEndian$1) {
      if (NewEndian$1!=Self.FEndian$1) {
         Self.FEndian$1 = NewEndian$1;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TJSVMDataType) : Integer
   ///  [line: 1689, column: 35, file: qtx.sysutils]
   ,SizeOfType$2:function(Kind$1) {
      return __SIZES$1[Kind$1];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TUInt8Array
   ///  [line: 1918, column: 29, file: qtx.sysutils]
   ,StringToBytes$2:function(Self, Value$45) {
      var Result = [];
      var Codec__$1 = null,
         rw = null;
      if (Value$45.length>0) {
         Codec__$1 = new TextEncoder("utf8");
         rw = Codec__$1.encode(Value$45);
         Codec__$1 = null;
         Result = Array.prototype.slice.call(rw, 0, (rw).byteLength);
         rw = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TUInt8Array
   ///  [line: 1892, column: 35, file: qtx.sysutils]
   ,TypedArrayToBytes$2:function(Value$46) {
      var Result = [];
      if (Value$46) {
         Result = Array.prototype.slice.call(Value$46);
      } else {
         throw Exception.Create($New(EConvertError$1),"Failed to convert, handle is nil or unassigned error");
      }
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: uint16) : TUInt8Array
   ///  [line: 2252, column: 29, file: qtx.sysutils]
   ,UInt16ToBytes$2:function(Self, Value$47) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setUint16(0,Value$47);
            break;
         case 1 :
            Self.FView$1.setUint16(0,Value$47,true);
            break;
         case 2 :
            Self.FView$1.setUint16(0,Value$47,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: uint32) : TUInt8Array
   ///  [line: 1863, column: 29, file: qtx.sysutils]
   ,UInt32ToBytes$2:function(Self, Value$48) {
      var Result = [];
      var LTypeSize$3 = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setUint32(0,Value$48);
            break;
         case 1 :
            Self.FView$1.setUint32(0,Value$48,true);
            break;
         case 2 :
            Self.FView$1.setUint32(0,Value$48,false);
            break;
      }
      LTypeSize$3 = __SIZES$1[5];
      --LTypeSize$3;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize$3 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(const Value: Variant) : TUInt8Array
   ///  [line: 2135, column: 29, file: qtx.sysutils]
   ,VariantToBytes$2:function(Self, Value$49) {
      var Result = {v:[]};
      try {
         var LType$4 = 0;
         function GetUnSignedIntType$1() {
            var Result = 0;
            if (Value$49<=255) {
               return 18;
            }
            if (Value$49<=65536) {
               return 24;
            }
            if (Value$49<=2147483647) {
               Result = 25;
            }
            return Result
         };
         function GetSignedIntType$1() {
            var Result = 0;
            if (Value$49>-32768) {
               return 19;
            }
            if (Value$49>-2147483648) {
               Result = 20;
            }
            return Result
         };
         function IsFloat32$1(x$14) {
            var Result = false;
            Result = isFinite(x$14) && x$14 == Math.fround(x$14);
            return Result
         };
         switch (TVariant$1.ExamineType(Value$49)) {
            case 2 :
               Result.v = [17].slice();
               Result.v.pusha(TDataTypeConverter$1.BooleanToBytes$2((Value$49?true:false)));
               break;
            case 3 :
               if (Value$49<0) {
                  LType$4 = GetSignedIntType$1();
               } else {
                  LType$4 = GetUnSignedIntType$1();
               }
               if (LType$4) {
                  Result.v = [LType$4].slice();
                  switch (LType$4) {
                     case 18 :
                        Result.v.push(TDataTypeConverter$1.InitInt08$2(parseInt(Value$49,10)));
                        break;
                     case 24 :
                        Result.v.pusha(TDataTypeConverter$1.UInt16ToBytes$2(Self,TDataTypeConverter$1.InitUint16$2(parseInt(Value$49,10))));
                        break;
                     case 25 :
                        Result.v.pusha(TDataTypeConverter$1.UInt32ToBytes$2(Self,TDataTypeConverter$1.InitUint32$2(parseInt(Value$49,10))));
                        break;
                     case 19 :
                        Result.v.pusha(TDataTypeConverter$1.Int16ToBytes$2(Self,TDataTypeConverter$1.InitInt16$2(parseInt(Value$49,10))));
                        break;
                     case 20 :
                        Result.v.pusha(TDataTypeConverter$1.Int32ToBytes$2(Self,TDataTypeConverter$1.InitInt32$2(parseInt(Value$49,10))));
                        break;
                  }
               } else {
                  throw Exception.Create($New(EConvertError$1),$R[41]);
               }
               break;
            case 4 :
               if (IsFloat32$1(Value$49)) {
                  Result.v = [21].slice();
                  Result.v.pusha(TDataTypeConverter$1.Float32ToBytes$2(Self,(Number(Value$49))));
               } else {
                  Result.v = [22].slice();
                  Result.v.pusha(TDataTypeConverter$1.Float64ToBytes$2(Self,(Number(Value$49))));
               }
               break;
            case 5 :
               Result.v = [23].slice();
               Result.v.pusha(TString$1.EncodeUTF8$1(String(Value$49)));
               break;
            default :
               throw Exception.Create($New(EConvertError$1),$R[42]);
         }
      } finally {return Result.v}
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$83$:function($){return $.ClassType.Create$83($)}
};
/// TQTXOwnedObject = class (TObject)
///  [line: 298, column: 3, file: qtx.classes]
var TQTXOwnedObject = {
   $ClassName:"TQTXOwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner$1 = null;
   }
   /// function TQTXOwnedObject.GetOwner() : TObject
   ///  [line: 375, column: 26, file: qtx.classes]
   ,GetOwner$2:function(Self) {
      return Self.FOwner$1;
   }
   /// procedure TQTXOwnedObject.SetOwner(const NewOwner: TObject)
   ///  [line: 385, column: 27, file: qtx.classes]
   ,SetOwner$1:function(Self, NewOwner$1) {
      if (NewOwner$1!==Self.FOwner$1) {
         if (TQTXOwnedObject.AcceptOwner$3$(Self,NewOwner$1)) {
            Self.FOwner$1 = NewOwner$1;
         } else {
            throw EException.CreateFmt$1($New(EQTXOwnedObject),"Owner was rejected in %s.%s error",[TObject.ClassName(Self.ClassType), "TQTXOwnedObject.SetOwner"]);
         }
      }
   }
   /// function TQTXOwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 380, column: 26, file: qtx.classes]
   ,AcceptOwner$3:function(Self, CandidateObject$2) {
      return true;
   }
   /// constructor TQTXOwnedObject.Create(const AOwner: TObject)
   ///  [line: 369, column: 29, file: qtx.classes]
   ,Create$95:function(Self, AOwner$5) {
      TObject.Create(Self);
      TQTXOwnedObject.SetOwner$1(Self,AOwner$5);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$3$:function($){return $.ClassType.AcceptOwner$3.apply($.ClassType, arguments)}
};
TQTXOwnedObject.$Intf={
   IQTXOwnedObjectAccess:[TQTXOwnedObject.AcceptOwner$3,TQTXOwnedObject.SetOwner$1,TQTXOwnedObject.GetOwner$2]
}
/// TQTXOwnedLockedObject = class (TQTXOwnedObject)
///  [line: 326, column: 3, file: qtx.classes]
var TQTXOwnedLockedObject = {
   $ClassName:"TQTXOwnedLockedObject",$Parent:TQTXOwnedObject
   ,$Init:function ($) {
      TQTXOwnedObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$3 = 0;
   }
   /// procedure TQTXOwnedLockedObject.DisableAlteration()
   ///  [line: 438, column: 33, file: qtx.classes]
   ,DisableAlteration$3:function(Self) {
      ++Self.FLocked$3;
      if (Self.FLocked$3==1) {
         TQTXOwnedLockedObject.ObjectLocked$4(Self);
      }
   }
   /// procedure TQTXOwnedLockedObject.EnableAlteration()
   ///  [line: 445, column: 33, file: qtx.classes]
   ,EnableAlteration$3:function(Self) {
      if (Self.FLocked$3>0) {
         --Self.FLocked$3;
         if (!Self.FLocked$3) {
            TQTXOwnedLockedObject.ObjectUnLocked$4(Self);
         }
      }
   }
   /// function TQTXOwnedLockedObject.GetLockState() : Boolean
   ///  [line: 455, column: 32, file: qtx.classes]
   ,GetLockState$3:function(Self) {
      return Self.FLocked$3>0;
   }
   /// procedure TQTXOwnedLockedObject.ObjectLocked()
   ///  [line: 460, column: 33, file: qtx.classes]
   ,ObjectLocked$4:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TQTXOwnedLockedObject.ObjectUnLocked()
   ///  [line: 466, column: 33, file: qtx.classes]
   ,ObjectUnLocked$4:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$3:TQTXOwnedObject.AcceptOwner$3
};
TQTXOwnedLockedObject.$Intf={
   IQTXLockObject:[TQTXOwnedLockedObject.DisableAlteration$3,TQTXOwnedLockedObject.EnableAlteration$3,TQTXOwnedLockedObject.GetLockState$3]
   ,IQTXOwnedObjectAccess:[TQTXOwnedObject.AcceptOwner$3,TQTXOwnedObject.SetOwner$1,TQTXOwnedObject.GetOwner$2]
}
/// TQTXLockedObject = class (TObject)
///  [line: 310, column: 3, file: qtx.classes]
var TQTXLockedObject = {
   $ClassName:"TQTXLockedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$4 = 0;
   }
   /// procedure TQTXLockedObject.DisableAlteration()
   ///  [line: 400, column: 28, file: qtx.classes]
   ,DisableAlteration$4:function(Self) {
      ++Self.FLocked$4;
      if (Self.FLocked$4==1) {
         TQTXLockedObject.ObjectLocked$5$(Self);
      }
   }
   /// procedure TQTXLockedObject.EnableAlteration()
   ///  [line: 407, column: 28, file: qtx.classes]
   ,EnableAlteration$4:function(Self) {
      if (Self.FLocked$4>0) {
         --Self.FLocked$4;
         if (!Self.FLocked$4) {
            TQTXLockedObject.ObjectUnLocked$5$(Self);
         }
      }
   }
   /// function TQTXLockedObject.GetLockState() : Boolean
   ///  [line: 417, column: 27, file: qtx.classes]
   ,GetLockState$4:function(Self) {
      return Self.FLocked$4>0;
   }
   /// procedure TQTXLockedObject.ObjectLocked()
   ///  [line: 422, column: 28, file: qtx.classes]
   ,ObjectLocked$5:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TQTXLockedObject.ObjectUnLocked()
   ///  [line: 428, column: 28, file: qtx.classes]
   ,ObjectUnLocked$5:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,ObjectLocked$5$:function($){return $.ClassType.ObjectLocked$5($)}
   ,ObjectUnLocked$5$:function($){return $.ClassType.ObjectUnLocked$5($)}
};
TQTXLockedObject.$Intf={
   IQTXLockObject:[TQTXLockedObject.DisableAlteration$4,TQTXLockedObject.EnableAlteration$4,TQTXLockedObject.GetLockState$4]
}
/// EException = class (Exception)
///  [line: 106, column: 3, file: qtx.sysutils]
var EException = {
   $ClassName:"EException",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EException.CreateFmt(Message: String; const Values: array of const)
   ///  [line: 2298, column: 24, file: qtx.sysutils]
   ,CreateFmt$1:function(Self, Message$1, Values$11) {
      Exception.Create(Self,Format(Message$1,Values$11.slice(0)));
      return Self
   }
   ,Destroy:Exception.Destroy
};
/// EQTXException = class (EException)
///  [line: 17, column: 3, file: qtx.classes]
var EQTXException = {
   $ClassName:"EQTXException",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EQTXOwnedObject = class (EQTXException)
///  [line: 22, column: 3, file: qtx.classes]
var EQTXOwnedObject = {
   $ClassName:"EQTXOwnedObject",$Parent:EQTXException
   ,$Init:function ($) {
      EQTXException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EQTXLockError = class (EQTXException)
///  [line: 21, column: 3, file: qtx.classes]
var EQTXLockError = {
   $ClassName:"EQTXLockError",$Parent:EQTXException
   ,$Init:function ($) {
      EQTXException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EQTXHandleError = class (EQTXException)
///  [line: 20, column: 3, file: qtx.classes]
var EQTXHandleError = {
   $ClassName:"EQTXHandleError",$Parent:EQTXException
   ,$Init:function ($) {
      EQTXException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WriteLnF$1(Text$5, Values$12) {
   var FormText = "";
   try {
      FormText = Format(Text$5,Values$12.slice(0));
      console.log(FormText);
   } catch ($e) {
      var e$7 = $W($e);
      /* null */
   }
};
function WriteLn$1(value$1) {
   var items$1,
      litem = "",
      LArr = [],
      a$286 = 0;
   var item;
   if (TVariant$1.IsString$1(value$1)) {
      if (((String(value$1)).indexOf("\r")+1)>0) {
         items$1 = value$1.split("\r");
         for (litem in items$1) {
            console.log(litem);
         }
         return;
      }
   } else if (TVariant$1.IsArray(value$1)) {
      LArr = value$1;
      var $temp24;
      for(a$286=0,$temp24=LArr.length;a$286<$temp24;a$286++) {
         item = LArr[a$286];
         WriteLn$1(item);
      }
      return;
   }
   console.log(value$1);
};
/// TVariantExportType enumeration
///  [line: 66, column: 3, file: qtx.sysutils]
var TVariantExportType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TVariant = class (TObject)
///  [line: 265, column: 3, file: qtx.sysutils]
var TVariant$1 = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.CreateObject() : Variant
   ///  [line: 1087, column: 25, file: qtx.sysutils]
   ,CreateObject$1:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// function TVariant.ExamineType(const Value: Variant) : TVariantExportType
   ///  [line: 1102, column: 25, file: qtx.sysutils]
   ,ExamineType:function(Value$50) {
      var Result = 1;
      if (Value$50) {
         {var $temp25 = (typeof(Value$50)).toLocaleLowerCase();
            if ($temp25=="object") {
               Result = (Value$50.hasOwnProperty("length"))?9:8;
            }
             else if ($temp25=="function") {
               Result = 7;
            }
             else if ($temp25=="symbol") {
               Result = 6;
            }
             else if ($temp25=="boolean") {
               Result = 2;
            }
             else if ($temp25=="string") {
               Result = 5;
            }
             else if ($temp25=="number") {
               Result = (!(~(Value$50%1)))?4:3;
            }
             else if ($temp25=="array") {
               Result = 9;
            }
             else {
               Result = 1;
            }
         }
      } else {
         Result = 1;
      }
      return Result
   }
   /// function TVariant.IsArray(const aValue: Variant) : Boolean
   ///  [line: 1139, column: 25, file: qtx.sysutils]
   ,IsArray:function(aValue$9) {
      var Result = false;
      if (Array.isArray(aValue$9) = true) {
      Result = true;
    }
      return Result
   }
   /// function TVariant.IsString(const aValue: Variant) : Boolean
   ///  [line: 1134, column: 25, file: qtx.sysutils]
   ,IsString$1:function(aValue$10) {
      return typeof(aValue$10)==__TYPE_MAP$1.String$2;
   }
   ,Destroy:TObject.Destroy
};
/// TValuePrefixType enumeration
///  [line: 324, column: 3, file: qtx.sysutils]
var TValuePrefixType$1 = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TString = class (TObject)
///  [line: 333, column: 3, file: qtx.sysutils]
var TString$1 = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.CharCodeFor(const Character: char) : Integer
   ///  [line: 2480, column: 24, file: qtx.sysutils]
   ,CharCodeFor$1:function(Character$1) {
      var Result = 0;
      Result = (Character$1).charCodeAt(0);
      return Result
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TUInt8Array) : String
   ///  [line: 2539, column: 24, file: qtx.sysutils]
   ,DecodeUTF8$1:function(BytesToDecode$1) {
      var Result = "";
      var LCodec$2 = null;
      LCodec$2 = TDataTypeConverter$1.Create$83$($New(TQTXCodecUTF8));
      try {
         Result = TQTXCodecUTF8.Decode$3(LCodec$2,BytesToDecode$1);
      } finally {
         TObject.Free(LCodec$2);
      }
      return Result
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TUInt8Array
   ///  [line: 2529, column: 24, file: qtx.sysutils]
   ,EncodeUTF8$1:function(TextToEncode$2) {
      var Result = [];
      var LCodec$3 = null;
      LCodec$3 = TDataTypeConverter$1.Create$83$($New(TQTXCodecUTF8));
      try {
         Result = TQTXCodecUTF8.Encode$3(LCodec$3,TextToEncode$2);
      } finally {
         TObject.Free(LCodec$3);
      }
      return Result
   }
   /// function TString.FromCharCode(const CharCode: uint8) : char
   ///  [line: 2503, column: 24, file: qtx.sysutils]
   ,FromCharCode$1:function(CharCode$1) {
      var Result = "";
      Result = String.fromCharCode(CharCode$1);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TJSVMEndianType enumeration
///  [line: 60, column: 3, file: qtx.sysutils]
var TJSVMEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
/// TJSVMDataType enumeration
///  [line: 45, column: 3, file: qtx.sysutils]
var TJSVMDataType = [ "dtUnknown", "dtBoolean", "dtByte", "dtChar", "dtWord", "dtLong", "dtInt16", "dtInt32", "dtFloat32", "dtFloat64", "dtString" ];
/// TInteger = class (TObject)
///  [line: 232, column: 3, file: qtx.sysutils]
var TInteger$1 = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ///  [line: 1547, column: 25, file: qtx.sysutils]
   ,Diff$1:function(Primary$1, Secondary$1) {
      var Result = 0;
      if (Primary$1!=Secondary$1) {
         if (Primary$1>Secondary$1) {
            Result = Primary$1-Secondary$1;
         } else {
            Result = Secondary$1-Primary$1;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const Value: Integer; const Lowest: Integer; const Highest: Integer) : Integer
   ///  [line: 1501, column: 25, file: qtx.sysutils]
   ,EnsureRange$1:function(Value$51, Lowest, Highest) {
      return (Value$51<Lowest)?Lowest:(Value$51>Highest)?Highest:Value$51;
   }
   /// function TInteger.WrapRange(const Value: Integer; const LowRange: Integer; const HighRange: Integer) : Integer
   ///  [line: 1515, column: 25, file: qtx.sysutils]
   ,WrapRange$1:function(Value$52, LowRange, HighRange) {
      var Result = 0;
      if (Value$52>HighRange) {
         Result = LowRange+TInteger$1.Diff$1(HighRange,(Value$52-1));
         if (Result>HighRange) {
            Result = TInteger$1.WrapRange$1(Result,LowRange,HighRange);
         }
      } else if (Value$52<LowRange) {
         Result = HighRange-TInteger$1.Diff$1(LowRange,(Value$52+1));
         if (Result<LowRange) {
            Result = TInteger$1.WrapRange$1(Result,LowRange,HighRange);
         }
      } else {
         Result = Value$52;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TEnumState enumeration
///  [line: 100, column: 3, file: qtx.sysutils]
var TEnumState$1 = [ "esBreak", "esContinue" ];
/// EConvertError = class (EException)
///  [line: 122, column: 3, file: qtx.sysutils]
var EConvertError$1 = {
   $ClassName:"EConvertError",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TTypeLookup = record
///  [line: 451, column: 3, file: qtx.sysutils]
function Copy$TTypeLookup(s,d) {
   d.Boolean$1=s.Boolean$1;
   d.Number$2=s.Number$2;
   d.String$2=s.String$2;
   d.Object$4=s.Object$4;
   d.Undefined$1=s.Undefined$1;
   d.Function$3=s.Function$3;
   return d;
}
function Clone$TTypeLookup($) {
   return {
      Boolean$1:$.Boolean$1,
      Number$2:$.Number$2,
      String$2:$.String$2,
      Object$4:$.Object$4,
      Undefined$1:$.Undefined$1,
      Function$3:$.Function$3
   }
}
function InitializeTypeMap() {
   __TYPE_MAP$1.Boolean$1 = typeof(true);
   __TYPE_MAP$1.Number$2 = typeof(0);
   __TYPE_MAP$1.String$2 = typeof("");
   __TYPE_MAP$1.Object$4 = typeof(TVariant$1.CreateObject$1());
   __TYPE_MAP$1.Undefined$1 = typeof(undefined);
   __TYPE_MAP$1.Function$3 = typeof(function () {
      /* null */
   });
};
function InitializeBase64() {
   var i$2 = 0;
   var $temp26;
   for(i$2=1,$temp26=CNT_B64_CHARSET$1.length;i$2<=$temp26;i$2++) {
      __B64_Lookup$1[i$2-1] = CNT_B64_CHARSET$1.charAt(i$2-1);
      __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2(CNT_B64_CHARSET$1.charAt(i$2-1))] = i$2-1;
   }
   __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2("-")] = 62;
   __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2("_")] = 63;
};
/// TManagedMemory = class (TDataTypeConverter)
///  [line: 34, column: 3, file: qtx.memory]
var TManagedMemory = {
   $ClassName:"TManagedMemory",$Parent:TDataTypeConverter$1
   ,$Init:function ($) {
      TDataTypeConverter$1.$Init($);
      $.OnMemoryReleased = null;
      $.OnMemoryAllocated = null;
      $.FArray = null;
      $.FBuffer$4 = $.FView$2 = null;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 50, column: 34, file: qtx.memory]
   ,a$263:function(Self) {
      return (Self.FBuffer$4!==null)?Self.FBuffer$4.byteLength:0;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 49, column: 38, file: qtx.memory]
   ,a$262:function(Self) {
      return Self.FBuffer$4===null;
   }
   /// procedure TManagedMemory.AfterAllocate()
   ///  [line: 375, column: 26, file: qtx.memory]
   ,AfterAllocate:function(Self) {
      if (Self.OnMemoryAllocated) {
         Self.OnMemoryAllocated(Self);
      }
   }
   /// procedure TManagedMemory.AfterRelease()
   ///  [line: 381, column: 26, file: qtx.memory]
   ,AfterRelease:function(Self) {
      if (Self.OnMemoryReleased) {
         Self.OnMemoryReleased(Self);
      }
   }
   /// procedure TManagedMemory.Allocate(BytesToAllocate: int64)
   ///  [line: 387, column: 26, file: qtx.memory]
   ,Allocate$1:function(Self, BytesToAllocate) {
      BytesToAllocate={v:BytesToAllocate};
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      if (BytesToAllocate.v>0) {
         TManagedMemory.BeforeAllocate(Self,BytesToAllocate);
         try {
            Self.FBuffer$4 = new ArrayBuffer(BytesToAllocate.v);
            Self.FView$2 = new DataView(Self.FBuffer$4);
            Self.FArray = new Uint8Array(Self.FBuffer$4);
         } catch ($e) {
            var e$8 = $W($e);
            Self.FBuffer$4 = null;
            Self.FView$2 = null;
            Self.FArray = null;
            throw $e;
         }
         TManagedMemory.AfterAllocate(Self);
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid size to allocate, value must be positive");
      }
   }
   /// procedure TManagedMemory.Append(const Data: TUInt8Array)
   ///  [line: 210, column: 26, file: qtx.memory]
   ,Append$2:function(Self, Data$42) {
      var LOffset$7 = 0;
      if (Data$42.length>0) {
         LOffset$7 = (Self.FBuffer$4!==null)?TManagedMemory.a$263(Self):0;
         TManagedMemory.Grow$1(Self,Data$42.length);
         TManagedMemory.WriteBuffer$4(Self,LOffset$7,Data$42);
      }
   }
   /// procedure TManagedMemory.Assign(const Memory: IManagedData)
   ///  [line: 191, column: 26, file: qtx.memory]
   ,Assign$1:function(Self, Memory$5) {
      var LSize$11 = 0;
      if (Memory$5===null) {
         TManagedMemory.Release$2(Self);
         return;
      }
      LSize$11 = Memory$5[2]();
      if (LSize$11<1) {
         TManagedMemory.Release$2(Self);
         return;
      }
      TManagedMemory.Allocate$1(Self,LSize$11);
      Self.FArray.set(Memory$5[0](),0);
   }
   /// procedure TManagedMemory.BeforeAllocate(var NewSize: Integer)
   ///  [line: 371, column: 26, file: qtx.memory]
   ,BeforeAllocate:function(Self, NewSize$4) {
      /* null */
   }
   /// procedure TManagedMemory.BeforeRelease()
   ///  [line: 367, column: 26, file: qtx.memory]
   ,BeforeRelease:function(Self) {
      /* null */
   }
   /// destructor TManagedMemory.Destroy()
   ///  [line: 148, column: 27, file: qtx.memory]
   ,Destroy:function(Self) {
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      TDataTypeConverter$1.Destroy(Self);
   }
   /// procedure TManagedMemory.FromBytes(const Bytes: TUInt8Array)
   ///  [line: 310, column: 26, file: qtx.memory]
   ,FromBytes:function(Self, Bytes$6) {
      var LLen$11 = 0;
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      LLen$11 = Bytes$6.length;
      if (LLen$11>0) {
         TManagedMemory.Allocate$1(Self,LLen$11);
         (Self.FArray).set(Bytes$6, 0);
      }
   }
   /// function TManagedMemory.GetPosition() : int64
   ///  [line: 155, column: 25, file: qtx.memory]
   ,GetPosition$3:function(Self) {
      return 0;
   }
   /// function TManagedMemory.GetSize() : int64
   ///  [line: 160, column: 25, file: qtx.memory]
   ,GetSize$5:function(Self) {
      return (Self.FBuffer$4!==null)?Self.FBuffer$4.byteLength:0;
   }
   /// procedure TManagedMemory.Grow(const BytesToGrow: Integer)
   ///  [line: 293, column: 26, file: qtx.memory]
   ,Grow$1:function(Self, BytesToGrow) {
      var LOldData = [];
      if (BytesToGrow>0) {
         if (Self.FBuffer$4!==null) {
            LOldData = TManagedMemory.ToBytes$1(Self);
         }
         TManagedMemory.Allocate$1(Self,TManagedMemory.a$263(Self)+BytesToGrow);
         if (LOldData.length>0) {
            TManagedMemory.WriteBuffer$4(Self,0,LOldData);
         }
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid growth value, expected 1 or above error");
      }
   }
   /// function TManagedMemory.ReadBuffer(Offset: int64; ReadLen: int64) : TUInt8Array
   ///  [line: 335, column: 25, file: qtx.memory]
   ,ReadBuffer$4:function(Self, Offset$40, ReadLen) {
      var Result = [];
      var LTemp$15 = null;
      if (Self.FBuffer$4!==null) {
         if (ReadLen>0) {
            if (Offset$40>=0&&Offset$40<TManagedMemory.a$263(Self)) {
               LTemp$15 = Self.FArray.subarray(Offset$40,Offset$40+ReadLen);
               Result = Array.prototype.slice.call(LTemp$15);
            } else {
               throw Exception.Create($New(EManagedMemory),"Invalid offset, expected 0.."+(TManagedMemory.a$263(Self)-1).toString()+" not "+Offset$40.toString()+" error");
            }
         }
      }
      return Result
   }
   /// procedure TManagedMemory.Release()
   ///  [line: 414, column: 26, file: qtx.memory]
   ,Release$2:function(Self) {
      if (Self.FBuffer$4!==null) {
         try {
            try {
               TManagedMemory.BeforeRelease(Self);
            } finally {
               Self.FArray = null;
               Self.FView$2 = null;
               Self.FBuffer$4 = null;
            }
         } finally {
            TManagedMemory.AfterRelease(Self);
         }
      }
   }
   /// procedure TManagedMemory.ScaleTo(NewSize: int64)
   ///  [line: 278, column: 26, file: qtx.memory]
   ,ScaleTo:function(Self, NewSize$5) {
      if (NewSize$5>0) {
         if (NewSize$5!=TManagedMemory.a$263(Self)) {
            if (NewSize$5>TManagedMemory.a$263(Self)) {
               TManagedMemory.Grow$1(Self,(NewSize$5-TManagedMemory.a$263(Self)));
            } else {
               TManagedMemory.Shrink$1(Self,(TManagedMemory.a$263(Self)-NewSize$5));
            }
         }
      } else {
         TManagedMemory.Release$2(Self);
      }
   }
   /// procedure TManagedMemory.Shrink(const BytesToShrink: Integer)
   ///  [line: 165, column: 26, file: qtx.memory]
   ,Shrink$1:function(Self, BytesToShrink) {
      var LNewSize = 0,
         LCache$1 = [];
      if (BytesToShrink>0) {
         LNewSize = TManagedMemory.a$263(Self)-BytesToShrink;
         if (LNewSize<=0) {
            TManagedMemory.Release$2(Self);
            return;
         }
         LCache$1 = TManagedMemory.ReadBuffer$4(Self,0,LNewSize);
         TManagedMemory.Allocate$1(Self,LNewSize);
         TManagedMemory.WriteBuffer$4(Self,0,LCache$1);
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid shrink value, expected 1 or above error");
      }
   }
   /// function TManagedMemory.ToBytes() : TUInt8Array
   ///  [line: 325, column: 25, file: qtx.memory]
   ,ToBytes$1:function(Self) {
      var Result = [];
      if (Self.FBuffer$4!==null) {
         Result = Array.prototype.slice.call(Self.FArray);
      }
      return Result
   }
   /// procedure TManagedMemory.WriteBuffer(Offset: int64; Data: TUInt8Array)
   ///  [line: 353, column: 26, file: qtx.memory]
   ,WriteBuffer$4:function(Self, Offset$41, Data$43) {
      if (Self.FBuffer$4!==null) {
         if (Data$43.length>0) {
            if (Offset$41>=0&&Offset$41<TManagedMemory.a$263(Self)) {
               Self.FArray.set(Data$43,Offset$41);
            } else {
               throw Exception.Create($New(EManagedMemory),"Invalid offset, expected 0.."+(TManagedMemory.a$263(Self)-1).toString()+" not "+Offset$41.toString()+" error");
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$83:TDataTypeConverter$1.Create$83
};
TManagedMemory.$Intf={
   IManagedData:[TManagedMemory.ToBytes$1,TManagedMemory.FromBytes,TManagedMemory.GetSize$5,TManagedMemory.GetPosition$3,TManagedMemory.ReadBuffer$4,TManagedMemory.WriteBuffer$4,TManagedMemory.Grow$1,TManagedMemory.Shrink$1,TManagedMemory.Assign$1,TManagedMemory.Append$2]
}
/// EManagedMemory = class (Exception)
///  [line: 17, column: 3, file: qtx.memory]
var EManagedMemory = {
   $ClassName:"EManagedMemory",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EQTXNetworkError = class (EException)
///  [line: 16, column: 3, file: qtx.network.service]
var EQTXNetworkError = {
   $ClassName:"EQTXNetworkError",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TWriteFileOptions = record
///  [line: 71, column: 3, file: NodeJS.fs]
function Copy$TWriteFileOptions(s,d) {
   return d;
}
function Clone$TWriteFileOptions($) {
   return {

   }
}
/// TWatchOptions = record
///  [line: 93, column: 3, file: NodeJS.fs]
function Copy$TWatchOptions(s,d) {
   return d;
}
function Clone$TWatchOptions($) {
   return {

   }
}
/// TWatchFileOptions = record
///  [line: 88, column: 3, file: NodeJS.fs]
function Copy$TWatchFileOptions(s,d) {
   return d;
}
function Clone$TWatchFileOptions($) {
   return {

   }
}
/// TWatchFileListenerObject = record
///  [line: 83, column: 3, file: NodeJS.fs]
function Copy$TWatchFileListenerObject(s,d) {
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {

   }
}
/// TReadFileSyncOptions = record
///  [line: 66, column: 3, file: NodeJS.fs]
function Copy$TReadFileSyncOptions(s,d) {
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {

   }
}
/// TReadFileOptions = record
///  [line: 61, column: 3, file: NodeJS.fs]
function Copy$TReadFileOptions(s,d) {
   return d;
}
function Clone$TReadFileOptions($) {
   return {

   }
}
/// TCreateWriteStreamOptions = class (TObject)
///  [line: 122, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.flags = "w";
   }
   ,Destroy:TObject.Destroy
};
/// TCreateWriteStreamOptionsEx = class (TCreateWriteStreamOptions)
///  [line: 131, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptions = class (TObject)
///  [line: 102, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptionsEx = class (TCreateReadStreamOptions)
///  [line: 112, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TAppendFileOptions = record
///  [line: 77, column: 3, file: NodeJS.fs]
function Copy$TAppendFileOptions(s,d) {
   return d;
}
function Clone$TAppendFileOptions($) {
   return {

   }
}
function NodeFsAPI() {
   return require("fs");
};
function NodePathAPI() {
   return require("path");
};
/// JPathParseData = record
///  [line: 20, column: 3, file: NodeJS.Path]
function Copy$JPathParseData(s,d) {
   return d;
}
function Clone$JPathParseData($) {
   return {

   }
}
/// TQTXHostBindings = class (TQTXLockedObject)
///  [line: 69, column: 3, file: qtx.network.bindings]
var TQTXHostBindings = {
   $ClassName:"TQTXHostBindings",$Parent:TQTXLockedObject
   ,$Init:function ($) {
      TQTXLockedObject.$Init($);
      $.FItems$1 = [];
   }
   /// function TQTXHostBindings.Add() : TQTXHostBinding
   ///  [line: 178, column: 27, file: qtx.network.bindings]
   ,Add$1:function(Self) {
      var Result = null;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw EException.CreateFmt$1($New(EQTXHostBindingError),"%s failed, bindings cannot be altered while active error",["TQTXHostBindings.Add"]);
      } else {
         Result = TQTXHostBinding.Create$98($New(TQTXHostBinding),Self);
         Self.FItems$1.push(Result);
      }
      return Result
   }
   /// function TQTXHostBindings.AddEx(Addresse: String; Port: uint16) : TQTXHostBinding
   ///  [line: 188, column: 27, file: qtx.network.bindings]
   ,AddEx$1:function(Self, Addresse, Port$7) {
      var Result = null;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw EException.CreateFmt$1($New(EQTXHostBindingError),"%s failed, bindings cannot be altered while active error",["TQTXHostBindings.AddEx"]);
      } else {
         if (TQTXHostBindings.FindByPort(Self,Port$7)!==null) {
            throw EException.CreateFmt$1($New(EQTXHostBindingError),"Failed to add binding, port [%d] is already bound",[Port$7]);
         }
         Result = TQTXHostBinding.Create$98($New(TQTXHostBinding),Self);
         TQTXHostBinding.SetHost(Result,Addresse);
         TQTXHostBinding.SetPort$5(Result,Port$7);
         Self.FItems$1.push(Result);
      }
      return Result
   }
   /// procedure TQTXHostBindings.Clear()
   ///  [line: 112, column: 28, file: qtx.network.bindings]
   ,Clear$7:function(Self) {
      var x$15 = 0;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw Exception.Create($New(EQTXLockError),"Bindings cannot be altered while active error");
      } else {
         try {
            var $temp27;
            for(x$15=0,$temp27=Self.FItems$1.length;x$15<$temp27;x$15++) {
               TObject.Free(Self.FItems$1[x$15]);
               Self.FItems$1[x$15]=null;
            }
         } finally {
            Self.FItems$1.length=0;
         }
      }
   }
   /// procedure TQTXHostBindings.Delete(const index: uint32)
   ///  [line: 203, column: 28, file: qtx.network.bindings]
   ,Delete$4:function(Self, index$2) {
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw Exception.Create($New(EQTXLockError),"Bindings cannot be altered while active error");
      } else {
         try {
            Self.FItems$1.splice(index$2,1)
            ;
         } catch ($e) {
            var e$9 = $W($e);
            throw EException.CreateFmt$1($New(EQTXHostBindingError),"Failed to delete binding, system threw exception %s with message %s",[TObject.ClassName(e$9.ClassType), e$9.FMessage]);
         }
      }
   }
   /// procedure TQTXHostBindings.DeleteEx(const Item: TQTXHostBinding)
   ///  [line: 218, column: 28, file: qtx.network.bindings]
   ,DeleteEx$1:function(Self, Item$1) {
      if (Item$1!==null) {
         TQTXHostBindings.Delete$4(Self,Self.FItems$1.indexOf(Item$1));
      } else {
         throw EException.CreateFmt$1($New(EQTXHostBindingError),"%s failed, item was NIL error",["TQTXHostBindings.DeleteEx"]);
      }
   }
   /// destructor TQTXHostBindings.Destroy()
   ///  [line: 105, column: 29, file: qtx.network.bindings]
   ,Destroy:function(Self) {
      if (Self.FItems$1.length>0) {
         TQTXHostBindings.Clear$7(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TQTXHostBindings.FindByHost(Address: String) : TQTXHostBinding
   ///  [line: 152, column: 27, file: qtx.network.bindings]
   ,FindByHost:function(Self, Address$5) {
      var Result = null;
      var x$16 = 0;
      var LItem$3 = null;
      Address$5 = Trim$_String_((Address$5).toLocaleLowerCase());
      if (Address$5.length>0) {
         var $temp28;
         for(x$16=0,$temp28=Self.FItems$1.length;x$16<$temp28;x$16++) {
            LItem$3 = Self.FItems$1[x$16];
            if (LItem$3.FHost$2==Address$5) {
               Result = LItem$3;
               break;
            }
         }
      }
      return Result
   }
   /// function TQTXHostBindings.FindByPort(const Port: uint16) : TQTXHostBinding
   ///  [line: 139, column: 27, file: qtx.network.bindings]
   ,FindByPort:function(Self, Port$8) {
      var Result = null;
      var x$17 = 0;
      var LItem$4 = null;
      var $temp29;
      for(x$17=0,$temp29=Self.FItems$1.length;x$17<$temp29;x$17++) {
         LItem$4 = Self.FItems$1[x$17];
         if (LItem$4.FPort$4==Port$8) {
            Result = LItem$4;
            break;
         }
      }
      return Result
   }
   /// function TQTXHostBindings.GetBinding(const index: int32) : TQTXHostBinding
   ///  [line: 134, column: 27, file: qtx.network.bindings]
   ,GetBinding$1:function(Self, index$3) {
      return Self.FItems$1[index$3];
   }
   /// function TQTXHostBindings.GetBindingCount() : int32
   ///  [line: 129, column: 27, file: qtx.network.bindings]
   ,GetBindingCount$1:function(Self) {
      return Self.FItems$1.length;
   }
   /// function TQTXHostBindings.IndexOf(const Item: TQTXHostBinding) : int32
   ///  [line: 169, column: 27, file: qtx.network.bindings]
   ,IndexOf$1:function(Self, Item$2) {
      var Result = 0;
      if (Item$2!==null) {
         Result = Self.FItems$1.indexOf(Item$2);
      } else {
         throw EException.CreateFmt$1($New(EQTXHostBindingError),"%s failed, item was NIL error",["TQTXHostBindings.IndexOf"]);
      }
      return Result
   }
   /// procedure TQTXHostBindings.ObjectLocked()
   ///  [line: 227, column: 28, file: qtx.network.bindings]
   ,ObjectLocked$5:function(Self) {
      var x$18 = 0;
      var LAccess$2 = null;
      var $temp30;
      for(x$18=0,$temp30=Self.FItems$1.length;x$18<$temp30;x$18++) {
         LAccess$2 = $AsIntf(Self.FItems$1[x$18],"IQTXLockObject");
         LAccess$2[0]();
      }
   }
   /// procedure TQTXHostBindings.ObjectUnLocked()
   ///  [line: 236, column: 28, file: qtx.network.bindings]
   ,ObjectUnLocked$5:function(Self) {
      var x$19 = 0;
      var LAccess$3 = null;
      var $temp31;
      for(x$19=0,$temp31=Self.FItems$1.length;x$19<$temp31;x$19++) {
         LAccess$3 = $AsIntf(Self.FItems$1[x$19],"IQTXLockObject");
         LAccess$3[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$5$:function($){return $.ClassType.ObjectLocked$5($)}
   ,ObjectUnLocked$5$:function($){return $.ClassType.ObjectUnLocked$5($)}
};
TQTXHostBindings.$Intf={
   IQTXHostBindings:[TQTXHostBindings.GetBindingCount$1,TQTXHostBindings.GetBinding$1,TQTXHostBindings.Add$1,TQTXHostBindings.AddEx$1,TQTXHostBindings.IndexOf$1,TQTXHostBindings.FindByHost,TQTXHostBindings.FindByPort,TQTXHostBindings.Delete$4,TQTXHostBindings.DeleteEx$1,TQTXHostBindings.Clear$7]
   ,IQTXLockObject:[TQTXLockedObject.DisableAlteration$4,TQTXLockedObject.EnableAlteration$4,TQTXLockedObject.GetLockState$4]
}
/// TQTXHostBinding = class (TQTXOwnedLockedObject)
///  [line: 25, column: 3, file: qtx.network.bindings]
var TQTXHostBinding = {
   $ClassName:"TQTXHostBinding",$Parent:TQTXOwnedLockedObject
   ,$Init:function ($) {
      TQTXOwnedLockedObject.$Init($);
      $.FHost$2 = "";
      $.FMembers = null;
      $.FPort$4 = 0;
   }
   /// function TQTXHostBinding.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 267, column: 26, file: qtx.network.bindings]
   ,AcceptOwner$3:function(Self, CandidateObject$3) {
      return (CandidateObject$3!==null)&&$Is(CandidateObject$3,TQTXHostBindings);
   }
   /// constructor TQTXHostBinding.Create(const AOwner: TQTXHostBindings)
   ///  [line: 249, column: 29, file: qtx.network.bindings]
   ,Create$98:function(Self, AOwner$6) {
      TQTXOwnedObject.Create$95(Self,AOwner$6);
      Self.FMembers = TObject.Create($New(TQTXHostMemberships));
      return Self
   }
   /// destructor TQTXHostBinding.Destroy()
   ///  [line: 255, column: 28, file: qtx.network.bindings]
   ,Destroy:function(Self) {
      TQTXHostMemberships.Clear$8(Self.FMembers);
      TObject.Free(Self.FMembers);
      TObject.Destroy(Self);
   }
   /// function TQTXHostBinding.GetMemberships() : IQTXHostMemberships
   ///  [line: 262, column: 26, file: qtx.network.bindings]
   ,GetMemberships:function(Self) {
      return $AsIntf(Self.FMembers,"IQTXHostMemberships");
   }
   /// procedure TQTXHostBinding.SetHost(const NewHost: String)
   ///  [line: 299, column: 27, file: qtx.network.bindings]
   ,SetHost:function(Self, NewHost$1) {
      if (NewHost$1!=Self.FHost$2) {
         if (TQTXOwnedLockedObject.GetLockState$3(Self)) {
            throw Exception.Create($New(EQTXLockError),"Property cannot be altered while active error");
         } else {
            Self.FHost$2 = NewHost$1;
         }
      }
   }
   /// procedure TQTXHostBinding.SetPort(const NewPort: uint16)
   ///  [line: 277, column: 27, file: qtx.network.bindings]
   ,SetPort$5:function(Self, NewPort) {
      if (NewPort!=Self.FPort$4) {
         if (TQTXOwnedLockedObject.GetLockState$3(Self)) {
            throw Exception.Create($New(EQTXLockError),"Port property cannot be altered while active error");
         } else {
            Self.FPort$4 = NewPort;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner$3$:function($){return $.ClassType.AcceptOwner$3.apply($.ClassType, arguments)}
};
TQTXHostBinding.$Intf={
   IQTXOwnedObjectAccess:[TQTXHostBinding.AcceptOwner$3,TQTXOwnedObject.SetOwner$1,TQTXOwnedObject.GetOwner$2]
   ,IQTXLockObject:[TQTXOwnedLockedObject.DisableAlteration$3,TQTXOwnedLockedObject.EnableAlteration$3,TQTXOwnedLockedObject.GetLockState$3]
}
/// EQTXHostBindingError = class (EQTXException)
///  [line: 23, column: 3, file: qtx.network.bindings]
var EQTXHostBindingError = {
   $ClassName:"EQTXHostBindingError",$Parent:EQTXException
   ,$Init:function ($) {
      EQTXException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TQTXHostMemberships = class (TQTXLockedObject)
///  [line: 56, column: 3, file: qtx.network.membership]
var TQTXHostMemberships = {
   $ClassName:"TQTXHostMemberships",$Parent:TQTXLockedObject
   ,$Init:function ($) {
      TQTXLockedObject.$Init($);
      $.FItems$2 = [];
   }
   /// function TQTXHostMemberships.Add() : TQTXHostMembership
   ///  [line: 151, column: 30, file: qtx.network.membership]
   ,Add$2:function(Self) {
      var Result = null;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw EException.CreateFmt$1($New(EQTXHostMembershipError),"%s failed, memberships cannot be altered while active error",["TQTXHostMemberships.Add"]);
      } else {
         Result = TQTXHostMembership.Create$99($New(TQTXHostMembership),Self);
         Self.FItems$2.push(Result);
      }
      return Result
   }
   /// function TQTXHostMemberships.AddEx(Addresse: String) : TQTXHostMembership
   ///  [line: 161, column: 30, file: qtx.network.membership]
   ,AddEx$2:function(Self, Addresse$1) {
      var Result = null;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw EException.CreateFmt$1($New(EQTXHostMembershipError),"%s failed, bindings cannot be altered while active error",["TQTXHostMemberships.AddEx"]);
      } else {
         if (TQTXHostMemberships.FindByHost$1(Self,Addresse$1)!==null) {
            throw EException.CreateFmt$1($New(EQTXHostMembershipError),"Failed to add membership, host [%d] is already in pool",[Addresse$1]);
         }
         Result = TQTXHostMembership.Create$99($New(TQTXHostMembership),Self);
         TQTXHostMembership.SetHost$1(Result,Addresse$1);
         Self.FItems$2.push(Result);
      }
      return Result
   }
   /// procedure TQTXHostMemberships.Clear()
   ///  [line: 98, column: 31, file: qtx.network.membership]
   ,Clear$8:function(Self) {
      var x$20 = 0;
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw Exception.Create($New(EQTXLockError),"Membership cannot be altered while active error");
      } else {
         try {
            var $temp32;
            for(x$20=0,$temp32=Self.FItems$2.length;x$20<$temp32;x$20++) {
               TObject.Free(Self.FItems$2[x$20]);
               Self.FItems$2[x$20]=null;
            }
         } finally {
            Self.FItems$2.length=0;
         }
      }
   }
   /// procedure TQTXHostMemberships.Delete(const index: uint32)
   ///  [line: 175, column: 31, file: qtx.network.membership]
   ,Delete$5:function(Self, index$4) {
      if (TQTXLockedObject.GetLockState$4(Self)) {
         throw Exception.Create($New(EQTXLockError),"Memberships cannot be altered while active error");
      } else {
         try {
            Self.FItems$2.splice(index$4,1)
            ;
         } catch ($e) {
            var e$10 = $W($e);
            throw EException.CreateFmt$1($New(EQTXHostMembershipError),"Failed to delete membership, system threw exception %s with message %s",[TObject.ClassName(e$10.ClassType), e$10.FMessage]);
         }
      }
   }
   /// procedure TQTXHostMemberships.DeleteEx(const Item: TQTXHostMembership)
   ///  [line: 190, column: 31, file: qtx.network.membership]
   ,DeleteEx$2:function(Self, Item$3) {
      if (Item$3!==null) {
         TQTXHostMemberships.Delete$5(Self,Self.FItems$2.indexOf(Item$3));
      } else {
         throw EException.CreateFmt$1($New(EQTXHostMembershipError),"%s failed, item was NIL error",["TQTXHostMemberships.DeleteEx"]);
      }
   }
   /// destructor TQTXHostMemberships.Destroy()
   ///  [line: 91, column: 32, file: qtx.network.membership]
   ,Destroy:function(Self) {
      if (Self.FItems$2.length>0) {
         TQTXHostMemberships.Clear$8(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TQTXHostMemberships.FindByHost(Address: String) : TQTXHostMembership
   ///  [line: 125, column: 30, file: qtx.network.membership]
   ,FindByHost$1:function(Self, Address$6) {
      var Result = null;
      var x$21 = 0;
      var LItem$5 = null;
      Address$6 = Trim$_String_((Address$6).toLocaleLowerCase());
      if (Address$6.length>0) {
         var $temp33;
         for(x$21=0,$temp33=Self.FItems$2.length;x$21<$temp33;x$21++) {
            LItem$5 = Self.FItems$2[x$21];
            if (LItem$5.FHost$3==Address$6) {
               Result = LItem$5;
               break;
            }
         }
      }
      return Result
   }
   /// function TQTXHostMemberships.Getmember(const index: int32) : TQTXHostMembership
   ///  [line: 120, column: 30, file: qtx.network.membership]
   ,Getmember:function(Self, index$5) {
      return Self.FItems$2[index$5];
   }
   /// function TQTXHostMemberships.GetMemberCount() : int32
   ///  [line: 115, column: 30, file: qtx.network.membership]
   ,GetMemberCount:function(Self) {
      return Self.FItems$2.length;
   }
   /// function TQTXHostMemberships.IndexOf(const Item: TQTXHostMembership) : int32
   ///  [line: 142, column: 30, file: qtx.network.membership]
   ,IndexOf$2:function(Self, Item$4) {
      var Result = 0;
      if (Item$4!==null) {
         Result = Self.FItems$2.indexOf(Item$4);
      } else {
         throw EException.CreateFmt$1($New(EQTXHostMembershipError),"%s failed, item was NIL error",["TQTXHostMemberships.IndexOf"]);
      }
      return Result
   }
   /// procedure TQTXHostMemberships.ObjectLocked()
   ///  [line: 199, column: 31, file: qtx.network.membership]
   ,ObjectLocked$5:function(Self) {
      var x$22 = 0;
      var LAccess$4 = null;
      var $temp34;
      for(x$22=0,$temp34=Self.FItems$2.length;x$22<$temp34;x$22++) {
         LAccess$4 = $AsIntf(Self.FItems$2[x$22],"IQTXLockObject");
         LAccess$4[0]();
      }
   }
   /// procedure TQTXHostMemberships.ObjectUnLocked()
   ///  [line: 208, column: 31, file: qtx.network.membership]
   ,ObjectUnLocked$5:function(Self) {
      var x$23 = 0;
      var LAccess$5 = null;
      var $temp35;
      for(x$23=0,$temp35=Self.FItems$2.length;x$23<$temp35;x$23++) {
         LAccess$5 = $AsIntf(Self.FItems$2[x$23],"IQTXLockObject");
         LAccess$5[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$5$:function($){return $.ClassType.ObjectLocked$5($)}
   ,ObjectUnLocked$5$:function($){return $.ClassType.ObjectUnLocked$5($)}
};
TQTXHostMemberships.$Intf={
   IQTXHostMemberships:[TQTXHostMemberships.GetMemberCount,TQTXHostMemberships.Getmember,TQTXHostMemberships.Add$2,TQTXHostMemberships.AddEx$2,TQTXHostMemberships.IndexOf$2,TQTXHostMemberships.FindByHost$1,TQTXHostMemberships.Delete$5,TQTXHostMemberships.DeleteEx$2,TQTXHostMemberships.Clear$8]
   ,IQTXLockObject:[TQTXLockedObject.DisableAlteration$4,TQTXLockedObject.EnableAlteration$4,TQTXLockedObject.GetLockState$4]
}
/// TQTXHostMembership = class (TQTXOwnedLockedObject)
///  [line: 24, column: 3, file: qtx.network.membership]
var TQTXHostMembership = {
   $ClassName:"TQTXHostMembership",$Parent:TQTXOwnedLockedObject
   ,$Init:function ($) {
      TQTXOwnedLockedObject.$Init($);
      $.FHost$3 = "";
   }
   /// function TQTXHostMembership.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 226, column: 29, file: qtx.network.membership]
   ,AcceptOwner$3:function(Self, CandidateObject$4) {
      return (CandidateObject$4!==null)&&$Is(CandidateObject$4,TQTXHostMemberships);
   }
   /// constructor TQTXHostMembership.Create(const AOwner: TQTXHostMemberships)
   ///  [line: 221, column: 32, file: qtx.network.membership]
   ,Create$99:function(Self, AOwner$7) {
      TQTXOwnedObject.Create$95(Self,AOwner$7);
      return Self
   }
   /// procedure TQTXHostMembership.SetHost(const NewHost: String)
   ///  [line: 236, column: 30, file: qtx.network.membership]
   ,SetHost$1:function(Self, NewHost$2) {
      if (NewHost$2!=Self.FHost$3) {
         if (TQTXOwnedLockedObject.GetLockState$3(Self)) {
            throw Exception.Create($New(EQTXLockError),"Property cannot be altered while active error");
         } else {
            Self.FHost$3 = NewHost$2;
         }
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$3$:function($){return $.ClassType.AcceptOwner$3.apply($.ClassType, arguments)}
};
TQTXHostMembership.$Intf={
   IQTXOwnedObjectAccess:[TQTXHostMembership.AcceptOwner$3,TQTXOwnedObject.SetOwner$1,TQTXOwnedObject.GetOwner$2]
   ,IQTXLockObject:[TQTXOwnedLockedObject.DisableAlteration$3,TQTXOwnedLockedObject.EnableAlteration$3,TQTXOwnedLockedObject.GetLockState$3]
}
/// EQTXHostMembershipError = class (EQTXException)
///  [line: 22, column: 3, file: qtx.network.membership]
var EQTXHostMembershipError = {
   $ClassName:"EQTXHostMembershipError",$Parent:EQTXException
   ,$Init:function ($) {
      EQTXException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WriteLn(Text$6) {
   util().log(String(Text$6));
};
/// TW3Win32DirectoryParser = class (TW3UnixDirectoryParser)
///  [line: 62, column: 3, file: SmartNJ.System]
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   /// function TW3Win32DirectoryParser.GetPathSeparator() : Char
   ///  [line: 609, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   /// function TW3Win32DirectoryParser.GetRootMoniker() : String
   ///  [line: 614, column: 34, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos$1 = 0;
         xpos$1 = (ThisPath.indexOf(":\\")+1);
         if (xpos$1>=2) {
            ++xpos$1;
            Result = ThisPath.substr(0,xpos$1);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3PosixDirectoryParser = class (TW3DirectoryParser)
///  [line: 68, column: 3, file: SmartNJ.System]
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3PosixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 479, column: 34, file: SmartNJ.System]
   ,ChangeFileExt:function(Self, FilePath$14, NewExt$1) {
      var Result = "";
      var LName$1 = "";
      var Separator$7 = "",
         x$24 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$14.length>0) {
         if ((FilePath$14.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$14]);
         } else {
            if (StrEndsWith(FilePath$14," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$14]);
            } else {
               Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$1,Separator$7)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[FilePath$14]);
               } else {
                  if ((FilePath$14.indexOf(Separator$7)+1)>0) {
                     LName$1 = TW3DirectoryParser.GetFileName$(Self,FilePath$14);
                     if (TW3ErrorObject.GetFailed$1(Self)) {
                        return Result;
                     }
                  } else {
                     LName$1 = FilePath$14;
                  }
                  if (LName$1.length>0) {
                     if ((LName$1.indexOf(".")+1)>0) {
                        for(x$24=LName$1.length;x$24>=1;x$24--) {
                           if (LName$1.charAt(x$24-1)==".") {
                              Result = LName$1.substr(0,(x$24-1))+NewExt$1;
                              break;
                           }
                        }
                     } else {
                        Result = LName$1+NewExt$1;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$14]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 596, column: 34, file: SmartNJ.System]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$15) {
      var Result = "";
      var Separator$8 = "";
      Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$15,Separator$8)) {
         Result = FilePath$15.substr((1+Separator$8.length)-1);
      } else {
         Result = FilePath$15;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 577, column: 34, file: SmartNJ.System]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$16) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$16,Separator$9)) {
         Result = FilePath$16.substr(0,(FilePath$16.length-Separator$9.length));
      } else {
         Result = FilePath$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 327, column: 34, file: SmartNJ.System]
   ,GetDevice:function(Self, FilePath$17) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$17.length>0) {
         if (StrBeginsWith(FilePath$17,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 534, column: 34, file: SmartNJ.System]
   ,GetDirectoryName:function(Self, FilePath$18) {
      var Result = "";
      var Separator$10 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$18.length>0) {
         if ((FilePath$18.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$18]);
         } else {
            if (StrEndsWith(FilePath$18," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$18]);
            } else {
               Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$18,Separator$10)) {
                  Result = FilePath$18;
                  return Result;
               }
               NameParts$1 = (FilePath$18).split(Separator$10);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$10)+Separator$10;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$18]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 419, column: 34, file: SmartNJ.System]
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var LName$2 = "";
      var Separator$11 = "",
         x$25 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$3.length>0) {
         if ((Filename$3.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$3]);
         } else {
            if (StrEndsWith(Filename$3," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$3]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$2,Separator$11)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[Filename$3]);
               } else {
                  if ((Filename$3.indexOf(Separator$11)+1)>0) {
                     LName$2 = TW3DirectoryParser.GetFileName$(Self,Filename$3);
                  } else {
                     LName$2 = Filename$3;
                  }
                  if (!TW3ErrorObject.GetFailed$1(Self)) {
                     for(x$25=Filename$3.length;x$25>=1;x$25--) {
                        {var $temp36 = Filename$3.charAt(x$25-1);
                           if ($temp36==".") {
                              dx$1 = Filename$3.length;
                              (dx$1-= x$25);
                              ++dx$1;
                              Result = RightStr(Filename$3,dx$1);
                              break;
                           }
                            else if ($temp36==Separator$11) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$3)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$3]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 342, column: 34, file: SmartNJ.System]
   ,GetFileName:function(Self, FilePath$19) {
      var Result = "";
      var Separator$12 = "",
         x$26 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$19.length>0) {
         if ((FilePath$19.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$19]);
         } else {
            if (StrEndsWith(FilePath$19," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$19]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$19,Separator$12)) {
                  for(x$26=FilePath$19.length;x$26>=1;x$26--) {
                     if (FilePath$19.charAt(x$26-1)!=Separator$12) {
                        Result = FilePath$19.charAt(x$26-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$19]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 372, column: 34, file: SmartNJ.System]
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var Separator$13 = "",
         LName$3 = "";
      var x$27 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$4.length>0) {
         if ((Filename$4.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$4]);
         } else {
            if (StrEndsWith(Filename$4," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$4]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$4.indexOf(Separator$13)+1)>0) {
                  LName$3 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
               } else {
                  LName$3 = Filename$4;
               }
               if (!TW3ErrorObject.GetFailed$1(Self)) {
                  if (LName$3.length>0) {
                     if ((LName$3.indexOf(".")+1)>0) {
                        for(x$27=LName$3.length;x$27>=1;x$27--) {
                           if (LName$3.charAt(x$27-1)==".") {
                              Result = LName$3.substr(0,(x$27-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$3;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$4]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 274, column: 34, file: SmartNJ.System]
   ,GetPathName:function(Self, FilePath$20) {
      var Result = "";
      var LParts = [],
         LTemp$16 = "";
      var Separator$14 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$20.length>0) {
         if ((FilePath$20.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$20]);
         } else {
            if (StrEndsWith(FilePath$20," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$20]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$20,Separator$14)) {
                  if (FilePath$20==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$16 = (FilePath$20).substr(0,(FilePath$20.length-Separator$14.length));
                  LParts = (LTemp$16).split(Separator$14);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$20).split(Separator$14);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$20]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 155, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.GetRootMoniker() : String
   ///  [line: 160, column: 35, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 165, column: 34, file: SmartNJ.System]
   ,HasValidFileNameChars:function(Self, FileName$1) {
      var Result = false;
      var el$2 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName$1.length>0) {
         if ((FileName$1.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$1]);
         } else {
            for (var $temp37=0;$temp37<FileName$1.length;$temp37++) {
               el$2=$uniCharAt(FileName$1,$temp37);
               if (!el$2) continue;
               Result = (((el$2>="A")&&(el$2<="Z"))||((el$2>="a")&&(el$2<="z"))||((el$2>="0")&&(el$2<="9"))||(el$2=="-")||(el$2=="_")||(el$2==".")||(el$2==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$2, FileName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 194, column: 34, file: SmartNJ.System]
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp38=0;$temp38<FolderName$1.length;$temp38++) {
               el$3=$uniCharAt(FolderName$1,$temp38);
               if (!el$3) continue;
               Result = (((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="-")||(el$3=="_")||(el$3==".")||(el$3==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$3, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 587, column: 34, file: SmartNJ.System]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$21) {
      var Result = "";
      var Separator$15 = "";
      Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$21,Separator$15)) {
         Result = FilePath$21;
      } else {
         Result = Separator$15+FilePath$21;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 567, column: 34, file: SmartNJ.System]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$22) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$22;
      if (!StrEndsWith(Result,Separator$16)) {
         Result+=Separator$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 223, column: 34, file: SmartNJ.System]
   ,IsValidPath:function(Self, FilePath$23) {
      var Result = false;
      var Separator$17 = "",
         PathParts$1 = [],
         Index$4 = 0,
         a$287 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$23.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$23]);
      } else {
         if (FilePath$23.length>0) {
            Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$23).split(Separator$17);
            Index$4 = 0;
            var $temp39;
            for(a$287=0,$temp39=PathParts$1.length;a$287<$temp39;a$287++) {
               part$1 = PathParts$1[a$287];
               {var $temp40 = part$1;
                  if ($temp40=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$23]);
                     return false;
                  }
                   else if ($temp40=="~") {
                     if (Index$4>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$23]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$4==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$4+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3EnvVariables = class (TObject)
///  [line: 92, column: 3, file: SmartNJ.System]
var TW3EnvVariables = {
   $ClassName:"TW3EnvVariables",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TRunPlatform enumeration
///  [line: 35, column: 3, file: SmartNJ.System]
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function PathSeparator() {
   return NodePathAPI().sep;
};
function ParamStr$1(index$6) {
   var Result = "";
   Result = process.argv[index$6];
   return Result
};
function IncludeTrailingPathDelimiter$4(PathName) {
   var Result = "";
   Result = Trim$_String_(PathName);
   if (!StrEndsWith(Result,PathSeparator())) {
      Result+=PathSeparator();
   }
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp41 = token;
      if ($temp41=="darwin") {
         Result = 3;
      }
       else if ($temp41=="win32") {
         Result = 1;
      }
       else if ($temp41=="linux") {
         Result = 2;
      }
       else if ($temp41=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
/// TW3RepeatResult enumeration
///  [line: 55, column: 3, file: System.Time]
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
/// TW3CustomRepeater = class (TObject)
///  [line: 71, column: 3, file: System.Time]
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive$2 = false;
      $.FDelay$1 = 0;
      $.FHandle$3 = undefined;
   }
   /// procedure TW3CustomRepeater.AllocTimer()
   ///  [line: 446, column: 29, file: System.Time]
   ,AllocTimer:function(Self) {
      if (Self.FHandle$3) {
         TW3CustomRepeater.FreeTimer(Self);
      }
      Self.FHandle$3 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   /// destructor TW3CustomRepeater.Destroy()
   ///  [line: 400, column: 30, file: System.Time]
   ,Destroy:function(Self) {
      if (Self.FActive$2) {
         TW3CustomRepeater.SetActive$2(Self,false);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomRepeater.FreeTimer()
   ///  [line: 455, column: 29, file: System.Time]
   ,FreeTimer:function(Self) {
      if (Self.FHandle$3) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$3);
         Self.FHandle$3 = undefined;
      }
   }
   /// procedure TW3CustomRepeater.SetActive(const NewActive: Boolean)
   ///  [line: 414, column: 29, file: System.Time]
   ,SetActive$2:function(Self, NewActive) {
      if (NewActive!=Self.FActive$2) {
         try {
            if (Self.FActive$2) {
               TW3CustomRepeater.FreeTimer(Self);
            } else {
               TW3CustomRepeater.AllocTimer(Self);
            }
         } finally {
            Self.FActive$2 = NewActive;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
};
/// TW3Dispatch = class (TObject)
///  [line: 135, column: 3, file: System.Time]
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TW3Dispatch.ClearInterval(const Handle: TW3DispatchHandle)
   ///  [line: 234, column: 29, file: System.Time]
   ,ClearInterval:function(Self, Handle$8) {
      clearInterval(Handle$8);
   }
   /// function TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ///  [line: 264, column: 28, file: System.Time]
   ,Execute$1:function(Self, EntryPoint$1, WaitForInMs) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs);
      return Result
   }
   /// procedure TW3Dispatch.RepeatExecute(const Entrypoint: TProcedureRef; const RepeatCount: Integer; const IntervalInMs: Integer)
   ///  [line: 272, column: 29, file: System.Time]
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute$1(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute$1(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   /// function TW3Dispatch.SetInterval(const Entrypoint: TProcedureRef; const IntervalDelayInMS: Integer) : TW3DispatchHandle
   ///  [line: 226, column: 28, file: System.Time]
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   ,Destroy:TObject.Destroy
};
function JDateToDateTime(Obj) {
   return Obj.getTime()/86400000+25569;
};
function DateTimeToJDate(Present) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present-25569)*86400000));
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
/// TNJFileWalker = class (TW3ErrorObject)
///  [line: 28, column: 3, file: SmartNJ.FileWalker]
var TNJFileWalker = {
   $ClassName:"TNJFileWalker",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnIncludeFile = null;
      $.OnAfterWalk = null;
      $.OnBeforeWalk = null;
      $.FBusy = $.FCancel = false;
      $.FCurrent$2 = $.FPath$1 = "";
      $.FDelay$2 = 1;
      $.FFinished = null;
      $.FItemStack = [];
      $.FListData = null;
   }
   /// procedure TNJFileWalker.Cancel()
   ///  [line: 129, column: 25, file: SmartNJ.FileWalker]
   ,Cancel:function(Self) {
      if (Self.FBusy) {
         Self.FCancel = true;
         Self.FItemStack.length=0;
      }
   }
   /// procedure TNJFileWalker.Clear()
   ///  [line: 117, column: 25, file: SmartNJ.FileWalker]
   ,Clear$6:function(Self) {
      Self.FListData.dlPath = "";
      Self.FListData.dlItems.length=0;
      Self.FListData = new TNJFileItemList();
      Self.FItemStack.length=0;
      Self.FBusy = false;
      Self.FCancel = false;
      Self.FFinished = null;
      Self.FPath$1 = "";
   }
   /// constructor TNJFileWalker.Create()
   ///  [line: 89, column: 27, file: SmartNJ.FileWalker]
   ,Create$48:function(Self) {
      TW3ErrorObject.Create$48(Self);
      Self.FListData = new TNJFileItemList();
      return Self
   }
   /// destructor TNJFileWalker.Destroy()
   ///  [line: 95, column: 26, file: SmartNJ.FileWalker]
   ,Destroy:function(Self) {
      try {
         try {
            if (Self.FBusy) {
               TNJFileWalker.Cancel(Self);
            }
         } finally {
            Self.FItemStack.length=0;
            Self.FListData.dlPath = "";
            Self.FListData.dlItems.length=0;
            Self.FListData = null;
            Self.FFinished = null;
         }
      } catch ($e) {
         var e$11 = $W($e);
         /* null */
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJFileWalker.DoAfterWalk()
   ///  [line: 167, column: 25, file: SmartNJ.FileWalker]
   ,DoAfterWalk:function(Self) {
      try {
         if (Self.FFinished) {
            Self.FFinished(Self,true);
         }
      } finally {
         if (Self.OnAfterWalk) {
            Self.OnAfterWalk(Self);
         }
      }
   }
   /// procedure TNJFileWalker.DoBeforeWalk()
   ///  [line: 161, column: 25, file: SmartNJ.FileWalker]
   ,DoBeforeWalk:function(Self) {
      if (Self.OnBeforeWalk) {
         Self.OnBeforeWalk(Self);
      }
   }
   /// procedure TNJFileWalker.Examine(Path: String; CB: TNJFileWalkerCallback)
   ///  [line: 342, column: 25, file: SmartNJ.FileWalker]
   ,Examine$4:function(Self, Path$1, CB$3) {
      var FFileSys$2 = null;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Self.FBusy) {
         TW3ErrorObject.SetLastError$1(Self,"Filewalker instance is busy error");
         if (CB$3) {
            CB$3(Self,false);
         }
         return;
      }
      TNJFileWalker.Clear$6(Self);
      Self.FBusy = true;
      Self.FFinished = CB$3;
      Self.FPath$1 = Path$1;
      TNJFileWalker.DoBeforeWalk(Self);
      FFileSys$2 = NodeFsAPI();
      FFileSys$2.readdir(Path$1,function (err, files) {
         var a$288 = 0;
         var fsName = "";
         if (err) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Failed to examine [%s], path does not exist error",[Self.FPath$1]);
            if (CB$3) {
               CB$3(Self,false);
            }
         } else {
            Self.FListData.dlPath = Path$1;
            var $temp42;
            for(a$288=0,$temp42=files.length;a$288<$temp42;a$288++) {
               fsName = files[a$288];
               Self.FItemStack.push(fsName);
            }
            TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         }
      });
   }
   /// function TNJFileWalker.ExtractAll() : TNJFileItems
   ///  [line: 285, column: 24, file: SmartNJ.FileWalker]
   ,ExtractAll:function(Self) {
      var Result = [];
      var a$289 = 0;
      var xItem = null;
      if (Self.FBusy) {
         Result = [];
      } else {
         var a$290 = [];
         a$290 = Self.FListData.dlItems;
         var $temp43;
         for(a$289=0,$temp43=a$290.length;a$289<$temp43;a$289++) {
            xItem = a$290[a$289];
            Result.push(xItem);
         }
      }
      return Result
   }
   /// procedure TNJFileWalker.NextOrDone()
   ///  [line: 192, column: 25, file: SmartNJ.FileWalker]
   ,NextOrDone:function(Self) {
      function SignalDone() {
         Self.FBusy = false;
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.DoAfterWalk),Self.FDelay$2);
      };
      if (Self.FCancel) {
         SignalDone();
         return;
      }
      if (Self.FItemStack.length>0) {
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.ProcessFromStack),Self.FDelay$2);
      } else {
         SignalDone();
      }
   }
   /// procedure TNJFileWalker.ProcessFromStack()
   ///  [line: 215, column: 25, file: SmartNJ.FileWalker]
   ,ProcessFromStack:function(Self) {
      var LKeep = {v:false},
         FileObjName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Self.FCancel) {
         TNJFileWalker.NextOrDone(Self);
         return;
      }
      if (Self.FItemStack.length>0) {
         LKeep.v = true;
         FileObjName = Self.FItemStack.pop();
         Self.FCurrent$2 = NodePathAPI().normalize(IncludeTrailingPathDelimiter$4(Self.FPath$1)+FileObjName);
         NodeFsAPI().stat(Self.FCurrent$2,function (err$1, stats) {
            var LItem$6 = null,
               LTemp$17 = "";
            if (err$1) {
               TW3ErrorObject.SetLastError$1(Self,err$1.message);
               TNJFileWalker.NextOrDone(Self);
               return;
            }
            LItem$6 = new TNJFileItem();
            LItem$6.diFileName = FileObjName;
            LItem$6.diFileType = (stats.isFile())?0:1;
            LItem$6.diFileSize = (stats.isFile())?stats.size:0;
            LItem$6.diCreated = (stats.ctime!==null)?JDateToDateTime(stats.ctime):Now();
            LItem$6.diModified = (stats.mtime!==null)?JDateToDateTime(stats.mtime):Now();
            if (stats.isFile()) {
               LTemp$17 = "";
               LTemp$17 = '0' + ((stats).mode & parseInt('777', 8)).toString(8);
               LItem$6.diFileMode = LTemp$17;
            }
            if (Self.OnIncludeFile) {
               Self.OnIncludeFile(Self,LItem$6,LKeep);
            }
            if (LKeep.v) {
               Self.FListData.dlItems.push(LItem$6);
            } else {
               LItem$6 = null;
            }
            Self.FCurrent$2 = "";
            TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         });
      } else {
         TNJFileWalker.NextOrDone(Self);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$48$:function($){return $.ClassType.Create$48($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TNJFileWalker.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TApplication = class (TObject)
///  [line: 29, column: 3, file: SmartNJ.Application]
var TApplication = {
   $ClassName:"TApplication",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TApplication.GetOSApi() : Jos_Exports
   ///  [line: 109, column: 23, file: SmartNJ.Application]
   ,GetOSApi:function(Self) {
      return NodeJSOsAPI();
   }
   ,Destroy:TObject.Destroy
};
function Application() {
   var Result = null;
   if (__App===null) {
      __App = TObject.Create($New(TApplication));
   }
   Result = __App;
   return Result
};
/// Tlisteners_result_object = class (TObject)
///  [line: 43, column: 3, file: NodeJS.cluster]
var Tlisteners_result_object = {
   $ClassName:"Tlisteners_result_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function NodeJSClusterAPI() {
   return require("cluster");
};
/// TNJUdpVersion enumeration
///  [line: 63, column: 3, file: SmartNJ.Udp]
var TNJUdpVersion = [ "udV4", "udv6", "udV7" ];
/// TNJNetworkService = class (TW3HandleBasedObject)
///  [line: 35, column: 3, file: SmartNJ.Network]
var TNJNetworkService = {
   $ClassName:"TNJNetworkService",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.OnAfterStopped = null;
      $.OnBeforeStopped = null;
      $.OnAfterStarted = null;
      $.FActive$3 = false;
      $.FHost$1 = "";
      $.FPort$1 = 0;
   }
   /// procedure TNJNetworkService.AfterStart()
   ///  [line: 110, column: 29, file: SmartNJ.Network]
   ,AfterStart$1:function(Self) {
      if (Self.OnAfterStarted) {
         Self.OnAfterStarted(Self);
      }
   }
   /// procedure TNJNetworkService.AfterStop()
   ///  [line: 122, column: 29, file: SmartNJ.Network]
   ,AfterStop$1:function(Self) {
      if (Self.OnAfterStopped) {
         Self.OnAfterStopped(Self);
      }
   }
   /// procedure TNJNetworkService.BeforeStop()
   ///  [line: 116, column: 29, file: SmartNJ.Network]
   ,BeforeStop$1:function(Self) {
      if (Self.OnBeforeStopped) {
         Self.OnBeforeStopped(Self);
      }
   }
   /// destructor TNJNetworkService.Destroy()
   ///  [line: 87, column: 30, file: SmartNJ.Network]
   ,Destroy:function(Self) {
      if (TNJNetworkService.GetActive$2(Self)) {
         try {
            try {
               TNJNetworkService.FinalizeService$3$(Self);
            } catch ($e) {
               var e$12 = $W($e);
               /* null */
            }
         } finally {
            TW3HandleBasedObject.SetObjectHandle(Self,undefined);
         }
      }
      TObject.Destroy(Self);
   }
   /// function TNJNetworkService.GetActive() : Boolean
   ///  [line: 157, column: 28, file: SmartNJ.Network]
   ,GetActive$2:function(Self) {
      return Self.FActive$3;
   }
   /// function TNJNetworkService.GetAddress() : String
   ///  [line: 128, column: 28, file: SmartNJ.Network]
   ,GetAddress$1:function(Self) {
      return Self.FHost$1;
   }
   /// function TNJNetworkService.GetPort() : Integer
   ///  [line: 142, column: 28, file: SmartNJ.Network]
   ,GetPort$1:function(Self) {
      return Self.FPort$1;
   }
   /// procedure TNJNetworkService.SetActiveEx(const NewValue: Boolean)
   ///  [line: 162, column: 29, file: SmartNJ.Network]
   ,SetActiveEx:function(Self, NewValue$1) {
      Self.FActive$3 = NewValue$1;
   }
   /// procedure TNJNetworkService.SetAddress(const NewHost: String)
   ///  [line: 133, column: 29, file: SmartNJ.Network]
   ,SetAddress$1:function(Self, NewHost$3) {
      if (TNJNetworkService.GetActive$2(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Address cannot be altered while object is active error");
      } else {
         Self.FHost$1 = Trim$_String_(NewHost$3);
      }
   }
   /// procedure TNJNetworkService.SetPort(const NewPort: Integer)
   ///  [line: 147, column: 29, file: SmartNJ.Network]
   ,SetPort$1:function(Self, NewPort$1) {
      if (TNJNetworkService.GetActive$2(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Port cannot be altered while object is active error");
      } else {
         Self.FPort$1 = NewPort$1;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,FinalizeService$3$:function($){return $.ClassType.FinalizeService$3($)}
   ,InitializeService$3$:function($){return $.ClassType.InitializeService$3($)}
};
/// ENJNetworkError = class (EW3Exception)
///  [line: 27, column: 3, file: SmartNJ.Network]
var ENJNetworkError = {
   $ClassName:"ENJNetworkError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3Structure = class (TObject)
///  [line: 93, column: 3, file: System.Structure]
var TW3Structure = {
   $ClassName:"TW3Structure",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Structure.ReadBool(const Name: String) : Boolean
   ///  [line: 207, column: 23, file: System.Structure]
   ,ReadBool$1:function(Self, Name$8) {
      return (TW3Structure.Read$3$(Self,Name$8)?true:false);
   }
   /// function TW3Structure.ReadBytes(const Name: String) : TByteArray
   ///  [line: 158, column: 23, file: System.Structure]
   ,ReadBytes$1:function(Self, Name$9) {
      return TW3Structure.Read$3$(Self,Name$9);
   }
   /// function TW3Structure.ReadDateTime(const Name: String) : TDateTime
   ///  [line: 217, column: 23, file: System.Structure]
   ,ReadDateTime$2:function(Self, Name$10) {
      return TW3Structure.Read$3$(Self,Name$10);
   }
   /// function TW3Structure.ReadFloat(const Name: String) : Float
   ///  [line: 212, column: 23, file: System.Structure]
   ,ReadFloat$1:function(Self, Name$11) {
      return Number(TW3Structure.Read$3$(Self,Name$11));
   }
   /// function TW3Structure.ReadInt(const Name: String) : Integer
   ///  [line: 202, column: 23, file: System.Structure]
   ,ReadInt$1:function(Self, Name$12) {
      return parseInt(TW3Structure.Read$3$(Self,Name$12),10);
   }
   /// function TW3Structure.ReadString(const Name: String; const Decode: Boolean) : String
   ///  [line: 194, column: 23, file: System.Structure]
   ,ReadString$3:function(Self, Name$13, Decode$6) {
      var Result = "";
      if (Decode$6) {
         Result = TString.DecodeBase64(TString,String(TW3Structure.Read$3$(Self,Name$13)));
      } else {
         Result = String(TW3Structure.Read$3$(Self,Name$13));
      }
      return Result
   }
   /// procedure TW3Structure.WriteBool(const Name: String; value: Boolean)
   ///  [line: 179, column: 24, file: System.Structure]
   ,WriteBool$1:function(Self, Name$14, value$2) {
      TW3Structure.Write$3$(Self,Name$14,value$2);
   }
   /// procedure TW3Structure.WriteBytes(const Name: String; const Value: TByteArray)
   ///  [line: 130, column: 24, file: System.Structure]
   ,WriteBytes$1:function(Self, Name$15, Value$53) {
      try {
         TW3Structure.Write$3$(Self,Name$15,TDatatype.BytesToBase64$1(TDatatype,Value$53));
      } catch ($e) {
         var e$13 = $W($e);
         throw EW3Exception.CreateFmt($New(EW3Structure),"Failed to write bytes to structure, system threw exception [%s] with message [%s]",[TObject.ClassName(e$13.ClassType), e$13.FMessage]);
      }
   }
   /// procedure TW3Structure.WriteDateTime(const Name: String; value: TDateTime)
   ///  [line: 189, column: 24, file: System.Structure]
   ,WriteDateTime$1:function(Self, Name$16, value$3) {
      TW3Structure.Write$3$(Self,Name$16,value$3);
   }
   /// procedure TW3Structure.WriteFloat(const Name: String; value: Float)
   ///  [line: 184, column: 24, file: System.Structure]
   ,WriteFloat:function(Self, Name$17, value$4) {
      TW3Structure.Write$3$(Self,Name$17,value$4);
   }
   /// procedure TW3Structure.WriteInt(const Name: String; value: Integer)
   ///  [line: 174, column: 24, file: System.Structure]
   ,WriteInt:function(Self, Name$18, value$5) {
      TW3Structure.Write$3$(Self,Name$18,value$5);
   }
   /// procedure TW3Structure.WriteStream(const Name: String; const Value: TStream)
   ///  [line: 142, column: 24, file: System.Structure]
   ,WriteStream:function(Self, Name$19, Value$54) {
      var LBytes$6 = [];
      if (Value$54!==null) {
         if (TStream.GetSize$(Value$54)>0) {
            TStream.SetPosition$(Value$54,0);
            LBytes$6 = TStream.Read$1(Value$54,TStream.GetSize$(Value$54));
         }
         TW3Structure.WriteBytes$1(Self,Name$19,LBytes$6);
      } else {
         throw Exception.Create($New(EW3Structure),"Failed to write stream to structure, stream was nil error");
      }
   }
   /// procedure TW3Structure.WriteString(Name: String; Value: String; const Encode: Boolean)
   ///  [line: 163, column: 24, file: System.Structure]
   ,WriteString$2:function(Self, Name$20, Value$55, Encode$6) {
      if (Encode$6) {
         Value$55 = TString.EncodeBase64(TString,Value$55);
      }
      TW3Structure.Write$3$(Self,Name$20,Value$55);
   }
   ,Destroy:TObject.Destroy
   ,Clear$2$:function($){return $.ClassType.Clear$2($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromBuffer$1$:function($){return $.ClassType.LoadFromBuffer$1.apply($.ClassType, arguments)}
   ,LoadFromStream$2$:function($){return $.ClassType.LoadFromStream$2.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToBuffer$1$:function($){return $.ClassType.SaveToBuffer$1.apply($.ClassType, arguments)}
   ,SaveToStream$2$:function($){return $.ClassType.SaveToStream$2.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
};
TW3Structure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$3,TW3Structure.Write$3]
   ,IW3StructureReadAccess:[TW3Structure.Exists$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$3,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
}
/// EW3Structure = class (EW3Exception)
///  [line: 92, column: 3, file: System.Structure]
var EW3Structure = {
   $ClassName:"EW3Structure",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONStructure = class (TW3Structure)
///  [line: 27, column: 3, file: System.Structure.JSON]
var TJSONStructure = {
   $ClassName:"TJSONStructure",$Parent:TW3Structure
   ,$Init:function ($) {
      TW3Structure.$Init($);
      $.FObj = null;
   }
   /// procedure TJSONStructure.Clear()
   ///  [line: 146, column: 26, file: System.Structure.JSON]
   ,Clear$2:function(Self) {
      TJSONObject.Clear$1(Self.FObj);
   }
   /// constructor TJSONStructure.Create()
   ///  [line: 64, column: 28, file: System.Structure.JSON]
   ,Create$63:function(Self) {
      TObject.Create(Self);
      Self.FObj = TJSONObject.Create$49($New(TJSONObject));
      return Self
   }
   /// destructor TJSONStructure.Destroy()
   ///  [line: 70, column: 27, file: System.Structure.JSON]
   ,Destroy:function(Self) {
      TObject.Free(Self.FObj);
      TObject.Destroy(Self);
   }
   /// function TJSONStructure.Exists(const Name: String) : Boolean
   ///  [line: 151, column: 25, file: System.Structure.JSON]
   ,Exists$1:function(Self, Name$21) {
      return TJSONObject.Exists(Self.FObj,Name$21);
   }
   /// procedure TJSONStructure.LoadFromBuffer(const Buffer: TBinaryData)
   ///  [line: 126, column: 26, file: System.Structure.JSON]
   ,LoadFromBuffer$1:function(Self, Buffer$9) {
      TJSONObject.LoadFromBuffer(Self.FObj,Buffer$9);
   }
   /// procedure TJSONStructure.LoadFromStream(const Stream: TStream)
   ///  [line: 116, column: 26, file: System.Structure.JSON]
   ,LoadFromStream$2:function(Self, Stream$6) {
      TJSONObject.LoadFromStream$1(Self.FObj,Stream$6);
   }
   /// function TJSONStructure.Read(const Name: String) : Variant
   ///  [line: 161, column: 25, file: System.Structure.JSON]
   ,Read$3:function(Self, Name$22) {
      var Result = {v:undefined};
      try {
         if (TJSONObject.Exists(Self.FObj,Name$22)) {
            TJSONObject.Read$2(Self.FObj,Name$22,Result);
         } else {
            Result.v = undefined;
         }
      } finally {return Result.v}
   }
   /// procedure TJSONStructure.SaveToBuffer(const Buffer: TBinaryData)
   ///  [line: 121, column: 26, file: System.Structure.JSON]
   ,SaveToBuffer$1:function(Self, Buffer$10) {
      TJSONObject.SaveToBuffer(Self.FObj,Buffer$10);
   }
   /// procedure TJSONStructure.SaveToStream(const Stream: TStream)
   ///  [line: 111, column: 26, file: System.Structure.JSON]
   ,SaveToStream$2:function(Self, Stream$7) {
      TJSONObject.SaveToStream$1(Self.FObj,Stream$7);
   }
   /// function TJSONStructure.ToJSon() : String
   ///  [line: 81, column: 25, file: System.Structure.JSON]
   ,ToJSon:function(Self) {
      return TJSONObject.ToJSON(Self.FObj);
   }
   /// procedure TJSONStructure.Write(const Name: String; const Value: Variant)
   ///  [line: 156, column: 26, file: System.Structure.JSON]
   ,Write$3:function(Self, Name$23, Value$56) {
      TJSONObject.AddOrSet(Self.FObj,Name$23,Value$56);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$2$:function($){return $.ClassType.Clear$2($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromBuffer$1$:function($){return $.ClassType.LoadFromBuffer$1.apply($.ClassType, arguments)}
   ,LoadFromStream$2$:function($){return $.ClassType.LoadFromStream$2.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToBuffer$1$:function($){return $.ClassType.SaveToBuffer$1.apply($.ClassType, arguments)}
   ,SaveToStream$2$:function($){return $.ClassType.SaveToStream$2.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
};
TJSONStructure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$3,TJSONStructure.Write$3]
   ,IW3StructureReadAccess:[TJSONStructure.Exists$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$3,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TJSONStructure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
}
function util() {
   return require("util");
};
/// JServerAddressResult = class (TObject)
///  [line: 64, column: 3, file: NodeJS.net]
var JServerAddressResult = {
   $ClassName:"JServerAddressResult",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJAddressBindings = class (TW3LockedObject)
///  [line: 78, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBindings = {
   $ClassName:"TNJAddressBindings",$Parent:TW3LockedObject
   ,$Init:function ($) {
      TW3LockedObject.$Init($);
      $.FItems = [];
   }
   /// procedure TNJAddressBindings.Clear()
   ///  [line: 172, column: 30, file: SmartNJ.Network.Bindings]
   ,Clear$4:function(Self) {
      var x$28 = 0;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw Exception.Create($New(EW3LockError),"Bindings cannot be altered while active error");
      } else {
         try {
            var $temp44;
            for(x$28=0,$temp44=Self.FItems.length;x$28<$temp44;x$28++) {
               TObject.Free(Self.FItems[x$28]);
               Self.FItems[x$28]=null;
            }
         } finally {
            Self.FItems.length=0;
         }
      }
   }
   /// destructor TNJAddressBindings.Destroy()
   ///  [line: 114, column: 31, file: SmartNJ.Network.Bindings]
   ,Destroy:function(Self) {
      if (Self.FItems.length>0) {
         TNJAddressBindings.Clear$4(Self);
      }
      TObject.Destroy(Self);
   }
   /// procedure TNJAddressBindings.ObjectLocked()
   ///  [line: 243, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectLocked$1:function(Self) {
      var x$29 = 0;
      var LAccess$6 = null;
      var $temp45;
      for(x$29=0,$temp45=Self.FItems.length;x$29<$temp45;x$29++) {
         LAccess$6 = $AsIntf(Self.FItems[x$29],"IW3LockObject");
         LAccess$6[0]();
      }
   }
   /// procedure TNJAddressBindings.ObjectUnLocked()
   ///  [line: 252, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectUnLocked$1:function(Self) {
      var x$30 = 0;
      var LAccess$7 = null;
      var $temp46;
      for(x$30=0,$temp46=Self.FItems.length;x$30<$temp46;x$30++) {
         LAccess$7 = $AsIntf(Self.FItems[x$30],"IW3LockObject");
         LAccess$7[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TNJAddressBindings.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TNJAddressBinding = class (TW3OwnedLockedObject)
///  [line: 42, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBinding = {
   $ClassName:"TNJAddressBinding",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
   }
   /// function TNJAddressBinding.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 270, column: 28, file: SmartNJ.Network.Bindings]
   ,AcceptOwner:function(Self, CandidateObject$5) {
      return (CandidateObject$5!==null)&&$Is(CandidateObject$5,TNJAddressBindings);
   }
   /// constructor TNJAddressBinding.Create(const AOwner: TNJAddressBindings)
   ///  [line: 265, column: 31, file: SmartNJ.Network.Bindings]
   ,Create$73:function(Self, AOwner$8) {
      TW3OwnedObject.Create$21(Self,AOwner$8);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$21:TW3OwnedObject.Create$21
};
TNJAddressBinding.$Intf={
   IW3OwnedObjectAccess:[TNJAddressBinding.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TNJCustomServer = class (TW3ErrorObject)
///  [line: 57, column: 3, file: SmartNJ.Server]
var TNJCustomServer = {
   $ClassName:"TNJCustomServer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnBeforeServerStopped = null;
      $.OnBeforeServerStarted = null;
      $.OnAfterServerStopped = null;
      $.OnAfterServerStarted = null;
      $.FActive$4 = false;
      $.FHandle$5 = undefined;
      $.FPort$3 = 0;
   }
   /// procedure TNJCustomServer.AfterStart()
   ///  [line: 164, column: 27, file: SmartNJ.Server]
   ,AfterStart$2:function(Self) {
      if (Self.OnAfterServerStarted) {
         Self.OnAfterServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.AfterStop()
   ///  [line: 170, column: 27, file: SmartNJ.Server]
   ,AfterStop$2:function(Self) {
      if (Self.OnAfterServerStopped) {
         Self.OnAfterServerStopped(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStart()
   ///  [line: 158, column: 27, file: SmartNJ.Server]
   ,BeforeStart$2:function(Self) {
      if (Self.OnBeforeServerStarted) {
         Self.OnBeforeServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStop()
   ///  [line: 152, column: 27, file: SmartNJ.Server]
   ,BeforeStop$2:function(Self) {
      if (Self.OnBeforeServerStopped) {
         Self.OnBeforeServerStopped(Self);
      }
   }
   /// function TNJCustomServer.GetActive() : Boolean
   ///  [line: 192, column: 26, file: SmartNJ.Server]
   ,GetActive$3:function(Self) {
      return Self.FActive$4;
   }
   /// function TNJCustomServer.GetHandle() : THandle
   ///  [line: 202, column: 26, file: SmartNJ.Server]
   ,GetHandle$1:function(Self) {
      return Self.FHandle$5;
   }
   /// function TNJCustomServer.GetPort() : Integer
   ///  [line: 176, column: 26, file: SmartNJ.Server]
   ,GetPort$2:function(Self) {
      return Self.FPort$3;
   }
   /// procedure TNJCustomServer.SetActive(const Value: Boolean)
   ///  [line: 197, column: 27, file: SmartNJ.Server]
   ,SetActive$4:function(Self, Value$57) {
      Self.FActive$4 = Value$57;
   }
   /// procedure TNJCustomServer.SetHandle(const Value: THandle)
   ///  [line: 207, column: 27, file: SmartNJ.Server]
   ,SetHandle:function(Self, Value$58) {
      Self.FHandle$5 = Value$58;
   }
   /// procedure TNJCustomServer.SetPort(const Value: Integer)
   ///  [line: 181, column: 27, file: SmartNJ.Server]
   ,SetPort$3:function(Self, Value$59) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Port cannot be altered while server is active error");
      } else {
         Self.FPort$3 = Value$59;
      }
   }
   /// procedure TNJCustomServer.StartServer()
   ///  [line: 142, column: 27, file: SmartNJ.Server]
   ,StartServer:function(Self) {
      TNJCustomServer.SetActive$4$(Self,true);
   }
   /// procedure TNJCustomServer.StopServer()
   ///  [line: 147, column: 27, file: SmartNJ.Server]
   ,StopServer:function(Self) {
      TNJCustomServer.SetActive$4$(Self,false);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,GetPort$2$:function($){return $.ClassType.GetPort$2($)}
   ,SetActive$4$:function($){return $.ClassType.SetActive$4.apply($.ClassType, arguments)}
   ,SetPort$3$:function($){return $.ClassType.SetPort$3.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJCustomServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJServerChildClass = class (TW3HandleBasedObject)
///  [line: 43, column: 3, file: SmartNJ.Server]
var TNJServerChildClass = {
   $ClassName:"TNJServerChildClass",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.FParent = null;
   }
   /// constructor TNJServerChildClass.Create(Server: TNJCustomServer)
   ///  [line: 108, column: 33, file: SmartNJ.Server]
   ,Create$78:function(Self, Server$4) {
      TObject.Create(Self);
      Self.FParent = Server$4;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerResponse = class (TNJServerChildClass)
///  [line: 54, column: 3, file: SmartNJ.Server]
var TNJCustomServerResponse = {
   $ClassName:"TNJCustomServerResponse",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerRequest = class (TNJServerChildClass)
///  [line: 51, column: 3, file: SmartNJ.Server]
var TNJCustomServerRequest = {
   $ClassName:"TNJCustomServerRequest",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// ENJServerError = class (EW3Exception)
///  [line: 36, column: 3, file: SmartNJ.Server]
var ENJServerError = {
   $ClassName:"ENJServerError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function http() {
   return require("http");
};
/// TNJHTTPServer = class (TNJCustomServer)
///  [line: 104, column: 3, file: SmartNJ.Server.Http]
var TNJHTTPServer = {
   $ClassName:"TNJHTTPServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
   }
   /// procedure TNJHTTPServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ///  [line: 147, column: 25, file: SmartNJ.Server.Http]
   ,Dispatch$2:function(Self, request$1, response) {
      var LRequest = null,
         LResponse = null;
      if (Self.OnRequest) {
         LRequest = TNJHttpRequest.Create$80($New(TNJHttpRequest),Self,request$1);
         LResponse = TNJHttpResponse.Create$79($New(TNJHttpResponse),Self,response);
         try {
            Self.OnRequest(Self,LRequest,LResponse);
         } catch ($e) {
            var e$14 = $W($e);
            throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$14.ClassType), e$14.FMessage]);
         }
         LResponse = null;
         LRequest = null;
      }
   }
   /// procedure TNJHTTPServer.InternalSetActive(const Value: Boolean)
   ///  [line: 212, column: 25, file: SmartNJ.Server.Http]
   ,InternalSetActive$1:function(Self, Value$60) {
      TNJCustomServer.SetActive$4(Self,Value$60);
   }
   /// procedure TNJHTTPServer.SetActive(const Value: Boolean)
   ///  [line: 127, column: 25, file: SmartNJ.Server.Http]
   ,SetActive$4:function(Self, Value$61) {
      if (Value$61!=TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.SetActive$4(Self,Value$61);
         try {
            if (TNJCustomServer.GetActive$3(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$15 = $W($e);
            TNJCustomServer.SetActive$4(Self,(!Value$61))         }
      }
   }
   /// procedure TNJHTTPServer.StartServer()
   ///  [line: 178, column: 25, file: SmartNJ.Server.Http]
   ,StartServer:function(Self) {
      var LServer$2 = null;
      try {
         LServer$2 = http().createServer($Event2(Self,TNJHTTPServer.Dispatch$2));
      } catch ($e) {
         var e$16 = $W($e);
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$16.ClassType), e$16.FMessage]);
      }
      try {
         LServer$2.listen(TNJCustomServer.GetPort$2$(Self),"");
      } catch ($e) {
         var e$17 = $W($e);
         LServer$2 = null;
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$17.ClassType), e$17.FMessage]);
      }
      TNJCustomServer.SetHandle(Self,LServer$2);
      TNJCustomServer.AfterStart$2(Self);
   }
   /// procedure TNJHTTPServer.StopServer()
   ///  [line: 217, column: 25, file: SmartNJ.Server.Http]
   ,StopServer:function(Self) {
      var cb = null;
      cb = function () {
         TNJHTTPServer.InternalSetActive$1(Self,false);
         TNJCustomServer.AfterStop$2(Self);
      };
      TNJCustomServer.GetHandle$1(Self).close(cb);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,GetPort$2:TNJCustomServer.GetPort$2
   ,SetActive$4$:function($){return $.ClassType.SetActive$4.apply($.ClassType, arguments)}
   ,SetPort$3:TNJCustomServer.SetPort$3
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJHTTPServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJHttpResponse = class (TNJCustomServerResponse)
///  [line: 69, column: 3, file: SmartNJ.Server.Http]
var TNJHttpResponse = {
   $ClassName:"TNJHttpResponse",$Parent:TNJCustomServerResponse
   ,$Init:function ($) {
      TNJCustomServerResponse.$Init($);
   }
   /// constructor TNJHttpResponse.Create(const Server: TNJCustomServer; const ResponseObject: JServerResponse)
   ///  [line: 232, column: 29, file: SmartNJ.Server.Http]
   ,Create$79:function(Self, Server$5, ResponseObject) {
      TNJServerChildClass.Create$78(Self,Server$5);
      TW3HandleBasedObject.SetObjectHandle(Self,ResponseObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJHttpRequest = class (TNJCustomServerRequest)
///  [line: 41, column: 3, file: SmartNJ.Server.Http]
var TNJHttpRequest = {
   $ClassName:"TNJHttpRequest",$Parent:TNJCustomServerRequest
   ,$Init:function ($) {
      TNJCustomServerRequest.$Init($);
   }
   /// constructor TNJHttpRequest.Create(const Server: TNJCustomServer; const RequestObject: JServerRequest)
   ///  [line: 324, column: 28, file: SmartNJ.Server.Http]
   ,Create$80:function(Self, Server$6, RequestObject) {
      TNJServerChildClass.Create$78(Self,Server$6);
      TW3HandleBasedObject.SetObjectHandle(Self,RequestObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// ENJHttpServerError = class (ENJServerError)
///  [line: 38, column: 3, file: SmartNJ.Server.Http]
var ENJHttpServerError = {
   $ClassName:"ENJHttpServerError",$Parent:ENJServerError
   ,$Init:function ($) {
      ENJServerError.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TNJWebSocketSocket = class (TW3ErrorObject)
///  [line: 58, column: 3, file: SmartNJ.Server.Websocket]
var TNJWebSocketSocket = {
   $ClassName:"TNJWebSocketSocket",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.RemoteAddress = "";
      $.FReq = $.FServer = $.FSocket = null;
   }
   /// constructor TNJWebSocketSocket.Create(const Server: TNJRawWebSocketServer; const WsSocket: JWsSocket)
   ///  [line: 724, column: 32, file: SmartNJ.Server.Websocket]
   ,Create$74:function(Self, Server$7, WsSocket) {
      TW3ErrorObject.Create$48(Self);
      if (Server$7!==null) {
         Self.FServer = Server$7;
         if (WsSocket!==null) {
            Self.FSocket = WsSocket;
            Self.FOptions$4.AutoWriteToConsole = true;
            Self.FOptions$4.ThrowExceptions = true;
            Self.FSocket["name"] = TW3Identifiers.GenerateUniqueObjectId(TW3Identifiers);
         } else {
            throw Exception.Create($New(Exception),"Failed to create socket, socket was nil error");
         }
      } else {
         throw Exception.Create($New(Exception),"Failed to create socket, server was nil error");
      }
      return Self
   }
   /// destructor TNJWebSocketSocket.Destroy()
   ///  [line: 746, column: 31, file: SmartNJ.Server.Websocket]
   ,Destroy:function(Self) {
      if (Self.FReq!==null) {
         TObject.Free(Self.FReq);
      }
      Self.FServer = null;
      Self.FSocket = null;
      TW3ErrorObject.Destroy(Self);
   }
   /// function TNJWebSocketSocket.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 756, column: 30, file: SmartNJ.Server.Websocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketSocket;
   }
   /// function TNJWebSocketSocket.GetSocketName() : String
   ///  [line: 844, column: 29, file: SmartNJ.Server.Websocket]
   ,GetSocketName:function(Self) {
      return String(Self.FSocket["name"]);
   }
   /// procedure TNJWebSocketSocket.SetSocketName(NewName: String)
   ///  [line: 800, column: 30, file: SmartNJ.Server.Websocket]
   ,SetSocketName:function(Self, NewName) {
      var OldName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName = Trim$_String_(NewName);
      OldName = TNJWebSocketSocket.GetSocketName(Self);
      if (NewName!=OldName) {
         if (NewName.length>0) {
            Self.FSocket["name"] = Trim$_String_(NewName);
            TNJRawWebSocketServer.DoClientNameChange(Self.FServer,Self,OldName);
         } else {
            TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty error");
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$48:TW3ErrorObject.Create$48
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TNJWebSocketSocket.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJRawWebSocketServer = class (TNJCustomServer)
///  [line: 123, column: 3, file: SmartNJ.Server.Websocket]
var TNJRawWebSocketServer = {
   $ClassName:"TNJRawWebSocketServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnBinMessage = null;
      $.OnTextMessage = null;
      $.OnClientDisconnected = null;
      $.OnClientConnected = null;
      $.OnError = null;
      $.FPath = "";
      $.FPayload = 0;
      $.FSocketLUT = null;
      $.FSockets = [];
      $.FTrack = false;
   }
   /// constructor TNJRawWebSocketServer.Create()
   ///  [line: 347, column: 35, file: SmartNJ.Server.Websocket]
   ,Create$48:function(Self) {
      TW3ErrorObject.Create$48(Self);
      Self.FSocketLUT = TW3CustomDictionary.Create$77($New(TW3ObjDictionary));
      Self.FOptions$4.AutoWriteToConsole = true;
      Self.FOptions$4.ThrowExceptions = true;
      Self.FPayload = 41943040;
      return Self
   }
   /// destructor TNJRawWebSocketServer.Destroy()
   ///  [line: 359, column: 34, file: SmartNJ.Server.Websocket]
   ,Destroy:function(Self) {
      TObject.Free(Self.FSocketLUT);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJRawWebSocketServer.Dispatch(Socket: TNJWebSocketSocket; Data: TNJWebsocketMessage)
   ///  [line: 446, column: 33, file: SmartNJ.Server.Websocket]
   ,Dispatch$1:function(Self, Socket$4, Data$44) {
      switch (Data$44.wiType) {
         case 0 :
            if (Self.OnTextMessage) {
               Self.OnTextMessage(Self,Socket$4,Clone$TNJWebsocketMessage(Data$44));
            }
            break;
         case 1 :
            if (Self.OnBinMessage) {
               Self.OnBinMessage(Self,Socket$4,Clone$TNJWebsocketMessage(Data$44));
            }
            break;
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientConnected(Socket: THandle; Request: THandle)
   ///  [line: 533, column: 33, file: SmartNJ.Server.Websocket]
   ,DoClientConnected:function(Self, Socket$5, Request$1) {
      var NJSocket = null;
      NJSocket = TNJWebSocketSocket.Create$74($New(TNJWebSocketSocket),Self,Socket$5);
      TW3ObjDictionary.a$146(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(NJSocket),NJSocket);
      Self.FSockets.push(NJSocket);
      if (Request$1) {
         if (Request$1.connection) {
            NJSocket.RemoteAddress = String(Request$1.connection.remoteAddress);
         }
      }
      if (Self.OnClientConnected) {
         Self.OnClientConnected(Self,NJSocket);
      }
      Socket$5.on("error",function (error$3) {
         var NJSocket$1 = null,
            SocketName;
         if (Self.OnError) {
            NJSocket$1 = null;
            SocketName = Socket$5["name"];
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName)))) {
               NJSocket$1 = $As(TW3ObjDictionary.a$147(Self.FSocketLUT,(String(SocketName))),TNJWebSocketSocket);
            } else {
               NJSocket$1 = TNJWebSocketSocket.Create$74($New(TNJWebSocketSocket),Self,Socket$5);
            }
            Self.OnError(Self,NJSocket$1,error$3);
         }
      });
      Socket$5.on("message",function (message$2) {
         var LInfo$1 = {wiType:0,wiBuffer:null,wiText:""};
         var NJSocket$2 = null,
            SocketName$1;
         NJSocket$2 = null;
         if (TW3VariantHelper$IsUInt8Array(message$2)) {
            LInfo$1.wiType = 1;
            LInfo$1.wiBuffer = message$2;
         } else {
            LInfo$1.wiType = 0;
            LInfo$1.wiText = String(message$2);
         }
         SocketName$1 = Socket$5["name"];
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName$1)))) {
            NJSocket$2 = $As(TW3ObjDictionary.a$147(Self.FSocketLUT,(String(SocketName$1))),TNJWebSocketSocket);
         } else {
            NJSocket$2 = TNJWebSocketSocket.Create$74($New(TNJWebSocketSocket),Self,Socket$5);
         }
         TNJRawWebSocketServer.Dispatch$1(Self,NJSocket$2,Clone$TNJWebsocketMessage(LInfo$1));
      });
      Socket$5.on("close",function (code$2, reason) {
         TNJRawWebSocketServer.DoClientDisconnected(Self,Socket$5,code$2,reason);
      });
   }
   /// procedure TNJRawWebSocketServer.DoClientDisconnected(Socket: THandle; Code: Integer; Reason: String)
   ///  [line: 606, column: 33, file: SmartNJ.Server.Websocket]
   ,DoClientDisconnected:function(Self, Socket$6, Code, Reason) {
      var RawSocket$1 = null,
         SocketId = "",
         NJSocket$3 = null,
         index$7 = 0;
      RawSocket$1 = Socket$6;
      SocketId = TVariant.AsString(Socket$6["name"]);
      NJSocket$3 = null;
      if (SocketId.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,SocketId)) {
            WriteLn("Found in name lookup!");
            NJSocket$3 = $As(TW3ObjDictionary.a$147(Self.FSocketLUT,SocketId),TNJWebSocketSocket);
         }
      }
      try {
         if (Self.OnClientDisconnected) {
            Self.OnClientDisconnected(Self,NJSocket$3);
         }
      } finally {
         if (NJSocket$3!==null) {
            index$7 = Self.FSockets.indexOf(NJSocket$3);
            Self.FSockets.splice(index$7,1)
            ;
            TW3CustomDictionary.Delete$3(Self.FSocketLUT,SocketId);
            TObject.Free(NJSocket$3);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientNameChange(Client: TNJWebSocketSocket; OldName: String)
   ///  [line: 505, column: 33, file: SmartNJ.Server.Websocket]
   ,DoClientNameChange:function(Self, Client, OldName$1) {
      var NewName$1 = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName$1 = Trim$_String_(TNJWebSocketSocket.GetSocketName(Client));
      if (NewName$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,NewName$1)) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid socket rename, a socket with that name [%s] already exists",[NewName$1]);
            return;
         }
         OldName$1 = Trim$_String_(OldName$1);
         if (OldName$1.length>0) {
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,OldName$1)) {
               TW3CustomDictionary.Delete$3(Self.FSocketLUT,OldName$1);
            }
         }
         TW3ObjDictionary.a$146(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(Client),Client);
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty errror");
      }
   }
   /// function TNJRawWebSocketServer.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 365, column: 32, file: SmartNJ.Server.Websocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketServer;
   }
   /// procedure TNJRawWebSocketServer.InternalSetActive(const Value: Boolean)
   ///  [line: 648, column: 33, file: SmartNJ.Server.Websocket]
   ,InternalSetActive:function(Self, Value$62) {
      TNJCustomServer.SetActive$4(Self,Value$62);
   }
   /// procedure TNJRawWebSocketServer.SetActive(const Value: Boolean)
   ///  [line: 420, column: 33, file: SmartNJ.Server.Websocket]
   ,SetActive$4:function(Self, Value$63) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Value$63!=TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.SetActive$4(Self,Value$63);
         try {
            if (TNJCustomServer.GetActive$3(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$18 = $W($e);
            TNJCustomServer.SetActive$4(Self,(!Value$63));
            TW3ErrorObject.SetLastErrorF$1(Self,"Failed to start server, system threw exception %s with message [%s]",[TObject.ClassName(e$18.ClassType), e$18.FMessage]);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.StartServer()
   ///  [line: 658, column: 33, file: SmartNJ.Server.Websocket]
   ,StartServer:function(Self) {
      var LOptions,
         LServer$3;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TW3CustomDictionary.Clear$5(Self.FSocketLUT);
      LOptions = TVariant.CreateObject();
      LOptions["clientTracking "] = Self.FTrack;
      if (Self.FPath.length>0) {
         LOptions["path"] = Self.FPath;
      }
      LOptions["port"] = TNJCustomServer.GetPort$2$(Self);
      LOptions["maxPayload"] = Self.FPayload;
      LServer$3 = null;
      try {
         LServer$3 = WebSocketCreateServer(LOptions);
      } catch ($e) {
         var e$19 = $W($e);
         TW3ErrorObject.SetLastErrorF$1(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$19.ClassType), e$19.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle(Self,LServer$3);
      LServer$3.on("error",function (error$4) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$4);
         }
      });
      LServer$3.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected));
      TNJCustomServer.AfterStart$2(Self);
   }
   /// procedure TNJRawWebSocketServer.StopServer()
   ///  [line: 706, column: 33, file: SmartNJ.Server.Websocket]
   ,StopServer:function(Self) {
      var LHandle$1 = undefined;
      LHandle$1 = TNJCustomServer.GetHandle$1(Self);
      if (LHandle$1) {
         LHandle$1.close(function () {
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop$2(Self);
            TW3CustomDictionary.Clear$5(Self.FSocketLUT);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$48$:function($){return $.ClassType.Create$48($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,GetPort$2:TNJCustomServer.GetPort$2
   ,SetActive$4$:function($){return $.ClassType.SetActive$4.apply($.ClassType, arguments)}
   ,SetPort$3:TNJCustomServer.SetPort$3
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJRawWebSocketServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebsocketMessageType enumeration
///  [line: 43, column: 3, file: SmartNJ.Server.Websocket]
var TNJWebsocketMessageType = [ "mtText", "mtBinary" ];
/// TNJWebsocketMessage = record
///  [line: 52, column: 3, file: SmartNJ.Server.Websocket]
function Copy$TNJWebsocketMessage(s,d) {
   d.wiType=s.wiType;
   d.wiBuffer=s.wiBuffer;
   d.wiText=s.wiText;
   return d;
}
function Clone$TNJWebsocketMessage($) {
   return {
      wiType:$.wiType,
      wiBuffer:$.wiBuffer,
      wiText:$.wiText
   }
}
/// JWsReadyState enumeration
///  [line: 45, column: 3, file: SmartNJ.Server.Websocket]
var JWsReadyState = [ "rsConnecting", "rsOpen", "rsClosing", "rsClosed" ];
/// ENJWebSocketSocket = class (EW3Exception)
///  [line: 35, column: 3, file: SmartNJ.Server.Websocket]
var ENJWebSocketSocket = {
   $ClassName:"ENJWebSocketSocket",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ENJWebSocketServer = class (EW3Exception)
///  [line: 36, column: 3, file: SmartNJ.Server.Websocket]
var ENJWebSocketServer = {
   $ClassName:"ENJWebSocketServer",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomDictionary = class (TObject)
///  [line: 23, column: 3, file: System.Dictionaries]
var TW3CustomDictionary = {
   $ClassName:"TW3CustomDictionary",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT = undefined;
   }
   /// procedure TW3CustomDictionary.Clear()
   ///  [line: 105, column: 31, file: System.Dictionaries]
   ,Clear$5:function(Self) {
      Self.FLUT = TVariant.CreateObject();
   }
   /// function TW3CustomDictionary.Contains(const ItemKey: String) : Boolean
   ///  [line: 165, column: 30, file: System.Dictionaries]
   ,Contains:function(Self, ItemKey) {
      var Result = false;
      if (ItemKey.length>0) {
         Result = (Self.FLUT.hasOwnProperty(ItemKey)?true:false);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Contains", TObject.ClassName(Self.ClassType), $R[23]]);
      }
      return Result
   }
   /// constructor TW3CustomDictionary.Create()
   ///  [line: 92, column: 33, file: System.Dictionaries]
   ,Create$77:function(Self) {
      TObject.Create(Self);
      Self.FLUT = TVariant.CreateObject();
      return Self
   }
   /// procedure TW3CustomDictionary.Delete(const ItemKey: String)
   ///  [line: 230, column: 31, file: System.Dictionaries]
   ,Delete$3:function(Self, ItemKey$1) {
      if (ItemKey$1.length>0) {
         try {
            delete (Self.FLUT[ItemKey$1]);
         } catch ($e) {
            var e$20 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), e$20.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), $R[23]]);
      }
   }
   /// destructor TW3CustomDictionary.Destroy()
   ///  [line: 98, column: 32, file: System.Dictionaries]
   ,Destroy:function(Self) {
      TW3CustomDictionary.Clear$5(Self);
      Self.FLUT = undefined;
      TObject.Destroy(Self);
   }
   /// function TW3CustomDictionary.GetItem(const ItemKey: String) : Variant
   ///  [line: 198, column: 30, file: System.Dictionaries]
   ,GetItem$1:function(Self, ItemKey$2) {
      var Result = undefined;
      if (ItemKey$2.length>0) {
         try {
            Result = Self.FLUT[ItemKey$2];
         } catch ($e) {
            var e$21 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), e$21.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), $R[23]]);
      }
      return Result
   }
   /// procedure TW3CustomDictionary.SetItem(const ItemKey: String; const KeyValue: Variant)
   ///  [line: 214, column: 31, file: System.Dictionaries]
   ,SetItem:function(Self, ItemKey$3, KeyValue) {
      if (ItemKey$3.length>0) {
         try {
            Self.FLUT[ItemKey$3] = KeyValue;
         } catch ($e) {
            var e$22 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), e$22.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), $R[23]]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TW3ObjDictionary = class (TW3CustomDictionary)
///  [line: 46, column: 3, file: System.Dictionaries]
var TW3ObjDictionary = {
   $ClassName:"TW3ObjDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 49, column: 13, file: System.Dictionaries]
   ,a$147:function(Self, ItemKey$4) {
      return TVariant.AsObject(TW3CustomDictionary.GetItem$1(Self,ItemKey$4));
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 50, column: 13, file: System.Dictionaries]
   ,a$146:function(Self, ItemKey$5, Value$64) {
      TW3CustomDictionary.SetItem(Self,ItemKey$5,Value$64);
   }
   ,Destroy:TW3CustomDictionary.Destroy
};
function WebSocketCreateServer(Options$6) {
   var Result = undefined;
   var Access = null;
   Access = WebSocketAPI();
   Result = new (Access).Server(Options$6);
   return Result
};
function WebSocketAPI() {
   return require("ws");
};
/// JWsVerifyClientInfo = class (TObject)
///  [line: 44, column: 3, file: Nodejs.websocket]
var JWsVerifyClientInfo = {
   $ClassName:"JWsVerifyClientInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.origin = "";
      $.req = null;
      $.secure = false;
   }
   ,Destroy:TObject.Destroy
};
/// TQTXCodec = class (TDataTypeConverter)
///  [line: 122, column: 3, file: qtx.codec]
var TQTXCodec = {
   $ClassName:"TQTXCodec",$Parent:TDataTypeConverter$1
   ,$Init:function ($) {
      TDataTypeConverter$1.$Init($);
      $.FBindings$3 = [];
      $.FCodecInfo$1 = null;
   }
   /// constructor TQTXCodec.Create()
   ///  [line: 275, column: 23, file: qtx.codec]
   ,Create$83:function(Self) {
      TDataTypeConverter$1.Create$83(Self);
      Self.FCodecInfo$1 = TQTXCodec.MakeCodecInfo$2$(Self);
      if (Self.FCodecInfo$1===null) {
         throw Exception.Create($New(ECodecError$1),"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   /// destructor TQTXCodec.Destroy()
   ///  [line: 283, column: 22, file: qtx.codec]
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo$1);
      TDataTypeConverter$1.Destroy(Self);
   }
   /// function TQTXCodec.MakeCodecInfo() : TQTXCodecInfo
   ///  [line: 289, column: 20, file: qtx.codec]
   ,MakeCodecInfo$2:function(Self) {
      return null;
   }
   /// procedure TQTXCodec.RegisterBinding(const Binding: TQTXCodecBinding)
   ///  [line: 294, column: 21, file: qtx.codec]
   ,RegisterBinding$1:function(Self, Binding) {
      if (Self.FBindings$3.indexOf(Binding)<0) {
         Self.FBindings$3.push(Binding);
      } else {
         throw Exception.Create($New(ECodecError$1),"Binding already connected to codec error");
      }
   }
   /// procedure TQTXCodec.UnRegisterBinding(const Binding: TQTXCodecBinding)
   ///  [line: 302, column: 21, file: qtx.codec]
   ,UnRegisterBinding$1:function(Self, Binding$1) {
      var LIndex = 0;
      LIndex = Self.FBindings$3.indexOf(Binding$1);
      if (LIndex>=0) {
         Self.FBindings$3.splice(LIndex,1)
         ;
      } else {
         throw Exception.Create($New(ECodecError$1),"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$83$:function($){return $.ClassType.Create$83($)}
   ,DecodeData$2$:function($){return $.ClassType.DecodeData$2.apply($.ClassType, arguments)}
   ,EncodeData$2$:function($){return $.ClassType.EncodeData$2.apply($.ClassType, arguments)}
   ,MakeCodecInfo$2$:function($){return $.ClassType.MakeCodecInfo$2($)}
};
TQTXCodec.$Intf={
   IQTXCodecProcess:[TQTXCodec.EncodeData$2,TQTXCodec.DecodeData$2]
   ,IQTXCodecBinding:[TQTXCodec.RegisterBinding$1,TQTXCodec.UnRegisterBinding$1]
}
/// TQTXCodecUTF8 = class (TQTXCodec)
///  [line: 39, column: 3, file: qtx.codec.utf8]
var TQTXCodecUTF8 = {
   $ClassName:"TQTXCodecUTF8",$Parent:TQTXCodec
   ,$Init:function ($) {
      TQTXCodec.$Init($);
   }
   /// function TQTXCodecUTF8.CanUseClampedArray() : Boolean
   ///  [line: 142, column: 30, file: qtx.codec.utf8]
   ,CanUseClampedArray$1:function() {
      var Result = {v:false};
      try {
         var LTemp$18 = undefined;
         try {
            LTemp$18 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$23 = $W($e);
            return Result.v;
         }
         if (LTemp$18) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   /// function TQTXCodecUTF8.CanUseNativeConverter() : Boolean
   ///  [line: 155, column: 30, file: qtx.codec.utf8]
   ,CanUseNativeConverter:function() {
      var Result = {v:false};
      try {
         var LTemp$19 = null;
         try {
            LTemp$19 = new TextEncoder("utf8");
         } catch ($e) {
            var e$24 = $W($e);
            return false;
         }
         Result.v = LTemp$19!==null;
      } finally {return Result.v}
   }
   /// function TQTXCodecUTF8.Decode(const BytesToDecode: TUInt8Array) : String
   ///  [line: 235, column: 24, file: qtx.codec.utf8]
   ,Decode$3:function(Self, BytesToDecode$2) {
      var Result = "";
      var LDecoder = null,
         LTyped = undefined,
         i$3 = 0,
         bytelen = 0,
         c = 0,
         c2 = 0,
         c2$1 = 0,
         c3 = 0,
         c4 = 0,
         u = 0,
         c2$2 = 0,
         c3$1 = 0;
      if (BytesToDecode$2.length<1) {
         return "";
      }
      if (TQTXCodecUTF8.CanUseNativeConverter()) {
         LDecoder = new TextDecoder("utf8");
         LTyped = TDataTypeConverter$1.BytesToTypedArray$2(Self,BytesToDecode$2);
         Result = LDecoder.decode(LTyped);
         LTyped = null;
         LDecoder = null;
      } else {
         i$3 = 0;
         bytelen = BytesToDecode$2.length;
         while (i$3<bytelen) {
            c = BytesToDecode$2[i$3];
            ++i$3;
            if (c<128) {
               Result+=TString$1.FromCharCode$1(c);
            } else if (c>191&&c<224) {
               c2 = BytesToDecode$2[i$3];
               ++i$3;
               Result+=TString$1.FromCharCode$1((((c&31)<<6)|(c2&63)));
            } else if (c>239&&c<365) {
               c2$1 = BytesToDecode$2[i$3];
               ++i$3;
               c3 = BytesToDecode$2[i$3];
               ++i$3;
               c4 = BytesToDecode$2[i$3];
               ++i$3;
               u = (((((c&7)<<18)|((c2$1&63)<<12))|((c3&63)<<6))|(c4&63))-65536;
               Result+=TString$1.FromCharCode$1((55296+(u>>>10)));
               Result+=TString$1.FromCharCode$1((56320+(u&1023)));
            } else {
               c2$2 = BytesToDecode$2[i$3];
               ++i$3;
               c3$1 = BytesToDecode$2[i$3];
               ++i$3;
               Result+=TString$1.FromCharCode$1((((c&15)<<12)|(((c2$2&63)<<6)|(c3$1&63))));
            }
         }
      }
      return Result
   }
   /// procedure TQTXCodecUTF8.DecodeData(const Source: IManagedData; const Target: IManagedData)
   ///  [line: 304, column: 25, file: qtx.codec.utf8]
   ,DecodeData$2:function(Self, Source$4, Target$3) {
      /* null */
   }
   /// function TQTXCodecUTF8.Encode(TextToEncode: String) : TUInt8Array
   ///  [line: 167, column: 24, file: qtx.codec.utf8]
   ,Encode$3:function(Self, TextToEncode$3) {
      var Result = [];
      var LEncoder = null,
         LTyped$1 = null,
         LClip = null;
      var LTemp$20 = null;
      var n = 0;
      if (TextToEncode$3.length<1) {
         return [];
      }
      if (TQTXCodecUTF8.CanUseNativeConverter()) {
         LEncoder = new TextEncoder("utf8");
         LTyped$1 = LEncoder.encode(TextToEncode$3);
         Result = TDataTypeConverter$1.TypedArrayToBytes$2(LTyped$1);
         LEncoder = null;
         LTyped$1 = null;
      } else {
         if (TQTXCodecUTF8.CanUseClampedArray$1()) {
            LClip = new Uint8ClampedArray(1);
            LTemp$20 = new Uint8ClampedArray(1);
         } else {
            LClip = new Uint8Array(1);
            LTemp$20 = new Uint8Array(1);
         }
         var $temp47;
         for(n=1,$temp47=TextToEncode$3.length;n<=$temp47;n++) {
            LClip[0]=TString$1.CharCodeFor$1(TextToEncode$3.charAt(n-1));
            if (LClip[0]<128) {
               Result.push(LClip[0]);
            } else if (LClip[0]>127&&LClip[0]<2048) {
               LTemp$20[0]=((LClip[0]>>>6)|192);
               Result.push(LTemp$20[0]);
               LTemp$20[0]=((LClip[0]&63)|128);
               Result.push(LTemp$20[0]);
            } else {
               LTemp$20[0]=((LClip[0]>>>12)|224);
               Result.push(LTemp$20[0]);
               LTemp$20[0]=(((LClip[0]>>>6)&63)|128);
               Result.push(LTemp$20[0]);
               Result.push((LClip[0]&63)|128);
               Result.push(LTemp$20[0]);
            }
         }
      }
      return Result
   }
   /// procedure TQTXCodecUTF8.EncodeData(const Source: IManagedData; const Target: IManagedData)
   ///  [line: 300, column: 25, file: qtx.codec.utf8]
   ,EncodeData$2:function(Self, Source$5, Target$4) {
      /* null */
   }
   /// function TQTXCodecUTF8.MakeCodecInfo() : TQTXCodecInfo
   ///  [line: 126, column: 24, file: qtx.codec.utf8]
   ,MakeCodecInfo$2:function(Self) {
      var Result = null;
      var LVersion = {viMajor$1:0,viMinor$1:0,viRevision$1:0},
         LAccess$8 = null;
      Result = TObject.Create($New(TQTXCodecInfo));
      LVersion = Create$89(0,2,0);
      LAccess$8 = $AsIntf(Result,"ICodecInfo$1");
      LAccess$8[0]("UTF8Codec");
      LAccess$8[1]("text\/utf8");
      LAccess$8[2](LVersion);
      LAccess$8[3]([6]);
      LAccess$8[5](1);
      LAccess$8[6](0);
      return Result
   }
   ,Destroy:TQTXCodec.Destroy
   ,Create$83:TQTXCodec.Create$83
   ,DecodeData$2$:function($){return $.ClassType.DecodeData$2.apply($.ClassType, arguments)}
   ,EncodeData$2$:function($){return $.ClassType.EncodeData$2.apply($.ClassType, arguments)}
   ,MakeCodecInfo$2$:function($){return $.ClassType.MakeCodecInfo$2($)}
};
TQTXCodecUTF8.$Intf={
   IQTXCodecProcess:[TQTXCodecUTF8.EncodeData$2,TQTXCodecUTF8.DecodeData$2]
   ,IQTXCodecBinding:[TQTXCodec.RegisterBinding$1,TQTXCodec.UnRegisterBinding$1]
}
/// TQTXCodecVersionInfo = record
///  [line: 24, column: 3, file: qtx.codec]
function Copy$TQTXCodecVersionInfo(s,d) {
   d.viMajor$1=s.viMajor$1;
   d.viMinor$1=s.viMinor$1;
   d.viRevision$1=s.viRevision$1;
   return d;
}
function Clone$TQTXCodecVersionInfo($) {
   return {
      viMajor$1:$.viMajor$1,
      viMinor$1:$.viMinor$1,
      viRevision$1:$.viRevision$1
   }
}
/// function TQTXCodecVersionInfo.Create(const Major: Integer; const Minor: Integer; const Revision: Integer) : TQTXCodecVersionInfo
///  [line: 419, column: 37, file: qtx.codec]
function Create$89(Major, Minor, Revision) {
   var Result = {viMajor$1:0,viMinor$1:0,viRevision$1:0};
   Result.viMajor$1 = Major;
   Result.viMinor$1 = Minor;
   Result.viRevision$1 = Revision;
   return Result
}
/// TQTXCodecManager = class (TObject)
///  [line: 145, column: 3, file: qtx.codec]
var TQTXCodecManager = {
   $ClassName:"TQTXCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs$1 = [];
   }
   /// function TQTXCodecManager.CodecByClass(const ClsType: TCodecClass) : TQTXCodec
   ///  [line: 234, column: 27, file: qtx.codec]
   ,CodecByClass$1:function(Self, ClsType) {
      var Result = null;
      var a$291 = 0;
      var LItem$7 = null;
      var a$292 = [];
      Result = null;
      a$292 = Self.FCodecs$1;
      var $temp48;
      for(a$291=0,$temp48=a$292.length;a$291<$temp48;a$291++) {
         LItem$7 = a$292[a$291];
         if (TObject.ClassType(LItem$7.ClassType)==ClsType) {
            Result = LItem$7;
            break;
         }
      }
      return Result
   }
   /// destructor TQTXCodecManager.Destroy()
   ///  [line: 177, column: 29, file: qtx.codec]
   ,Destroy:function(Self) {
      if (Self.FCodecs$1.length>0) {
         TQTXCodecManager.Reset$5(Self);
      }
      TObject.Destroy(Self);
   }
   /// procedure TQTXCodecManager.RegisterCodec(const CodecClass: TCodecClass)
   ///  [line: 247, column: 28, file: qtx.codec]
   ,RegisterCodec$1:function(Self, CodecClass) {
      var LItem$8 = null;
      LItem$8 = TQTXCodecManager.CodecByClass$1(Self,CodecClass);
      if (LItem$8===null) {
         LItem$8 = TDataTypeConverter$1.Create$83$($NewDyn(CodecClass,""));
         Self.FCodecs$1.push(LItem$8);
      } else {
         throw Exception.Create($New(ECodecManager$1),"Codec already registered error");
      }
   }
   /// procedure TQTXCodecManager.Reset()
   ///  [line: 184, column: 28, file: qtx.codec]
   ,Reset$5:function(Self) {
      var a$293 = 0;
      var codec = null;
      try {
         var a$294 = [];
         a$294 = Self.FCodecs$1;
         var $temp49;
         for(a$293=0,$temp49=a$294.length;a$293<$temp49;a$293++) {
            codec = a$294[a$293];
            try {
               TObject.Free(codec);
            } catch ($e) {
               /* null */
            }
         }
      } finally {
         Self.FCodecs$1.length=0;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TQTXCodecInfo = class (TObject)
///  [line: 59, column: 3, file: qtx.codec]
var TQTXCodecInfo = {
   $ClassName:"TQTXCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout$1 = $.ciMime$1 = $.ciName$1 = "";
      $.ciDataFlow$1 = [0];
      $.ciInput$1 = 0;
      $.ciOutput$1 = 0;
      $.ciVersion$1 = {viMajor$1:0,viMinor$1:0,viRevision$1:0};
   }
   /// procedure TQTXCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow)
   ///  [line: 478, column: 25, file: qtx.codec]
   ,SetDataFlow$1:function(Self, coFlow) {
      Self.ciDataFlow$1 = coFlow.slice(0);
   }
   /// procedure TQTXCodecInfo.SetDescription(const coInfo: String)
   ///  [line: 473, column: 25, file: qtx.codec]
   ,SetDescription$1:function(Self, coInfo) {
      Self.ciAbout$1 = coInfo;
   }
   /// procedure TQTXCodecInfo.SetInput(const InputType: TCodecDataFormat)
   ///  [line: 463, column: 25, file: qtx.codec]
   ,SetInput$1:function(Self, InputType) {
      Self.ciInput$1 = InputType;
   }
   /// procedure TQTXCodecInfo.SetMime(const coMime: String)
   ///  [line: 451, column: 25, file: qtx.codec]
   ,SetMime$1:function(Self, coMime) {
      Self.ciMime$1 = coMime;
   }
   /// procedure TQTXCodecInfo.SetName(const coName: String)
   ///  [line: 446, column: 25, file: qtx.codec]
   ,SetName$2:function(Self, coName) {
      Self.ciName$1 = coName;
   }
   /// procedure TQTXCodecInfo.SetOutput(const OutputType: TCodecDataFormat)
   ///  [line: 468, column: 25, file: qtx.codec]
   ,SetOutput$1:function(Self, OutputType) {
      Self.ciOutput$1 = OutputType;
   }
   /// procedure TQTXCodecInfo.SetVersion(const coVersion: TQTXCodecVersionInfo)
   ///  [line: 456, column: 25, file: qtx.codec]
   ,SetVersion$1:function(Self, coVersion) {
      Self.ciVersion$1.viMajor$1 = coVersion.viMajor$1;
      Self.ciVersion$1.viMinor$1 = coVersion.viMinor$1;
      Self.ciVersion$1.viRevision$1 = coVersion.viRevision$1;
   }
   ,Destroy:TObject.Destroy
};
TQTXCodecInfo.$Intf={
   ICodecInfo$1:[TQTXCodecInfo.SetName$2,TQTXCodecInfo.SetMime$1,TQTXCodecInfo.SetVersion$1,TQTXCodecInfo.SetDataFlow$1,TQTXCodecInfo.SetDescription$1,TQTXCodecInfo.SetInput$1,TQTXCodecInfo.SetOutput$1]
}
/// TQTXCodecBinding = class (TObject)
///  [line: 90, column: 3, file: qtx.codec]
var TQTXCodecBinding = {
   $ClassName:"TQTXCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec$1 = null;
   }
   /// constructor TQTXCodecBinding.Create(const EndPoint: TQTXCodec)
   ///  [line: 316, column: 30, file: qtx.codec]
   ,Create$90:function(Self, EndPoint) {
      var LAccess$9 = null;
      TObject.Create(Self);
      if (EndPoint!==null) {
         Self.FCodec$1 = EndPoint;
         LAccess$9 = $AsIntf(Self.FCodec$1,"IQTXCodecBinding");
         LAccess$9[0](Self);
      } else {
         throw Exception.Create($New(ECodecBinding$1),"Binding failed, invalid endpoint error");
      }
      return Self
   }
   /// destructor TQTXCodecBinding.Destroy()
   ///  [line: 329, column: 29, file: qtx.codec]
   ,Destroy:function(Self) {
      var LAccess$10 = null;
      LAccess$10 = $AsIntf(Self.FCodec$1,"IQTXCodecBinding");
      LAccess$10[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TCodecDataFormat enumeration
///  [line: 40, column: 3, file: qtx.codec]
var TCodecDataFormat = [ "cdBinary", "cdText" ];
/// function TCodecDataFlowHelper.Ordinal(const Self: TCodecDataFlow) : Integer
///  [line: 403, column: 31, file: qtx.codec]
function TCodecDataFlowHelper$Ordinal(Self$13) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$13,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$13,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
/// TCodecDataDirection enumeration
///  [line: 34, column: 3, file: qtx.codec]
var TCodecDataDirection = { 1:"cdRead", 2:"cdWrite" };
/// ECodecError = class (EException)
///  [line: 14, column: 3, file: qtx.codec]
var ECodecError$1 = {
   $ClassName:"ECodecError",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecManager = class (ECodecError)
///  [line: 16, column: 3, file: qtx.codec]
var ECodecManager$1 = {
   $ClassName:"ECodecManager",$Parent:ECodecError$1
   ,$Init:function ($) {
      ECodecError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecBinding = class (ECodecError)
///  [line: 15, column: 3, file: qtx.codec]
var ECodecBinding$1 = {
   $ClassName:"ECodecBinding",$Parent:ECodecError$1
   ,$Init:function ($) {
      ECodecError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager$1() {
   var Result = null;
   if (__Manager$1===null) {
      __Manager$1 = TObject.Create($New(TQTXCodecManager));
   }
   Result = __Manager$1;
   return Result
};
/// TCustomCodec = class (TObject)
///  [line: 134, column: 3, file: System.Codec]
var TCustomCodec = {
   $ClassName:"TCustomCodec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBindings$1 = [];
      $.FCodecInfo = null;
   }
   /// constructor TCustomCodec.Create()
   ///  [line: 290, column: 26, file: System.Codec]
   ,Create$33:function(Self) {
      TObject.Create(Self);
      Self.FCodecInfo = TCustomCodec.MakeCodecInfo$(Self);
      if (Self.FCodecInfo===null) {
         throw EW3Exception.Create$32($New(ECodecError),"TCustomCodec.Create",Self,"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   /// destructor TCustomCodec.Destroy()
   ///  [line: 299, column: 25, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo);
      TObject.Destroy(Self);
   }
   /// function TCustomCodec.MakeCodecInfo() : TCodecInfo
   ///  [line: 305, column: 23, file: System.Codec]
   ,MakeCodecInfo:function(Self) {
      return null;
   }
   /// procedure TCustomCodec.RegisterBinding(const Binding: TCodecBinding)
   ///  [line: 310, column: 24, file: System.Codec]
   ,RegisterBinding:function(Self, Binding$2) {
      if (Self.FBindings$1.indexOf(Binding$2)<0) {
         Self.FBindings$1.push(Binding$2);
      } else {
         throw EW3Exception.Create$32($New(ECodecError),"TCustomCodec.RegisterBinding",Self,"Binding already connected to codec error");
      }
   }
   /// procedure TCustomCodec.UnRegisterBinding(const Binding: TCodecBinding)
   ///  [line: 319, column: 24, file: System.Codec]
   ,UnRegisterBinding:function(Self, Binding$3) {
      var LIndex$1 = 0;
      LIndex$1 = Self.FBindings$1.indexOf(Binding$3);
      if (LIndex$1>=0) {
         Self.FBindings$1.splice(LIndex$1,1)
         ;
      } else {
         throw EW3Exception.Create$32($New(ECodecError),"TCustomCodec.UnRegisterBinding",Self,"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TCustomCodec.$Intf={
   ICodecProcess:[TCustomCodec.EncodeData,TCustomCodec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
/// TUTF8Codec = class (TCustomCodec)
///  [line: 23, column: 3, file: System.codec.utf8]
var TUTF8Codec = {
   $ClassName:"TUTF8Codec",$Parent:TCustomCodec
   ,$Init:function ($) {
      TCustomCodec.$Init($);
   }
   /// function TUTF8Codec.CanUseClampedArray() : Boolean
   ///  [line: 67, column: 27, file: System.codec.utf8]
   ,CanUseClampedArray:function() {
      var Result = {v:false};
      try {
         var LTemp$21 = undefined;
         try {
            LTemp$21 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$25 = $W($e);
            return Result.v;
         }
         if (LTemp$21) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   /// procedure TUTF8Codec.EncodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 211, column: 22, file: System.codec.utf8]
   ,EncodeData:function(Self, Source$6, Target$5) {
      var LReader = null,
         LWriter = null,
         BytesToRead$2 = 0,
         LCache$2 = [],
         Text$7 = "";
      LReader = TW3CustomReader.Create$34($New(TStreamReader),Source$6);
      try {
         LWriter = TW3CustomWriter.Create$8($New(TStreamWriter),Target$5);
         try {
            BytesToRead$2 = TW3CustomReader.GetTotalSize$1(LReader)-TW3CustomReader.GetReadOffset(LReader);
            if (BytesToRead$2>0) {
               LCache$2 = TW3CustomReader.Read(LReader,BytesToRead$2);
               Text$7 = TDatatype.BytesToString$1(TDatatype,LCache$2);
               LCache$2.length=0;
               LCache$2 = TUTF8Codec.Encode(Self,Text$7);
               TW3CustomWriter.Write(LWriter,LCache$2);
            }
         } finally {
            TObject.Free(LWriter);
         }
      } finally {
         TObject.Free(LReader);
      }
   }
   /// procedure TUTF8Codec.DecodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 241, column: 22, file: System.codec.utf8]
   ,DecodeData:function(Self, Source$7, Target$6) {
      var LReader$1 = null,
         LWriter$1 = null,
         BytesToRead$3 = 0,
         LCache$3 = [],
         Text$8 = "";
      LReader$1 = TW3CustomReader.Create$34($New(TReader),Source$7);
      try {
         LWriter$1 = TW3CustomWriter.Create$8($New(TWriter),Target$6);
         try {
            BytesToRead$3 = TW3CustomReader.GetTotalSize$1(LReader$1)-TW3CustomReader.GetReadOffset(LReader$1);
            if (BytesToRead$3>0) {
               LCache$3 = TW3CustomReader.Read(LReader$1,BytesToRead$3);
               Text$8 = TUTF8Codec.Decode(Self,LCache$3);
               LCache$3.length=0;
               LCache$3 = TDatatype.StringToBytes$1(TDatatype,Text$8);
               TW3CustomWriter.Write(LWriter$1,LCache$3);
            }
         } finally {
            TObject.Free(LWriter$1);
         }
      } finally {
         TObject.Free(LReader$1);
      }
   }
   /// function TUTF8Codec.MakeCodecInfo() : TCodecInfo
   ///  [line: 46, column: 21, file: System.codec.utf8]
   ,MakeCodecInfo:function(Self) {
      var Result = null;
      var LAccess$11 = null;
      var LVersion$1 = {viMajor:0,viMinor:0,viRevision:0};
      Result = TObject.Create($New(TCodecInfo));
      LVersion$1.viMajor = 0;
      LVersion$1.viMinor = 2;
      LVersion$1.viRevision = 0;
      LAccess$11 = $AsIntf(Result,"ICodecInfo");
      LAccess$11[0]("UTF8Codec");
      LAccess$11[1]("text\/utf8");
      LAccess$11[2](LVersion$1);
      LAccess$11[3]([6]);
      LAccess$11[5](1);
      LAccess$11[6](0);
      return Result
   }
   /// function TUTF8Codec.Encode(TextToEncode: String) : TByteArray
   ///  [line: 80, column: 21, file: System.codec.utf8]
   ,Encode:function(Self, TextToEncode$4) {
      var Result = {v:[]};
      try {
         var LEncoder$1 = null;
         var LClip$1 = null;
         var LTemp$22 = null;
         var n$1 = 0;
         var LTyped$2 = null;
         try {
            LEncoder$1 = new TextEncoder();
         } catch ($e) {
            if (TextToEncode$4.length>0) {
               if (TUTF8Codec.CanUseClampedArray()) {
                  LClip$1 = new Uint8ClampedArray(1);
                  LTemp$22 = new Uint8ClampedArray(1);
               } else {
                  LClip$1 = new Uint8Array(1);
                  LTemp$22 = new Uint8Array(1);
               }
               var $temp50;
               for(n$1=1,$temp50=TextToEncode$4.length;n$1<=$temp50;n$1++) {
                  LClip$1[0]=TString.CharCodeFor(TString,TextToEncode$4.charAt(n$1-1));
                  if (LClip$1[0]<128) {
                     Result.v.push(LClip$1[0]);
                  } else if (LClip$1[0]>127&&LClip$1[0]<2048) {
                     LTemp$22[0]=((LClip$1[0]>>>6)|192);
                     Result.v.push(LTemp$22[0]);
                     LTemp$22[0]=((LClip$1[0]&63)|128);
                     Result.v.push(LTemp$22[0]);
                  } else {
                     LTemp$22[0]=((LClip$1[0]>>>12)|224);
                     Result.v.push(LTemp$22[0]);
                     LTemp$22[0]=(((LClip$1[0]>>>6)&63)|128);
                     Result.v.push(LTemp$22[0]);
                     Result.v.push((LClip$1[0]&63)|128);
                     Result.v.push(LTemp$22[0]);
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$2 = LEncoder$1.encode(TextToEncode$4);
            Result.v = TDatatype.TypedArrayToBytes$1(TDatatype,LTyped$2);
         } finally {
            LEncoder$1 = null;
         }
      } finally {return Result.v}
   }
   /// function TUTF8Codec.Decode(const BytesToDecode: TByteArray) : String
   ///  [line: 145, column: 21, file: System.codec.utf8]
   ,Decode:function(Self, BytesToDecode$3) {
      var Result = {v:""};
      try {
         var LDecoder$1 = null;
         var bytelen$1 = 0,
            i$4 = 0,
            c$1 = 0,
            c2$3 = 0,
            c2$4 = 0,
            c3$2 = 0,
            c4$1 = 0,
            u$1 = 0,
            c2$5 = 0,
            c3$3 = 0,
            LTyped$3 = undefined;
         try {
            LDecoder$1 = new TextDecoder();
         } catch ($e) {
            bytelen$1 = BytesToDecode$3.length;
            if (bytelen$1>0) {
               i$4 = 0;
               while (i$4<bytelen$1) {
                  c$1 = BytesToDecode$3[i$4];
                  ++i$4;
                  if (c$1<128) {
                     Result.v+=TString.FromCharCode(TString,c$1);
                  } else if (c$1>191&&c$1<224) {
                     c2$3 = BytesToDecode$3[i$4];
                     ++i$4;
                     Result.v+=TString.FromCharCode(TString,(((c$1&31)<<6)|(c2$3&63)));
                  } else if (c$1>239&&c$1<365) {
                     c2$4 = BytesToDecode$3[i$4];
                     ++i$4;
                     c3$2 = BytesToDecode$3[i$4];
                     ++i$4;
                     c4$1 = BytesToDecode$3[i$4];
                     ++i$4;
                     u$1 = (((((c$1&7)<<18)|((c2$4&63)<<12))|((c3$2&63)<<6))|(c4$1&63))-65536;
                     Result.v+=TString.FromCharCode(TString,(55296+(u$1>>>10)));
                     Result.v+=TString.FromCharCode(TString,(56320+(u$1&1023)));
                  } else {
                     c2$5 = BytesToDecode$3[i$4];
                     ++i$4;
                     c3$3 = BytesToDecode$3[i$4];
                     ++i$4;
                     Result.v+=TString.FromCharCode(TString,(((c$1&15)<<12)|(((c2$5&63)<<6)|(c3$3&63))));
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$3 = TDatatype.BytesToTypedArray$1(TDatatype,BytesToDecode$3);
            Result.v = LDecoder$1.decode(LTyped$3);
         } finally {
            LDecoder$1 = null;
         }
      } finally {return Result.v}
   }
   ,Destroy:TCustomCodec.Destroy
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TUTF8Codec.$Intf={
   ICodecProcess:[TUTF8Codec.EncodeData,TUTF8Codec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
/// TCodecVersionInfo = record
///  [line: 38, column: 3, file: System.Codec]
function Copy$TCodecVersionInfo(s,d) {
   d.viMajor=s.viMajor;
   d.viMinor=s.viMinor;
   d.viRevision=s.viRevision;
   return d;
}
function Clone$TCodecVersionInfo($) {
   return {
      viMajor:$.viMajor,
      viMinor:$.viMinor,
      viRevision:$.viRevision
   }
}
/// TCodecManager = class (TObject)
///  [line: 159, column: 3, file: System.Codec]
var TCodecManager = {
   $ClassName:"TCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs = [];
   }
   /// function TCodecManager.CodecByClass(const ClsType: TCodecClass) : TCustomCodec
   ///  [line: 249, column: 24, file: System.Codec]
   ,CodecByClass:function(Self, ClsType$1) {
      var Result = null;
      var a$295 = 0;
      var LItem$9 = null;
      var a$296 = [];
      Result = null;
      a$296 = Self.FCodecs;
      var $temp51;
      for(a$295=0,$temp51=a$296.length;a$295<$temp51;a$295++) {
         LItem$9 = a$296[a$295];
         if (TObject.ClassType(LItem$9.ClassType)==ClsType$1) {
            Result = LItem$9;
            break;
         }
      }
      return Result
   }
   /// destructor TCodecManager.Destroy()
   ///  [line: 193, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   /// procedure TCodecManager.RegisterCodec(const CodecClass: TCodecClass)
   ///  [line: 262, column: 25, file: System.Codec]
   ,RegisterCodec:function(Self, CodecClass$1) {
      var LItem$10 = null;
      LItem$10 = TCodecManager.CodecByClass(Self,CodecClass$1);
      if (LItem$10===null) {
         LItem$10 = TCustomCodec.Create$33($NewDyn(CodecClass$1,""));
         Self.FCodecs.push(LItem$10);
      } else {
         throw EW3Exception.Create$32($New(ECodecManager),"TCodecManager.RegisterCodec",Self,"Codec already registered error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TCodecInfo = class (TObject)
///  [line: 71, column: 3, file: System.Codec]
var TCodecInfo = {
   $ClassName:"TCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout = $.ciMime = $.ciName = "";
      $.ciDataFlow = [0];
      $.ciInput = 0;
      $.ciOutput = 0;
      $.ciVersion = {viMajor:0,viMinor:0,viRevision:0};
   }
   /// procedure TCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow)
   ///  [line: 494, column: 22, file: System.Codec]
   ,SetDataFlow:function(Self, coFlow$1) {
      Self.ciDataFlow = coFlow$1.slice(0);
   }
   /// procedure TCodecInfo.SetDescription(const coInfo: String)
   ///  [line: 489, column: 22, file: System.Codec]
   ,SetDescription:function(Self, coInfo$1) {
      Self.ciAbout = coInfo$1;
   }
   /// procedure TCodecInfo.SetInput(const InputType: TCodecDataFormat)
   ///  [line: 479, column: 22, file: System.Codec]
   ,SetInput:function(Self, InputType$1) {
      Self.ciInput = InputType$1;
   }
   /// procedure TCodecInfo.SetMime(const coMime: String)
   ///  [line: 467, column: 22, file: System.Codec]
   ,SetMime:function(Self, coMime$1) {
      Self.ciMime = coMime$1;
   }
   /// procedure TCodecInfo.SetName(const coName: String)
   ///  [line: 462, column: 22, file: System.Codec]
   ,SetName:function(Self, coName$1) {
      Self.ciName = coName$1;
   }
   /// procedure TCodecInfo.SetOutput(const OutputType: TCodecDataFormat)
   ///  [line: 484, column: 22, file: System.Codec]
   ,SetOutput:function(Self, OutputType$1) {
      Self.ciOutput = OutputType$1;
   }
   /// procedure TCodecInfo.SetVersion(const coVersion: TCodecVersionInfo)
   ///  [line: 472, column: 22, file: System.Codec]
   ,SetVersion:function(Self, coVersion$1) {
      Self.ciVersion.viMajor = coVersion$1.viMajor;
      Self.ciVersion.viMinor = coVersion$1.viMinor;
      Self.ciVersion.viRevision = coVersion$1.viRevision;
   }
   ,Destroy:TObject.Destroy
};
TCodecInfo.$Intf={
   ICodecInfo:[TCodecInfo.SetName,TCodecInfo.SetMime,TCodecInfo.SetVersion,TCodecInfo.SetDataFlow,TCodecInfo.SetDescription,TCodecInfo.SetInput,TCodecInfo.SetOutput]
}
/// TCodecDataFormat enumeration
///  [line: 52, column: 3, file: System.Codec]
var TCodecDataFormat$1 = [ "cdBinary", "cdText" ];
/// function TCodecDataFlowHelper.Ordinal(const Self: TCodecDataFlow) : Integer
///  [line: 426, column: 31, file: System.Codec]
function TCodecDataFlowHelper$1$Ordinal$1(Self$14) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$14,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$14,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
/// TCodecDataDirection enumeration
///  [line: 47, column: 3, file: System.Codec]
var TCodecDataDirection$1 = { 1:"cdRead", 2:"cdWrite" };
/// TCodecBinding = class (TObject)
///  [line: 102, column: 3, file: System.Codec]
var TCodecBinding = {
   $ClassName:"TCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec = null;
   }
   /// constructor TCodecBinding.Create(const EndPoint: TCustomCodec)
   ///  [line: 334, column: 27, file: System.Codec]
   ,Create$44:function(Self, EndPoint$1) {
      var LAccess$12 = null;
      TObject.Create(Self);
      if (EndPoint$1!==null) {
         Self.FCodec = EndPoint$1;
         LAccess$12 = $AsIntf(Self.FCodec,"ICodecBinding");
         LAccess$12[0](Self);
      } else {
         throw EW3Exception.Create$32($New(ECodecBinding),"TCodecBinding.Create",Self,"Binding failed, invalid endpoint error");
      }
      return Self
   }
   /// destructor TCodecBinding.Destroy()
   ///  [line: 349, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      var LAccess$13 = null;
      LAccess$13 = $AsIntf(Self.FCodec,"ICodecBinding");
      LAccess$13[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// ECodecManager = class (EW3Exception)
///  [line: 29, column: 3, file: System.Codec]
var ECodecManager = {
   $ClassName:"ECodecManager",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecError = class (EW3Exception)
///  [line: 30, column: 3, file: System.Codec]
var ECodecError = {
   $ClassName:"ECodecError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecBinding = class (EW3Exception)
///  [line: 28, column: 3, file: System.Codec]
var ECodecBinding = {
   $ClassName:"ECodecBinding",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager() {
   var Result = null;
   if (!__Manager) {
      __Manager = TObject.Create($New(TCodecManager));
   }
   Result = __Manager;
   return Result
};
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$16 = 0;
var a$19 = 0;
var a$18 = 0;
var a$17 = 0;
var a$20 = 0;
var a$21 = 0;
var a$38 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var CRC_Table_Ready$1 = false;
var CRC_Table$1 = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var __Resolved = false;
var __SupportCTA = false;
var a$194 = null;
var NodeProgram = null,
   _UDPVersionStr = ["","",""];
var _UDPVersionStr = ["udp4", "udp6", "udp7"];
var __App = null;
var module_events_ = undefined;
var __TYPE_MAP$1 = {Boolean$1:undefined,Number$2:undefined,String$2:undefined,Object$4:undefined,Undefined$1:undefined,Function$3:undefined};
var __SIZES$1 = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES$1 = ["","","","","","","","","","",""],
   __B64_Lookup$1 = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup$1 = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET$1 = "";
var __SIZES$1 = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES$1 = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET$1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __Manager$1 = null;
var __EventsRef = undefined;
var __Parser = null;
var __RESERVED = [];
var __RESERVED = ["$ClassName", "$Parent", "$Init", "toString", "toLocaleString", "valueOf", "indexOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor", "destructor"].slice();
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
var __Manager = null;
InitializeTypeMap();
InitializeBase64();
if (!Uint8Array.prototype.fill) {
      Uint8Array.prototype.fill = Array.prototype.fill;
    }
;
var __Def_Converter = TDataTypeConverter.Create$7$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
SetupTypeLUT();
TQTXCodecManager.RegisterCodec$1(CodecManager$1(),TQTXCodecUTF8);
TCodecManager.RegisterCodec(CodecManager(),TUTF8Codec);
var __EventsRef = require("events");
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$48$($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$48$($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var module_events_ = require("events");
var $Application = function() {
   NodeProgram = TNodeProgram.Create$6($New(TNodeProgram));
   TNodeProgram.Execute(NodeProgram);
}
$Application();


unit service.auth.core;

interface

{$DEFINE DEBUG}

const
  CNT_PREFS_DEFAULTPORT = 1882;
  CNT_PREFS_FILENAME    = 'preferences.ini';
  CNT_PREFS_DBNAME      = 'authdb.db';

uses
  System.Types,
  System.Types.Convert,
  System.Objects,
  System.Inifile,
  System.IOUtils,

  System.db,
  System.dataset,
  SmartNJ.Sqlite3,

  qtx.logfile,
  qtx.orm,

  ragnarok.types,
  ragnarok.json,
  ragnarok.Server,
  ragnarok.messages.base,
  ragnarok.messages.factory,
  ragnarok.messages.network,

  service.base,
  service.dispatcher,
  service.auth.types,
  service.auth.messages,

  SmartNJ.Server.WebSocket,
  SmartNJ.Server.WebSocketSecure,

  SmartNJ.Inifile,
  SmartNJ.System,
  SmartNJ.Device.Storage,
  SmartNJ.Application,
  SmartNJ.Server.Http;

type

  TQTXAuthenticationService = class(TRagnarokService)
  private
    FPrefs:     TIniFile;
    FLog:       TQTXLogEmitter;
    FDatabase:  TSQLite3Database;

    // API calls
    procedure   HandleLogin(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
    procedure   HandleGetAccountInfo(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
    procedure   HandlePutAccountInfo(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
    procedure   HandleLogOut(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);

  protected
    procedure   SetupPreferences(const CB: TRagnarokServiceCB);
    procedure   SetupLogfile(LogFileName: string;const CB: TRagnarokServiceCB);
    procedure   SetupDatabase(const CB: TRagnarokServiceCB);

    procedure   SetupProfileTable(const TagValue: variant; const CB: TRagnarokServiceCB);
    procedure   SetupProfileDataTable(const TagValue: variant; const CB: TRagnarokServiceCB);

    procedure   AfterServerStarted; override;
    procedure   BeforeServerStopped; override;
    procedure   Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage); override;

  public
    property    Preferences: TIniFile read FPrefs;
    property    Database: TSQLite3Database read FDatabase;

    procedure   SetupService(const CB: TRagnarokServiceCB);

    constructor Create; override;
    destructor  Destroy; override;
  end;



implementation

//#############################################################################
// TQTXAuthenticationService
//#############################################################################

constructor TQTXAuthenticationService.Create;
begin
  inherited;
  FPrefs := TIniFile.Create();
  FLog := TQTXLogEmitter.Create();
  FDatabase := TSQLite3Database.Create(nil);
end;

destructor TQTXAuthenticationService.Destroy;
begin
  // decouple logger from our instance
  self.logging := nil;

  FPrefs.free;
  FLog.free;
  inherited;
end;

procedure TQTXAuthenticationService.SetupService(const CB: TRagnarokServiceCB);
begin
  SetupPreferences( procedure (Error: Exception)
  begin
    // No logfile yet setup (!)
    if Error <> nil then
    begin
      WriteToLog("Preferences setup: Failed!");
      WriteToLog(error.message);
      raise error;
    end else
    WriteToLog("Preferences setup: OK");

    // logfile-name is always relative to the executable
    var LLogFileName := IncludeTrailingPathDelimiter(ExtractFilePath(Application.Exename));
    LLogFileName += FPrefs.ReadString('log', 'logfile', 'log.txt');

    // Port is defined in the ancestor, so we assigns it here
    Port := FPrefs.ReadInteger('networking', 'port', CNT_PREFS_DEFAULTPORT);

    SetupLogfile(LLogFileName, procedure (Error: Exception)
    begin
      if Error <> nil then
      begin
        WriteToLog("Logfile setup: Failed!");
        WriteToLog(error.message);
        raise error;
      end else
      WriteToLog("Logfile setup: OK");

      SetupDatabase( procedure (Error: Exception)
      begin
        if Error <> nil then
        begin
          WriteToLog("Database setup: Failed!");
          WriteToLog(error.message);
          if assigned(CB) then
            CB(Error)
          else
            raise Error;
        end else
        WriteToLog("Database setup: OK");

        if assigned(CB) then
          CB(nil);
      end);

    end);
  end);
end;

procedure TQTXAuthenticationService.SetupPreferences(const CB: TRagnarokServiceCB);
begin
  // Get directory from our exe path
  var LBasePath := TPath.GetDirectoryName(Application.ExeName);

  // Complete our preferences filename
  var LPrefsFile := TPath.IncludeTrailingPathDelimiter(LBasePath) + CNT_PREFS_FILENAME;

  if FileExists(LPrefsFile) then
  begin

    try
      FPrefs.LoadFromFile(LPrefsFile);
    except
      on e: exception do
      begin
        var LError := EW3Exception.CreateFmt('Failed to load preferences file (%s): %s', [LPrefsFile, e.message]);
        WriteToLog(LError.message);
        if assigned(CB) then
          CB(LError)
        else
          raise LError;
        exit;
      end;
    end;

    if assigned(CB) then
      CB(nil);

  end else
  begin
    var LError := Exception.Create('Could not locate preferences file: ' + LPrefsFile);
    WriteToLog(LError.message);
    if assigned(CB) then
      CB(LError)
    else
      raise LError;
  end;
end;

procedure TQTXAuthenticationService.SetupLogfile(LogFileName: string;const CB: TRagnarokServiceCB);
begin

  // Attempt to open logfile
  // Note: Log-object error options is set to throw exceptions
  try
    FLog.Open(LogFileName);
  except
    on e: exception do
    begin
      if assigned(CB) then
      begin
        CB(e);
        exit;
      end else
      begin
        writeln(e.message);
        raise;
      end;
    end;
  end;

  // We inherit from TQTXLogObject, which means we can pipe
  // all errors etc directly using built-in functions. So here
  // we simply glue our instance to the log-file, and its all good
  self.Logging := FLog as IQTXLogClient;

  if assigned(CB) then
    CB(nil);
end;


procedure TQTXAuthenticationService.SetupProfileTable(const TagValue: variant; const CB: TRagnarokServiceCB);
begin
  FDatabase.Execute(
    #'
      create table if not exists profiles
          (
            id integer primary key AUTOINCREMENT,
            state     integer,
            privilege text,
            username  text,
            password  text,
            path      text,
            created   real,
            lastlogin real
          );
          ', [],
    procedure (Sender: TObject; Success: boolean)
    begin
      if not Success then
      begin
        var LError := EW3Exception.Create(FDatabase.LastError);
        WriteToLog(LError.message);
        if assigned(CB) then
          CB(LError)
        else
          raise LError;
        exit;
      end else
      if assigned(CB) then
        CB(nil);
    end);


  { FDatabase.RecordCount('profiles', procedure (Count: integer)
  begin
    // Initialize default roles if DB is empty
    if Count < 1 then
    begin
      FDatabase.Execute('insert into profiles (username) values( "admin" );', [] , nil);
      FDatabase.Execute('insert into profiles (username) values( "gamer" );', [] , nil);
      FDatabase.Execute('insert into profiles (username) values( "guest" );', [] , nil);
    end;

    writeln("RecordCount =" + Count.ToString() );
  end);  }

end;


procedure TQTXAuthenticationService.SetupProfileDataTable(const TagValue: variant; const CB: TRagnarokServiceCB);
begin
  FDatabase.Execute(
    #'
      create table if not exists profiledata
          (
            id integer primary key AUTOINCREMENT,
            userid      integer,
            token       text,
            data        text,
            FOREIGN KEY (userid) REFERENCES profiles(id)
          );

          ', [],
    procedure (Sender: TObject; Success: boolean)
    begin
      if not Success then
      begin
        var LError := EW3Exception.Create(FDatabase.LastError);
        WriteToLog(LError.message);
        if assigned(CB) then
          CB(LError)
        else
          raise LError;
        exit;
      end else
      if assigned(CB) then
        CB(nil);
    end);
end;

procedure TQTXAuthenticationService.SetupDatabase(const CB: TRagnarokServiceCB);
begin
  // Try to read database-path from preferences file
  var LDbFileToOpen := FPrefs.ReadString("database", "database_name", "");

  // Trim away spaces, check if there is a filename
  LDbFileToOpen := LDbFileToOpen.trim();
  if LDbFileToOpen.length < 1 then
  begin
    // No filename? Fall back on pre-defined file in CWD
    var LBasePath := TPath.GetDirectoryName(Application.ExeName);
    LDbFileToOpen := TPath.IncludeTrailingPathDelimiter(LBasePath) + CNT_PREFS_DBNAME;
  end;

  FDatabase.AccessMode := TSQLite3AccessMode.sqaReadWriteCreate;
  FDatabase.Open(LDbFileToOpen,
    procedure (Sender: TObject; Success: boolean)
    begin
      if not Success then
      begin
        var LError := EW3Exception.Create(FDatabase.LastError);
        WriteToLog(LError.message);
        if assigned(CB) then
        begin
          CB(LError);
          exit;
        end else
        raise LError;
      end;

      WriteToLog("Initializing Profiles");
      SetupProfileTable(nil, procedure (Error: exception)
      begin
        if Error <> nil then
        begin
          WriteToLog("Profiles initialized: **failed");
          WriteToLog(error.message);
          if assigned(CB) then
            CB(Error)
          else
            raise Error;
          exit;
        end else
        writeToLog("Profiles initialized: OK");

        WriteToLog("Initializing ProfileData");
        SetupProfileTable(nil, procedure (Error: exception)
        begin
          if Error <> nil then
          begin
            WriteToLog("ProfileData initialized: **failed");
            WriteToLog(error.message);
            if assigned(CB) then
              CB(Error);
            exit;
          end else
          writeToLog("ProfileData initialized: OK");

          if assigned(CB) then
            CB(nil);

        end);
      end);
    end);
end;

procedure TQTXAuthenticationService.AfterServerStarted;
begin
  inherited;
  MessageDispatch.RegisterMessage(TQTXAuthLoginRequest, @HandleLogin);
  MessageDispatch.RegisterMessage(TQTXGetAccountInfoRequest, @HandleGetAccountInfo);
  MessageDispatch.RegisterMessage(TQTXPutAccountInfoRequest, @HandlePutAccountInfo);
  MessageDispatch.RegisterMessage(TQTXAuthLogoutRequest, @HandleLogout);
end;

procedure TQTXAuthenticationService.BeforeServerStopped;
begin
  inherited;
end;

procedure TQTXAuthenticationService.Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage);
begin
  var LInfo := MessageDispatch.GetMessageInfoForClass(Message);
  if LInfo <> nil then
  begin
    try
      LInfo.MessageHandler(Socket, Message);
    except
      on e: exception do
      begin
        //Log error
        WriteToLog(e.message);
      end;
    end;
  end;
end;

procedure TQTXAuthenticationService.HandleLogin(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
begin
  writeln("login called");
end;

procedure TQTXAuthenticationService.HandleGetAccountInfo(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
begin
end;

procedure TQTXAuthenticationService.HandlePutAccountInfo(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
begin
end;

procedure TQTXAuthenticationService.HandleLogOut(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);
begin
end;


end.

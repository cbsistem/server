unit service.auth.messages;

interface

uses 
  System.Types,
  System.Types.Convert,

  qtx.logfile,
  qtx.orm,

  service.auth.types,

  ragnarok.types,
  ragnarok.json,
  ragnarok.Server,
  ragnarok.messages.base,
  ragnarok.messages.factory,
  ragnarok.messages.network;

type

  //##########################################################################
  // Message envelope types
  //##########################################################################

  // Request: Creates a new session
  TQTXAuthLoginRequest = class(TQTXBaseMessage)
  end;

  // Response: Returns that the session has been updated
  TQTXAuthLoginResponse = class(TQTXResponseMessage)
  end;

  //----------------------------------------------------

  // Request: Requests the data for the session-object.
  //          If an object doesnt exist, one is created.
  TQTXGetAccountInfoRequest = class(TQTXBaseMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    UserName: string;
  end;

  // Response: Returns the data for a session-object.
  TQTXGetAccountInfoResponse = class(TQTXResponseMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Id: integer;
    property    UserId: integer;
    property    UserName: string;
    property    Privilege: TQTXAccountPrivileges;
    property    AccountState: TQTXAccountState;
  end;

  //----------------------------------------------------

  // Request: Requests a session object be updated.
  TQTXPutAccountInfoRequest = class(TQTXBaseMessage)
  end;

  // Response: Returns a simple status that the data has been updated.
  TQTXPutAccountInfoResponse = class(TQTXResponseMessage)
  end;

  //----------------------------------------------------

  // Request: terminates a session object (if ref count < 1)
  TQTXAuthLogoutRequest = class(TQTXBaseMessage)
  end;

  // Response: returns status-code for destruction of session
  TQTXAuthLogoutResponse = class(TQTXResponseMessage)
  end;


implementation


//#############################################################################
// TQTXGetAccountInfoRequest
//#############################################################################

procedure TQTXGetAccountInfoRequest.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('identity');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('username', TString.EncodeBase64(Username) );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["credentials"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXGetAccountInfoRequest.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('identity',  false);
    if LSource <> nil then
    begin
      Username := TString.DecodeBase64( LSource.ReadString('username') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["credentials"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXGetAccountInfoResponse
//#############################################################################

procedure TQTXGetAccountInfoResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('identity');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('id', Id );
      LTarget.WriteOrAdd('userid', UserId );
      LTarget.WriteOrAdd('username', TString.EncodeBase64(Username) );
      LTarget.WriteOrAdd('privilege', TString.EncodeBase64(Privilege.ToString()) );
      LTarget.WriteOrAdd('state', ord(AccountState) );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["identity"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXGetAccountInfoResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('identity',  false);
    if LSource <> nil then
    begin
      Id := LSource.ReadInteger('id');
      UserId := LSource.ReadInteger('userid');
      Username := TString.DecodeBase64( LSource.ReadString('username') );
      Privilege := TQTXAccountPrivileges.FromString( TString.DecodeBase64(LSource.ReadString('privilege')) );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["identity"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;


end.

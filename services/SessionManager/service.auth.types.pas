unit service.auth.types;

interface

uses 
  System.Types,
  qtx.orm;

type


  //##########################################################################
  // Account types
  //##########################################################################

  TQTXAccountPrivilege  =  (
    apExecute,  // user can execute applications
    apRead,     // user can read files
    apWrite,    // user can write files
    apCreate,   // user can create files
    apInstall,  // user can install new software
    apAppstore  // user has access to software repository [appstore]
    );
  TQTXAccountPrivileges = set of TQTXAccountPrivilege;

  TQTXAccountPrivilegeHelper = helper for TQTXAccountPrivileges
    function Compare(const Value: TQTXAccountPrivileges): boolean;
    function ToString: string;
    class function FromString(Text: string): TQTXAccountPrivileges;
  end;

  TQTXAccountState = (
    asUnKnown,
    asActive,       // Account is active and authenticated
    asLocked,       // Account is active and authenticated, but locked by administrator
    asUnConfirmed   // Session is created, but user has not yet authenticated [Default state]
    );

  TQTXTUserAccount = class(TQTXORMObject)
  private
    FPrivilege: TQTXAccountPrivileges;
    FUserId:    integer;
    FUserName:  string;
    FPassword:  string;
    FState:     TQTXAccountState;
    FId:        integer;
    procedure   SetPrivilege(const Value: TQTXAccountPrivileges);
    procedure   SetUserId(const Value: integer);
    procedure   SetUserName(const Value: string);
    procedure   SetPassword(const Value: string);
    procedure   SetAccountState(const Value: TQTXAccountState);
  public
    procedure   Read(const TagValue: variant; const CB: TQTXORMReadFieldsCB); override;
    procedure   Write(const TagValue: variant; const CB: TQTXORMWriteFieldsCB); override;
    procedure   Reset; override;
  published
    property    Id: integer read FId;
    property    Privilege: TQTXAccountPrivileges read FPrivilege write SetPrivilege;
    property    UserId: integer read FUserId write SetUserId;
    property    Username: string read FUserName write SetUserName;
    property    Password: string read FPassword write SetPassword;
    property    AccountState: TQTXAccountState read FState write SetAccountState;
  end;


implementation


//#############################################################################
// TQTXTUserAccount
//#############################################################################

procedure TQTXTUserAccount.Reset;
begin
  SetChanged(false);
  FPrivilege := [];
  FUserId := -1;
  FUserName := '';
  FPassword := '';
  FState := asUnKnown;
end;

procedure TQTXTUserAccount.Read(const TagValue: variant; const CB: TQTXORMReadFieldsCB);
begin
end;

procedure TQTXTUserAccount.Write(const TagValue: variant; const CB: TQTXORMWriteFieldsCB);
begin
end;

procedure TQTXTUserAccount.SetPrivilege(const Value: TQTXAccountPrivileges);
begin
  if not FPrivilege.Compare(Value) then
  begin
    FPrivilege := Value;
    SetChanged(true);
  end;
end;

procedure TQTXTUserAccount.SetUserId(const Value: integer);
begin
  if Value <> FUserId then
  begin
    FUserId := Value;
    SetChanged(true);
  end;
end;

procedure TQTXTUserAccount.SetUserName(const Value: string);
begin
  if Value <> FUserName then
  begin
    FUserName := Value;
    SetChanged(true);
  end;
end;

procedure TQTXTUserAccount.SetPassword(const Value: string);
begin
  if Value <> FPassword then
  begin
    FPassword := Value;
    SetChanged(true);
  end;
end;

procedure TQTXTUserAccount.SetAccountState(const Value: TQTXAccountState);
begin
  if Value <> FState then
  begin
    FState := Value;
    SetChanged(true);
  end;
end;

//#############################################################################
// TQTXAccountPrivilegeHelper
//#############################################################################

function TQTXAccountPrivilegeHelper.ToString: string;
begin
  if (apExecute in self) then result += 'E';
  if (apRead in self) then result += 'R';
  if (apWrite in self) then result += 'W';
  if (apCreate in self) then result += 'C';
  if (apInstall in self) then result += 'I';
  if (apAppstore in self) then result += 'A';
end;

class function TQTXAccountPrivilegeHelper.FromString(Text: string): TQTXAccountPrivileges;
begin
  text := Text.trim().ToUpper();
  result := [];
  for var el in Text do
  begin
    case el of
    'E':  include(result, apExecute);
    'R':  include(result, apRead);
    'W':  include(result, apWrite);
    'C':  include(result, apCreate);
    'I':  include(result, apInstall);
    'A':  include(result, apAppstore);
    end;
  end;
end;

function TQTXAccountPrivilegeHelper.Compare(const Value: TQTXAccountPrivileges): boolean;
begin
  var LChanged := 0;

  if (apExecute in self) then
  begin
    if not (apExecute in Value) then
      inc(LChanged);
  end else
  begin
    if ApExecute in value then
      inc(LChanged);
  end;

  if (apRead in self) then
  begin
    if not (apRead in Value) then
      inc(LChanged);
  end else
  begin
    if apRead in value then
      inc(LChanged);
  end;

  if (apWrite in self) then
  begin
    if not (apWrite in Value) then
      inc(LChanged);
  end else
  begin
    if apWrite in value then
      inc(LChanged);
  end;

  if (apCreate in self) then
  begin
    if not (apCreate in Value) then
      inc(LChanged);
  end else
  begin
    if apCreate in value then
      inc(LChanged);
  end;

  if (apInstall in self) then
  begin
    if not (apInstall in Value) then
      inc(LChanged);
  end else
  begin
    if apInstall in value then
      inc(LChanged);
  end;

  if (apAppstore in self) then
  begin
    if not (apAppstore in Value) then
      inc(LChanged);
  end else
  begin
    if apAppstore in value then
      inc(LChanged);
  end;

  result := LChanged > 0;

end;


end.

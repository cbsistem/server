var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"File operation [%s] failed, system threw exception: %s",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"'Invalid handle for object (%s), reference rejected error",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"0123456789",
	"0123456789ABCDEF",
	"Invalid bit index, expected 0..31",
	"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",
	"Invalid datatype, failed to identify number [integer] type error",
	"Invalid datatype, byte conversion failed error",
	"Seek failed, stream is empty error",
	"Read operation failed, %s bytes exceeds storage medium error",
	"Read operation failed, invalid signature error [%s]",
	"Bookmarks not supported by medium error",
	"No bookmarks to roll back error",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid datasize [%d] error",
	"'Invalid handle [%s], reference was rejected error"];
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function StrReplace(s,o,n) { return o?s.replace(new RegExp(StrRegExp(o), "g"), n):s }
function StrRegExp(s) { return s.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") }
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function RandomInt(i) { return Math.floor(Random()*i) }
/*

Copyright (C) 2010 by Johannes Baag�e <baagoe@baagoe.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

From http://baagoe.com/en/RandomMusings/javascript/
*/
function $alea() {
  return (function(args) {
    // Johannes Baagøe <baagoe@baagoe.com>, 2010
    var s0 = 0;
    var s1 = 0;
    var s2 = 0;
    var c = 1;

    if (args.length == 0) {
      args = [+new Date];
    }
    var mash = function() {
       var n = 0xefc8249d;
    
       var mash = function(data) {
         data = data.toString();
         for (var i = 0; i < data.length; i++) {
           n += data.charCodeAt(i);
           var h = 0.02519603282416938 * n;
           n = h >>> 0;
           h -= n;
           h *= n;
           n = h >>> 0;
           h -= n;
           n += h * 0x100000000; // 2^32
         }
         return (n >>> 0) * 2.3283064365386963e-10; // 2^-32
       };
    
       //mash.version = 'Mash 0.9';
       return mash;
    }();
    s0 = mash(' ');
    s1 = mash(' ');
    s2 = mash(' ');

    for (var i = 0; i < args.length; i++) {
      s0 -= mash(args[i]);
      if (s0 < 0) {
        s0 += 1;
      }
      s1 -= mash(args[i]);
      if (s1 < 0) {
        s1 += 1;
      }
      s2 -= mash(args[i]);
      if (s2 < 0) {
        s2 += 1;
      }
    }
    mash = null;

    var random = function() {
      var t = 2091639 * s0 + c * 2.3283064365386963e-10; // 2^-32
      s0 = s1;
      s1 = s2;
      return s2 = t - (c = t | 0);
    };
    /*random.uint32 = function() {
      return random() * 0x100000000; // 2^32
    };
    random.fract53 = function() {
      return random() +
        (random() * 0x200000 | 0) * 1.1102230246251565e-16; // 2^-53
    };*/
    //random.version = 'Alea 0.9';
    random.args = args;
    return random;

  } (Array.prototype.slice.call(arguments)));
};var Random = $alea();
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
function IntToHex(v,d) { var r=v.toString(16); return "00000000".substr(0, d-r.length)+r }
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event3(i,f) {
	var li=i,lf=f;
	return function(a,b,c) {
		return lf.call(li,li,a,b,c)
	}
}
function $Event1(i,f) {
	var li=i,lf=f;
	return function(a) {
		return lf.call(li,li,a)
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $Div(a,b) { var r=a/b; return (r>=0)?Math.floor(r):Math.ceil(r) }
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
var TKeygenService = {
   $ClassName:"TKeygenService",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCache = [];
      $.FGenerator = null;
      $.FParams = {batchsize:"",failed_:false,method:"",modulator:"",obfuscation:"",output:"",rootkey:""};
      $.FRoot = [0,0,0,0,0,0,0,0,0,0,0,0];
   }
   ,Create$3:function(Self) {
      TObject.Create(Self);
      Self.FGenerator = THexIronWoodSerialNumber.Create$80($New(THexIronwoodGenerator));
      Self.FGenerator.FOnAcceptSerial = $Event3(Self,TKeygenService.HandleNewKey);
      Self.FGenerator.FOnBeforeMint = $Event1(Self,TKeygenService.HandleBeforeMint);
      Self.FGenerator.FOnAfterMint = $Event1(Self,TKeygenService.HandleAfterMint);
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FGenerator);
      TObject.Destroy(Self);
   }
   ,Execute:function(Self) {
      Self.FParams = TKeygenService.ParseParams(Self);
      if (Self.FParams.failed_) {
         return;
      }
      if (Trim$_String_(Self.FParams.output).length<3) {
         if (Self.FParams.output!="stdout") {
            WriteLn("Invalid output, no -output:filename has been set. Aborting mint.");
            return;
         }
      }
      if (Self.FParams.output!="stdout") {
         WriteLnF("Quartex Key Mint v%d.%d",[1, 0]);
         WriteLn("Written by Jon Lennart Aasenden");
         WriteLn("Copyright Quartex Components LTD");
         WriteLn("--------------------------------");
         WriteLn("Output set to: "+Self.FParams.output);
      }
      try {
         TKeygenService.MintTo(Self,Self.FParams);
      } catch ($e) {
         var e = $W($e);
         WriteLn("Minting failed with:"+e.FMessage)      }
   }
   ,HandleAfterMint:function(Self, Sender) {
      if (Self.FParams.output!="stdout") {
         WriteLnF("Minting complete, %d keys generated",[Self.FCache.length]);
      }
   }
   ,HandleBeforeMint:function(Self, Sender$1) {
      if (Self.FParams.output!="stdout") {
         WriteLn("Minting begins");
      }
   }
   ,HandleNewKey:function(Self, Sender$2, serial, OK) {
      OK.v = Self.FCache.indexOf(serial)<0;
      if (OK.v) {
         if (THexCustomSerialNumber.Validate$($As(Sender$2,THexIronwoodGenerator),serial)) {
            Self.FCache.push(serial);
            if (Self.FGenerator.FObfuscator!==null) {
               WriteLn(THexObfuscation.Encode$6(Self.FGenerator.FObfuscator,serial));
            } else {
               WriteLn(serial);
            }
         }
      }
   }
   ,MintTo:function(Self, Params) {
      var x$1 = 0;
      var LCount = {v:0},
         LBatch = "";
      if (Params.rootkey=="random") {
         if (Params.output!="stdout") {
            WriteLn("Randomizing seed");
         }
         for(x$1=0;x$1<=11;x$1++) {
            Self.FRoot[x$1] = 1+RandomInt(254);
         }
      }
      if (Params.obfuscation.length>0) {
         {var $temp1 = (Params.obfuscation).toLocaleLowerCase();
            if ($temp1=="none") {
               Self.FGenerator.FObfuscator = null;
            }
             else if ($temp1=="amun") {
               Self.FGenerator.FObfuscator = THexObfuscationTypes.Amun(THexObfuscationTypes);
            }
             else if ($temp1=="sepsephos") {
               Self.FGenerator.FObfuscator = THexObfuscationTypes.Sepsephos(THexObfuscationTypes);
            }
             else if ($temp1=="hebrew") {
               Self.FGenerator.FObfuscator = THexObfuscationTypes.Hebrew(THexObfuscationTypes);
            }
             else if ($temp1=="latin") {
               Self.FGenerator.FObfuscator = THexObfuscationTypes.Latin(THexObfuscationTypes);
            }
             else if ($temp1=="beatus") {
               Self.FGenerator.FObfuscator = THexObfuscationTypes.Latin(THexObfuscationTypes);
            }
             else {
               throw Exception.Create($New(Exception),("Unknown obfuscator: "+Params.obfuscation.toString()));
            }
         }
      }
      {var $temp2 = Trim$_String_((Params.modulator).toLocaleLowerCase());
         if ($temp2=="leonardo") {
            Self.FGenerator.FModulator = THexNumberModulator.Create$82($New(THexLeonardoModulator));
         }
          else if ($temp2=="fibonacci") {
            Self.FGenerator.FModulator = THexNumberModulator.Create$82($New(THexFibonacciModulator));
         }
          else if ($temp2=="lucas") {
            Self.FGenerator.FModulator = THexNumberModulator.Create$82($New(THexLucasModulator));
         }
          else {
            throw Exception.Create($New(Exception),("Unknown modulator: "+Params.modulator.toString()));
         }
      }
      try {
         THexIronWoodSerialNumber.Build(Self.FGenerator,Self.FRoot);
      } catch ($e) {
         var e$1 = $W($e);
         WriteLn("Failed to build partition tables");
         throw $e;
      }
      if (Params.output!="stdout") {
         WriteLn("Partition tables constructed");
      }
      LCount.v = 0;
      LBatch = Trim$_String_(Params.batchsize);
      if (TryStrToInt(LBatch,LCount)) {
         if (LCount.v>0) {
            {var $temp3 = Trim$_String_((Params.method).toLocaleLowerCase());
               if ($temp3=="dispersed") {
                  THexIronwoodGenerator.Mint$1(Self.FGenerator,LCount.v,0)               }
                else if ($temp3=="canonical") {
                  THexIronwoodGenerator.Mint$1(Self.FGenerator,LCount.v,1)               }
                else {
                  throw Exception.Create($New(Exception),"Unknown minting method error: "+Params.method);
               }
            }
         } else {
            throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid batchsize value [%d] error",[LCount.v]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Unknown batchsize value [%s], not a number",[LBatch]);
      }
   }
   ,ParseParams:function(Self) {
      var Result = {batchsize:"",failed_:false,method:"",modulator:"",obfuscation:"",output:"",rootkey:""};
      var LParams = [],
         LFailed = false,
         x$2 = 0;
      var a$122 = 0;
      var Lpar = {v:""},
         LPos = 0,
         LName = "";
      LFailed = false;
      if (ParamCount$1()<3) {
         Result.failed_ = true;
         WriteLn("No parameters, keygen aborted");
         return Result;
      }
      var $temp4;
      for(x$2=2,$temp4=ParamCount$1();x$2<$temp4;x$2++) {
         LParams.push(ParamStr$1(x$2));
      }
      var $temp5;
      for(a$122=0,$temp5=LParams.length;a$122<$temp5;a$122++) {
         Lpar.v = LParams[a$122];
         LPos = (Lpar.v.indexOf("=")+1);
         if (LPos<2) {
            WriteLnF("Syntax error, invalid parameter [%s]",[Lpar.v]);
            Result.failed_ = true;
            break;
         }
         LName = Trim$_String_(Lpar.v.substr(0,(LPos-1)));
         Delete(Lpar,1,LPos);
         {var $temp6 = (LName).toLocaleLowerCase();
            if ($temp6=="--modulator") {
               Result.modulator = Trim$_String_(Lpar.v);
            }
             else if ($temp6=="--method") {
               Result.method = Trim$_String_(Lpar.v);
            }
             else if ($temp6=="--obfuscator") {
               Result.obfuscation = Trim$_String_(Lpar.v);
            }
             else if ($temp6=="--rootkey") {
               Result.rootkey = Trim$_String_(Lpar.v);
            }
             else if ($temp6=="--batch") {
               Result.batchsize = Trim$_String_(Lpar.v);
            }
             else if ($temp6=="--output") {
               Result.output = Trim$_String_(Lpar.v);
            }
             else {
               WriteLnF("Syntax error, unknown option: %s",[LName]);
               Result.failed_ = true;
               break;
            }
         }
      }
      if (Result.failed_) {
         return Result;
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
function Copy$TAppParams(s,d) {
   d.batchsize=s.batchsize;
   d.failed_=s.failed_;
   d.method=s.method;
   d.modulator=s.modulator;
   d.obfuscation=s.obfuscation;
   d.output=s.output;
   d.rootkey=s.rootkey;
   return d;
}
function Clone$TAppParams($) {
   return {
      batchsize:$.batchsize,
      failed_:$.failed_,
      method:$.method,
      modulator:$.modulator,
      obfuscation:$.obfuscation,
      output:$.output,
      rootkey:$.rootkey
   }
}
var THexRange = {
   $ClassName:"THexRange",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHigh = 0;
      $.FLow = 0;
   }
   ,Create$64:function(Self, FromValue, ToValue) {
      TObject.Create(Self);
      THexRange.Define(Self,FromValue,ToValue);
      return Self
   }
   ,Define:function(Self, FromValue$1, ToValue$1) {
      if (FromValue$1<ToValue$1) {
         Self.FLow = FromValue$1;
         Self.FHigh = ToValue$1;
      } else if (FromValue$1>ToValue$1) {
         Self.FLow = ToValue$1;
         Self.FHigh = FromValue$1;
      } else {
         Self.FLow = FromValue$1;
         Self.FHigh = ToValue$1;
      }
   }
   ,GetTop:function(Self) {
      return Self.FHigh+1;
   }
   ,Destroy:TObject.Destroy
};
function WriteLn$1(value) {
   var items$1,
      litem = "",
      LArr = [],
      a$123 = 0;
   var item;
   if (TVariant$1.IsString$1(value)) {
      if (((String(value)).indexOf("\r")+1)>0) {
         items$1 = value.split("\r");
         for (litem in items$1) {
            console.log(litem);
         }
         return;
      }
   } else if (TVariant$1.IsArray(value)) {
      LArr = value;
      var $temp7;
      for(a$123=0,$temp7=LArr.length;a$123<$temp7;a$123++) {
         item = LArr[a$123];
         WriteLn$1(item);
      }
      return;
   }
   console.log(value);
};
var TVariantExportType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
var TVariant$1 = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,CreateObject$1:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   ,ExamineType:function(Value) {
      var Result = 1;
      if (Value) {
         {var $temp8 = (typeof(Value)).toLocaleLowerCase();
            if ($temp8=="object") {
               Result = (Value.hasOwnProperty("length"))?9:8;
            }
             else if ($temp8=="function") {
               Result = 7;
            }
             else if ($temp8=="symbol") {
               Result = 6;
            }
             else if ($temp8=="boolean") {
               Result = 2;
            }
             else if ($temp8=="string") {
               Result = 5;
            }
             else if ($temp8=="number") {
               Result = (!(~(Value%1)))?4:3;
            }
             else if ($temp8=="array") {
               Result = 9;
            }
             else {
               Result = 1;
            }
         }
      } else {
         Result = 1;
      }
      return Result
   }
   ,IsArray:function(aValue) {
      var Result = false;
      if (Array.isArray(aValue) = true) {
      Result = true;
    }
      return Result
   }
   ,IsString$1:function(aValue$1) {
      return typeof(aValue$1)==__TYPE_MAP$1.String$2;
   }
   ,Destroy:TObject.Destroy
};
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
var TString$1 = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,BinaryStrToInt$1:function(BinStr) {
      var Result = {v:0};
      try {
         if (!TString$1.ExamineBinary$1(BinStr,Result)) {
            throw EException.CreateFmt$1($New(EConvertBinaryStringInvalid$1),"Failed to convert binary string (%s)",[BinStr]);
         }
      } finally {return Result.v}
   }
   ,CharCodeFor$1:function(Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   ,DecodeUTF8$1:function(BytesToDecode) {
      var Result = {v:""};
      try {
         var LCodec = null;
         LCodec = TDataTypeConverter$1.Create$65$($New(TQTXCodecUTF8));
         try {
            Result.v = TQTXCodecUTF8.Decode$3(LCodec,BytesToDecode);
         } finally {
            TObject.Free(LCodec);
         }
      } finally {return Result.v}
   }
   ,EncodeUTF8$1:function(TextToEncode) {
      var Result = {v:[]};
      try {
         var LCodec$1 = null;
         LCodec$1 = TDataTypeConverter$1.Create$65$($New(TQTXCodecUTF8));
         try {
            Result.v = TQTXCodecUTF8.Encode$3(LCodec$1,TextToEncode);
         } finally {
            TObject.Free(LCodec$1);
         }
      } finally {return Result.v}
   }
   ,ExamineBinary$1:function(Text$1, value$1) {
      var Result = false;
      var BitIndex = 0,
         x$3 = 0;
      value$1.v = TDataTypeConverter$1.InitUint32$2(0);
      if ((Text$1.charAt(0)=="%")) {
         Text$1 = (Text$1).substring(1);
      } else if ((Text$1.substr(0,2)=="0b")) {
         Text$1 = (Text$1).substring(2);
      }
      if (!TString$1.ValidBinChars$1(Text$1)) {
         Result = false;
         return Result;
      }
      BitIndex = 0;
      for(x$3=Text$1.length;x$3>=1;x$3--) {
         if (Text$1.charAt(x$3-1)=="1") {
            TInteger$1.SetBit$2(BitIndex,true,value$1);
         }
         ++BitIndex;
         if (BitIndex>31) {
            break;
         }
      }
      Result = true;
      return Result
   }
   ,ExamineInteger$1:function(Text$2, Value$1) {
      var Result = false;
      var TextLen = 0,
         Prefix = {v:0};
      Text$2 = Trim$_String_(Text$2);
      TextLen = Text$2.length;
      if (TextLen>0) {
         Prefix.v = 0;
         if (TString$1.ExamineTypePrefix(Text$2,Prefix)) {
            switch (Prefix.v) {
               case 1 :
                  --TextLen;
                  Text$2 = RightStr(Text$2,TextLen);
                  Result = TString$1.ValidHexChars$1(Text$2);
                  if (Result) {
                     Value$1.v = parseInt("0x"+Text$2,16);
                  }
                  break;
               case 2 :
                  (TextLen-= 2);
                  Text$2 = RightStr(Text$2,TextLen);
                  Result = TString$1.ValidHexChars$1(Text$2);
                  if (Result) {
                     Value$1.v = parseInt("0x"+Text$2,16);
                  }
                  break;
               case 3 :
                  --TextLen;
                  Text$2 = RightStr(Text$2,TextLen);
                  Result = TString$1.ValidBinChars$1(Text$2);
                  if (Result) {
                     Value$1.v = TString$1.BinaryStrToInt$1(Text$2);
                  }
                  break;
               case 4 :
                  (TextLen-= 2);
                  Text$2 = RightStr(Text$2,TextLen);
                  Result = TString$1.ValidBinChars$1(Text$2);
                  if (Result) {
                     Value$1.v = TString$1.BinaryStrToInt$1(Text$2);
                  }
                  break;
               case 5 :
                  return Result;
                  break;
               default :
                  Result = TString$1.ValidDecChars$1(Text$2);
                  if (Result) {
                     Value$1.v = parseInt(Text$2,10);
                  }
            }
         } else {
            Result = TString$1.ValidDecChars$1(Text$2);
            if (Result) {
               Value$1.v = parseInt(Text$2,10);
            }
         }
      }
      return Result
   }
   ,ExamineTypePrefix:function(Text$3, Prefix$1) {
      var Result = false;
      Prefix$1.v = 0;
      if (Text$3.length>0) {
         if ((Text$3.charAt(0)=="$")) {
            Prefix$1.v = 1;
         } else if ((Text$3.substr(0,2)=="0x")) {
            Prefix$1.v = 2;
         } else if ((Text$3.charAt(0)=="%")) {
            Prefix$1.v = 3;
         } else if ((Text$3.substr(0,2)=="0b")) {
            Prefix$1.v = 4;
         } else if ((Text$3.charAt(0)=="\"")) {
            Prefix$1.v = 5;
         }
         Result = (Prefix$1.v!=0);
      }
      return Result
   }
   ,FromCharCode$1:function(CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   ,HexStrToInt$1:function(HexStr) {
      var Result = {v:0};
      try {
         if (!TString$1.ExamineInteger$1(HexStr,Result)) {
            throw EException.CreateFmt$1($New(EConvertHexStringInvalid$1),"Failed to convert hex string (%s)",[HexStr]);
         }
      } finally {return Result.v}
   }
   ,ValidBinChars$1:function(Text$4) {
      var Result = false;
      var character = "";
      for (var $temp9=0;$temp9<Text$4.length;$temp9++) {
         character=$uniCharAt(Text$4,$temp9);
         if (!character) continue;
         Result = ((character=="0")||(character=="1"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,ValidDecChars$1:function(Text$5) {
      var Result = false;
      var character$1 = "";
      for (var $temp10=0;$temp10<Text$5.length;$temp10++) {
         character$1=$uniCharAt(Text$5,$temp10);
         if (!character$1) continue;
         Result = ((character$1>="0")&&(character$1<="9"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,ValidHexChars$1:function(Text$6) {
      var Result = false;
      var character$2 = "";
      for (var $temp11=0;$temp11<Text$6.length;$temp11++) {
         character$2=$uniCharAt(Text$6,$temp11);
         if (!character$2) continue;
         Result = (((character$2>="0")&&(character$2<="9"))||((character$2>="a")&&(character$2<="f"))||((character$2>="A")&&(character$2<="F")));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var TJSVMEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
var TJSVMDataType = [ "dtUnknown", "dtBoolean", "dtByte", "dtChar", "dtWord", "dtLong", "dtInt16", "dtInt32", "dtFloat32", "dtFloat64", "dtString" ];
var TInteger$1 = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Diff$1:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   ,EnsureRange$1:function(Value$2, Lowest, Highest) {
      return (Value$2<Lowest)?Lowest:(Value$2>Highest)?Highest:Value$2;
   }
   ,SetBit$2:function(index, Value$3, buffer$1) {
      if (index>=0&&index<=31) {
         if (Value$3) {
            buffer$1.v = buffer$1.v|(1<<index);
         } else {
            buffer$1.v = buffer$1.v&(~(1<<index));
         }
      } else {
         throw Exception.Create($New(EException),$R[24]);
      }
   }
   ,WrapRange$1:function(Value$4, LowRange, HighRange) {
      var Result = 0;
      if (Value$4>HighRange) {
         Result = LowRange+TInteger$1.Diff$1(HighRange,(Value$4-1));
         if (Result>HighRange) {
            Result = TInteger$1.WrapRange$1(Result,LowRange,HighRange);
         }
      } else if (Value$4<LowRange) {
         Result = HighRange-TInteger$1.Diff$1(LowRange,(Value$4+1));
         if (Result<LowRange) {
            Result = TInteger$1.WrapRange$1(Result,LowRange,HighRange);
         }
      } else {
         Result = Value$4;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var TFloat$1 = {
   $ClassName:"TFloat",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Power$4:function(Value$5, Factor) {
      var Result = undefined;
      Result = Math.pow(Value$5,Factor);
      return Result
   }
   ,Destroy:TObject.Destroy
};
var TEnumState = [ "esBreak", "esContinue" ];
var TDataTypeConverter$1 = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer$3 = $.FView$1 = null;
      $.FEndian$1 = 0;
      $.FTyped$1 = null;
   }
   ,BooleanToBytes$2:function(Value$6) {
      var Result = [];
      Result.push((Value$6)?1:0);
      return Result
   }
   ,BytesToBoolean$2:function(Self, Data$2) {
      return Data$2[0]>0;
   }
   ,BytesToFloat32$2:function(Self, Data$3) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$3[0]);
      Self.FView$1.setUint8(1,Data$3[1]);
      Self.FView$1.setUint8(2,Data$3[2]);
      Self.FView$1.setUint8(3,Data$3[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView$1.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getFloat32(0,false);
            break;
      }
      return Result
   }
   ,BytesToFloat64$2:function(Self, Data$4) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$4[0]);
      Self.FView$1.setUint8(1,Data$4[1]);
      Self.FView$1.setUint8(2,Data$4[2]);
      Self.FView$1.setUint8(3,Data$4[3]);
      Self.FView$1.setUint8(4,Data$4[4]);
      Self.FView$1.setUint8(5,Data$4[5]);
      Self.FView$1.setUint8(6,Data$4[6]);
      Self.FView$1.setUint8(7,Data$4[7]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView$1.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getFloat64(0,false);
            break;
      }
      return Result
   }
   ,BytesToInt16$2:function(Self, Data$5) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$5[0]);
      Self.FView$1.setUint8(1,Data$5[1]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getInt16(0);
            break;
         case 1 :
            Result = Self.FView$1.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getInt16(0,false);
            break;
      }
      return Result
   }
   ,BytesToInt32$2:function(Self, Data$6) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$6[0]);
      Self.FView$1.setUint8(1,Data$6[1]);
      Self.FView$1.setUint8(2,Data$6[2]);
      Self.FView$1.setUint8(3,Data$6[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getInt32(0);
            break;
         case 1 :
            Result = Self.FView$1.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getInt32(0,false);
            break;
      }
      return Result
   }
   ,BytesToString$2:function(Self, Data$7) {
      var Result = "";
      var LTemp = null,
         Codec__ = null;
      if (Data$7.length>0) {
         LTemp = new Uint8Array(Data$7.length);
         (LTemp).set(Data$7, 0);
         Codec__ = new TextDecoder("utf8");
         Result = Codec__.decode(LTemp);
         Codec__ = null;
      }
      return Result
   }
   ,BytesToTypedArray$2:function(Self, Values) {
      var Result = undefined;
      var LLen = 0;
      LLen = Values.length;
      Result = new Uint8Array(LLen);
      (Result).set(Values, 0);
      return Result
   }
   ,BytesToUInt16$2:function(Self, Data$8) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$8[0]);
      Self.FView$1.setUint8(1,Data$8[1]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getUint16(0);
            break;
         case 1 :
            Result = Self.FView$1.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getUint16(0,false);
            break;
      }
      return Result
   }
   ,BytesToUInt32$2:function(Self, Data$9) {
      var Result = 0;
      Self.FView$1.setUint8(0,Data$9[0]);
      Self.FView$1.setUint8(1,Data$9[1]);
      Self.FView$1.setUint8(2,Data$9[2]);
      Self.FView$1.setUint8(3,Data$9[3]);
      switch (Self.FEndian$1) {
         case 0 :
            Result = Self.FView$1.getUint32(0);
            break;
         case 1 :
            Result = Self.FView$1.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView$1.getUint32(0,false);
            break;
      }
      return Result
   }
   ,BytesToVariant$2:function(Self, Data$10) {
      var Result = undefined;
      var LType = 0;
      LType = Data$10[0];
      Data$10.shift();
      switch (LType) {
         case 17 :
            Result = TDataTypeConverter$1.BytesToBoolean$2(Self,Data$10);
            break;
         case 18 :
            Result = Data$10[0];
            break;
         case 24 :
            Result = TDataTypeConverter$1.BytesToUInt16$2(Self,Data$10);
            break;
         case 25 :
            Result = TDataTypeConverter$1.BytesToUInt32$2(Self,Data$10);
            break;
         case 19 :
            Result = TDataTypeConverter$1.BytesToInt16$2(Self,Data$10);
            break;
         case 20 :
            Result = TDataTypeConverter$1.BytesToInt32$2(Self,Data$10);
            break;
         case 21 :
            Result = TDataTypeConverter$1.BytesToFloat32$2(Self,Data$10);
            break;
         case 22 :
            Result = TDataTypeConverter$1.BytesToFloat64$2(Self,Data$10);
            break;
         case 23 :
            Result = TString$1.DecodeUTF8$1(Data$10);
            break;
         default :
            throw EException.CreateFmt$1($New(EConvertError$1),$R[25],[IntToHex2(LType)]);
      }
      return Result
   }
   ,CharToByte$2:function(Value$7) {
      var Result = 0;
      Result = (Value$7).charCodeAt(0);
      return Result
   }
   ,Create$65:function(Self) {
      TObject.Create(Self);
      Self.FBuffer$3 = new ArrayBuffer(16);
    Self.FView$1   = new DataView(Self.FBuffer$3);
      Self.FTyped$1 = new Uint8Array(Self.FBuffer$3,0,15);
      return Self
   }
   ,Destroy:function(Self) {
      Self.FTyped$1 = null;
      Self.FView$1 = null;
      Self.FBuffer$3 = null;
      TObject.Destroy(Self);
   }
   ,Float32ToBytes$2:function(Self, Value$8) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setFloat32(0,Value$8);
            break;
         case 1 :
            Self.FView$1.setFloat32(0,Value$8,true);
            break;
         case 2 :
            Self.FView$1.setFloat32(0,Value$8,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   ,Float64ToBytes$2:function(Self, Value$9) {
      var Result = [];
      var LTypeSize = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setFloat64(0,Number(Value$9));
            break;
         case 1 :
            Self.FView$1.setFloat64(0,Number(Value$9),true);
            break;
         case 2 :
            Self.FView$1.setFloat64(0,Number(Value$9),false);
            break;
      }
      LTypeSize = __SIZES$1[9];
      --LTypeSize;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize );
      return Result
   }
   ,InitInt08$2:function(Value$10) {
      var Result = 0;
      var temp = null;
      temp = new Int8Array(1);
      temp[0]=((Value$10<-128)?-128:(Value$10>127)?127:Value$10);
      Result = temp[0];
      return Result
   }
   ,InitInt16$2:function(Value$11) {
      var Result = 0;
      var temp$1 = null;
      temp$1 = new Int16Array(1);
      temp$1[0]=((Value$11<-32768)?-32768:(Value$11>32767)?32767:Value$11);
      Result = temp$1[0];
      return Result
   }
   ,InitInt32$2:function(Value$12) {
      var Result = 0;
      var temp$2 = null;
      temp$2 = new Int32Array(1);
      temp$2[0]=((Value$12<-2147483648)?-2147483648:(Value$12>2147483647)?2147483647:Value$12);
      Result = temp$2[0];
      return Result
   }
   ,InitUint08$2:function(Value$13) {
      var Result = 0;
      var LTemp$1 = null;
      LTemp$1 = new Uint8Array(1);
      LTemp$1[0]=((Value$13<0)?0:(Value$13>255)?255:Value$13);
      Result = LTemp$1[0];
      return Result
   }
   ,InitUint16$2:function(Value$14) {
      var Result = 0;
      var temp$3 = null;
      temp$3 = new Uint16Array(1);
      temp$3[0]=((Value$14<0)?0:(Value$14>65536)?65536:Value$14);
      Result = temp$3[0];
      return Result
   }
   ,InitUint32$2:function(Value$15) {
      var Result = 0;
      var temp$4 = null;
      temp$4 = new Uint32Array(1);
      temp$4[0]=((Value$15<0)?0:(Value$15>4294967295)?4294967295:Value$15);
      Result = temp$4[0];
      return Result
   }
   ,Int16ToBytes$2:function(Self, Value$16) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setInt16(0,Value$16);
            break;
         case 1 :
            Self.FView$1.setInt16(0,Value$16,true);
            break;
         case 2 :
            Self.FView$1.setInt16(0,Value$16,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   ,Int32ToBytes$2:function(Self, Value$17) {
      var Result = [];
      var LTypeSize$1 = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setInt32(0,Value$17);
            break;
         case 1 :
            Self.FView$1.setInt32(0,Value$17,true);
            break;
         case 2 :
            Self.FView$1.setInt32(0,Value$17,false);
            break;
      }
      LTypeSize$1 = __SIZES$1[7];
      --LTypeSize$1;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize$1 );
      return Result
   }
   ,SetEndian$2:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian$1) {
         Self.FEndian$1 = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   ,SizeOfType$2:function(Kind) {
      return __SIZES$1[Kind];
   }
   ,StringToBytes$2:function(Self, Value$18) {
      var Result = [];
      var Codec__$1 = null,
         rw = null;
      if (Value$18.length>0) {
         Codec__$1 = new TextEncoder("utf8");
         rw = Codec__$1.encode(Value$18);
         Codec__$1 = null;
         Result = Array.prototype.slice.call(rw, 0, (rw).byteLength);
         rw = null;
      } else {
         Result = [];
      }
      return Result
   }
   ,TypedArrayToBytes$2:function(Value$19) {
      var Result = [];
      if (Value$19) {
         Result = Array.prototype.slice.call(Value$19);
      } else {
         throw Exception.Create($New(EConvertError$1),"Failed to convert, handle is nil or unassigned error");
      }
      return Result
   }
   ,UInt16ToBytes$2:function(Self, Value$20) {
      var Result = [];
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setUint16(0,Value$20);
            break;
         case 1 :
            Self.FView$1.setUint16(0,Value$20,true);
            break;
         case 2 :
            Self.FView$1.setUint16(0,Value$20,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   ,UInt32ToBytes$2:function(Self, Value$21) {
      var Result = [];
      var LTypeSize$2 = 0;
      switch (Self.FEndian$1) {
         case 0 :
            Self.FView$1.setUint32(0,Value$21);
            break;
         case 1 :
            Self.FView$1.setUint32(0,Value$21,true);
            break;
         case 2 :
            Self.FView$1.setUint32(0,Value$21,false);
            break;
      }
      LTypeSize$2 = __SIZES$1[5];
      --LTypeSize$2;
      Result = Array.prototype.slice.call( (Self).FTyped, 0, LTypeSize$2 );
      return Result
   }
   ,VariantToBytes$2:function(Self, Value$22) {
      var Result = [];
      var LType$1 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$22<=255) {
            return 18;
         }
         if (Value$22<=65536) {
            return 24;
         }
         if (Value$22<=2147483647) {
            Result = 25;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$22>-32768) {
            return 19;
         }
         if (Value$22>-2147483648) {
            Result = 20;
         }
         return Result
      };
      function IsFloat32(x$4) {
         var Result = false;
         Result = isFinite(x$4) && x$4 == Math.fround(x$4);
         return Result
      };
      switch (TVariant$1.ExamineType(Value$22)) {
         case 2 :
            Result = [17].slice();
            Result.pusha(TDataTypeConverter$1.BooleanToBytes$2((Value$22?true:false)));
            break;
         case 3 :
            if (Value$22<0) {
               LType$1 = GetSignedIntType();
            } else {
               LType$1 = GetUnSignedIntType();
            }
            if (LType$1) {
               Result = [LType$1].slice();
               switch (LType$1) {
                  case 18 :
                     Result.push(TDataTypeConverter$1.InitInt08$2(parseInt(Value$22,10)));
                     break;
                  case 24 :
                     Result.pusha(TDataTypeConverter$1.UInt16ToBytes$2(Self,TDataTypeConverter$1.InitUint16$2(parseInt(Value$22,10))));
                     break;
                  case 25 :
                     Result.pusha(TDataTypeConverter$1.UInt32ToBytes$2(Self,TDataTypeConverter$1.InitUint32$2(parseInt(Value$22,10))));
                     break;
                  case 19 :
                     Result.pusha(TDataTypeConverter$1.Int16ToBytes$2(Self,TDataTypeConverter$1.InitInt16$2(parseInt(Value$22,10))));
                     break;
                  case 20 :
                     Result.pusha(TDataTypeConverter$1.Int32ToBytes$2(Self,TDataTypeConverter$1.InitInt32$2(parseInt(Value$22,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EConvertError$1),$R[26]);
            }
            break;
         case 4 :
            if (IsFloat32(Value$22)) {
               Result = [21].slice();
               Result.pusha(TDataTypeConverter$1.Float32ToBytes$2(Self,(Number(Value$22))));
            } else {
               Result = [22].slice();
               Result.pusha(TDataTypeConverter$1.Float64ToBytes$2(Self,(Number(Value$22))));
            }
            break;
         case 5 :
            Result = [23].slice();
            Result.pusha(TString$1.EncodeUTF8$1(String(Value$22)));
            break;
         default :
            throw Exception.Create($New(EConvertError$1),$R[27]);
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$65$:function($){return $.ClassType.Create$65($)}
};
var EException = {
   $ClassName:"EException",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,CreateFmt$1:function(Self, Message$1, Values$1) {
      Exception.Create(Self,Format(Message$1,Values$1.slice(0)));
      return Self
   }
   ,Destroy:Exception.Destroy
};
var EConvertError$1 = {
   $ClassName:"EConvertError",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EConvertHexStringInvalid$1 = {
   $ClassName:"EConvertHexStringInvalid",$Parent:EConvertError$1
   ,$Init:function ($) {
      EConvertError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EConvertBinaryStringInvalid$1 = {
   $ClassName:"EConvertBinaryStringInvalid",$Parent:EConvertError$1
   ,$Init:function ($) {
      EConvertError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function Copy$TTypeLookup(s,d) {
   d.Boolean$1=s.Boolean$1;
   d.Number$2=s.Number$2;
   d.String$2=s.String$2;
   d.Object$3=s.Object$3;
   d.Undefined$1=s.Undefined$1;
   d.Function$2=s.Function$2;
   return d;
}
function Clone$TTypeLookup($) {
   return {
      Boolean$1:$.Boolean$1,
      Number$2:$.Number$2,
      String$2:$.String$2,
      Object$3:$.Object$3,
      Undefined$1:$.Undefined$1,
      Function$2:$.Function$2
   }
}
function InitializeTypeMap() {
   __TYPE_MAP$1.Boolean$1 = typeof(true);
   __TYPE_MAP$1.Number$2 = typeof(0);
   __TYPE_MAP$1.String$2 = typeof("");
   __TYPE_MAP$1.Object$3 = typeof(TVariant$1.CreateObject$1());
   __TYPE_MAP$1.Undefined$1 = typeof(undefined);
   __TYPE_MAP$1.Function$2 = typeof(function () {
      /* null */
   });
};
function InitializeBase64() {
   var i = 0;
   var $temp12;
   for(i=1,$temp12=CNT_B64_CHARSET$1.length;i<=$temp12;i++) {
      __B64_Lookup$1[i-1] = CNT_B64_CHARSET$1.charAt(i-1);
      __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2(CNT_B64_CHARSET$1.charAt(i-1))] = i-1;
   }
   __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2("-")] = 62;
   __B64_RevLookup$1[TDataTypeConverter$1.CharToByte$2("_")] = 63;
};
var THexNumberModulator = {
   $ClassName:"THexNumberModulator",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCache$1 = function () {
         for (var r=[],i=0; i<256; i++) r.push(0);
         return r
      }();
   }
   ,a$121:function(Self, index$1) {
      return Self.FCache$1[index$1];
   }
   ,a$120:function(Self, index$2, Value$23) {
      Self.FCache$1[index$2] = Value$23;
   }
   ,Create$82:function(Self) {
      TObject.Create(Self);
      Self.FCache$1 = THexNumberModulator.BuildNumberSeries$(Self);
      return Self
   }
   ,ToNearest$2:function(Self, Value$24) {
      var Result = 0;
      if (Value$24>1) {
         Result = THexNumberModulator.ToNearest$2$(Self,(Value$24-1))+THexNumberModulator.ToNearest$2$(Self,(Value$24-2));
      } else if (!Value$24) {
         Result = 2;
      } else {
         Result = 1;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,BuildNumberSeries$:function($){return $.ClassType.BuildNumberSeries($)}
   ,ToNearest$2$:function($){return $.ClassType.ToNearest$2.apply($.ClassType, arguments)}
};
var THexLucasModulator = {
   $ClassName:"THexLucasModulator",$Parent:THexNumberModulator
   ,$Init:function ($) {
      THexNumberModulator.$Init($);
   }
   ,BuildNumberSeries:function(Self) {
      var Result = function () {
         for (var r=[],i=0; i<256; i++) r.push(0);
         return r
      }();
      var x$5 = 0;
      var a = 0;
      var b = 0;
      Result[0] = 2;
      Result[1] = 1;
      for(x$5=2;x$5<=255;x$5++) {
         a = Result[x$5-2];
         b = Result[x$5-1];
         Result[x$5] = a+b;
      }
      return Result
   }
   ,DoFindNearest:function(Self, Value$25) {
      var Result = 0;
      var LBestDiff = 0,
         LMatch = 0,
         c = 0;
      var a$124 = 0,
         b$1 = 0,
         LDistance = 0;
      LBestDiff = 2147483647;
      LMatch = -1;
      a$124 = 2;
      b$1 = 1;
      do {
         c = a$124+b$1;
         LDistance = c-Value$25;
         if (LDistance>=0) {
            if (LDistance<LBestDiff) {
               LBestDiff = LDistance;
               LMatch = c;
            }
         }
         a$124 = b$1;
         b$1 = c;
      } while (!(c>Value$25||(LBestDiff==0)));
      if (LMatch>0) {
         Result = LMatch;
      } else {
         Result = Value$25;
      }
      return Result
   }
   ,ToNearest$2:function(Self, Value$26) {
      var Result = 0;
      var LForward = 0;
      var LBackward = 0;
      var LForwardDistance = 0;
      var LBackwardsDistance = 0;
      LForward = THexLucasModulator.DoFindNearest(Self,Value$26);
      LBackward = THexLucasModulator.DoFindNearest(Self,(Value$26-1));
      if (LForward!=LBackward) {
         LForwardDistance = LForward-Value$26;
         LBackwardsDistance = Value$26-LBackward;
         if (LForwardDistance<LBackwardsDistance) {
            Result = LForward;
         } else {
            Result = LBackward;
         }
      } else {
         Result = LForward;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,BuildNumberSeries$:function($){return $.ClassType.BuildNumberSeries($)}
   ,ToNearest$2$:function($){return $.ClassType.ToNearest$2.apply($.ClassType, arguments)}
};
var THexLeonardoModulator = {
   $ClassName:"THexLeonardoModulator",$Parent:THexNumberModulator
   ,$Init:function ($) {
      THexNumberModulator.$Init($);
   }
   ,BuildNumberSeries:function(Self) {
      var Result = function () {
         for (var r=[],i=0; i<256; i++) r.push(0);
         return r
      }();
      var x$6 = 0;
      var a$125 = 0,
         b$2 = 0;
      Result[0] = 1;
      Result[1] = 1;
      for(x$6=2;x$6<=255;x$6++) {
         a$125 = Result[x$6-2];
         b$2 = Result[x$6-1];
         Result[x$6] = a$125+b$2+1;
      }
      return Result
   }
   ,ToNearest$2:function(Self, Value$27) {
      var Result = 0;
      if (Value$27==1) {
         Result = 1;
      } else if (Value$27==2) {
         Result = 1;
      } else {
         Result = THexNumberModulator.ToNearest$2$(Self,(Value$27-1))+THexNumberModulator.ToNearest$2$(Self,(Value$27-2))+1;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,BuildNumberSeries$:function($){return $.ClassType.BuildNumberSeries($)}
   ,ToNearest$2$:function($){return $.ClassType.ToNearest$2.apply($.ClassType, arguments)}
};
var THexFibonacciModulator = {
   $ClassName:"THexFibonacciModulator",$Parent:THexNumberModulator
   ,$Init:function ($) {
      THexNumberModulator.$Init($);
   }
   ,BuildNumberSeries:function(Self) {
      var Result = function () {
         for (var r=[],i=0; i<256; i++) r.push(0);
         return r
      }();
      var x$7 = 0;
      var a$126 = 0,
         b$3 = 0;
      Result[0] = 0;
      Result[1] = 1;
      for(x$7=2;x$7<=255;x$7++) {
         a$126 = Result[x$7-2];
         b$3 = Result[x$7-1];
         Result[x$7] = a$126+b$3;
      }
      return Result
   }
   ,ToNearest$2:function(Self, Value$28) {
      var Result = 0;
      var LForward$1 = 0;
      var LBackward$1 = 0;
      var LForwardDistance$1 = 0;
      var LBackwardsDistance$1 = 0;
      LForward$1 = Math.round(TFloat$1.Power$4(1.61803398874989,Value$28)/2.23606797749979);
      LBackward$1 = Math.round(TFloat$1.Power$4(1.61803398874989,(Value$28-1))/2.23606797749979);
      if (LForward$1!=LBackward$1) {
         LForwardDistance$1 = LForward$1-Value$28;
         LBackwardsDistance$1 = Value$28-LBackward$1;
         if (LForwardDistance$1<LBackwardsDistance$1) {
            Result = LForward$1;
         } else {
            Result = LBackward$1;
         }
      } else {
         Result = LForward$1;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,BuildNumberSeries$:function($){return $.ClassType.BuildNumberSeries($)}
   ,ToNearest$2$:function($){return $.ClassType.ToNearest$2.apply($.ClassType, arguments)}
};
var THexObfuscationTypes = {
   $ClassName:"THexObfuscationTypes",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Sepsephos:function(Self) {
      return THexObfuscation.Create$83($New(THexObfuscationSepsephos));
   }
   ,Hebrew:function(Self) {
      return THexObfuscation.Create$83($New(THexObfuscationHebrew));
   }
   ,Latin:function(Self) {
      return THexObfuscation.Create$83($New(THexObfuscationLatin));
   }
   ,Amun:function(Self) {
      return THexObfuscation.Create$83($New(THexObfuscationAmun));
   }
   ,Destroy:TObject.Destroy
};
var THexObfuscation = {
   $ClassName:"THexObfuscation",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FMatrix = function () {
         for (var r=[],i=0; i<26; i++) r.push(0);
         return r
      }();
   }
   ,Create$83:function(Self) {
      TObject.Create(Self);
      Self.FMatrix = THexObfuscation.GetMatrix$(Self);
      return Self
   }
   ,Encode$6:function(Self, Text$7) {
      var Result = "";
      var xpos = 0;
      var x$8 = 0;
      var LValue = 0;
      var LChar = "";
      Text$7 = (Trim$_String_(Text$7)).toLocaleLowerCase();
      if (Text$7.length>0) {
         var $temp13;
         for(x$8=1,$temp13=Text$7.length;x$8<=$temp13;x$8++) {
            LChar = (Text$7.charAt(x$8-1)).toLocaleLowerCase();
            xpos = ("abcdefghijklmnopqrstuvwxyz".indexOf(LChar)+1);
            if (xpos>0) {
               --xpos;
               LValue = Self.FMatrix[xpos];
               Result+=IntToHex(LValue,4);
            } else {
               Result+="C932"+IntToHex2(TString$1.CharCodeFor$1(LChar));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,GetMatrix$:function($){return $.ClassType.GetMatrix($)}
};
var THexObfuscationSepsephos = {
   $ClassName:"THexObfuscationSepsephos",$Parent:THexObfuscation
   ,$Init:function ($) {
      THexObfuscation.$Init($);
   }
   ,GetMatrix:function(Self) {
      return [1, 2, 3, 4, 5, 6, 7, 8, 10, 100, 10, 20, 30, 40, 50, 3, 70, 80, 200, 300, 400, 6, 80, 60, 10, 800];
   }
   ,Destroy:TObject.Destroy
   ,GetMatrix$:function($){return $.ClassType.GetMatrix($)}
};
var THexObfuscationLatin = {
   $ClassName:"THexObfuscationLatin",$Parent:THexObfuscation
   ,$Init:function ($) {
      THexObfuscation.$Init($);
   }
   ,GetMatrix:function(Self) {
      return [1, 2, 700, 4, 5, 500, 3, 8, 10, 10, 20, 30, 40, 50, 70, 80, 600, 100, 200, 300, 400, 6, 800, 60, 10, 7];
   }
   ,Destroy:TObject.Destroy
   ,GetMatrix$:function($){return $.ClassType.GetMatrix($)}
};
var THexObfuscationHebrew = {
   $ClassName:"THexObfuscationHebrew",$Parent:THexObfuscation
   ,$Init:function ($) {
      THexObfuscation.$Init($);
   }
   ,GetMatrix:function(Self) {
      return [0, 2, 100, 4, 0, 80, 3, 5, 10, 10, 20, 30, 40, 50, 0, 80, 100, 200, 300, 9, 6, 6, 6, 60, 10, 7];
   }
   ,Destroy:TObject.Destroy
   ,GetMatrix$:function($){return $.ClassType.GetMatrix($)}
};
var THexObfuscationAmun = {
   $ClassName:"THexObfuscationAmun",$Parent:THexObfuscation
   ,$Init:function ($) {
      THexObfuscation.$Init($);
   }
   ,GetMatrix:function(Self) {
      return [1, 19, 46, 21, 9, 19, 73, 31, 18, 60, 12, 17, 37, 4, 7, 8, 17, 13, 244, 364, 496, 512, 122, 196, 291, 600];
   }
   ,Destroy:TObject.Destroy
   ,GetMatrix$:function($){return $.ClassType.GetMatrix($)}
};
var THexCustomSerialNumber = {
   $ClassName:"THexCustomSerialNumber",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
   ,Validate$:function($){return $.ClassType.Validate.apply($.ClassType, arguments)}
};
var THexIronWoodSerialNumber = {
   $ClassName:"THexIronWoodSerialNumber",$Parent:THexCustomSerialNumber
   ,$Init:function ($) {
      THexCustomSerialNumber.$Init($);
      $.FGateCount = 0;
      $.FModulator = $.FObfuscator = null;
      $.FPartitions = [];
      $.FRootData = [0,0,0,0,0,0,0,0,0,0,0,0];
   }
   ,Build:function(Self, Rootkey) {
      Self.FRootData = Rootkey.slice(0);
      THexIronWoodSerialNumber.RebuildPartitionTable(Self);
   }
   ,CanUseRootData:function(Self) {
      var Result = false;
      var LFilled = 0,
         LTop = 0,
         x$9 = 0;
      LFilled = 0;
      LTop = 12;
      (LTop-= 2);
      for(x$9=0;x$9<=11;x$9++) {
         if (Self.FRootData[x$9]) {
            ++LFilled;
         }
      }
      Result = LFilled>=LTop;
      return Result
   }
   ,Create$80:function(Self) {
      TObject.Create(Self);
      Self.FGateCount = 64;
      return Self
   }
   ,Destroy:function(Self) {
      var x$10 = 0;
      try {
         var $temp14;
         for(x$10=0,$temp14=Self.FPartitions.length;x$10<$temp14;x$10++) {
            try {
               TObject.Free(Self.FPartitions[x$10]);
            } catch ($e) {
               var e$2 = $W($e);
               /* null */
            }
            Self.FPartitions[x$10]=null;
         }
      } finally {
         Self.FPartitions.length=0;
      }
      TObject.Destroy(Self);
   }
   ,RebuildPartitionTable:function(Self) {
      var x$11 = 0;
      var LPartition = null;
      Self.FPartitions.length=0;
      if (THexIronWoodSerialNumber.CanUseRootData(Self)) {
         for(x$11=1;x$11<=12;x$11++) {
            LPartition = THexIronwoodPartition.Create$81($New(THexIronwoodPartition),Self.FGateCount);
            LPartition.FFormula = Self.FModulator;
            Self.FPartitions.push(LPartition);
            if (Self.FModulator!==null) {
               THexIronwoodPartition.Build$1(LPartition,Self.FRootData[x$11-1]);
            } else {
               throw Exception.Create($New(Exception),"No modulator assigned, failed to rebuild partition-table error");
            }
         }
      } else {
         throw Exception.Create($New(Exception),"Unsuitable root-key, failed to rebuild partition-table error");
      }
   }
   ,Validate:function(Self, SerialNumber) {
      SerialNumber={v:SerialNumber};
      var Result = false;
      var x$12 = 0,
         LVector = 0,
         LLock = 0,
         LBlock = "",
         LRaw = 0;
      SerialNumber.v = StrReplace(SerialNumber.v,"-","");
      if (SerialNumber.v.length==(Self.FPartitions.length*2)) {
         x$12 = 1;
         LVector = 0;
         LLock = 0;
         while (SerialNumber.v.length>0) {
            LBlock = "$"+SerialNumber.v.substr(0,2);
            Delete(SerialNumber,1,2);
            (x$12+= 2);
            LRaw = TInteger$1.EnsureRange$1(TString$1.HexStrToInt$1(LBlock),0,255);
            if (THexIronwoodPartition.Valid$3(Self.FPartitions[LVector],LRaw)) {
               ++LLock;
            }
            ++LVector;
         }
         Result = LLock==Self.FPartitions.length;
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Validate$:function($){return $.ClassType.Validate.apply($.ClassType, arguments)}
};
var THexIronwoodPartition = {
   $ClassName:"THexIronwoodPartition",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FFormula = $.FRange = null;
      $.FGates = [];
      $.FGateTotal = 0;
   }
   ,a$118:function(Self) {
      return Self.FGates.length;
   }
   ,Build$1:function(Self, RootKeyValue) {
      var x$13 = 0;
      var LValue$1 = 0;
      Self.FGates.length=0;
      if (Self.FFormula!==null) {
         x$13 = 1;
         while (Self.FGates.length<Self.FGateTotal) {
            LValue$1 = (RootKeyValue+THexNumberModulator.a$121(Self.FFormula,x$13)-x$13)%THexRange.GetTop(Self.FRange);
            if (Self.FGates.indexOf(LValue$1)<0) {
               Self.FGates.push(LValue$1);
            }
            ++x$13;
            if (x$13>255) {
               x$13 = 1;
            }
         }
      }
   }
   ,Create$81:function(Self, PartitionGateCount) {
      TObject.Create(Self);
      Self.FRange = THexRange.Create$64($New(THexRange),0,255);
      Self.FGateTotal = TInteger$1.EnsureRange$1(PartitionGateCount,16,128);
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FRange);
      TObject.Destroy(Self);
   }
   ,GetGate:function(Self, Index$1) {
      return Self.FGates[Index$1];
   }
   ,SetGate:function(Self, Index$2, Value$29) {
      Self.FGates[Index$2]=Value$29;
   }
   ,ToString$5:function(Self) {
      var Result = "";
      var x$14 = 0;
      var $temp15;
      for(x$14=0,$temp15=Self.FGates.length;x$14<$temp15;x$14++) {
         if (x$14<(Self.FGates.length-1)) {
            Result+=(IntToHex2(Self.FGates[x$14])).toUpperCase()+"-";
         } else {
            Result+=(IntToHex2(Self.FGates[x$14])).toUpperCase();
         }
      }
      return Result
   }
   ,Valid$3:function(Self, Data$11) {
      var Result = false;
      if (Self.FGates.length>0) {
         Result = Self.FGates.indexOf(Data$11)>=0;
      } else {
         Result = false;
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var THexSerialGenerateMethod = [ "gmDispersed", "gmCanonical" ];
var THexIronwoodGenerator = {
   $ClassName:"THexIronwoodGenerator",$Parent:THexIronWoodSerialNumber
   ,$Init:function ($) {
      THexIronWoodSerialNumber.$Init($);
      $.FMethod = 0;
      $.FOnAcceptSerial = null;
      $.FOnAfterMint = null;
      $.FOnBeforeMint = null;
   }
   ,Deliver:function(Self, SerialNumber$1) {
      var Result = {v:false};
      try {
         Result.v = false;
         if (Self.FOnAcceptSerial) {
            Self.FOnAcceptSerial(Self,SerialNumber$1,Result);
         }
      } finally {return Result.v}
   }
   ,DoCanonical:function(Self, Count$2) {
      var LAtrophy = 0,
         LText = "",
         x$15 = 0;
      var LPartition$1 = null,
         id$2 = 0;
      while (Count$2>0) {
         LAtrophy = 0;
         LText = "";
         var $temp16;
         for(x$15=0,$temp16=Self.FPartitions.length;x$15<$temp16;x$15++) {
            LPartition$1 = Self.FPartitions[x$15];
            id$2 = RandomInt(THexIronwoodPartition.a$118(LPartition$1));
            LText+=_hex_char_lut[THexIronwoodPartition.GetGate(LPartition$1,id$2)];
            if (x$15%4==3&&x$15<Self.FPartitions.length-1) {
               LText+="-";
            }
         }
         if (THexIronwoodGenerator.Deliver(Self,(LText).toLocaleUpperCase())) {
            --Count$2;
         } else {
            ++LAtrophy;
         }
      }
   }
   ,DoDispersed:function(Self, Count$3) {
      var LAtrophy$1 = 0,
         LPartition$2 = null;
      var LText$1 = "",
         x$16 = 0;
      var id$3 = 0;
      LAtrophy$1 = 0;
      while (Count$3>0) {
         LText$1 = "";
         var $temp17;
         for(x$16=0,$temp17=Self.FPartitions.length;x$16<$temp17;x$16++) {
            LPartition$2 = Self.FPartitions[x$16];
            id$3 = RandomInt(THexIronwoodPartition.a$118(LPartition$2));
            LText$1+=_hex_char_lut[THexIronwoodPartition.GetGate(LPartition$2,id$3)];
            if (x$16%4==3&&x$16<Self.FPartitions.length-1) {
               LText$1+="-";
            }
         }
         if (THexIronwoodGenerator.Deliver(Self,(LText$1).toLocaleUpperCase())) {
            --Count$3;
         } else {
            ++LAtrophy$1;
         }
      }
   }
   ,Mint$1:function(Self, Count$4, Method) {
      var LOld = 0;
      LOld = Self.FMethod;
      try {
         THexIronwoodGenerator.Mint(Self,Count$4);
      } finally {
         Self.FMethod = LOld;
      }
   }
   ,Mint:function(Self, Count$5) {
      if (Self.FOnBeforeMint) {
         Self.FOnBeforeMint(Self);
      }
      try {
         switch (Self.FMethod) {
            case 0 :
               THexIronwoodGenerator.DoDispersed(Self,Count$5);
               break;
            case 1 :
               THexIronwoodGenerator.DoCanonical(Self,Count$5);
               break;
         }
      } finally {
         if (Self.FOnAfterMint) {
            Self.FOnAfterMint(Self);
         }
      }
   }
   ,Destroy:THexIronWoodSerialNumber.Destroy
   ,Validate:THexIronWoodSerialNumber.Validate
};
function TW3VariantHelper$DataType(Self$1) {
   var Result = 1;
   var LType$2 = "";
   if (TW3VariantHelper$Valid$4(Self$1)) {
      LType$2 = typeof(Self$1);
      {var $temp18 = (LType$2).toLocaleLowerCase();
         if ($temp18=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp18=="function") {
            Result = 7;
         }
          else if ($temp18=="symbol") {
            Result = 6;
         }
          else if ($temp18=="boolean") {
            Result = 2;
         }
          else if ($temp18=="string") {
            Result = 5;
         }
          else if ($temp18=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp18=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
function TW3VariantHelper$Valid$4(Self$2) {
   var Result = false;
   Result = !( (Self$2 == undefined) || (Self$2 == null) );
   return Result
}
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   ,Create$16:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   ,ValidRef:function(aValue$2) {
      return aValue$2!=undefined&&aValue$2!=null;
   }
   ,Destroy:TObject.Destroy
};
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
function TStringHelper$ContainsHex(Self$3) {
   var Result = false;
   var x$17 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen$1 = 0;
   Result = false;
   LLen$1 = Self$3.length;
   if (LLen$1>=1) {
      LStart = 1;
      if (Self$3.charAt(0)=="$") {
         ++LStart;
         --LLen$1;
      } else {
         LItem = (Self$3.substr(0,1)).toLocaleUpperCase();
         Result = ($R[23].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen$1>=1) {
         var $temp19;
         for(x$17=LStart,$temp19=Self$3.length;x$17<=$temp19;x$17++) {
            LItem = (Self$3.charAt(x$17-1)).toLocaleUpperCase();
            Result = ($R[23].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
function TStringHelper$ContainsOrdinal(Self$4) {
   var Result = false;
   var LLen$2 = 0,
      x$18 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$2 = Self$4.length;
   if (LLen$2>=1) {
      var $temp20;
      for(x$18=1,$temp20=LLen$2;x$18<=$temp20;x$18++) {
         LItem$1 = Self$4.charAt(x$18-1);
         Result = ($R[22].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
function TStringHelper$ContainsFloat(Self$5) {
   var Result = false;
   var x$19 = 0;
   var LItem$2 = "";
   var LLen$3 = 0;
   var LLine = false;
   Result = false;
   LLen$3 = Self$5.length;
   if (LLen$3>=1) {
      LLine = false;
      var $temp21;
      for(x$19=1,$temp21=LLen$3;x$19<=$temp21;x$19++) {
         LItem$2 = Self$5.charAt(x$19-1);
         if (LItem$2==".") {
            if (x$19==1&&LLen$3==1) {
               break;
            }
            if (x$19==1&&LLen$3>1) {
               LLine = true;
               continue;
            }
            if (x$19>1&&x$19<LLen$3) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
function TStringHelper$ContainsQuote(Self$6) {
   var Result = false;
   var LLen$4 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$4 = Self$6.length;
   if (LLen$4>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$4) {
         if (Self$6.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$6.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$6.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$4) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$4) {
         if (Self$6.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$6.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$4) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$4) {
         if (Self$6.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,BinaryStrToInt:function(BinStr$1) {
      var Result = {v:0};
      try {
         if (!TString.ExamineBinary(BinStr$1,Result)) {
            throw EW3Exception.CreateFmt($New(EConvertBinaryStringInvalid),"Failed to convert binary string (%s)",[BinStr$1]);
         }
      } finally {return Result.v}
   }
   ,CharCodeFor:function(Self, Character$1) {
      var Result = 0;
      Result = (Character$1).charCodeAt(0);
      return Result
   }
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   ,DecodeUTF8:function(Self, BytesToDecode$1) {
      var Result = "";
      var LCodec$2 = null;
      LCodec$2 = TCustomCodec.Create$28($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Decode(LCodec$2,BytesToDecode$1);
      } finally {
         TObject.Free(LCodec$2);
      }
      return Result
   }
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = {v:[]};
      try {
         var LCodec$3 = null;
         LCodec$3 = TCustomCodec.Create$28($New(TUTF8Codec));
         try {
            Result.v = TUTF8Codec.Encode(LCodec$3,TextToEncode$1);
         } finally {
            TObject.Free(LCodec$3);
         }
      } finally {return Result.v}
   }
   ,ExamineBinary:function(Text$8, value$2) {
      var Result = false;
      var BitIndex$1 = 0,
         x$20 = 0;
      value$2.v = TDataTypeConverter.InitUint32(0);
      if ((Text$8.charAt(0)=="%")) {
         Text$8 = (Text$8).substring(1);
      } else if ((Text$8.substr(0,2)=="0b")) {
         Text$8 = (Text$8).substring(2);
      }
      if (!TString.ValidBinChars(Text$8)) {
         Result = false;
         return Result;
      }
      BitIndex$1 = 0;
      for(x$20=Text$8.length;x$20>=1;x$20--) {
         if (Text$8.charAt(x$20-1)=="1") {
            TInteger.SetBit(BitIndex$1,true,value$2);
         }
         ++BitIndex$1;
         if (BitIndex$1>31) {
            break;
         }
      }
      Result = true;
      return Result
   }
   ,ExamineInteger:function(Text$9, Value$30) {
      var Result = false;
      var TextLen$1 = 0,
         Prefix$2 = 0;
      Text$9 = Trim$_String_(Text$9);
      TextLen$1 = Text$9.length;
      if (TextLen$1>0) {
         Prefix$2 = TString.ExaminePrefixType(Text$9);
         if (Prefix$2) {
            switch (Prefix$2) {
               case 1 :
                  --TextLen$1;
                  Text$9 = RightStr(Text$9,TextLen$1);
                  Result = TString.ValidHexChars(Text$9);
                  if (Result) {
                     Value$30.v = parseInt("0x"+Text$9,16);
                  }
                  break;
               case 2 :
                  (TextLen$1-= 2);
                  Text$9 = RightStr(Text$9,TextLen$1);
                  Result = TString.ValidHexChars(Text$9);
                  if (Result) {
                     Value$30.v = parseInt("0x"+Text$9,16);
                  }
                  break;
               case 3 :
                  --TextLen$1;
                  Text$9 = RightStr(Text$9,TextLen$1);
                  Result = TString.ValidBinChars(Text$9);
                  if (Result) {
                     Value$30.v = TString.BinaryStrToInt(Text$9);
                  }
                  break;
               case 4 :
                  (TextLen$1-= 2);
                  Text$9 = RightStr(Text$9,TextLen$1);
                  Result = TString.ValidBinChars(Text$9);
                  if (Result) {
                     Value$30.v = TString.BinaryStrToInt(Text$9);
                  }
                  break;
               case 5 :
                  return Result;
                  break;
               default :
                  Result = TString.ValidDecChars(Text$9);
                  if (Result) {
                     Value$30.v = parseInt(Text$9,10);
                  }
            }
         } else {
            Result = TString.ValidDecChars(Text$9);
            if (Result) {
               Value$30.v = parseInt(Text$9,10);
            }
         }
      }
      return Result
   }
   ,ExaminePrefixType:function(Text$10) {
      var Result = 0;
      Result = 0;
      if (Text$10.length>0) {
         if ((Text$10.charAt(0)=="$")) {
            Result = 1;
         } else if ((Text$10.substr(0,2)=="0x")) {
            Result = 2;
         } else if ((Text$10.charAt(0)=="%")) {
            Result = 3;
         } else if ((Text$10.substr(0,2)=="0b")) {
            Result = 4;
         } else if ((Text$10.charAt(0)=="\"")) {
            Result = 5;
         }
      }
      return Result
   }
   ,FromCharCode:function(Self, CharCode$1) {
      var Result = "";
      Result = String.fromCharCode(CharCode$1);
      return Result
   }
   ,ValidBinChars:function(Text$11) {
      var Result = false;
      var character$3 = "";
      for (var $temp22=0;$temp22<Text$11.length;$temp22++) {
         character$3=$uniCharAt(Text$11,$temp22);
         if (!character$3) continue;
         Result = ((character$3=="0")||(character$3=="1"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,ValidDecChars:function(Text$12) {
      var Result = false;
      var character$4 = "";
      for (var $temp23=0;$temp23<Text$12.length;$temp23++) {
         character$4=$uniCharAt(Text$12,$temp23);
         if (!character$4) continue;
         Result = ((character$4>="0")&&(character$4<="9"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,ValidHexChars:function(Text$13) {
      var Result = false;
      var character$5 = "";
      for (var $temp24=0;$temp24<Text$13.length;$temp24++) {
         character$5=$uniCharAt(Text$13,$temp24);
         if (!character$5) continue;
         Result = (((character$5>="0")&&(character$5<="9"))||((character$5>="a")&&(character$5<="f"))||((character$5>="A")&&(character$5<="F")));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Diff:function(Primary$1, Secondary$1) {
      var Result = 0;
      if (Primary$1!=Secondary$1) {
         if (Primary$1>Secondary$1) {
            Result = Primary$1-Secondary$1;
         } else {
            Result = Secondary$1-Primary$1;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   ,EnsureRange:function(aValue$3, aMin, aMax) {
      return ClampInt(aValue$3,aMin,aMax);
   }
   ,SetBit:function(index$3, aValue$4, buffer$2) {
      if (index$3>=0&&index$3<=31) {
         if (aValue$4) {
            buffer$2.v = buffer$2.v|(1<<index$3);
         } else {
            buffer$2.v = buffer$2.v&(~(1<<index$3));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   ,SubtractSmallest:function(First, Second) {
      var Result = 0;
      if (First<Second) {
         Result = Second-First;
      } else {
         Result = First-Second;
      }
      return Result
   }
   ,ToNearest:function(Value$31, Factor$1) {
      var Result = 0;
      var FTemp = 0;
      Result = Value$31;
      FTemp = Value$31%Factor$1;
      if (FTemp>0) {
         (Result+= (Factor$1-FTemp));
      }
      return Result
   }
   ,WrapRange:function(aValue$5, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$5>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$5-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$5<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$5+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$5;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
var TEnumState$1 = { 1:"esContinue", 0:"esAbort" };
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   ,Create$27:function(Self, MethodName, Instance, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance)?TObject.ClassName(Instance.ClassType):"Anonymous";
      EW3Exception.CreateFmt(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
};
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
var TValuePrefixType$1 = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
function TryStrToInt(Data$12, Value$32) {
   return TString.ExamineInteger(Data$12,Value$32);
};
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   ,BooleanToBytes:function(Self, Value$33) {
      var Result = [];
      Result.push((Value$33)?1:0);
      return Result
   }
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   ,BytesToBoolean:function(Self, Data$13) {
      return Data$13[0]>0;
   }
   ,BytesToFloat32:function(Self, Data$14) {
      var Result = 0;
      Self.FView.setUint8(0,Data$14[0]);
      Self.FView.setUint8(1,Data$14[1]);
      Self.FView.setUint8(2,Data$14[2]);
      Self.FView.setUint8(3,Data$14[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   ,BytesToFloat64:function(Self, Data$15) {
      var Result = 0;
      Self.FView.setUint8(0,Data$15[0]);
      Self.FView.setUint8(1,Data$15[1]);
      Self.FView.setUint8(2,Data$15[2]);
      Self.FView.setUint8(3,Data$15[3]);
      Self.FView.setUint8(4,Data$15[4]);
      Self.FView.setUint8(5,Data$15[5]);
      Self.FView.setUint8(6,Data$15[6]);
      Self.FView.setUint8(7,Data$15[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   ,BytesToInt16:function(Self, Data$16) {
      var Result = 0;
      Self.FView.setUint8(0,Data$16[0]);
      Self.FView.setUint8(1,Data$16[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   ,BytesToInt32:function(Self, Data$17) {
      var Result = 0;
      Self.FView.setUint8(0,Data$17[0]);
      Self.FView.setUint8(1,Data$17[1]);
      Self.FView.setUint8(2,Data$17[2]);
      Self.FView.setUint8(3,Data$17[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   ,BytesToString:function(Self, Data$18) {
      var Result = "";
      Result = String.fromCharCode.apply(String, Data$18);
      return Result
   }
   ,BytesToTypedArray:function(Self, Values$2) {
      var Result = undefined;
      var LLen$5 = 0;
      LLen$5 = Values$2.length;
      Result = new Uint8Array(LLen$5);
      (Result).set(Values$2, 0);
      return Result
   }
   ,BytesToUInt16:function(Self, Data$19) {
      var Result = 0;
      Self.FView.setUint8(0,Data$19[0]);
      Self.FView.setUint8(1,Data$19[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   ,BytesToUInt32:function(Self, Data$20) {
      var Result = 0;
      Self.FView.setUint8(0,Data$20[0]);
      Self.FView.setUint8(1,Data$20[1]);
      Self.FView.setUint8(2,Data$20[2]);
      Self.FView.setUint8(3,Data$20[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   ,BytesToVariant:function(Self, Data$21) {
      var Result = undefined;
      var LType$3 = 0;
      LType$3 = Data$21[0];
      Data$21.shift();
      switch (LType$3) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$21);
            break;
         case 162 :
            Result = Data$21[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$21);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$21);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$21);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$21);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$21);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$21);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$21);
            break;
         default :
            throw EW3Exception.CreateFmt($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$3)]);
      }
      return Result
   }
   ,ByteToChar:function(Self, Value$34) {
      var Result = "";
      Result = String.fromCharCode(Value$34);
      return Result
   }
   ,CharToByte:function(Self, Value$35) {
      var Result = 0;
      Result = (Value$35).charCodeAt(0);
      return Result
   }
   ,Create$15:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   ,Float32ToBytes:function(Self, Value$36) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$36);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$36,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$36,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   ,Float64ToBytes:function(Self, Value$37) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$37));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$37),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$37),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   ,InitFloat32:function(Value$38) {
      var Result = 0;
      var temp$5 = null;
      temp$5 = new Float32Array(1);
      temp$5[0]=Value$38;
      Result = temp$5[0];
      temp$5 = null;
      return Result
   }
   ,InitFloat64:function(Value$39) {
      var Result = undefined;
      var temp$6 = null;
      temp$6 = new Float64Array(1);
      temp$6[0]=(Number(Value$39));
      Result = temp$6[0];
      temp$6 = null;
      return Result
   }
   ,InitInt08:function(Value$40) {
      var Result = 0;
      var temp$7 = null;
      temp$7 = new Int8Array(1);
      temp$7[0]=((Value$40<-128)?-128:(Value$40>127)?127:Value$40);
      Result = temp$7[0];
      temp$7 = null;
      return Result
   }
   ,InitInt16:function(Value$41) {
      var Result = 0;
      var temp$8 = null;
      temp$8 = new Int16Array(1);
      temp$8[0]=((Value$41<-32768)?-32768:(Value$41>32767)?32767:Value$41);
      Result = temp$8[0];
      temp$8 = null;
      return Result
   }
   ,InitInt32:function(Value$42) {
      var Result = 0;
      var temp$9 = null;
      temp$9 = new Int32Array(1);
      temp$9[0]=((Value$42<-2147483648)?-2147483648:(Value$42>2147483647)?2147483647:Value$42);
      Result = temp$9[0];
      temp$9 = null;
      return Result
   }
   ,InitUint08:function(Value$43) {
      var Result = 0;
      var LTemp$2 = null;
      LTemp$2 = new Uint8Array(1);
      LTemp$2[0]=((Value$43<0)?0:(Value$43>255)?255:Value$43);
      Result = LTemp$2[0];
      LTemp$2 = null;
      return Result
   }
   ,InitUint16:function(Value$44) {
      var Result = 0;
      var temp$10 = null;
      temp$10 = new Uint16Array(1);
      temp$10[0]=((Value$44<0)?0:(Value$44>65536)?65536:Value$44);
      Result = temp$10[0];
      temp$10 = null;
      return Result
   }
   ,InitUint32:function(Value$45) {
      var Result = 0;
      var temp$11 = null;
      temp$11 = new Uint32Array(1);
      temp$11[0]=((Value$45<0)?0:(Value$45>4294967295)?4294967295:Value$45);
      Result = temp$11[0];
      temp$11 = null;
      return Result
   }
   ,Int16ToBytes:function(Self, Value$46) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$46);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$46,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$46,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   ,Int32ToBytes:function(Self, Value$47) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$47);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$47,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$47,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   ,SetEndian:function(Self, NewEndian$1) {
      if (NewEndian$1!=Self.FEndian) {
         Self.FEndian = NewEndian$1;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   ,SizeOfType:function(Self, Kind$1) {
      return __SIZES[Kind$1];
   }
   ,StringToBytes:function(Self, Value$48) {
      var Result = [];
      Result = (Value$48).split("").map( function( val ) {
        return val.charCodeAt( 0 );
    } );
      return Result
   }
   ,TypedArrayToBytes:function(Self, Value$49) {
      var Result = [];
      if (!TVariant.ValidRef(Value$49)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[16]);
      }
      Result = Array.prototype.slice.call(Value$49);
      return Result
   }
   ,TypedArrayToStr:function(Self, Value$50) {
      var Result = "";
      var x$21 = 0;
      if (TVariant.ValidRef(Value$50)) {
         if (Value$50.length>0) {
            var $temp25;
            for(x$21=0,$temp25=Value$50.length-1;x$21<=$temp25;x$21++) {
               Result += String.fromCharCode((Value$50)[x$21]);
            }
         }
      }
      return Result
   }
   ,TypedArrayToUInt32:function(Self, Value$51) {
      var Result = 0;
      var LBuffer = null,
         LBytes = 0,
         LTypeSize$3 = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$51)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[16]);
      }
      LBuffer = Value$51.buffer;
      LBytes = LBuffer.byteLength;
      LTypeSize$3 = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes<LTypeSize$3) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[15],["Int32", LTypeSize$3, LBytes]));
      }
      if (LBytes>LTypeSize$3) {
         LBytes = LTypeSize$3;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getUint32(0);
            break;
         case 1 :
            Result = LView.getUint32(0,true);
            break;
         case 2 :
            Result = LView.getUint32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   ,UInt16ToBytes:function(Self, Value$52) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$52);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$52,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$52,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   ,UInt32ToBytes:function(Self, Value$53) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$53);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$53,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$53,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   ,VariantToBytes:function(Self, Value$54) {
      var Result = [];
      var LType$4 = 0;
      function GetUnSignedIntType$1() {
         var Result = 0;
         if (Value$54<=255) {
            return 162;
         }
         if (Value$54<=65536) {
            return 168;
         }
         if (Value$54<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType$1() {
         var Result = 0;
         if (Value$54>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$54>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32$1(x$22) {
         var Result = false;
         Result = isFinite(x$22) && x$22 == Math.fround(x$22);
         return Result
      };
      switch (TW3VariantHelper$DataType(Value$54)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$54?true:false)));
            break;
         case 3 :
            if (Value$54<0) {
               LType$4 = GetSignedIntType$1();
            } else {
               LType$4 = GetUnSignedIntType$1();
            }
            if (LType$4) {
               Result = [LType$4].slice();
               switch (LType$4) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$54,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$54,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$54,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$54,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$54,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32$1(Value$54)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$54))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$54))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$54)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
};
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,BytesToString$1:function(Self, Data$22) {
      return TDataTypeConverter.BytesToString(__Def_Converter,Data$22);
   }
   ,BytesToTypedArray$1:function(Self, Values$3) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$3);
   }
   ,StringToBytes$1:function(Self, Value$55) {
      return TDataTypeConverter.StringToBytes(__Def_Converter,Value$55);
   }
   ,TypedArrayToBytes$1:function(Self, Value$56) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$56);
   }
   ,TypedArrayToUInt32$1:function(Self, Value$57) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$57);
   }
   ,Destroy:TObject.Destroy
};
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Base64ToBytes$2:function(Self, b64) {
      var Result = [];
      var ASeg = 0;
      var BSeg = 0;
      var CSeg = 0;
      var DSeg = 0;
      var LTextLen = 0,
         LPlaceholderCount = 0,
         BufferSize = 0,
         xpos$1 = 0,
         idx = 0,
         temp$12 = 0,
         temp$13 = 0,
         temp$14 = 0;
      LTextLen = b64.length;
      if (LTextLen>0) {
         LPlaceholderCount = 0;
         if (LTextLen%4<1) {
            LPlaceholderCount = (b64.charAt((LTextLen-1)-1)=="=")?2:(b64.charAt(LTextLen-1)=="=")?1:0;
         }
         BufferSize = ($Div(LTextLen*3,4))-LPlaceholderCount;
         $ArraySetLenC(Result,BufferSize,function (){return 0});
         if (LPlaceholderCount>0) {
            (LTextLen-= 4);
         }
         xpos$1 = 1;
         idx = 0;
         while (xpos$1<LTextLen) {
            ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<18;
            BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]<<12;
            CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+1))]<<6;
            DSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+2))];
            temp$12 = ((ASeg|BSeg)|CSeg)|DSeg;
            Result[idx]=(temp$12>>>16)&255;
            ++idx;
            Result[idx]=(temp$12>>>8)&255;
            ++idx;
            Result[idx]=temp$12&255;
            ++idx;
            (xpos$1+= 4);
         }
         switch (LPlaceholderCount) {
            case 1 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<2;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]>>>4;
               temp$13 = ASeg|BSeg;
               Result[idx]=temp$13&255;
               break;
            case 2 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<10;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]<<4;
               CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+1))]>>>2;
               temp$14 = (ASeg|BSeg)|CSeg;
               Result[idx]=(temp$14>>>8)&255;
               ++idx;
               Result[idx]=temp$14&255;
               break;
         }
      }
      return Result
   }
   ,BytesToBase64$2:function(Self, Data$23) {
      var Result = "";
      var LLen$6 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i$1 = 0,
         Ahead = 0,
         SegSize = 0,
         output$1 = "",
         LTemp$3 = 0,
         LTemp$4 = 0;
      LLen$6 = Data$23.length;
      if (LLen$6>0) {
         LExtra = Data$23.length%3;
         LStrideLen = LLen$6-LExtra;
         LMaxChunkLength = 16383;
         i$1 = 0;
         while (i$1<LStrideLen) {
            Ahead = i$1+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$23,i$1,SegSize);
            (i$1+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$6;
         }
         output$1 = "";
         switch (LExtra) {
            case 1 :
               LTemp$3 = Data$23[LLen$6];
               output$1+=__B64_Lookup[LTemp$3>>>2];
               output$1+=__B64_Lookup[(LTemp$3<<4)&63];
               output$1+="==";
               break;
            case 2 :
               LTemp$4 = (Data$23[LLen$6-1]<<8)+Data$23[LLen$6];
               output$1+=__B64_Lookup[LTemp$4>>>10];
               output$1+=__B64_Lookup[(LTemp$4>>>4)&63];
               output$1+=__B64_Lookup[(LTemp$4<<2)&63];
               output$1+="=";
               break;
         }
         Result+=output$1;
      }
      return Result
   }
   ,EncodeChunk:function(Self, Data$24, startpos, endpos) {
      var Result = "";
      var temp$15 = 0;
      while (startpos<endpos) {
         temp$15 = (Data$24[startpos]<<16)+(Data$24[startpos+1]<<8)+Data$24[startpos+2];
         Result+=__B64_Lookup[(temp$15>>>18)&63]+__B64_Lookup[(temp$15>>>12)&63]+__B64_Lookup[(temp$15>>>6)&63]+__B64_Lookup[temp$15&63];
         (startpos+= 3);
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EConvertBinaryStringInvalid = {
   $ClassName:"EConvertBinaryStringInvalid",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e$3 = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$2 = 0;
   var $temp26;
   for(i$2=1,$temp26=CNT_B64_CHARSET.length;i$2<=$temp26;i$2++) {
      __B64_Lookup[i$2-1] = CNT_B64_CHARSET.charAt(i$2-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$2-1))] = i$2-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,AllocMemA:function(Self, Size$9) {
      var Result = undefined;
      if (Size$9>0) {
         Result = new Uint8Array(Size$9);
      } else {
         Result = null;
      }
      return Result
   }
   ,ReAllocMemA:function(Self, Memory$1, Size$10) {
      var Result = undefined;
      if (Memory$1) {
         if (Size$10>0) {
            Result = new Uint8Array(Size$10);
            TMarshal.Move$1(TMarshal,Memory$1,0,Result,0,Size$10);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$10);
      }
      return Result
   }
   ,ReadMemoryA:function(Self, Memory$2, Offset, Size$11) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$2&&Offset>=0) {
         LTotal = Offset+Size$11;
         if (LTotal>Memory$2.length) {
            Size$11 = parseInt((Memory$2.length-LTotal),10);
         }
         if (Size$11>0) {
            Result = new Uint8Array(Memory$2.buffer.slice(Offset,Size$11));
         }
      }
      return Result
   }
   ,WriteMemoryA:function(Self, Memory$3, Offset$1, Data$25) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$3) {
         if (Data$25) {
            mTotal = Offset$1+Data$25.length;
            if (mTotal>Memory$3.length) {
               Result = parseInt((Memory$3.length-mTotal),10);
            } else {
               Result = parseInt(Data$25.length,10);
            }
            if (Result>0) {
               if (Offset$1+Data$25.length<=Memory$3.length) {
                  Memory$3.set(Data$25,Offset$1);
               } else {
                  mChunk = Data$25.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$3.set(mTemp,Offset$1);
               }
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function TMemoryHandleHelper$Valid$5(Self$7) {
   var Result = false;
   Result = !( (Self$7 == undefined) || (Self$7 == null) );
   return Result
}
function TMemoryHandleHelper$Defined(Self$8) {
   var Result = false;
   Result = !(Self$8 == undefined);
   return Result
}
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Move$1:function(Self, Source, SourceStart, Target, TargetStart, Size$12) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined(Source)&&TMemoryHandleHelper$Valid$5(Source)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined(Target)&&TMemoryHandleHelper$Valid$5(Target)&&TargetStart>=0) {
            LRef = Source.subarray(SourceStart,SourceStart+Size$12);
            Target.set(LRef,TargetStart);
         }
      }
   }
   ,Move:function(Self, Source$1, Target$1, Size$13) {
      if (Source$1!==null) {
         if (Target$1!==null) {
            if (Size$13>0) {
               TMarshal.Move$1(Self,Source$1.FBuffer$2,Source$1.FOffset$1,Target$1.FBuffer$2,Target$1.FOffset$1,Size$13);
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
function TBufferHandleHelper$Valid$6(Self$9) {
   var Result = false;
   Result = !( (Self$9 == undefined) || (Self$9 == null) );
   return Result
}
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$2 = undefined;
      $.FOffset$1 = 0;
   }
   ,Create$35:function(Self, Memory$4) {
      if (Memory$4!==null) {
         if (TAllocation.GetSize$3(Memory$4)>0) {
            TAddress.Create$33(Self,TAllocation.GetHandle(Memory$4),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   ,Create$34:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle($As(Stream,TMemoryStream).FBuffer$1);
         if (LRef$1) {
            TAddress.Create$33(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   ,Create$33:function(Self, Segment$1, Offset$2) {
      TObject.Create(Self);
      if (Segment$1&&TBufferHandleHelper$Valid$6(Segment$1)) {
         Self.FBuffer$2 = Segment$1;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$2>=0) {
         Self.FOffset$1 = Offset$2;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   ,Destroy:function(Self) {
      Self.FBuffer$2 = null;
      Self.FOffset$1 = 0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WriteLnF(Text$14, Data$26) {
   util().log(Format(String(Text$14),Data$26.slice(0)));
};
function WriteLn(Text$15) {
   util().log(String(Text$15));
};
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError = "";
      $.FOptions$3 = null;
   }
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   ,Create$43:function(Self) {
      TObject.Create(Self);
      Self.FOptions$3 = TW3ErrorObjectOptions.Create$49($New(TW3ErrorObjectOptions));
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$3);
      TObject.Destroy(Self);
   }
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   ,GetLastError:function(Self) {
      return Self.FLastError;
   }
   ,SetLastError:function(Self, ErrorText$1) {
      var ErrClass = null;
      Self.FLastError = Trim$_String_(ErrorText$1);
      if (Self.FLastError.length>0) {
         if (Self.FOptions$3.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
       }
         }
         if (Self.FOptions$3.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass,""),Self.FLastError);
         }
      }
   }
   ,SetLastErrorF:function(Self, ErrorText$2, Values$4) {
      TW3ErrorObject.SetLastError(Self,Format(ErrorText$2,Values$4.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
TW3ErrorObject.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator = "",
         x$23 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Separator = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator)) {
         TW3ErrorObject.SetLastError(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$23=FilePath$2.length;x$23>=1;x$23--) {
         {var $temp27 = FilePath$2.charAt(x$23-1);
            if ($temp27==".") {
               Result = FilePath$2.substr(0,(x$23-1))+NewExt.v;
               break;
            }
             else if ($temp27==Separator) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$1 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$6.length>0) {
         Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$1)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$1);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$1)+Separator$1;
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   ,GetExtension:function(Self, Filename$1) {
      var Result = "";
      var Separator$2 = "",
         x$24 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$1.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$1,Separator$2)) {
            TW3ErrorObject.SetLastError(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$24=Filename$1.length;x$24>=1;x$24--) {
               {var $temp28 = Filename$1.charAt(x$24-1);
                  if ($temp28==".") {
                     dx = Filename$1.length;
                     (dx-= x$24);
                     ++dx;
                     Result = RightStr(Filename$1,dx);
                     break;
                  }
                   else if ($temp28==Separator$2) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$3 = "",
         Parts = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$3)) {
            TW3ErrorObject.SetLastError(Self,"No filename part in path error");
         } else {
            Parts = (Temp$1).split(Separator$3);
            Result = Parts[(Parts.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   ,GetFileNameWithoutExtension:function(Self, Filename$2) {
      var Result = "";
      var temp$16 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      temp$16 = TW3DirectoryParser.GetFileName$(Self,Filename$2);
      if (!TW3ErrorObject.GetFailed(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$16,"");
      }
      return Result
   }
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$1 = [],
         Separator$4 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$4)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$1 = (Temp$2).split(Separator$4);
            Result = Parts$1[(Parts$1.length-1)];
            return Result;
         }
         Parts$1 = (Temp$2).split(Separator$4);
         if (Parts$1.length>1) {
            Result = Parts$1[(Parts$1.length-1)-1];
         } else {
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp29=0;$temp29<FileName.length;$temp29++) {
               el=$uniCharAt(FileName,$temp29);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp30=0;$temp30<FolderName.length;$temp30++) {
               el$1=$uniCharAt(FolderName,$temp30);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$5 = "";
      Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$5)) {
         Result = FilePath$9;
      } else {
         Result = Separator$5+FilePath$9;
      }
      return Result
   }
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$6 = "",
         PathParts = [],
         Index$3 = 0,
         a$127 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$6);
            Index$3 = 0;
            var $temp31;
            for(a$127=0,$temp31=PathParts.length;a$127<$temp31;a$127++) {
               part = PathParts[a$127];
               {var $temp32 = part;
                  if ($temp32=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp32=="~") {
                     if (Index$3>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index$3==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index$3+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos$2 = 0;
         xpos$2 = (ThisPath.indexOf(":\\")+1);
         if (xpos$2>=2) {
            ++xpos$2;
            Result = ThisPath.substr(0,xpos$2);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   ,ChangeFileExt:function(Self, FilePath$12, NewExt$1) {
      var Result = "";
      var LName$1 = "";
      var Separator$7 = "",
         x$25 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$12.length>0) {
         if ((FilePath$12.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$12]);
         } else {
            if (StrEndsWith(FilePath$12," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$12]);
            } else {
               Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$1,Separator$7)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[FilePath$12]);
               } else {
                  if ((FilePath$12.indexOf(Separator$7)+1)>0) {
                     LName$1 = TW3DirectoryParser.GetFileName$(Self,FilePath$12);
                     if (TW3ErrorObject.GetFailed(Self)) {
                        return Result;
                     }
                  } else {
                     LName$1 = FilePath$12;
                  }
                  if (LName$1.length>0) {
                     if ((LName$1.indexOf(".")+1)>0) {
                        for(x$25=LName$1.length;x$25>=1;x$25--) {
                           if (LName$1.charAt(x$25-1)==".") {
                              Result = LName$1.substr(0,(x$25-1))+NewExt$1;
                              break;
                           }
                        }
                     } else {
                        Result = LName$1+NewExt$1;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$12]);
      }
      return Result
   }
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$13) {
      var Result = "";
      var Separator$8 = "";
      Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$13,Separator$8)) {
         Result = FilePath$13.substr((1+Separator$8.length)-1);
      } else {
         Result = FilePath$13;
      }
      return Result
   }
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$14) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$14,Separator$9)) {
         Result = FilePath$14.substr(0,(FilePath$14.length-Separator$9.length));
      } else {
         Result = FilePath$14;
      }
      return Result
   }
   ,GetDevice:function(Self, FilePath$15) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$15.length>0) {
         if (StrBeginsWith(FilePath$15,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   ,GetDirectoryName:function(Self, FilePath$16) {
      var Result = "";
      var Separator$10 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$16.length>0) {
         if ((FilePath$16.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$16]);
         } else {
            if (StrEndsWith(FilePath$16," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$16]);
            } else {
               Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$16,Separator$10)) {
                  Result = FilePath$16;
                  return Result;
               }
               NameParts$1 = (FilePath$16).split(Separator$10);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$10)+Separator$10;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$16]);
      }
      return Result
   }
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var LName$2 = "";
      var Separator$11 = "",
         x$26 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$3.length>0) {
         if ((Filename$3.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$3]);
         } else {
            if (StrEndsWith(Filename$3," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$3]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$2,Separator$11)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[Filename$3]);
               } else {
                  if ((Filename$3.indexOf(Separator$11)+1)>0) {
                     LName$2 = TW3DirectoryParser.GetFileName$(Self,Filename$3);
                  } else {
                     LName$2 = Filename$3;
                  }
                  if (!TW3ErrorObject.GetFailed(Self)) {
                     for(x$26=Filename$3.length;x$26>=1;x$26--) {
                        {var $temp33 = Filename$3.charAt(x$26-1);
                           if ($temp33==".") {
                              dx$1 = Filename$3.length;
                              (dx$1-= x$26);
                              ++dx$1;
                              Result = RightStr(Filename$3,dx$1);
                              break;
                           }
                            else if ($temp33==Separator$11) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$3)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$3]);
      }
      return Result
   }
   ,GetFileName:function(Self, FilePath$17) {
      var Result = "";
      var Separator$12 = "",
         x$27 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$17.length>0) {
         if ((FilePath$17.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$17]);
         } else {
            if (StrEndsWith(FilePath$17," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$17]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$17,Separator$12)) {
                  for(x$27=FilePath$17.length;x$27>=1;x$27--) {
                     if (FilePath$17.charAt(x$27-1)!=Separator$12) {
                        Result = FilePath$17.charAt(x$27-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$17]);
      }
      return Result
   }
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var Separator$13 = "",
         LName$3 = "";
      var x$28 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$4.length>0) {
         if ((Filename$4.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$4]);
         } else {
            if (StrEndsWith(Filename$4," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$4]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$4.indexOf(Separator$13)+1)>0) {
                  LName$3 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
               } else {
                  LName$3 = Filename$4;
               }
               if (!TW3ErrorObject.GetFailed(Self)) {
                  if (LName$3.length>0) {
                     if ((LName$3.indexOf(".")+1)>0) {
                        for(x$28=LName$3.length;x$28>=1;x$28--) {
                           if (LName$3.charAt(x$28-1)==".") {
                              Result = LName$3.substr(0,(x$28-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$3;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$4]);
      }
      return Result
   }
   ,GetPathName:function(Self, FilePath$18) {
      var Result = "";
      var LParts = [],
         LTemp$5 = "";
      var Separator$14 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$18.length>0) {
         if ((FilePath$18.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$18]);
         } else {
            if (StrEndsWith(FilePath$18," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$18]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$18,Separator$14)) {
                  if (FilePath$18==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$5 = (FilePath$18).substr(0,(FilePath$18.length-Separator$14.length));
                  LParts = (LTemp$5).split(Separator$14);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$18).split(Separator$14);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$18]);
      }
      return Result
   }
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   ,HasValidFileNameChars:function(Self, FileName$1) {
      var Result = false;
      var el$2 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName$1.length>0) {
         if ((FileName$1.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$1]);
         } else {
            for (var $temp34=0;$temp34<FileName$1.length;$temp34++) {
               el$2=$uniCharAt(FileName$1,$temp34);
               if (!el$2) continue;
               Result = (((el$2>="A")&&(el$2<="Z"))||((el$2>="a")&&(el$2<="z"))||((el$2>="0")&&(el$2<="9"))||(el$2=="-")||(el$2=="_")||(el$2==".")||(el$2==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$2, FileName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp35=0;$temp35<FolderName$1.length;$temp35++) {
               el$3=$uniCharAt(FolderName$1,$temp35);
               if (!el$3) continue;
               Result = (((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="-")||(el$3=="_")||(el$3==".")||(el$3==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$3, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$19) {
      var Result = "";
      var Separator$15 = "";
      Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$19,Separator$15)) {
         Result = FilePath$19;
      } else {
         Result = Separator$15+FilePath$19;
      }
      return Result
   }
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$20) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$20;
      if (!StrEndsWith(Result,Separator$16)) {
         Result+=Separator$16;
      }
      return Result
   }
   ,IsValidPath:function(Self, FilePath$21) {
      var Result = false;
      var Separator$17 = "",
         PathParts$1 = [],
         Index$4 = 0,
         a$128 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$21.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$21]);
      } else {
         if (FilePath$21.length>0) {
            Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$21).split(Separator$17);
            Index$4 = 0;
            var $temp36;
            for(a$128=0,$temp36=PathParts$1.length;a$128<$temp36;a$128++) {
               part$1 = PathParts$1[a$128];
               {var $temp37 = part$1;
                  if ($temp37=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$21]);
                     return false;
                  }
                   else if ($temp37=="~") {
                     if (Index$4>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$21]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$4==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$4+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function ParamStr$1(index$4) {
   var Result = "";
   Result = process.argv[index$4];
   return Result
};
function ParamCount$1() {
   var Result = 0;
   Result = process.argv.length;
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp38 = token;
      if ($temp38=="darwin") {
         Result = 3;
      }
       else if ($temp38=="win32") {
         Result = 1;
      }
       else if ($temp38=="linux") {
         Result = 2;
      }
       else if ($temp38=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,GetDevice$3:function(FilePath$22) {
      var Result = "";
      var Access = null,
         Error$1 = null;
      Access = GetDirectoryParser();
      Error$1 = Access[2]();
      Result = Access[10](FilePath$22);
      if (Error$1[0]()) {
         throw Exception.Create($New(EPathError),Error$1[1]());
      }
      return Result
   }
   ,IsPathRooted$1:function(FilePath$23) {
      var Result = false;
      var Access$1 = null,
         Error$2 = null;
      Access$1 = GetDirectoryParser();
      Error$2 = Access$1[2]();
      Result = Access$1[7](FilePath$23);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$43($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$4 = null;
   }
   ,Create$16:function(Self, AOwner$1) {
      TW3OwnedObject.Create$16(Self,AOwner$1);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$49($New(TW3ErrorObjectOptions));
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   ,SetLastErrorF$1:function(Self, ErrorText$3, Values$5) {
      Self.FLastError$1 = Format(ErrorText$3,Values$5.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2(Self);
      }
   }
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2(Self);
         }
      }
   }
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   ,Create$49:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
};
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.FInitialized = false;
   }
   ,Create$45:function(Self, AOwner$2) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$2);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   ,CreateEx:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$3);
      Self.FInitialized = false;
      return Self
   }
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   ,FinalizeObject:function(Self) {
      /* null */
   }
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$45$:function($){return $.ClassType.Create$45.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive = false;
      $.FDelay$1 = 0;
      $.FHandle$1 = undefined;
   }
   ,AllocTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3CustomRepeater.FreeTimer(Self);
      }
      Self.FHandle$1 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   ,Destroy:function(Self) {
      if (Self.FActive) {
         TW3CustomRepeater.SetActive(Self,false);
      }
      TObject.Destroy(Self);
   }
   ,FreeTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$1);
         Self.FHandle$1 = undefined;
      }
   }
   ,SetActive:function(Self, NewActive) {
      if (NewActive!=Self.FActive) {
         try {
            if (Self.FActive) {
               TW3CustomRepeater.FreeTimer(Self);
            } else {
               TW3CustomRepeater.AllocTimer(Self);
            }
         } finally {
            Self.FActive = NewActive;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
};
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,ClearInterval:function(Self, Handle$6) {
      clearInterval(Handle$6);
   }
   ,Execute$1:function(Self, EntryPoint$1, WaitForInMs) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs);
      return Result
   }
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute$1(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute$1(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   ,Destroy:TObject.Destroy
};
function DateTimeToJDate(Present) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present-25569)*86400000));
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   ,CopyFrom:function(Self, Source$2, Count$6) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,DataGetSize:function(Self) {
      return TStream.GetSize$(Self);
   }
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   ,DataRead:function(Self, Offset$3, ByteCount) {
      return TStream.ReadBuffer$(Self,Offset$3,ByteCount);
   }
   ,DataWrite:function(Self, Offset$4, Bytes$2) {
      TStream.WriteBuffer$(Self,Bytes$2,Offset$4);
   }
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,GetSize:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,ReadBuffer:function(Self, Offset$5, Count$7) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,Seek:function(Self, Offset$6, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,SetSize:function(Self, NewSize) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   ,Write$1:function(Self, Buffer$2) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$2,TStream.GetPosition$(Self));
      Result = Buffer$2.length;
      return Result
   }
   ,WriteBuffer:function(Self, Buffer$3, Offset$7) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$1 = null;
      $.FPos = 0;
   }
   ,CopyFrom:function(Self, Source$3, Count$8) {
      var Result = 0;
      var LData = [];
      LData = TStream.ReadBuffer$(Source$3,TStream.GetPosition$(Source$3),Count$8);
      TStream.WriteBuffer$(Self,LData,TStream.GetPosition$(Self));
      Result = LData.length;
      return Result
   }
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FBuffer$1 = TDataTypeConverter.Create$15$($New(TAllocation));
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$1);
      TDataTypeConverter.Destroy(Self);
   }
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   ,GetSize:function(Self) {
      return TAllocation.GetSize$3(Self.FBuffer$1);
   }
   ,ReadBuffer:function(Self, Offset$8, Count$9) {
      var Result = [];
      var LLen$7 = 0,
         LBytesToMove = 0,
         LTemp$6 = null;
      LLen$7 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$(Self)) {
         LLen$7 = TStream.GetSize$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$7>0) {
         try {
            LBytesToMove = Count$9;
            if (LBytesToMove>LLen$7) {
               LBytesToMove = LLen$7;
            }
            LTemp$6 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Self.FBuffer$1),Offset$8,LTemp$6,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$6);
            TStream.SetPosition$(Self,Offset$8+Result.length);
         } catch ($e) {
            var e$4 = $W($e);
            throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$4.FMessage]);
         }
      }
      return Result
   }
   ,Seek:function(Self, Offset$9, Origin$1) {
      var Result = 0;
      var LSize = 0;
      LSize = TStream.GetSize$(Self);
      if (LSize>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$9>-1) {
                  TStream.SetPosition$(Self,Offset$9);
                  Result = Offset$9;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$9),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$9)),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$1 = 0;
      LSize$1 = TStream.GetSize$(Self);
      if (LSize$1>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$1);
      }
   }
   ,SetSize:function(Self, NewSize$1) {
      var LSize$2 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$2 = TStream.GetSize$(Self);
      if (NewSize$1>0) {
         if (NewSize$1==LSize$2) {
            return;
         }
         if (NewSize$1>LSize$2) {
            LDiff = NewSize$1-LSize$2;
            if (TAllocation.GetSize$3(Self.FBuffer$1)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$1,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         } else {
            LDiff$1 = LSize$2-NewSize$1;
            if (TAllocation.GetSize$3(Self.FBuffer$1)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$1,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$1);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$(Self));
   }
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$3 = 0,
         LTotal$1 = 0;
      LSize$3 = TStream.GetSize$(Self);
      if (LSize$3>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$3) {
            LTotal$1 = LSize$3-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   ,WriteBuffer:function(Self, Buffer$4, Offset$10) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$28(Self.FBuffer$1)&&Offset$10<1) {
            TAllocation.Allocate(Self.FBuffer$1,Buffer$4.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$4);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle(Self.FBuffer$1),0,Buffer$4.length);
         } else {
            if (TStream.GetPosition$(Self)==TStream.GetSize$(Self)) {
               TAllocation.Grow(Self.FBuffer$1,Buffer$4.length);
               mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$4);
               TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle(Self.FBuffer$1),Offset$10,Buffer$4.length);
            } else {
               TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$4),0,TAllocation.GetHandle(Self.FBuffer$1),Offset$10,Buffer$4.length);
            }
         }
         TStream.SetPosition$(Self,Offset$10+Buffer$4.length);
      } catch ($e) {
         var e$5 = $W($e);
         throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$5.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$1 = 0;
      $.FFilename = "";
   }
   ,SetAccessMode:function(Self, Value$58) {
      Self.FAccess$1 = Value$58;
   }
   ,SetFilename:function(Self, Value$59) {
      Self.FFilename = Value$59;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom:TStream.CopyFrom
   ,GetPosition:TStream.GetPosition
   ,GetSize:TStream.GetSize
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   ,a$27:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TAllocation);
   }
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   ,Create$36:function(Self, AOwner$4) {
      TW3OwnedObject.Create$16(Self,AOwner$4);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed(Self);
   }
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle(TAllocationOptions.a$27(Self)).length-TAllocation.GetSize$3(TAllocationOptions.a$27(Self)))),10);
      }
      return Result
   }
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle = undefined;
      $.FOptions$1 = null;
      $.FSize = 0;
   }
   ,a$28:function(Self) {
      return ((!Self.FHandle)?true:false);
   }
   ,Allocate:function(Self, NumberOfBytes) {
      var NewSize$2 = 0;
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes>0) {
         NewSize$2 = 0;
         if (Self.FOptions$1.FUseCache) {
            NewSize$2 = TInteger.ToNearest(NumberOfBytes,Self.FOptions$1.FCacheSize);
         } else {
            NewSize$2 = NumberOfBytes;
         }
         Self.FHandle = TUnManaged.AllocMemA(TUnManaged,NewSize$2);
         Self.FSize = NumberOfBytes;
         TAllocation.HandleAllocated$(Self);
      }
   }
   ,Create$38:function(Self, ByteSize) {
      TDataTypeConverter.Create$15$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$1 = TAllocationOptions.Create$36($New(TAllocationOptions),Self);
      return Self
   }
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self);
   }
   ,DataOffset$1:function(Self) {
      return 0;
   }
   ,DataRead$1:function(Self, Offset$11, ByteCount$1) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$11,ByteCount$1));
   }
   ,DataWrite$1:function(Self, Offset$12, Bytes$3) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$12,TDataTypeConverter.BytesToTypedArray(Self,Bytes$3));
   }
   ,Destroy:function(Self) {
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$1);
      TDataTypeConverter.Destroy(Self);
   }
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle) {
         Result = Self.FHandle.buffer;
      }
      return Result
   }
   ,GetHandle:function(Self) {
      return Self.FHandle;
   }
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if (Self.FHandle) {
         Result = parseInt(Self.FHandle.length,10);
      }
      return Result
   }
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   ,Grow:function(Self, NumberOfBytes$1) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle) {
         ExactNewSize = Self.FSize+NumberOfBytes$1;
         if (Self.FOptions$1.FUseCache) {
            if (NumberOfBytes$1<TAllocationOptions.GetCacheFree(Self.FOptions$1)) {
               (Self.FSize+= NumberOfBytes$1);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$1.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$1);
      }
   }
   ,HandleAllocated:function(Self) {
      /* null */
   }
   ,HandleReleased:function(Self) {
      /* null */
   }
   ,ReAllocate:function(Self, NewSize$3) {
      var LSizeToSet = 0;
      if (NewSize$3>0) {
         if (Self.FHandle) {
            if (NewSize$3!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$1.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,Self.FOptions$1.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,16);
               }
               Self.FHandle = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle,LSizeToSet);
               Self.FSize = NewSize$3;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$3);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   ,Release$1:function(Self) {
      if (Self.FHandle) {
         try {
            Self.FHandle = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   ,Shrink:function(Self, NumberOfBytes$2) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$2),0,2147483647);
         if (Self.FOptions$1.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$1.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$1.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   ,Transport:function(Self, Target$2) {
      var Data$27 = [];
      if (Target$2===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$28(Self)) {
            try {
               Data$27 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle(Self));
               Target$2[3](Target$2[0](),Data$27);
            } catch ($e) {
               var e$6 = $W($e);
               throw EW3Exception.CreateFmt($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$6.ClassType), e$6.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
function a$129(Self) {
   return ((!Self[0]())?true:false);
}var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   ,AppendBytes:function(Self, Bytes$4) {
      var LLen$8 = 0,
         LOffset$1 = 0;
      LLen$8 = Bytes$4.length;
      if (LLen$8>0) {
         LOffset$1 = TAllocation.GetSize$3(Self);
         TAllocation.Grow(Self,LLen$8);
         TAllocation.GetHandle(Self).set(Bytes$4,LOffset$1);
      }
   }
   ,AppendFloat32:function(Self, Value$60) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$60);
   }
   ,AppendFloat64:function(Self, Value$61) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$61);
   }
   ,AppendMemory:function(Self, Buffer$5, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$5!==null) {
         try {
            if (TAllocation.GetSize$3(Buffer$5)>0) {
               LOffset$4 = TAllocation.GetSize$3(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3(Buffer$5));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$5);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$5);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   ,AppendStr:function(Self, Text$16) {
      var LLen$9 = 0,
         LOffset$5 = 0,
         LTemp$7 = [],
         x$29 = 0;
      LLen$9 = Text$16.length;
      if (LLen$9>0) {
         LOffset$5 = TAllocation.GetSize$3(Self);
         LTemp$7 = TString.EncodeUTF8(TString,Text$16);
         TAllocation.Grow(Self,LTemp$7.length);
         var $temp39;
         for(x$29=0,$temp39=LTemp$7.length;x$29<$temp39;x$29++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$7[x$29]);
            ++LOffset$5;
         }
      }
   }
   ,Clone:function(Self) {
      return TBinaryData.Create$42($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   ,Copy$1:function(Self, Offset$13, ByteLen) {
      var Result = [];
      var LSize$4 = 0,
         LTemp$8 = null;
      LSize$4 = TAllocation.GetSize$3(Self);
      if (LSize$4>0) {
         if (Offset$13>=0&&Offset$13<LSize$4) {
            if (Offset$13+ByteLen<=LSize$4) {
               LTemp$8 = new Uint8Array(Self.FDataView.buffer,Offset$13,ByteLen);
               Result = Array.prototype.slice.call(LTemp$8);
               LTemp$8.buffer = null;
               LTemp$8 = null;
            }
         }
      }
      return Result
   }
   ,CopyFrom$2:function(Self, Buffer$6, Offset$14, ByteLen$1) {
      if (Buffer$6!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle(Buffer$6),Offset$14,ByteLen$1);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   ,CopyFromMemory:function(Self, Raw$1, Offset$15, ByteLen$2) {
      if (TMemoryHandleHelper$Valid$5(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$15)) {
            if (ByteLen$2>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle(Self),Offset$15,ByteLen$2);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$15]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   ,Create$42:function(Self, aHandle) {
      var LSignature;
      TDataTypeConverter.Create$15$(Self);
      if (TMemoryHandleHelper$Defined(aHandle)&&TMemoryHandleHelper$Valid$5(aHandle)) {
         if (aHandle.toString) {
            LSignature = aHandle.toString();
            if (SameText(String(LSignature),"[object Uint8Array]")||SameText(String(LSignature),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   ,CutBinaryData:function(Self, Offset$16, ByteLen$3) {
      var Result = null;
      var LSize$5 = 0,
         LNewBuffer = null;
      if (ByteLen$3>0) {
         LSize$5 = TAllocation.GetSize$3(Self);
         if (LSize$5>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$16)) {
               LNewBuffer = TAllocation.GetHandle(Self).subarray(Offset$16,Offset$16+ByteLen$3-1);
               Result = TBinaryData.Create$42($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$16]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$42($New(TBinaryData),null);
      }
      return Result
   }
   ,CutStream:function(Self, Offset$17, ByteLen$4) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$17,ByteLen$4));
   }
   ,CutTypedArray:function(Self, Offset$18, ByteLen$5) {
      var Result = undefined;
      var LTemp$9 = null;
      if (ByteLen$5>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$18)) {
            if (TAllocation.GetSize$3(Self)-Offset$18>0) {
               LTemp$9 = Self.FDataView.buffer.slice(Offset$18,Offset$18+ByteLen$5);
               Result = new Uint8Array(LTemp$9);
            }
         }
      }
      return Result
   }
   ,FromBase64:function(Self, FileData) {
      var LBytes$1 = [];
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LBytes$1 = TBase64EncDec.Base64ToBytes$2(TBase64EncDec,FileData);
         if (LBytes$1.length>0) {
            TBinaryData.AppendBytes(Self,LBytes$1);
         }
      }
   }
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$28(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   ,GetBit$1:function(Self, BitIndex$2) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex$2>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex$2%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex$2]);
      }
      return Result
   }
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3(Self)<<3;
   }
   ,GetByte:function(Self, Index$5) {
      var Result = 0;
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$5)) {
            Result = Self.FDataView.getUint8(Index$5);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$5]);
         }
      }
      return Result
   }
   ,HandleAllocated:function(Self) {
      var LRef$2 = undefined;
      LRef$2 = TAllocation.GetBufferHandle(Self);
      (Self.FDataView) = new DataView(LRef$2);
   }
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   ,OffsetInRange:function(Self, Offset$19) {
      var Result = false;
      var LSize$6 = 0;
      LSize$6 = TAllocation.GetSize$3(Self);
      if (LSize$6>0) {
         Result = Offset$19>=0&&Offset$19<=LSize$6;
      } else {
         Result = (Offset$19==0);
      }
      return Result
   }
   ,ReadBool:function(Self, Offset$20) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         Result = Self.FDataView.getUint8(Offset$20)>0;
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$20, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadBytes:function(Self, Offset$21, ByteLen$6) {
      var Result = [];
      var LSize$7 = 0,
         x$30 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$21)) {
         LSize$7 = TAllocation.GetSize$3(Self);
         if (Offset$21+ByteLen$6<=LSize$7) {
            var $temp40;
            for(x$30=0,$temp40=ByteLen$6;x$30<$temp40;x$30++) {
               Result.push(Self.FDataView.getUint8(Offset$21+x$30));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$21, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadFloat32:function(Self, Offset$22) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$22)) {
         if (Offset$22+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$22);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$22,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$22,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$22, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadFloat64:function(Self, Offset$23) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$23)) {
         if (Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$23);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$23,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$23,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$23, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadInt:function(Self, Offset$24) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$24)) {
         if (Offset$24+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$24);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$24,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$24,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$24, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadInt16:function(Self, Offset$25) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         if (Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$25);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$25,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$25,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$25, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadLong$1:function(Self, Offset$26) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$26)) {
         if (Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$26);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$26,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$26,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$26, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadStr$1:function(Self, Offset$27, ByteLen$7) {
      var Result = "";
      var LSize$8 = 0,
         LFetch = [],
         x$31 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         LSize$8 = TAllocation.GetSize$3(Self);
         if (Offset$27+ByteLen$7<=LSize$8) {
            var $temp41;
            for(x$31=0,$temp41=ByteLen$7;x$31<$temp41;x$31++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$27+x$31)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$27, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,ReadWord$1:function(Self, Offset$28) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$28);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$28,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$28,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[11],[Offset$28, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   ,SetBit$1:function(Self, BitIndex$3, value$3) {
      TBinaryData.SetByte(Self,(BitIndex$3>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$3%8),TBinaryData.GetByte(Self,(BitIndex$3>>>3)),value$3));
   }
   ,SetByte:function(Self, Index$6, Value$62) {
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$6)) {
            Self.FDataView.setUint8(Index$6,Value$62);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$6]);
         }
      }
   }
   ,ToBase64:function(Self) {
      return TDataTypeConverter.BytesToBase64(Self.ClassType,TBinaryData.ToBytes(Self));
   }
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$9 = 0,
         LTemp$10 = null;
      LSize$9 = TAllocation.GetSize$3(Self);
      if (LSize$9>0) {
         LTemp$10 = new Uint8Array(Self.FDataView.buffer,0,LSize$9);
         Result = Array.prototype.slice.call(LTemp$10);
         LTemp$10.buffer = null;
         LTemp$10 = null;
      } else {
         Result = [];
      }
      return Result
   }
   ,ToHexDump:function(Self, BytesPerRow, Options$1) {
      var Result = "";
      var mDump = [],
         mCount = 0,
         x$32 = 0;
      var LByte = 0,
         mPad = 0,
         z = 0;
      var mPad$1 = 0,
         z$1 = 0;
      function SliceToText(DataSlice) {
         var Result = "";
         var y = 0;
         var LChar$1 = "",
            LOff = 0;
         if (DataSlice.length>0) {
            var $temp42;
            for(y=0,$temp42=DataSlice.length;y<$temp42;y++) {
               LChar$1 = TDataTypeConverter.ByteToChar(TDataTypeConverter,DataSlice[y]);
               if ((((LChar$1>="A")&&(LChar$1<="Z"))||((LChar$1>="a")&&(LChar$1<="z"))||((LChar$1>="0")&&(LChar$1<="9"))||(LChar$1==",")||(LChar$1==";")||(LChar$1=="<")||(LChar$1==">")||(LChar$1=="{")||(LChar$1=="}")||(LChar$1=="[")||(LChar$1=="]")||(LChar$1=="-")||(LChar$1=="_")||(LChar$1=="#")||(LChar$1=="$")||(LChar$1=="%")||(LChar$1=="&")||(LChar$1=="\/")||(LChar$1=="(")||(LChar$1==")")||(LChar$1=="!")||(LChar$1=="§")||(LChar$1=="^")||(LChar$1==":")||(LChar$1==",")||(LChar$1=="?"))) {
                  Result+=LChar$1;
               } else {
                  Result+="_";
               }
            }
         }
         LOff = BytesPerRow-DataSlice.length;
         while (LOff>0) {
            Result+="_";
            --LOff;
         }
         Result+="\r\n";
         return Result
      };
      if (TAllocation.GetHandle(Self)) {
         mCount = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp43;
         for(x$32=0,$temp43=TAllocation.GetSize$3(Self);x$32<$temp43;x$32++) {
            LByte = TBinaryData.GetByte(Self,x$32);
            mDump.push(LByte);
            if ($SetIn(Options$1,0,0,2)) {
               Result+="$"+(IntToHex2(LByte)).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(LByte)).toLocaleUpperCase();
            }
            ++mCount;
            if (mCount>=BytesPerRow) {
               if ($SetIn(Options$1,1,0,2)&&mCount>0) {
                  Result+=" ";
                  mPad = BytesPerRow-mCount;
                  var $temp44;
                  for(z=1,$temp44=mPad;z<=$temp44;z++) {
                     if ($SetIn(Options$1,0,0,2)) {
                        Result+="$";
                     }
                     Result+="00";
                     ++mCount;
                     if (mCount>=BytesPerRow) {
                        mCount = 0;
                        break;
                     } else {
                        Result+=" ";
                     }
                  }
               }
               if (mDump.length>0) {
                  Result+=SliceToText(mDump);
                  mDump.length=0;
                  mCount = 0;
               }
            } else {
               Result+=" ";
            }
         }
         if (mDump.length>0) {
            if ($SetIn(Options$1,1,0,2)&&mCount>0) {
               mPad$1 = BytesPerRow-mCount;
               var $temp45;
               for(z$1=1,$temp45=mPad$1;z$1<=$temp45;z$1++) {
                  if ($SetIn(Options$1,0,0,2)) {
                     Result+="$";
                  }
                  Result+="00";
                  ++mCount;
                  if (mCount>=BytesPerRow) {
                     break;
                  } else {
                     Result+=" ";
                  }
               }
            }
            Result+=" "+SliceToText(mDump);
            mCount = 0;
            mDump.length=0;
         }
      }
      return Result
   }
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$10 = 0,
         LCache = [],
         LTemp$11 = null;
      Result = TDataTypeConverter.Create$15$($New(TMemoryStream));
      LSize$10 = TAllocation.GetSize$3(Self);
      if (LSize$10>0) {
         LTemp$11 = new Uint8Array(Self.FDataView.buffer,0,LSize$10);
         LCache = Array.prototype.slice.call(LTemp$11);
         LTemp$11 = null;
         if (LCache.length>0) {
            try {
               TStream.Write$1(Result,LCache);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$7 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   ,ToString$2:function(Self) {
      var Result = "";
      var CHUNK_SIZE = 32768;
      var LRef$3 = undefined;
      if (TAllocation.GetHandle(Self)) {
         LRef$3 = TAllocation.GetHandle(Self);
         var c = [];
    for (var i=0; i < (LRef$3).length; i += CHUNK_SIZE) {
      c.push(String.fromCharCode.apply(null, (LRef$3).subarray(i, i + CHUNK_SIZE)));
    }
    Result = c.join("");
      }
      return Result
   }
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$10 = 0,
         LTemp$12 = null;
      LLen$10 = TAllocation.GetSize$3(Self);
      if (LLen$10>0) {
         LTemp$12 = Self.FDataView.buffer.slice(0,LLen$10);
         Result = new Uint8Array(LTemp$12);
      }
      return Result
   }
   ,WriteBinaryData:function(Self, Offset$29, Data$28) {
      var LGrowth = 0;
      if (Data$28!==null) {
         if (TAllocation.GetSize$3(Data$28)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$29)) {
               LGrowth = 0;
               if (Offset$29+TAllocation.GetSize$3(Data$28)>TAllocation.GetSize$3(Self)-1) {
                  LGrowth = Offset$29+TAllocation.GetSize$3(Data$28)-TAllocation.GetSize$3(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Data$28),0,TAllocation.GetHandle(Self),0,TAllocation.GetSize$3(Data$28));
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$29]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   ,WriteBool:function(Self, Offset$30, Data$29) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$30)) {
         LGrowth$1 = 0;
         if (Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$1 = Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$29) {
            Self.FDataView.setUint8(Offset$30,1);
         } else {
            Self.FDataView.setUint8(Offset$30,0);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$30]);
      }
   }
   ,WriteBuffer$2:function(Self, Offset$31, Data$30) {
      var LBuffer$1 = null,
         LGrowth$2 = 0;
      if (Data$30) {
         if (Data$30.buffer) {
            LBuffer$1 = Data$30.buffer;
            if (LBuffer$1.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$31)) {
                  LGrowth$2 = 0;
                  if (Offset$31+Data$30.length>TAllocation.GetSize$3(Self)-1) {
                     LGrowth$2 = Offset$31+Data$30.length-TAllocation.GetSize$3(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$30,0,TAllocation.GetHandle(Self),Offset$31,parseInt(TAllocation.GetHandle(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$31]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   ,WriteBytes:function(Self, Offset$32, Data$31) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$32)) {
         if (Data$31.length>0) {
            LGrowth$3 = 0;
            if (Offset$32+Data$31.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$3 = Offset$32+Data$31.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle(Self).set(Data$31,Offset$32);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$32]);
      }
   }
   ,WriteFloat32:function(Self, Offset$33, Data$32) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$33)) {
         LGrowth$4 = 0;
         if (Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$4 = Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$33,Data$32);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$33,Data$32,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$33,Data$32,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$33]);
      }
   }
   ,WriteFloat64:function(Self, Offset$34, Data$33) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$34)) {
         LGrowth$5 = 0;
         if (Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$5 = Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$33));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$33),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$33),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$34]);
      }
   }
   ,WriteInt16:function(Self, Offset$35, Data$34) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$35)) {
         LGrowth$6 = 0;
         if (Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$6 = Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$35,Data$34);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$35,Data$34,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$35,Data$34,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$35]);
      }
   }
   ,WriteInt32:function(Self, Offset$36, Data$35) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$36)) {
         LGrowth$7 = 0;
         if (Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$7 = Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$36,Data$35);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$36,Data$35,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$36,Data$35,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$36]);
      }
   }
   ,WriteLong:function(Self, Offset$37, Data$36) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$37)) {
         LGrowth$8 = 0;
         if (Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$8 = Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$37,Data$36);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$37,Data$36,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$37,Data$36,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$37]);
      }
   }
   ,WriteStr$1:function(Self, Offset$38, Data$37) {
      var LTemp$13 = [],
         LGrowth$9 = 0,
         x$33 = 0;
      if (Data$37.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$38)) {
            LTemp$13 = TString.EncodeUTF8(TString,Data$37);
            LGrowth$9 = 0;
            if (Offset$38+LTemp$13.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$9 = Offset$38+LTemp$13.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp46;
            for(x$33=0,$temp46=LTemp$13.length;x$33<$temp46;x$33++) {
               Self.FDataView.setUint8(Offset$38+x$33,LTemp$13[x$33]);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$38]);
         }
      }
   }
   ,WriteWord$1:function(Self, Offset$39, Data$38) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$39)) {
         LGrowth$10 = 0;
         if (Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$10 = Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$39,Data$38);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$39,Data$38,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$39,Data$38,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$39]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$15:TAllocation.Create$15
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TBinaryData.$Intf={
   IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$2,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Get:function(Self, index$5, Value$63) {
      var Result = false;
      var mMask = 0;
      if (index$5>=0&&index$5<8) {
         mMask = 1<<index$5;
         Result = ((Value$63&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$5]);
      }
      return Result
   }
   ,Set$4:function(Self, Index$7, Value$64, Data$39) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$64;
      if (Index$7>=0&&Index$7<8) {
         mMask$1 = 1<<Index$7;
         mSet = ((Value$64&mMask$1)!=0);
         if (mSet!=Data$39) {
            if (Data$39) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FActive$1 = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$5 = [0];
   }
   ,FinalizeObject:function(Self) {
      if (Self.FActive$1) {
         TW3StorageDevice.UnMount(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   ,GetActive:function(Self) {
      return Self.FActive$1;
   }
   ,GetDeviceId:function(Self, CB) {
      if (CB) {
         CB(Self.FIdentifier,true);
      }
   }
   ,GetDeviceOptions:function(Self, CB$1) {
      if (CB$1) {
         CB$1(Self.FOptions$5.slice(0),true);
      }
   }
   ,GetName:function(Self, CB$2) {
      if (CB$2) {
         CB$2(Self.FName,true);
      }
   }
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$5 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$45:TW3Component.Create$45
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath$1,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists$1,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine$1,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

function Copy$TWriteFileOptions(s,d) {
   return d;
}
function Clone$TWriteFileOptions($) {
   return {

   }
}
function Copy$TWatchOptions(s,d) {
   return d;
}
function Clone$TWatchOptions($) {
   return {

   }
}
function Copy$TWatchFileOptions(s,d) {
   return d;
}
function Clone$TWatchFileOptions($) {
   return {

   }
}
function Copy$TWatchFileListenerObject(s,d) {
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {

   }
}
function Copy$TReadFileSyncOptions(s,d) {
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {

   }
}
function Copy$TReadFileOptions(s,d) {
   return d;
}
function Clone$TReadFileOptions($) {
   return {

   }
}
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function Copy$TAppendFileOptions(s,d) {
   return d;
}
function Clone$TAppendFileOptions($) {
   return {

   }
}
function NodeFsAPI() {
   return require("fs");
};
function NodePathAPI() {
   return require("path");
};
function Copy$JPathParseData(s,d) {
   return d;
}
function Clone$JPathParseData($) {
   return {

   }
}
function util() {
   return require("util");
};
var TCustomCodec = {
   $ClassName:"TCustomCodec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBindings = [];
      $.FCodecInfo = null;
   }
   ,Create$28:function(Self) {
      TObject.Create(Self);
      Self.FCodecInfo = TCustomCodec.MakeCodecInfo$(Self);
      if (Self.FCodecInfo===null) {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.Create",Self,"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo);
      TObject.Destroy(Self);
   }
   ,MakeCodecInfo:function(Self) {
      return null;
   }
   ,RegisterBinding:function(Self, Binding) {
      if (Self.FBindings.indexOf(Binding)<0) {
         Self.FBindings.push(Binding);
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.RegisterBinding",Self,"Binding already connected to codec error");
      }
   }
   ,UnRegisterBinding:function(Self, Binding$1) {
      var LIndex = 0;
      LIndex = Self.FBindings.indexOf(Binding$1);
      if (LIndex>=0) {
         Self.FBindings.splice(LIndex,1)
         ;
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.UnRegisterBinding",Self,"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TCustomCodec.$Intf={
   ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
   ,ICodecProcess:[TCustomCodec.EncodeData,TCustomCodec.DecodeData]
}
var TUTF8Codec = {
   $ClassName:"TUTF8Codec",$Parent:TCustomCodec
   ,$Init:function ($) {
      TCustomCodec.$Init($);
   }
   ,CanUseClampedArray:function() {
      var Result = {v:false};
      try {
         var LTemp$14 = undefined;
         try {
            LTemp$14 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$8 = $W($e);
            return Result.v;
         }
         if (LTemp$14) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   ,EncodeData:function(Self, Source$4, Target$3) {
      var LReader = null,
         LWriter = null,
         BytesToRead = 0,
         LCache$1 = [],
         Text$17 = "";
      LReader = TW3CustomReader.Create$39($New(TStreamReader),Source$4);
      try {
         LWriter = TW3CustomWriter.Create$29($New(TStreamWriter),Target$3);
         try {
            BytesToRead = TW3CustomReader.GetTotalSize$2(LReader)-TW3CustomReader.GetReadOffset(LReader);
            if (BytesToRead>0) {
               LCache$1 = TW3CustomReader.Read$1(LReader,BytesToRead);
               Text$17 = TDatatype.BytesToString$1(TDatatype,LCache$1);
               LCache$1.length=0;
               LCache$1 = TUTF8Codec.Encode(Self,Text$17);
               TW3CustomWriter.Write(LWriter,LCache$1);
            }
         } finally {
            TObject.Free(LWriter);
         }
      } finally {
         TObject.Free(LReader);
      }
   }
   ,DecodeData:function(Self, Source$5, Target$4) {
      var LReader$1 = null,
         LWriter$1 = null,
         BytesToRead$1 = 0,
         LCache$2 = [],
         Text$18 = "";
      LReader$1 = TW3CustomReader.Create$39($New(TReader),Source$5);
      try {
         LWriter$1 = TW3CustomWriter.Create$29($New(TWriter),Target$4);
         try {
            BytesToRead$1 = TW3CustomReader.GetTotalSize$2(LReader$1)-TW3CustomReader.GetReadOffset(LReader$1);
            if (BytesToRead$1>0) {
               LCache$2 = TW3CustomReader.Read$1(LReader$1,BytesToRead$1);
               Text$18 = TUTF8Codec.Decode(Self,LCache$2);
               LCache$2.length=0;
               LCache$2 = TDatatype.StringToBytes$1(TDatatype,Text$18);
               TW3CustomWriter.Write(LWriter$1,LCache$2);
            }
         } finally {
            TObject.Free(LWriter$1);
         }
      } finally {
         TObject.Free(LReader$1);
      }
   }
   ,MakeCodecInfo:function(Self) {
      var Result = null;
      var LAccess = null;
      var LVersion = {viMajor:0,viMinor:0,viRevision:0};
      Result = TObject.Create($New(TCodecInfo));
      LVersion.viMajor = 0;
      LVersion.viMinor = 2;
      LVersion.viRevision = 0;
      LAccess = $AsIntf(Result,"ICodecInfo");
      LAccess[0]("UTF8Codec");
      LAccess[1]("text\/utf8");
      LAccess[2](LVersion);
      LAccess[3]([6]);
      LAccess[5](1);
      LAccess[6](0);
      return Result
   }
   ,Encode:function(Self, TextToEncode$2) {
      var Result = {v:[]};
      try {
         var LEncoder = null;
         var LClip = null;
         var LTemp$15 = null;
         var n = 0;
         var LTyped = null;
         try {
            LEncoder = new TextEncoder();
         } catch ($e) {
            if (TextToEncode$2.length>0) {
               if (TUTF8Codec.CanUseClampedArray()) {
                  LClip = new Uint8ClampedArray(1);
                  LTemp$15 = new Uint8ClampedArray(1);
               } else {
                  LClip = new Uint8Array(1);
                  LTemp$15 = new Uint8Array(1);
               }
               var $temp47;
               for(n=1,$temp47=TextToEncode$2.length;n<=$temp47;n++) {
                  LClip[0]=TString.CharCodeFor(TString,TextToEncode$2.charAt(n-1));
                  if (LClip[0]<128) {
                     Result.v.push(LClip[0]);
                  } else if (LClip[0]>127&&LClip[0]<2048) {
                     LTemp$15[0]=((LClip[0]>>>6)|192);
                     Result.v.push(LTemp$15[0]);
                     LTemp$15[0]=((LClip[0]&63)|128);
                     Result.v.push(LTemp$15[0]);
                  } else {
                     LTemp$15[0]=((LClip[0]>>>12)|224);
                     Result.v.push(LTemp$15[0]);
                     LTemp$15[0]=(((LClip[0]>>>6)&63)|128);
                     Result.v.push(LTemp$15[0]);
                     Result.v.push((LClip[0]&63)|128);
                     Result.v.push(LTemp$15[0]);
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped = LEncoder.encode(TextToEncode$2);
            Result.v = TDatatype.TypedArrayToBytes$1(TDatatype,LTyped);
         } finally {
            LEncoder = null;
         }
      } finally {return Result.v}
   }
   ,Decode:function(Self, BytesToDecode$2) {
      var Result = {v:""};
      try {
         var LDecoder = null;
         var bytelen = 0,
            i$3 = 0,
            c$1 = 0,
            c2 = 0,
            c2$1 = 0,
            c3 = 0,
            c4 = 0,
            u = 0,
            c2$2 = 0,
            c3$1 = 0,
            LTyped$1 = undefined;
         try {
            LDecoder = new TextDecoder();
         } catch ($e) {
            bytelen = BytesToDecode$2.length;
            if (bytelen>0) {
               i$3 = 0;
               while (i$3<bytelen) {
                  c$1 = BytesToDecode$2[i$3];
                  ++i$3;
                  if (c$1<128) {
                     Result.v+=TString.FromCharCode(TString,c$1);
                  } else if (c$1>191&&c$1<224) {
                     c2 = BytesToDecode$2[i$3];
                     ++i$3;
                     Result.v+=TString.FromCharCode(TString,(((c$1&31)<<6)|(c2&63)));
                  } else if (c$1>239&&c$1<365) {
                     c2$1 = BytesToDecode$2[i$3];
                     ++i$3;
                     c3 = BytesToDecode$2[i$3];
                     ++i$3;
                     c4 = BytesToDecode$2[i$3];
                     ++i$3;
                     u = (((((c$1&7)<<18)|((c2$1&63)<<12))|((c3&63)<<6))|(c4&63))-65536;
                     Result.v+=TString.FromCharCode(TString,(55296+(u>>>10)));
                     Result.v+=TString.FromCharCode(TString,(56320+(u&1023)));
                  } else {
                     c2$2 = BytesToDecode$2[i$3];
                     ++i$3;
                     c3$1 = BytesToDecode$2[i$3];
                     ++i$3;
                     Result.v+=TString.FromCharCode(TString,(((c$1&15)<<12)|(((c2$2&63)<<6)|(c3$1&63))));
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$1 = TDatatype.BytesToTypedArray$1(TDatatype,BytesToDecode$2);
            Result.v = LDecoder.decode(LTyped$1);
         } finally {
            LDecoder = null;
         }
      } finally {return Result.v}
   }
   ,Destroy:TCustomCodec.Destroy
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TUTF8Codec.$Intf={
   ICodecProcess:[TUTF8Codec.EncodeData,TUTF8Codec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$2 = null;
      $.FOffset$2 = $.FTotalSize$1 = 0;
      $.FOptions$2 = [0];
   }
   ,Create$39:function(Self, Access$2) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$2 = [1];
      Self.FAccess$2 = Access$2;
      Self.FOffset$2 = Self.FAccess$2[0]();
      Self.FTotalSize$1 = Self.FAccess$2[1]();
      return Self
   }
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FOffset$2;
      } else {
         Result = Self.FAccess$2[0]();
      }
      return Result
   }
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$2[1]();
      }
      return Result
   }
   ,Read$1:function(Self, BytesToRead$2) {
      var Result = [];
      if (BytesToRead$2>0) {
         Result = Self.FAccess$2[2](TW3CustomReader.GetReadOffset(Self),BytesToRead$2);
         if ($SetIn(Self.FOptions$2,0,0,1)) {
            (Self.FOffset$2+= Result.length);
         }
      } else {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead$2.toString()+")"));
      }
      return Result
   }
   ,SetUpdateOption:function(Self, NewUpdateMode) {
      Self.FOptions$2 = NewUpdateMode.slice(0);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var TStreamReader = {
   $ClassName:"TStreamReader",$Parent:TReader
   ,$Init:function ($) {
      TReader.$Init($);
   }
   ,Create$40:function(Self, Stream$1) {
      TW3CustomReader.Create$39(Self,$AsIntf(Stream$1,"IBinaryTransport"));
      if (!$SetIn(Self.FOptions$2,0,0,1)) {
         TW3CustomReader.SetUpdateOption(Self,[1]);
      }
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions = [0];
   }
   ,Create$29:function(Self, Access$3) {
      TDataTypeConverter.Create$15(Self);
      Self.FAccess = Access$3;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions = [3];
      return Self
   }
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions = NewOptions.slice(0);
   }
   ,Write:function(Self, Data$40) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$40.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$40);
            if ($SetIn(Self.FOptions,0,0,2)) {
               (Self.FOffset+= Data$40.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$40,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$40);
               if ($SetIn(Self.FOptions,0,0,2)) {
                  (Self.FOffset+= Data$40.length);
               }
            } else {
               throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[19],[Data$40.length]));
            }
         }
         Result = Data$40.length;
      } else {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[21],[LBytesToWrite]));
      }
      return Result
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Create$30:function(Self, Stream$2) {
      TW3CustomWriter.Create$29(Self,$AsIntf(Stream$2,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function Copy$TCodecVersionInfo(s,d) {
   d.viMajor=s.viMajor;
   d.viMinor=s.viMinor;
   d.viRevision=s.viRevision;
   return d;
}
function Clone$TCodecVersionInfo($) {
   return {
      viMajor:$.viMajor,
      viMinor:$.viMinor,
      viRevision:$.viRevision
   }
}
var TCodecManager = {
   $ClassName:"TCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs = [];
   }
   ,CodecByClass:function(Self, ClsType) {
      var Result = null;
      var a$130 = 0;
      var LItem$3 = null;
      var a$131 = [];
      Result = null;
      a$131 = Self.FCodecs;
      var $temp48;
      for(a$130=0,$temp48=a$131.length;a$130<$temp48;a$130++) {
         LItem$3 = a$131[a$130];
         if (TObject.ClassType(LItem$3.ClassType)==ClsType) {
            Result = LItem$3;
            break;
         }
      }
      return Result
   }
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   ,RegisterCodec:function(Self, CodecClass) {
      var LItem$4 = null;
      LItem$4 = TCodecManager.CodecByClass(Self,CodecClass);
      if (LItem$4===null) {
         LItem$4 = TCustomCodec.Create$28($NewDyn(CodecClass,""));
         Self.FCodecs.push(LItem$4);
      } else {
         throw EW3Exception.Create$27($New(ECodecManager),"TCodecManager.RegisterCodec",Self,"Codec already registered error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var TCodecInfo = {
   $ClassName:"TCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout = $.ciMime = $.ciName = "";
      $.ciDataFlow = [0];
      $.ciInput = 0;
      $.ciOutput = 0;
      $.ciVersion = {viMajor:0,viMinor:0,viRevision:0};
   }
   ,SetDataFlow:function(Self, coFlow) {
      Self.ciDataFlow = coFlow.slice(0);
   }
   ,SetDescription:function(Self, coInfo) {
      Self.ciAbout = coInfo;
   }
   ,SetInput:function(Self, InputType) {
      Self.ciInput = InputType;
   }
   ,SetMime:function(Self, coMime) {
      Self.ciMime = coMime;
   }
   ,SetName:function(Self, coName) {
      Self.ciName = coName;
   }
   ,SetOutput:function(Self, OutputType) {
      Self.ciOutput = OutputType;
   }
   ,SetVersion:function(Self, coVersion) {
      Self.ciVersion.viMajor = coVersion.viMajor;
      Self.ciVersion.viMinor = coVersion.viMinor;
      Self.ciVersion.viRevision = coVersion.viRevision;
   }
   ,Destroy:TObject.Destroy
};
TCodecInfo.$Intf={
   ICodecInfo:[TCodecInfo.SetName,TCodecInfo.SetMime,TCodecInfo.SetVersion,TCodecInfo.SetDataFlow,TCodecInfo.SetDescription,TCodecInfo.SetInput,TCodecInfo.SetOutput]
}
var TCodecDataFormat = [ "cdBinary", "cdText" ];
function TCodecDataFlowHelper$Ordinal(Self$10) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$10,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$10,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
var TCodecDataDirection = { 1:"cdRead", 2:"cdWrite" };
var TCodecBinding = {
   $ClassName:"TCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec = null;
   }
   ,Create$41:function(Self, EndPoint) {
      var LAccess$1 = null;
      TObject.Create(Self);
      if (EndPoint!==null) {
         Self.FCodec = EndPoint;
         LAccess$1 = $AsIntf(Self.FCodec,"ICodecBinding");
         LAccess$1[0](Self);
      } else {
         throw EW3Exception.Create$27($New(ECodecBinding),"TCodecBinding.Create",Self,"Binding failed, invalid endpoint error");
      }
      return Self
   }
   ,Destroy:function(Self) {
      var LAccess$2 = null;
      LAccess$2 = $AsIntf(Self.FCodec,"ICodecBinding");
      LAccess$2[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var ECodecManager = {
   $ClassName:"ECodecManager",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var ECodecError = {
   $ClassName:"ECodecError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var ECodecBinding = {
   $ClassName:"ECodecBinding",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager() {
   var Result = null;
   if (!__Manager) {
      __Manager = TObject.Create($New(TCodecManager));
   }
   Result = __Manager;
   return Result
};
var TQTXCodec = {
   $ClassName:"TQTXCodec",$Parent:TDataTypeConverter$1
   ,$Init:function ($) {
      TDataTypeConverter$1.$Init($);
      $.FBindings$1 = [];
      $.FCodecInfo$1 = null;
   }
   ,Create$65:function(Self) {
      TDataTypeConverter$1.Create$65(Self);
      Self.FCodecInfo$1 = TQTXCodec.MakeCodecInfo$2$(Self);
      if (Self.FCodecInfo$1===null) {
         throw Exception.Create($New(ECodecError$1),"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo$1);
      TDataTypeConverter$1.Destroy(Self);
   }
   ,MakeCodecInfo$2:function(Self) {
      return null;
   }
   ,RegisterBinding$1:function(Self, Binding$2) {
      if (Self.FBindings$1.indexOf(Binding$2)<0) {
         Self.FBindings$1.push(Binding$2);
      } else {
         throw Exception.Create($New(ECodecError$1),"Binding already connected to codec error");
      }
   }
   ,UnRegisterBinding$1:function(Self, Binding$3) {
      var LIndex$1 = 0;
      LIndex$1 = Self.FBindings$1.indexOf(Binding$3);
      if (LIndex$1>=0) {
         Self.FBindings$1.splice(LIndex$1,1)
         ;
      } else {
         throw Exception.Create($New(ECodecError$1),"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$65$:function($){return $.ClassType.Create$65($)}
   ,DecodeData$2$:function($){return $.ClassType.DecodeData$2.apply($.ClassType, arguments)}
   ,EncodeData$2$:function($){return $.ClassType.EncodeData$2.apply($.ClassType, arguments)}
   ,MakeCodecInfo$2$:function($){return $.ClassType.MakeCodecInfo$2($)}
};
TQTXCodec.$Intf={
   IQTXCodecProcess:[TQTXCodec.EncodeData$2,TQTXCodec.DecodeData$2]
   ,IQTXCodecBinding:[TQTXCodec.RegisterBinding$1,TQTXCodec.UnRegisterBinding$1]
}
var TQTXCodecUTF8 = {
   $ClassName:"TQTXCodecUTF8",$Parent:TQTXCodec
   ,$Init:function ($) {
      TQTXCodec.$Init($);
   }
   ,CanUseClampedArray$1:function() {
      var Result = {v:false};
      try {
         var LTemp$16 = undefined;
         try {
            LTemp$16 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$9 = $W($e);
            return Result.v;
         }
         if (LTemp$16) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   ,CanUseNativeConverter:function() {
      var Result = {v:false};
      try {
         var LTemp$17 = null;
         try {
            LTemp$17 = new TextEncoder("utf8");
         } catch ($e) {
            var e$10 = $W($e);
            return false;
         }
         Result.v = LTemp$17!==null;
      } finally {return Result.v}
   }
   ,Decode$3:function(Self, BytesToDecode$3) {
      var Result = "";
      var LDecoder$1 = null,
         LTyped$2 = undefined,
         i$4 = 0,
         bytelen$1 = 0,
         c$2 = 0,
         c2$3 = 0,
         c2$4 = 0,
         c3$2 = 0,
         c4$1 = 0,
         u$1 = 0,
         c2$5 = 0,
         c3$3 = 0;
      if (BytesToDecode$3.length<1) {
         return "";
      }
      if (TQTXCodecUTF8.CanUseNativeConverter()) {
         LDecoder$1 = new TextDecoder("utf8");
         LTyped$2 = TDataTypeConverter$1.BytesToTypedArray$2(Self,BytesToDecode$3);
         Result = LDecoder$1.decode(LTyped$2);
         LTyped$2 = null;
         LDecoder$1 = null;
      } else {
         i$4 = 0;
         bytelen$1 = BytesToDecode$3.length;
         while (i$4<bytelen$1) {
            c$2 = BytesToDecode$3[i$4];
            ++i$4;
            if (c$2<128) {
               Result+=TString$1.FromCharCode$1(c$2);
            } else if (c$2>191&&c$2<224) {
               c2$3 = BytesToDecode$3[i$4];
               ++i$4;
               Result+=TString$1.FromCharCode$1((((c$2&31)<<6)|(c2$3&63)));
            } else if (c$2>239&&c$2<365) {
               c2$4 = BytesToDecode$3[i$4];
               ++i$4;
               c3$2 = BytesToDecode$3[i$4];
               ++i$4;
               c4$1 = BytesToDecode$3[i$4];
               ++i$4;
               u$1 = (((((c$2&7)<<18)|((c2$4&63)<<12))|((c3$2&63)<<6))|(c4$1&63))-65536;
               Result+=TString$1.FromCharCode$1((55296+(u$1>>>10)));
               Result+=TString$1.FromCharCode$1((56320+(u$1&1023)));
            } else {
               c2$5 = BytesToDecode$3[i$4];
               ++i$4;
               c3$3 = BytesToDecode$3[i$4];
               ++i$4;
               Result+=TString$1.FromCharCode$1((((c$2&15)<<12)|(((c2$5&63)<<6)|(c3$3&63))));
            }
         }
      }
      return Result
   }
   ,DecodeData$2:function(Self, Source$6, Target$5) {
      /* null */
   }
   ,Encode$3:function(Self, TextToEncode$3) {
      var Result = [];
      var LEncoder$1 = null,
         LTyped$3 = null,
         LClip$1 = null;
      var LTemp$18 = null;
      var n$1 = 0;
      if (TextToEncode$3.length<1) {
         return [];
      }
      if (TQTXCodecUTF8.CanUseNativeConverter()) {
         LEncoder$1 = new TextEncoder("utf8");
         LTyped$3 = LEncoder$1.encode(TextToEncode$3);
         Result = TDataTypeConverter$1.TypedArrayToBytes$2(LTyped$3);
         LEncoder$1 = null;
         LTyped$3 = null;
      } else {
         if (TQTXCodecUTF8.CanUseClampedArray$1()) {
            LClip$1 = new Uint8ClampedArray(1);
            LTemp$18 = new Uint8ClampedArray(1);
         } else {
            LClip$1 = new Uint8Array(1);
            LTemp$18 = new Uint8Array(1);
         }
         var $temp49;
         for(n$1=1,$temp49=TextToEncode$3.length;n$1<=$temp49;n$1++) {
            LClip$1[0]=TString$1.CharCodeFor$1(TextToEncode$3.charAt(n$1-1));
            if (LClip$1[0]<128) {
               Result.push(LClip$1[0]);
            } else if (LClip$1[0]>127&&LClip$1[0]<2048) {
               LTemp$18[0]=((LClip$1[0]>>>6)|192);
               Result.push(LTemp$18[0]);
               LTemp$18[0]=((LClip$1[0]&63)|128);
               Result.push(LTemp$18[0]);
            } else {
               LTemp$18[0]=((LClip$1[0]>>>12)|224);
               Result.push(LTemp$18[0]);
               LTemp$18[0]=(((LClip$1[0]>>>6)&63)|128);
               Result.push(LTemp$18[0]);
               Result.push((LClip$1[0]&63)|128);
               Result.push(LTemp$18[0]);
            }
         }
      }
      return Result
   }
   ,EncodeData$2:function(Self, Source$7, Target$6) {
      /* null */
   }
   ,MakeCodecInfo$2:function(Self) {
      var Result = null;
      var LVersion$1 = {viMajor$1:0,viMinor$1:0,viRevision$1:0},
         LAccess$3 = null;
      Result = TObject.Create($New(TQTXCodecInfo));
      LVersion$1 = Create$71(0,2,0);
      LAccess$3 = $AsIntf(Result,"ICodecInfo$1");
      LAccess$3[0]("UTF8Codec");
      LAccess$3[1]("text\/utf8");
      LAccess$3[2](LVersion$1);
      LAccess$3[3]([6]);
      LAccess$3[5](1);
      LAccess$3[6](0);
      return Result
   }
   ,Destroy:TQTXCodec.Destroy
   ,Create$65:TQTXCodec.Create$65
   ,DecodeData$2$:function($){return $.ClassType.DecodeData$2.apply($.ClassType, arguments)}
   ,EncodeData$2$:function($){return $.ClassType.EncodeData$2.apply($.ClassType, arguments)}
   ,MakeCodecInfo$2$:function($){return $.ClassType.MakeCodecInfo$2($)}
};
TQTXCodecUTF8.$Intf={
   IQTXCodecProcess:[TQTXCodecUTF8.EncodeData$2,TQTXCodecUTF8.DecodeData$2]
   ,IQTXCodecBinding:[TQTXCodec.RegisterBinding$1,TQTXCodec.UnRegisterBinding$1]
}
function Copy$TQTXCodecVersionInfo(s,d) {
   d.viMajor$1=s.viMajor$1;
   d.viMinor$1=s.viMinor$1;
   d.viRevision$1=s.viRevision$1;
   return d;
}
function Clone$TQTXCodecVersionInfo($) {
   return {
      viMajor$1:$.viMajor$1,
      viMinor$1:$.viMinor$1,
      viRevision$1:$.viRevision$1
   }
}
function Create$71(Major, Minor, Revision) {
   var Result = {viMajor$1:0,viMinor$1:0,viRevision$1:0};
   Result.viMajor$1 = Major;
   Result.viMinor$1 = Minor;
   Result.viRevision$1 = Revision;
   return Result
}
var TQTXCodecManager = {
   $ClassName:"TQTXCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs$1 = [];
   }
   ,CodecByClass$1:function(Self, ClsType$1) {
      var Result = null;
      var a$132 = 0;
      var LItem$5 = null;
      var a$133 = [];
      Result = null;
      a$133 = Self.FCodecs$1;
      var $temp50;
      for(a$132=0,$temp50=a$133.length;a$132<$temp50;a$132++) {
         LItem$5 = a$133[a$132];
         if (TObject.ClassType(LItem$5.ClassType)==ClsType$1) {
            Result = LItem$5;
            break;
         }
      }
      return Result
   }
   ,Destroy:function(Self) {
      if (Self.FCodecs$1.length>0) {
         TQTXCodecManager.Reset$6(Self);
      }
      TObject.Destroy(Self);
   }
   ,RegisterCodec$1:function(Self, CodecClass$1) {
      var LItem$6 = null;
      LItem$6 = TQTXCodecManager.CodecByClass$1(Self,CodecClass$1);
      if (LItem$6===null) {
         LItem$6 = TDataTypeConverter$1.Create$65$($NewDyn(CodecClass$1,""));
         Self.FCodecs$1.push(LItem$6);
      } else {
         throw Exception.Create($New(ECodecManager$1),"Codec already registered error");
      }
   }
   ,Reset$6:function(Self) {
      var a$134 = 0;
      var codec = null;
      try {
         var a$135 = [];
         a$135 = Self.FCodecs$1;
         var $temp51;
         for(a$134=0,$temp51=a$135.length;a$134<$temp51;a$134++) {
            codec = a$135[a$134];
            try {
               TObject.Free(codec);
            } catch ($e) {
               /* null */
            }
         }
      } finally {
         Self.FCodecs$1.length=0;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var TQTXCodecInfo = {
   $ClassName:"TQTXCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout$1 = $.ciMime$1 = $.ciName$1 = "";
      $.ciDataFlow$1 = [0];
      $.ciInput$1 = 0;
      $.ciOutput$1 = 0;
      $.ciVersion$1 = {viMajor$1:0,viMinor$1:0,viRevision$1:0};
   }
   ,SetDataFlow$1:function(Self, coFlow$1) {
      Self.ciDataFlow$1 = coFlow$1.slice(0);
   }
   ,SetDescription$1:function(Self, coInfo$1) {
      Self.ciAbout$1 = coInfo$1;
   }
   ,SetInput$1:function(Self, InputType$1) {
      Self.ciInput$1 = InputType$1;
   }
   ,SetMime$1:function(Self, coMime$1) {
      Self.ciMime$1 = coMime$1;
   }
   ,SetName$2:function(Self, coName$1) {
      Self.ciName$1 = coName$1;
   }
   ,SetOutput$1:function(Self, OutputType$1) {
      Self.ciOutput$1 = OutputType$1;
   }
   ,SetVersion$1:function(Self, coVersion$1) {
      Self.ciVersion$1.viMajor$1 = coVersion$1.viMajor$1;
      Self.ciVersion$1.viMinor$1 = coVersion$1.viMinor$1;
      Self.ciVersion$1.viRevision$1 = coVersion$1.viRevision$1;
   }
   ,Destroy:TObject.Destroy
};
TQTXCodecInfo.$Intf={
   ICodecInfo$1:[TQTXCodecInfo.SetName$2,TQTXCodecInfo.SetMime$1,TQTXCodecInfo.SetVersion$1,TQTXCodecInfo.SetDataFlow$1,TQTXCodecInfo.SetDescription$1,TQTXCodecInfo.SetInput$1,TQTXCodecInfo.SetOutput$1]
}
var TQTXCodecBinding = {
   $ClassName:"TQTXCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec$1 = null;
   }
   ,Create$72:function(Self, EndPoint$1) {
      var LAccess$4 = null;
      TObject.Create(Self);
      if (EndPoint$1!==null) {
         Self.FCodec$1 = EndPoint$1;
         LAccess$4 = $AsIntf(Self.FCodec$1,"IQTXCodecBinding");
         LAccess$4[0](Self);
      } else {
         throw Exception.Create($New(ECodecBinding$1),"Binding failed, invalid endpoint error");
      }
      return Self
   }
   ,Destroy:function(Self) {
      var LAccess$5 = null;
      LAccess$5 = $AsIntf(Self.FCodec$1,"IQTXCodecBinding");
      LAccess$5[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
var TCodecDataFormat$1 = [ "cdBinary", "cdText" ];
function TCodecDataFlowHelper$1$Ordinal$1(Self$11) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$11,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$11,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
var TCodecDataDirection$1 = { 1:"cdRead", 2:"cdWrite" };
var ECodecError$1 = {
   $ClassName:"ECodecError",$Parent:EException
   ,$Init:function ($) {
      EException.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var ECodecManager$1 = {
   $ClassName:"ECodecManager",$Parent:ECodecError$1
   ,$Init:function ($) {
      ECodecError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var ECodecBinding$1 = {
   $ClassName:"ECodecBinding",$Parent:ECodecError$1
   ,$Init:function ($) {
      ECodecError$1.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager$1() {
   var Result = null;
   if (__Manager$1===null) {
      __Manager$1 = TObject.Create($New(TQTXCodecManager));
   }
   Result = __Manager$1;
   return Result
};
var TManagedMemory = {
   $ClassName:"TManagedMemory",$Parent:TDataTypeConverter$1
   ,$Init:function ($) {
      TDataTypeConverter$1.$Init($);
      $.OnMemoryReleased = null;
      $.OnMemoryAllocated = null;
      $.FArray = null;
      $.FBuffer$4 = $.FView$2 = null;
   }
   ,a$101:function(Self) {
      return (Self.FBuffer$4!==null)?Self.FBuffer$4.byteLength:0;
   }
   ,a$100:function(Self) {
      return Self.FBuffer$4===null;
   }
   ,AfterAllocate:function(Self) {
      if (Self.OnMemoryAllocated) {
         Self.OnMemoryAllocated(Self);
      }
   }
   ,AfterRelease:function(Self) {
      if (Self.OnMemoryReleased) {
         Self.OnMemoryReleased(Self);
      }
   }
   ,Allocate$1:function(Self, BytesToAllocate) {
      BytesToAllocate={v:BytesToAllocate};
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      if (BytesToAllocate.v>0) {
         TManagedMemory.BeforeAllocate(Self,BytesToAllocate);
         try {
            Self.FBuffer$4 = new ArrayBuffer(BytesToAllocate.v);
            Self.FView$2 = new DataView(Self.FBuffer$4);
            Self.FArray = new Uint8Array(Self.FBuffer$4);
         } catch ($e) {
            var e$11 = $W($e);
            Self.FBuffer$4 = null;
            Self.FView$2 = null;
            Self.FArray = null;
            throw $e;
         }
         TManagedMemory.AfterAllocate(Self);
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid size to allocate, value must be positive");
      }
   }
   ,Append$2:function(Self, Data$41) {
      var LOffset$7 = 0;
      if (Data$41.length>0) {
         LOffset$7 = (Self.FBuffer$4!==null)?TManagedMemory.a$101(Self):0;
         TManagedMemory.Grow$1(Self,Data$41.length);
         TManagedMemory.WriteBuffer$4(Self,LOffset$7,Data$41);
      }
   }
   ,Assign$1:function(Self, Memory$5) {
      var LSize$11 = 0;
      if (Memory$5===null) {
         TManagedMemory.Release$2(Self);
         return;
      }
      LSize$11 = Memory$5[2]();
      if (LSize$11<1) {
         TManagedMemory.Release$2(Self);
         return;
      }
      TManagedMemory.Allocate$1(Self,LSize$11);
      Self.FArray.set(Memory$5[0](),0);
   }
   ,BeforeAllocate:function(Self, NewSize$4) {
      /* null */
   }
   ,BeforeRelease:function(Self) {
      /* null */
   }
   ,Destroy:function(Self) {
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      TDataTypeConverter$1.Destroy(Self);
   }
   ,FromBytes:function(Self, Bytes$5) {
      var LLen$11 = 0;
      if (Self.FBuffer$4!==null) {
         TManagedMemory.Release$2(Self);
      }
      LLen$11 = Bytes$5.length;
      if (LLen$11>0) {
         TManagedMemory.Allocate$1(Self,LLen$11);
         (Self.FArray).set(Bytes$5, 0);
      }
   }
   ,GetPosition$3:function(Self) {
      return 0;
   }
   ,GetSize$5:function(Self) {
      return (Self.FBuffer$4!==null)?Self.FBuffer$4.byteLength:0;
   }
   ,Grow$1:function(Self, BytesToGrow) {
      var LOldData = [];
      if (BytesToGrow>0) {
         if (Self.FBuffer$4!==null) {
            LOldData = TManagedMemory.ToBytes$1(Self);
         }
         TManagedMemory.Allocate$1(Self,TManagedMemory.a$101(Self)+BytesToGrow);
         if (LOldData.length>0) {
            TManagedMemory.WriteBuffer$4(Self,0,LOldData);
         }
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid growth value, expected 1 or above error");
      }
   }
   ,ReadBuffer$4:function(Self, Offset$40, ReadLen) {
      var Result = [];
      var LTemp$19 = null;
      if (Self.FBuffer$4!==null) {
         if (ReadLen>0) {
            if (Offset$40>=0&&Offset$40<TManagedMemory.a$101(Self)) {
               LTemp$19 = Self.FArray.subarray(Offset$40,Offset$40+ReadLen);
               Result = Array.prototype.slice.call(LTemp$19);
            } else {
               throw Exception.Create($New(EManagedMemory),"Invalid offset, expected 0.."+(TManagedMemory.a$101(Self)-1).toString()+" not "+Offset$40.toString()+" error");
            }
         }
      }
      return Result
   }
   ,Release$2:function(Self) {
      if (Self.FBuffer$4!==null) {
         try {
            try {
               TManagedMemory.BeforeRelease(Self);
            } finally {
               Self.FArray = null;
               Self.FView$2 = null;
               Self.FBuffer$4 = null;
            }
         } finally {
            TManagedMemory.AfterRelease(Self);
         }
      }
   }
   ,ScaleTo:function(Self, NewSize$5) {
      if (NewSize$5>0) {
         if (NewSize$5!=TManagedMemory.a$101(Self)) {
            if (NewSize$5>TManagedMemory.a$101(Self)) {
               TManagedMemory.Grow$1(Self,(NewSize$5-TManagedMemory.a$101(Self)));
            } else {
               TManagedMemory.Shrink$1(Self,(TManagedMemory.a$101(Self)-NewSize$5));
            }
         }
      } else {
         TManagedMemory.Release$2(Self);
      }
   }
   ,Shrink$1:function(Self, BytesToShrink) {
      var LNewSize = 0,
         LCache$3 = [];
      if (BytesToShrink>0) {
         LNewSize = TManagedMemory.a$101(Self)-BytesToShrink;
         if (LNewSize<=0) {
            TManagedMemory.Release$2(Self);
            return;
         }
         LCache$3 = TManagedMemory.ReadBuffer$4(Self,0,LNewSize);
         TManagedMemory.Allocate$1(Self,LNewSize);
         TManagedMemory.WriteBuffer$4(Self,0,LCache$3);
      } else {
         throw Exception.Create($New(EManagedMemory),"Invalid shrink value, expected 1 or above error");
      }
   }
   ,ToBytes$1:function(Self) {
      var Result = [];
      if (Self.FBuffer$4!==null) {
         Result = Array.prototype.slice.call(Self.FArray);
      }
      return Result
   }
   ,WriteBuffer$4:function(Self, Offset$41, Data$42) {
      if (Self.FBuffer$4!==null) {
         if (Data$42.length>0) {
            if (Offset$41>=0&&Offset$41<TManagedMemory.a$101(Self)) {
               Self.FArray.set(Data$42,Offset$41);
            } else {
               throw Exception.Create($New(EManagedMemory),"Invalid offset, expected 0.."+(TManagedMemory.a$101(Self)-1).toString()+" not "+Offset$41.toString()+" error");
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$65:TDataTypeConverter$1.Create$65
};
TManagedMemory.$Intf={
   IManagedData:[TManagedMemory.ToBytes$1,TManagedMemory.FromBytes,TManagedMemory.GetSize$5,TManagedMemory.GetPosition$3,TManagedMemory.ReadBuffer$4,TManagedMemory.WriteBuffer$4,TManagedMemory.Grow$1,TManagedMemory.Shrink$1,TManagedMemory.Assign$1,TManagedMemory.Append$2]
}
var EManagedMemory = {
   $ClassName:"EManagedMemory",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
var CRC_Table_Ready$1 = false;
var CRC_Table$1 = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$7 = 0;
var a$10 = 0;
var a$9 = 0;
var a$8 = 0;
var a$11 = 0;
var a$12 = 0;
var a$30 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var NodeProgram = null;
var __EventsRef = undefined;
var __Parser = null;
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
var __Manager = null;
var _hex_char_lut = function () {
      for (var r=[],i=0; i<256; i++) r.push("");
      return r
   }();
var _hex_char_lut = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df", "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff"];
var __Resolved = false;
var __SupportCTA = false;
var __TYPE_MAP$1 = {Boolean$1:undefined,Number$2:undefined,String$2:undefined,Object$3:undefined,Undefined$1:undefined,Function$2:undefined};
var __SIZES$1 = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES$1 = ["","","","","","","","","","",""],
   __B64_Lookup$1 = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup$1 = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET$1 = "";
var __SIZES$1 = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES$1 = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET$1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __Manager$1 = null;
InitializeTypeMap();
InitializeBase64();
if (!Uint8Array.prototype.fill) {
      Uint8Array.prototype.fill = Array.prototype.fill;
    }
;
TQTXCodecManager.RegisterCodec$1(CodecManager$1(),TQTXCodecUTF8);
var __Def_Converter = TDataTypeConverter.Create$15$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
SetupTypeLUT();
TCodecManager.RegisterCodec(CodecManager(),TUTF8Codec);
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$43($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$43($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var __EventsRef = require("events");
var $Application = function() {
   NodeProgram = TKeygenService.Create$3($New(TKeygenService));
   TKeygenService.Execute(NodeProgram);
}
$Application();


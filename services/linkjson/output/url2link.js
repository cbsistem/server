var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"File operation [%s] failed, system threw exception: %s",
	"'Invalid handle for object (%s), reference rejected error",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"0123456789",
	"0123456789ABCDEF"];
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $Div(a,b) { var r=a/b; return (r>=0)?Math.floor(r):Math.ceil(r) }
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
/// TLinkApp = class (TObject)
///  [line: 27, column: 3, file: Unit1]
var TLinkApp = {
   $ClassName:"TLinkApp",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FParams = {failed_:false,url:"",title:"",handshake:"",output:""};
   }
   /// function TLinkApp.ParseParams() : TAppParams
   ///  [line: 53, column: 19, file: Unit1]
   ,ParseParams:function(Self) {
      var Result = {failed_:false,url:"",title:"",handshake:"",output:""};
      var LParams = [],
         LFailed = false,
         x$1 = 0;
      var a$89 = 0;
      var Lpar = {v:""},
         LPos = 0,
         LName = "";
      LFailed = false;
      if (ParamCount$1()<3) {
         Result.failed_ = true;
         WriteLn("No parameters, keygen aborted");
         return Result;
      }
      var $temp1;
      for(x$1=2,$temp1=ParamCount$1();x$1<$temp1;x$1++) {
         LParams.push(ParamStr$1(x$1));
      }
      var $temp2;
      for(a$89=0,$temp2=LParams.length;a$89<$temp2;a$89++) {
         Lpar.v = LParams[a$89];
         LPos = (Lpar.v.indexOf("=")+1);
         if (LPos<2) {
            WriteLnF("Syntax error, invalid parameter [%s]",[Lpar.v]);
            Result.failed_ = true;
            break;
         }
         LName = Trim$_String_(Lpar.v.substr(0,(LPos-1)));
         Delete(Lpar,1,LPos);
         {var $temp3 = (LName).toLocaleLowerCase();
            if ($temp3=="--url") {
               Result.url = Trim$_String_(Lpar.v);
            }
             else if ($temp3=="--out") {
               Result.output = Trim$_String_(Lpar.v);
            }
             else if ($temp3=="--title") {
               Result.title = Trim$_String_(Lpar.v);
            }
             else if ($temp3=="--handshake") {
               Result.handshake = Trim$_String_(Lpar.v);
            }
             else {
               WriteLnF("Syntax error, unknown option: %s",[LName]);
               Result.failed_ = true;
               break;
            }
         }
      }
      if (Result.failed_) {
         return Result;
      }
      return Result
   }
   /// procedure TLinkApp.Execute()
   ///  [line: 108, column: 20, file: Unit1]
   ,Execute:function(Self) {
      var LJSONTemplate = "",
         LRaw = undefined,
         LBytes = [],
         LObj = null,
         LFile = null;
      Self.FParams = TLinkApp.ParseParams(Self);
      if (Self.FParams.failed_) {
         return;
      }
      if (Trim$_String_(Self.FParams.output).length<3) {
         if (Self.FParams.output!="stdout") {
            WriteLn("Invalid output, no -output:filename has been set. Aborting mint.");
            return;
         }
      }
      if (Self.FParams.output!="stdout") {
         WriteLnF("Quartex Url to Linkfile",[1, 0]);
         WriteLn("Written by Jon Lennart Aasenden");
         WriteLn("Copyright Quartex Components LTD");
         WriteLn("--------------------------------");
         WriteLn("Output set to: "+Self.FParams.output);
      }
      LJSONTemplate = "{\r\n  \"datatype\": \"link\",\r\n  \"version\": {\r\n    \"major\": 1,\r\n    \"minor\": 0,\r\n    \"revision\": 0\r\n},\r\n  \"action\": {\r\n    \"method\": \"open\",\r\n    \"source\": \"Url\"\r\n},\r\n\"container\": {\r\n    \"type\": \"hosted\",\r\n\t\"title\": \"Application\",\r\n    \"width\": 820,\r\n    \"height\": 656,\r\n    \"fullscreen\": false,\r\n    \"handshake\": false,\r\n    \"keyboard\": true,\r\n    \"resize\": true\r\n  }\r\n}";
      LRaw = JSON.parse(LJSONTemplate);
      LRaw["action"]["source"] = TString.EncodeBase64(TString,Self.FParams.url);
      LRaw["container"]["handshake"] = ((Self.FParams.handshake).toLocaleLowerCase()=="yes")?true:false;
      LRaw["container"]["title"] = TString.EncodeBase64(TString,Self.FParams.title);
      LBytes = TDatatype.StringToBytes$1(TDatatype,JSON.stringify(LRaw,null,2));
      LRaw = null;
      LObj = TJSONObject.Create$44($New(TJSONObject),LRaw);
      try {
         LFile = TCustomFileStream.Create$32$($New(TFileStream),Self.FParams.output,1);
         try {
            TStream.Write$1(LFile,LBytes);
         } finally {
            TObject.Free(LFile);
         }
      } finally {
         TObject.Free(LObj);
      }
   }
   /// constructor TLinkApp.Create()
   ///  [line: 43, column: 22, file: Unit1]
   ,Create$3:function(Self) {
      TObject.Create(Self);
      return Self
   }
   /// destructor TLinkApp.Destroy()
   ///  [line: 48, column: 21, file: Unit1]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TAppParams = record
///  [line: 19, column: 3, file: Unit1]
function Copy$TAppParams(s,d) {
   d.failed_=s.failed_;
   d.url=s.url;
   d.title=s.title;
   d.handshake=s.handshake;
   d.output=s.output;
   return d;
}
function Clone$TAppParams($) {
   return {
      failed_:$.failed_,
      url:$.url,
      title:$.title,
      handshake:$.handshake,
      output:$.output
   }
}
/// function TW3VariantHelper.DataType(const Self: Variant) : TW3VariantDataType
///  [line: 1944, column: 27, file: System.Types]
function TW3VariantHelper$DataType(Self$1) {
   var Result = 1;
   var LType = "";
   if (TW3VariantHelper$Valid$2(Self$1)) {
      LType = typeof(Self$1);
      {var $temp4 = (LType).toLocaleLowerCase();
         if ($temp4=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp4=="function") {
            Result = 7;
         }
          else if ($temp4=="symbol") {
            Result = 6;
         }
          else if ($temp4=="boolean") {
            Result = 2;
         }
          else if ($temp4=="string") {
            Result = 5;
         }
          else if ($temp4=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp4=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
/// function TW3VariantHelper.IsObject(const Self: Variant) : Boolean
///  [line: 1991, column: 27, file: System.Types]
function TW3VariantHelper$IsObject(Self$2) {
   var Result = false;
   Result = ((Self$2) !== undefined)
      && (Self$2 !== null)
      && (typeof Self$2  === "object")
      && ((Self$2).length === undefined);
   return Result
}
/// function TW3VariantHelper.Valid(const Self: Variant) : Boolean
///  [line: 1902, column: 27, file: System.Types]
function TW3VariantHelper$Valid$2(Self$3) {
   var Result = false;
   Result = !( (Self$3 == undefined) || (Self$3 == null) );
   return Result
}
/// TW3VariantDataType enumeration
///  [line: 574, column: 3, file: System.Types]
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TW3OwnedObject = class (TObject)
///  [line: 373, column: 3, file: System.Types]
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   /// function TW3OwnedObject.GetOwner() : TObject
   ///  [line: 1306, column: 26, file: System.Types]
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   /// procedure TW3OwnedObject.SetOwner(const NewOwner: TObject)
   ///  [line: 1316, column: 26, file: System.Types]
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   /// function TW3OwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1311, column: 25, file: System.Types]
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   /// constructor TW3OwnedObject.Create(const AOwner: TObject)
   ///  [line: 1300, column: 28, file: System.Types]
   ,Create$16:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3FilePermissionMask enumeration
///  [line: 80, column: 3, file: System.Types]
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
/// TVariant = class (TObject)
///  [line: 525, column: 3, file: System.Types]
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.AsBool(const aValue: Variant) : Boolean
   ///  [line: 2350, column: 25, file: System.Types]
   ,AsBool:function(aValue) {
      var Result = false;
      if (aValue!=undefined&&aValue!=null) {
         Result = (aValue?true:false);
      }
      return Result
   }
   /// function TVariant.AsFloat(const aValue: Variant) : Float
   ///  [line: 2333, column: 25, file: System.Types]
   ,AsFloat:function(aValue$1) {
      var Result = 0;
      if (aValue$1!=undefined&&aValue$1!=null) {
         Result = Number(aValue$1);
      }
      return Result
   }
   /// function TVariant.AsInteger(const aValue: Variant) : Integer
   ///  [line: 2319, column: 25, file: System.Types]
   ,AsInteger:function(aValue$2) {
      var Result = 0;
      if (aValue$2!=undefined&&aValue$2!=null) {
         Result = parseInt(aValue$2,10);
      }
      return Result
   }
   /// function TVariant.AsString(const aValue: Variant) : String
   ///  [line: 2326, column: 25, file: System.Types]
   ,AsString:function(aValue$3) {
      var Result = "";
      if (aValue$3!=undefined&&aValue$3!=null) {
         Result = String(aValue$3);
      }
      return Result
   }
   /// function TVariant.CreateObject() : Variant
   ///  [line: 2371, column: 25, file: System.Types]
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// procedure TVariant.ForEachProperty(const Data: Variant; const CallBack: TW3ObjectKeyCallback)
   ///  [line: 2454, column: 26, file: System.Types]
   ,ForEachProperty:function(Data$1, CallBack) {
      var LObj$1,
         Keys$1 = [],
         a$90 = 0;
      var LName$1 = "";
      if (CallBack) {
         Keys$1 = TVariant.Properties(Data$1);
         var $temp5;
         for(a$90=0,$temp5=Keys$1.length;a$90<$temp5;a$90++) {
            LName$1 = Keys$1[a$90];
            LObj$1 = Keys$1[LName$1];
            if ((~CallBack(LName$1,LObj$1))==1) {
               break;
            }
         }
      }
   }
   /// function TVariant.Properties(const Data: Variant) : TStrArray
   ///  [line: 2473, column: 25, file: System.Types]
   ,Properties:function(Data$2) {
      var Result = [];
      if (Data$2) {
         if (!(Object.keys === undefined)) {
        Result = Object.keys(Data$2);
        return Result;
      }
         if (!(Object.getOwnPropertyNames === undefined)) {
          Result = Object.getOwnPropertyNames(Data$2);
          return Result;
      }
         for (var qtxenum in Data$2) {
        if ( (Data$2).hasOwnProperty(qtxenum) == true )
          (Result).push(qtxenum);
      }
      return Result;
      }
      return Result
   }
   /// function TVariant.ValidRef(const aValue: Variant) : Boolean
   ///  [line: 2314, column: 25, file: System.Types]
   ,ValidRef:function(aValue$4) {
      return aValue$4!=undefined&&aValue$4!=null;
   }
   ,Destroy:TObject.Destroy
};
/// TTextFormation enumeration
///  [line: 216, column: 3, file: System.Types]
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
/// function TStringHelper.ContainsHex(const Self: String) : Boolean
///  [line: 1769, column: 24, file: System.Types]
function TStringHelper$ContainsHex(Self$4) {
   var Result = false;
   var x$2 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen = 0;
   Result = false;
   LLen = Self$4.length;
   if (LLen>=1) {
      LStart = 1;
      if (Self$4.charAt(0)=="$") {
         ++LStart;
         --LLen;
      } else {
         LItem = (Self$4.substr(0,1)).toLocaleUpperCase();
         Result = ($R[23].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen>=1) {
         var $temp6;
         for(x$2=LStart,$temp6=Self$4.length;x$2<=$temp6;x$2++) {
            LItem = (Self$4.charAt(x$2-1)).toLocaleUpperCase();
            Result = ($R[23].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsOrdinal(const Self: String) : Boolean
///  [line: 1753, column: 24, file: System.Types]
function TStringHelper$ContainsOrdinal(Self$5) {
   var Result = false;
   var LLen$1 = 0,
      x$3 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$1 = Self$5.length;
   if (LLen$1>=1) {
      var $temp7;
      for(x$3=1,$temp7=LLen$1;x$3<=$temp7;x$3++) {
         LItem$1 = Self$5.charAt(x$3-1);
         Result = ($R[22].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsFloat(const Self: String) : Boolean
///  [line: 1698, column: 24, file: System.Types]
function TStringHelper$ContainsFloat(Self$6) {
   var Result = false;
   var x$4 = 0;
   var LItem$2 = "";
   var LLen$2 = 0;
   var LLine = false;
   Result = false;
   LLen$2 = Self$6.length;
   if (LLen$2>=1) {
      LLine = false;
      var $temp8;
      for(x$4=1,$temp8=LLen$2;x$4<=$temp8;x$4++) {
         LItem$2 = Self$6.charAt(x$4-1);
         if (LItem$2==".") {
            if (x$4==1&&LLen$2==1) {
               break;
            }
            if (x$4==1&&LLen$2>1) {
               LLine = true;
               continue;
            }
            if (x$4>1&&x$4<LLen$2) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsQuote(const Self: String) : Boolean
///  [line: 1617, column: 24, file: System.Types]
function TStringHelper$ContainsQuote(Self$7) {
   var Result = false;
   var LLen$3 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$3 = Self$7.length;
   if (LLen$3>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$3) {
         if (Self$7.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$7.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$7.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$3) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$3) {
         if (Self$7.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$7.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$3) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$3) {
         if (Self$7.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
/// TString = class (TObject)
///  [line: 235, column: 3, file: System.Types.Convert]
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.CharCodeFor(const Character: Char) : Integer
   ///  [line: 1369, column: 24, file: System.Types]
   ,CharCodeFor:function(Self, Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   /// function TString.CreateGUID() : String
   ///  [line: 1954, column: 24, file: System.Types.Convert]
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TByteArray) : String
   ///  [line: 1428, column: 24, file: System.Types]
   ,DecodeUTF8:function(Self, BytesToDecode) {
      var Result = "";
      var LCodec = null;
      LCodec = TCustomCodec.Create$28($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Decode(LCodec,BytesToDecode);
      } finally {
         TObject.Free(LCodec);
      }
      return Result
   }
   /// function TString.EncodeBase64(TextToEncode: String) : String
   ///  [line: 1972, column: 24, file: System.Types.Convert]
   ,EncodeBase64:function(Self, TextToEncode) {
      return TBase64EncDec.StringToBase64(TBase64EncDec,TextToEncode);
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TByteArray
   ///  [line: 1418, column: 24, file: System.Types]
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = {v:[]};
      try {
         var LCodec$1 = null;
         LCodec$1 = TCustomCodec.Create$28($New(TUTF8Codec));
         try {
            Result.v = TUTF8Codec.Encode(LCodec$1,TextToEncode$1);
         } finally {
            TObject.Free(LCodec$1);
         }
      } finally {return Result.v}
   }
   /// function TString.FromCharCode(const CharCode: Byte) : Char
   ///  [line: 1392, column: 24, file: System.Types]
   ,FromCharCode:function(Self, CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TInteger = class (TObject)
///  [line: 474, column: 3, file: System.Types]
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ///  [line: 2203, column: 25, file: System.Types]
   ,Diff:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const aValue: Integer; const aMin: Integer; const aMax: Integer) : Integer
   ///  [line: 2157, column: 25, file: System.Types]
   ,EnsureRange:function(aValue$5, aMin, aMax) {
      return ClampInt(aValue$5,aMin,aMax);
   }
   /// procedure TInteger.SetBit(index: Integer; aValue: Boolean; var buffer: Integer)
   ///  [line: 2072, column: 26, file: System.Types]
   ,SetBit:function(index, aValue$6, buffer$1) {
      if (index>=0&&index<=31) {
         if (aValue$6) {
            buffer$1.v = buffer$1.v|(1<<index);
         } else {
            buffer$1.v = buffer$1.v&(~(1<<index));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   /// function TInteger.SubtractSmallest(const First: Integer; const Second: Integer) : Integer
   ///  [line: 2129, column: 25, file: System.Types]
   ,SubtractSmallest:function(First, Second) {
      var Result = 0;
      if (First<Second) {
         Result = Second-First;
      } else {
         Result = First-Second;
      }
      return Result
   }
   /// function TInteger.ToNearest(const Value: Integer; const Factor: Integer) : Integer
   ///  [line: 2188, column: 25, file: System.Types]
   ,ToNearest:function(Value, Factor) {
      var Result = 0;
      var FTemp = 0;
      Result = Value;
      FTemp = Value%Factor;
      if (FTemp>0) {
         (Result+= (Factor-FTemp));
      }
      return Result
   }
   /// function TInteger.WrapRange(const aValue: Integer; const aLowRange: Integer; const aHighRange: Integer) : Integer
   ///  [line: 2171, column: 25, file: System.Types]
   ,WrapRange:function(aValue$7, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$7>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$7-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$7<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$7+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$7;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TFileAccessMode enumeration
///  [line: 135, column: 3, file: System.Types]
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
/// TEnumState enumeration
///  [line: 129, column: 3, file: System.Types]
var TEnumState = { 1:"esContinue", 0:"esAbort" };
/// TDataTypeMap = record
///  [line: 515, column: 3, file: System.Types]
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
/// EW3Exception = class (Exception)
///  [line: 245, column: 3, file: System.Types]
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EW3Exception.CreateFmt(aText: String; const aValues: array of const)
   ///  [line: 1884, column: 26, file: System.Types]
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   /// constructor EW3Exception.Create(const MethodName: String; const Instance: TObject; const ErrorText: String)
   ///  [line: 1889, column: 26, file: System.Types]
   ,Create$27:function(Self, MethodName, Instance$1, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance$1)?TObject.ClassName(Instance$1.ClassType):"Anonymous";
      EW3Exception.CreateFmt(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
};
/// EW3OwnedObject = class (EW3Exception)
///  [line: 364, column: 3, file: System.Types]
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
/// TValuePrefixType enumeration
///  [line: 52, column: 3, file: System.Types.Convert]
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TSystemEndianType enumeration
///  [line: 66, column: 3, file: System.Types.Convert]
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
/// TRTLDatatype enumeration
///  [line: 37, column: 3, file: System.Types.Convert]
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
/// TDataTypeConverter = class (TObject)
///  [line: 73, column: 3, file: System.Types.Convert]
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TByteArray
   ///  [line: 1041, column: 35, file: System.Types.Convert]
   ,BooleanToBytes:function(Self, Value$1) {
      var Result = [];
      Result.push((Value$1)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1269, column: 35, file: System.Types.Convert]
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TByteArray) : Boolean
   ///  [line: 1274, column: 29, file: System.Types.Convert]
   ,BytesToBoolean:function(Self, Data$3) {
      return Data$3[0]>0;
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TByteArray) : Float
   ///  [line: 1219, column: 29, file: System.Types.Convert]
   ,BytesToFloat32:function(Self, Data$4) {
      var Result = 0;
      Self.FView.setUint8(0,Data$4[0]);
      Self.FView.setUint8(1,Data$4[1]);
      Self.FView.setUint8(2,Data$4[2]);
      Self.FView.setUint8(3,Data$4[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TByteArray) : Float
   ///  [line: 990, column: 29, file: System.Types.Convert]
   ,BytesToFloat64:function(Self, Data$5) {
      var Result = 0;
      Self.FView.setUint8(0,Data$5[0]);
      Self.FView.setUint8(1,Data$5[1]);
      Self.FView.setUint8(2,Data$5[2]);
      Self.FView.setUint8(3,Data$5[3]);
      Self.FView.setUint8(4,Data$5[4]);
      Self.FView.setUint8(5,Data$5[5]);
      Self.FView.setUint8(6,Data$5[6]);
      Self.FView.setUint8(7,Data$5[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TByteArray) : SmallInt
   ///  [line: 1008, column: 29, file: System.Types.Convert]
   ,BytesToInt16:function(Self, Data$6) {
      var Result = 0;
      Self.FView.setUint8(0,Data$6[0]);
      Self.FView.setUint8(1,Data$6[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TByteArray) : Integer
   ///  [line: 946, column: 29, file: System.Types.Convert]
   ,BytesToInt32:function(Self, Data$7) {
      var Result = 0;
      Self.FView.setUint8(0,Data$7[0]);
      Self.FView.setUint8(1,Data$7[1]);
      Self.FView.setUint8(2,Data$7[2]);
      Self.FView.setUint8(3,Data$7[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TByteArray) : String
   ///  [line: 1307, column: 29, file: System.Types.Convert]
   ,BytesToString:function(Self, Data$8) {
      var Result = "";
      Result = String.fromCharCode.apply(String, Data$8);
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1020, column: 29, file: System.Types.Convert]
   ,BytesToTypedArray:function(Self, Values$1) {
      var Result = undefined;
      var LLen$4 = 0;
      LLen$4 = Values$1.length;
      Result = new Uint8Array(LLen$4);
      (Result).set(Values$1, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TByteArray) : Word
   ///  [line: 1090, column: 29, file: System.Types.Convert]
   ,BytesToUInt16:function(Self, Data$9) {
      var Result = 0;
      Self.FView.setUint8(0,Data$9[0]);
      Self.FView.setUint8(1,Data$9[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TByteArray) : Longword
   ///  [line: 892, column: 29, file: System.Types.Convert]
   ,BytesToUInt32:function(Self, Data$10) {
      var Result = 0;
      Self.FView.setUint8(0,Data$10[0]);
      Self.FView.setUint8(1,Data$10[1]);
      Self.FView.setUint8(2,Data$10[2]);
      Self.FView.setUint8(3,Data$10[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TByteArray) : Variant
   ///  [line: 1115, column: 29, file: System.Types.Convert]
   ,BytesToVariant:function(Self, Data$11) {
      var Result = undefined;
      var LType$1 = 0;
      LType$1 = Data$11[0];
      Data$11.shift();
      switch (LType$1) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$11);
            break;
         case 162 :
            Result = Data$11[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$11);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$11);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$11);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$11);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$11);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$11);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$11);
            break;
         default :
            throw EW3Exception.CreateFmt($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$1)]);
      }
      return Result
   }
   /// function TDataTypeConverter.ByteToChar(const Value: Byte) : Char
   ///  [line: 1314, column: 35, file: System.Types.Convert]
   ,ByteToChar:function(Self, Value$2) {
      var Result = "";
      Result = String.fromCharCode(Value$2);
      return Result
   }
   /// function TDataTypeConverter.CharToByte(const Value: Char) : Word
   ///  [line: 1064, column: 35, file: System.Types.Convert]
   ,CharToByte:function(Self, Value$3) {
      var Result = 0;
      Result = (Value$3).charCodeAt(0);
      return Result
   }
   /// constructor TDataTypeConverter.Create()
   ///  [line: 630, column: 32, file: System.Types.Convert]
   ,Create$15:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ///  [line: 640, column: 31, file: System.Types.Convert]
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TByteArray
   ///  [line: 1102, column: 29, file: System.Types.Convert]
   ,Float32ToBytes:function(Self, Value$4) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$4);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$4,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$4,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TByteArray
   ///  [line: 918, column: 29, file: System.Types.Convert]
   ,Float64ToBytes:function(Self, Value$5) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$5));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$5),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$5),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   /// function TDataTypeConverter.InitFloat32(const Value: float32) : float32
   ///  [line: 714, column: 35, file: System.Types.Convert]
   ,InitFloat32:function(Value$6) {
      var Result = 0;
      var temp = null;
      temp = new Float32Array(1);
      temp[0]=Value$6;
      Result = temp[0];
      temp = null;
      return Result
   }
   /// function TDataTypeConverter.InitFloat64(const Value: float64) : float64
   ///  [line: 722, column: 35, file: System.Types.Convert]
   ,InitFloat64:function(Value$7) {
      var Result = undefined;
      var temp$1 = null;
      temp$1 = new Float64Array(1);
      temp$1[0]=(Number(Value$7));
      Result = temp$1[0];
      temp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: Byte) : Byte
   ///  [line: 746, column: 35, file: System.Types.Convert]
   ,InitInt08:function(Value$8) {
      var Result = 0;
      var temp$2 = null;
      temp$2 = new Int8Array(1);
      temp$2[0]=((Value$8<-128)?-128:(Value$8>127)?127:Value$8);
      Result = temp$2[0];
      temp$2 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: SmallInt) : SmallInt
   ///  [line: 738, column: 35, file: System.Types.Convert]
   ,InitInt16:function(Value$9) {
      var Result = 0;
      var temp$3 = null;
      temp$3 = new Int16Array(1);
      temp$3[0]=((Value$9<-32768)?-32768:(Value$9>32767)?32767:Value$9);
      Result = temp$3[0];
      temp$3 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: Integer) : Integer
   ///  [line: 730, column: 35, file: System.Types.Convert]
   ,InitInt32:function(Value$10) {
      var Result = 0;
      var temp$4 = null;
      temp$4 = new Int32Array(1);
      temp$4[0]=((Value$10<-2147483648)?-2147483648:(Value$10>2147483647)?2147483647:Value$10);
      Result = temp$4[0];
      temp$4 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: Byte) : Byte
   ///  [line: 770, column: 35, file: System.Types.Convert]
   ,InitUint08:function(Value$11) {
      var Result = 0;
      var LTemp = null;
      LTemp = new Uint8Array(1);
      LTemp[0]=((Value$11<0)?0:(Value$11>255)?255:Value$11);
      Result = LTemp[0];
      LTemp = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: Word) : Word
   ///  [line: 762, column: 35, file: System.Types.Convert]
   ,InitUint16:function(Value$12) {
      var Result = 0;
      var temp$5 = null;
      temp$5 = new Uint16Array(1);
      temp$5[0]=((Value$12<0)?0:(Value$12>65536)?65536:Value$12);
      Result = temp$5[0];
      temp$5 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: Longword) : Longword
   ///  [line: 754, column: 35, file: System.Types.Convert]
   ,InitUint32:function(Value$13) {
      var Result = 0;
      var temp$6 = null;
      temp$6 = new Uint32Array(1);
      temp$6[0]=((Value$13<0)?0:(Value$13>4294967295)?4294967295:Value$13);
      Result = temp$6[0];
      temp$6 = null;
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: SmallInt) : TByteArray
   ///  [line: 1245, column: 29, file: System.Types.Convert]
   ,Int16ToBytes:function(Self, Value$14) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$14);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$14,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$14,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: Integer) : TByteArray
   ///  [line: 867, column: 29, file: System.Types.Convert]
   ,Int32ToBytes:function(Self, Value$15) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$15);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$15,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$15,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TSystemEndianType)
   ///  [line: 648, column: 30, file: System.Types.Convert]
   ,SetEndian:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian) {
         Self.FEndian = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TRTLDatatype) : Integer
   ///  [line: 684, column: 35, file: System.Types.Convert]
   ,SizeOfType:function(Self, Kind) {
      return __SIZES[Kind];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1298, column: 29, file: System.Types.Convert]
   ,StringToBytes:function(Self, Value$16) {
      var Result = [];
      Result = (Value$16).split("").map( function( val ) {
        return val.charCodeAt( 0 );
    } );
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TByteArray
   ///  [line: 906, column: 29, file: System.Types.Convert]
   ,TypedArrayToBytes:function(Self, Value$17) {
      var Result = [];
      if (!TVariant.ValidRef(Value$17)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[21]);
      }
      Result = Array.prototype.slice.call(Value$17);
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle) : String
   ///  [line: 930, column: 29, file: System.Types.Convert]
   ,TypedArrayToStr:function(Self, Value$18) {
      var Result = "";
      var x$5 = 0;
      if (TVariant.ValidRef(Value$18)) {
         if (Value$18.length>0) {
            var $temp9;
            for(x$5=0,$temp9=Value$18.length-1;x$5<=$temp9;x$5++) {
               Result += String.fromCharCode((Value$18)[x$5]);
            }
         }
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle) : Longword
   ///  [line: 831, column: 29, file: System.Types.Convert]
   ,TypedArrayToUInt32:function(Self, Value$19) {
      var Result = 0;
      var LBuffer = null,
         LBytes$1 = 0,
         LTypeSize = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$19)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[21]);
      }
      LBuffer = Value$19.buffer;
      LBytes$1 = LBuffer.byteLength;
      LTypeSize = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes$1<LTypeSize) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[20],["Int32", LTypeSize, LBytes$1]));
      }
      if (LBytes$1>LTypeSize) {
         LBytes$1 = LTypeSize;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getUint32(0);
            break;
         case 1 :
            Result = LView.getUint32(0,true);
            break;
         case 2 :
            Result = LView.getUint32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: Word) : TByteArray
   ///  [line: 1257, column: 29, file: System.Types.Convert]
   ,UInt16ToBytes:function(Self, Value$20) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$20);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$20,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$20,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: Longword) : TByteArray
   ///  [line: 880, column: 29, file: System.Types.Convert]
   ,UInt32ToBytes:function(Self, Value$21) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$21);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$21,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$21,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(Value: Variant) : TByteArray
   ///  [line: 1138, column: 29, file: System.Types.Convert]
   ,VariantToBytes:function(Self, Value$22) {
      var Result = [];
      var LType$2 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$22<=255) {
            return 162;
         }
         if (Value$22<=65536) {
            return 168;
         }
         if (Value$22<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$22>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$22>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32(x$6) {
         var Result = false;
         Result = isFinite(x$6) && x$6 == Math.fround(x$6);
         return Result
      };
      switch (TW3VariantHelper$DataType(Value$22)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$22?true:false)));
            break;
         case 3 :
            if (Value$22<0) {
               LType$2 = GetSignedIntType();
            } else {
               LType$2 = GetUnSignedIntType();
            }
            if (LType$2) {
               Result = [LType$2].slice();
               switch (LType$2) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$22,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$22,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$22,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$22,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$22,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32(Value$22)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$22))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$22))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$22)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
};
/// TDatatype = class (TObject)
///  [line: 158, column: 3, file: System.Types.Convert]
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TDatatype.BytesToString(const Data: TByteArray) : String
   ///  [line: 1452, column: 26, file: System.Types.Convert]
   ,BytesToString$1:function(Self, Data$12) {
      return TDataTypeConverter.BytesToString(__Def_Converter,Data$12);
   }
   /// function TDatatype.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1442, column: 26, file: System.Types.Convert]
   ,BytesToTypedArray$1:function(Self, Values$2) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$2);
   }
   /// function TDatatype.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1447, column: 26, file: System.Types.Convert]
   ,StringToBytes$1:function(Self, Value$23) {
      return TDataTypeConverter.StringToBytes(__Def_Converter,Value$23);
   }
   /// function TDatatype.TypedArrayToBytes(const Value: TDefaultBufferType) : TByteArray
   ///  [line: 1340, column: 26, file: System.Types.Convert]
   ,TypedArrayToBytes$1:function(Self, Value$24) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$24);
   }
   /// function TDatatype.TypedArrayToUInt32(const Value: TDefaultBufferType) : Longword
   ///  [line: 1345, column: 26, file: System.Types.Convert]
   ,TypedArrayToUInt32$1:function(Self, Value$25) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$25);
   }
   ,Destroy:TObject.Destroy
};
/// TBase64EncDec = class (TObject)
///  [line: 291, column: 3, file: System.Types.Convert]
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBase64EncDec.Base64ToBytes(const b64: String) : TByteArray
   ///  [line: 461, column: 30, file: System.Types.Convert]
   ,Base64ToBytes$2:function(Self, b64) {
      var Result = [];
      var ASeg = 0;
      var BSeg = 0;
      var CSeg = 0;
      var DSeg = 0;
      var LTextLen = 0,
         LPlaceholderCount = 0,
         BufferSize = 0,
         xpos = 0,
         idx = 0,
         temp$7 = 0,
         temp$8 = 0,
         temp$9 = 0;
      LTextLen = b64.length;
      if (LTextLen>0) {
         LPlaceholderCount = 0;
         if (LTextLen%4<1) {
            LPlaceholderCount = (b64.charAt((LTextLen-1)-1)=="=")?2:(b64.charAt(LTextLen-1)=="=")?1:0;
         }
         BufferSize = ($Div(LTextLen*3,4))-LPlaceholderCount;
         $ArraySetLenC(Result,BufferSize,function (){return 0});
         if (LPlaceholderCount>0) {
            (LTextLen-= 4);
         }
         xpos = 1;
         idx = 0;
         while (xpos<LTextLen) {
            ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<18;
            BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<12;
            CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]<<6;
            DSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+2))];
            temp$7 = ((ASeg|BSeg)|CSeg)|DSeg;
            Result[idx]=(temp$7>>>16)&255;
            ++idx;
            Result[idx]=(temp$7>>>8)&255;
            ++idx;
            Result[idx]=temp$7&255;
            ++idx;
            (xpos+= 4);
         }
         switch (LPlaceholderCount) {
            case 1 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<2;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]>>>4;
               temp$8 = ASeg|BSeg;
               Result[idx]=temp$8&255;
               break;
            case 2 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<10;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<4;
               CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]>>>2;
               temp$9 = (ASeg|BSeg)|CSeg;
               Result[idx]=(temp$9>>>8)&255;
               ++idx;
               Result[idx]=temp$9&255;
               break;
         }
      }
      return Result
   }
   /// function TBase64EncDec.BytesToBase64(const Data: TByteArray) : String
   ///  [line: 514, column: 30, file: System.Types.Convert]
   ,BytesToBase64$2:function(Self, Data$13) {
      var Result = "";
      var LLen$5 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i = 0,
         Ahead = 0,
         SegSize = 0,
         output$1 = "",
         LTemp$1 = 0,
         LTemp$2 = 0;
      LLen$5 = Data$13.length;
      if (LLen$5>0) {
         LExtra = Data$13.length%3;
         LStrideLen = LLen$5-LExtra;
         LMaxChunkLength = 16383;
         i = 0;
         while (i<LStrideLen) {
            Ahead = i+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$13,i,SegSize);
            (i+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$5;
         }
         output$1 = "";
         switch (LExtra) {
            case 1 :
               LTemp$1 = Data$13[LLen$5];
               output$1+=__B64_Lookup[LTemp$1>>>2];
               output$1+=__B64_Lookup[(LTemp$1<<4)&63];
               output$1+="==";
               break;
            case 2 :
               LTemp$2 = (Data$13[LLen$5-1]<<8)+Data$13[LLen$5];
               output$1+=__B64_Lookup[LTemp$2>>>10];
               output$1+=__B64_Lookup[(LTemp$2>>>4)&63];
               output$1+=__B64_Lookup[(LTemp$2<<2)&63];
               output$1+="=";
               break;
         }
         Result+=output$1;
      }
      return Result
   }
   /// function TBase64EncDec.EncodeChunk(const Data: TByteArray; startpos: Integer; endpos: Integer) : String
   ///  [line: 424, column: 30, file: System.Types.Convert]
   ,EncodeChunk:function(Self, Data$14, startpos, endpos) {
      var Result = "";
      var temp$10 = 0;
      while (startpos<endpos) {
         temp$10 = (Data$14[startpos]<<16)+(Data$14[startpos+1]<<8)+Data$14[startpos+2];
         Result+=__B64_Lookup[(temp$10>>>18)&63]+__B64_Lookup[(temp$10>>>12)&63]+__B64_Lookup[(temp$10>>>6)&63]+__B64_Lookup[temp$10&63];
         (startpos+= 3);
      }
      return Result
   }
   /// function TBase64EncDec.StringToBase64(const Text: String) : String
   ///  [line: 402, column: 30, file: System.Types.Convert]
   ,StringToBase64:function(Self, Text$1) {
      var Result = "";
      Result = btoa(Text$1);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EDatatype = class (EW3Exception)
///  [line: 22, column: 3, file: System.Types.Convert]
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EConvertError = class (EW3Exception)
///  [line: 23, column: 3, file: System.Types.Convert]
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$1 = 0;
   var $temp10;
   for(i$1=1,$temp10=CNT_B64_CHARSET.length;i$1<=$temp10;i$1++) {
      __B64_Lookup[i$1-1] = CNT_B64_CHARSET.charAt(i$1-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$1-1))] = i$1-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
/// TUnManaged = class (TObject)
///  [line: 106, column: 3, file: System.Memory]
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TUnManaged.AllocMemA(const Size: Integer) : TMemoryHandle
   ///  [line: 245, column: 27, file: System.Memory]
   ,AllocMemA:function(Self, Size$5) {
      var Result = undefined;
      if (Size$5>0) {
         Result = new Uint8Array(Size$5);
      } else {
         Result = null;
      }
      return Result
   }
   /// function TUnManaged.ReAllocMemA(const Memory: TMemoryHandle; Size: Integer) : TMemoryHandle
   ///  [line: 264, column: 27, file: System.Memory]
   ,ReAllocMemA:function(Self, Memory$1, Size$6) {
      var Result = undefined;
      if (Memory$1) {
         if (Size$6>0) {
            Result = new Uint8Array(Size$6);
            TMarshal.Move$1(TMarshal,Memory$1,0,Result,0,Size$6);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$6);
      }
      return Result
   }
   /// function TUnManaged.ReadMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer) : TMemoryHandle
   ///  [line: 347, column: 27, file: System.Memory]
   ,ReadMemoryA:function(Self, Memory$2, Offset, Size$7) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$2&&Offset>=0) {
         LTotal = Offset+Size$7;
         if (LTotal>Memory$2.length) {
            Size$7 = parseInt((Memory$2.length-LTotal),10);
         }
         if (Size$7>0) {
            Result = new Uint8Array(Memory$2.buffer.slice(Offset,Size$7));
         }
      }
      return Result
   }
   /// function TUnManaged.WriteMemoryA(const Memory: TMemoryHandle; const Offset: Integer; const Data: TMemoryHandle) : Integer
   ///  [line: 313, column: 27, file: System.Memory]
   ,WriteMemoryA:function(Self, Memory$3, Offset$1, Data$15) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$3) {
         if (Data$15) {
            mTotal = Offset$1+Data$15.length;
            if (mTotal>Memory$3.length) {
               Result = parseInt((Memory$3.length-mTotal),10);
            } else {
               Result = parseInt(Data$15.length,10);
            }
            if (Result>0) {
               if (Offset$1+Data$15.length<=Memory$3.length) {
                  Memory$3.set(Data$15,Offset$1);
               } else {
                  mChunk = Data$15.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$3.set(mTemp,Offset$1);
               }
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function TMemoryHandleHelper.Valid(const Self: TMemoryHandle) : Boolean
///  [line: 212, column: 30, file: System.Memory]
function TMemoryHandleHelper$Valid$3(Self$8) {
   var Result = false;
   Result = !( (Self$8 == undefined) || (Self$8 == null) );
   return Result
}
/// function TMemoryHandleHelper.Defined(const Self: TMemoryHandle) : Boolean
///  [line: 219, column: 30, file: System.Memory]
function TMemoryHandleHelper$Defined(Self$9) {
   var Result = false;
   Result = !(Self$9 == undefined);
   return Result
}
/// TMarshal = class (TObject)
///  [line: 133, column: 3, file: System.Memory]
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TMarshal.Move(const Source: TMemoryHandle; const SourceStart: Integer; const Target: TMemoryHandle; const TargetStart: Integer; const Size: Integer)
   ///  [line: 553, column: 26, file: System.Memory]
   ,Move$1:function(Self, Source, SourceStart, Target, TargetStart, Size$8) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined(Source)&&TMemoryHandleHelper$Valid$3(Source)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined(Target)&&TMemoryHandleHelper$Valid$3(Target)&&TargetStart>=0) {
            LRef = Source.subarray(SourceStart,SourceStart+Size$8);
            if (LRef.length>Size$8) {
               console.log("Move fucked up");
            }
            Target.set(LRef,TargetStart);
         }
      }
   }
   /// procedure TMarshal.Move(const Source: TAddress; const Target: TAddress; const Size: Integer)
   ///  [line: 577, column: 26, file: System.Memory]
   ,Move:function(Self, Source$1, Target$1, Size$9) {
      if (Source$1!==null) {
         if (Target$1!==null) {
            if (Size$9>0) {
               TMarshal.Move$1(Self,Source$1.FBuffer$2,Source$1.FOffset$1,Target$1.FBuffer$2,Target$1.FOffset$1,Size$9);
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TBufferHandleHelper.Valid(const Self: TBufferHandle) : Boolean
///  [line: 180, column: 30, file: System.Memory]
function TBufferHandleHelper$Valid$4(Self$10) {
   var Result = false;
   Result = !( (Self$10 == undefined) || (Self$10 == null) );
   return Result
}
/// TAddress = class (TObject)
///  [line: 71, column: 3, file: System.Streams]
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$2 = undefined;
      $.FOffset$1 = 0;
   }
   /// constructor TAddress.Create(const Memory: TBinaryData)
   ///  [line: 259, column: 22, file: System.Memory.Buffer]
   ,Create$35:function(Self, Memory$4) {
      if (Memory$4!==null) {
         if (TAllocation.GetSize$3(Memory$4)>0) {
            TAddress.Create$33(Self,TAllocation.GetHandle(Memory$4),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   /// constructor TAddress.Create(const Stream: TStream)
   ///  [line: 228, column: 22, file: System.Streams]
   ,Create$34:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle($As(Stream,TMemoryStream).FBuffer$1);
         if (LRef$1) {
            TAddress.Create$33(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   /// constructor TAddress.Create(const Segment: TBufferHandle; const Offset: Integer)
   ///  [line: 628, column: 22, file: System.Memory]
   ,Create$33:function(Self, Segment$1, Offset$2) {
      TObject.Create(Self);
      if (Segment$1&&TBufferHandleHelper$Valid$4(Segment$1)) {
         Self.FBuffer$2 = Segment$1;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$2>=0) {
         Self.FOffset$1 = Offset$2;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   /// destructor TAddress.Destroy()
   ///  [line: 642, column: 21, file: System.Memory]
   ,Destroy:function(Self) {
      Self.FBuffer$2 = null;
      Self.FOffset$1 = 0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EAddress = class (EW3Exception)
///  [line: 83, column: 3, file: System.Memory]
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONObject = class (TObject)
///  [line: 37, column: 3, file: System.json]
var TJSONObject = {
   $ClassName:"TJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance = undefined;
      $.FOptions$3 = [0];
   }
   /// function TJSONObject.AddOrSet(const PropertyName: String; const Data: Variant) : TJSONObject
   ///  [line: 415, column: 22, file: System.json]
   ,AddOrSet:function(Self, PropertyName, Data$16) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists(Self,PropertyName)) {
         if ($SetIn(Self.FOptions$3,3,0,4)) {
            Self.FInstance[PropertyName] = Data$16;
         } else {
            throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName]);
         }
      } else if ($SetIn(Self.FOptions$3,1,0,4)) {
         Self.FInstance[PropertyName] = Data$16;
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to add value [%s], instance does not allow new properties",[PropertyName]);
      }
      return Result
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions; Clone: Boolean)
   ///  [line: 185, column: 25, file: System.json]
   ,Create$46:function(Self, Instance$2, Options$2, Clone$1) {
      TObject.Create(Self);
      Self.FOptions$3 = Options$2.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$2)) {
         if (TW3VariantHelper$IsObject(Instance$2)) {
            if (Clone$1) {
               Self.FInstance = TVariant.CreateObject();
               TVariant.ForEachProperty(Instance$2,function (Name$5, Data$17) {
                  var Result = 1;
                  TJSONObject.AddOrSet(Self,Name$5,Data$17);
                  Result = 1;
                  return Result
               });
            } else {
               Self.FInstance = Instance$2;
            }
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to clone instance, reference is not an object");
         }
      } else {
         if ($SetIn(Self.FOptions$3,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions)
   ///  [line: 161, column: 25, file: System.json]
   ,Create$45:function(Self, Instance$3, Options$3) {
      TObject.Create(Self);
      Self.FOptions$3 = Options$3.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$3)&&TW3VariantHelper$IsObject(Instance$3)) {
         Self.FInstance = Instance$3;
      } else {
         if ($SetIn(Self.FOptions$3,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance)
   ///  [line: 142, column: 25, file: System.json]
   ,Create$44:function(Self, Instance$4) {
      TObject.Create(Self);
      Self.FOptions$3 = [15];
      if (Instance$4) {
         if (TW3VariantHelper$IsObject(Instance$4)) {
            Self.FInstance = Instance$4;
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         Self.FInstance = TVariant.CreateObject();
      }
      return Self
   }
   /// constructor TJSONObject.Create()
   ///  [line: 135, column: 25, file: System.json]
   ,Create$43:function(Self) {
      TObject.Create(Self);
      Self.FOptions$3 = [15];
      Self.FInstance = TVariant.CreateObject();
      return Self
   }
   /// destructor TJSONObject.Destroy()
   ,Destroy$11:function(Self) {
      Self.FInstance = null;
      TObject.Destroy(Self);
   }
   /// function TJSONObject.Exists(const PropertyName: String) : Boolean
   ///  [line: 434, column: 22, file: System.json]
   ,Exists:function(Self, PropertyName$1) {
      return (Object.hasOwnProperty.call(Self.FInstance,PropertyName$1)?true:false);
   }
   ,Destroy:TObject.Destroy
};
/// EJSONObject = class (EW3Exception)
///  [line: 35, column: 3, file: System.json]
var EJSONObject = {
   $ClassName:"EJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TAllocation = class (TDataTypeConverter)
///  [line: 51, column: 3, file: System.Memory.Allocation]
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle = undefined;
      $.FOptions$1 = null;
      $.FSize = 0;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 73, column: 37, file: System.Memory.Allocation]
   ,a$28:function(Self) {
      return ((!Self.FHandle)?true:false);
   }
   /// procedure TAllocation.Allocate(const NumberOfBytes: Integer)
   ///  [line: 258, column: 23, file: System.Memory.Allocation]
   ,Allocate:function(Self, NumberOfBytes) {
      var NewSize = 0;
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes>0) {
         NewSize = 0;
         if (Self.FOptions$1.FUseCache) {
            NewSize = TInteger.ToNearest(NumberOfBytes,Self.FOptions$1.FCacheSize);
         } else {
            NewSize = NumberOfBytes;
         }
         Self.FHandle = TUnManaged.AllocMemA(TUnManaged,NewSize);
         Self.FSize = NumberOfBytes;
         TAllocation.HandleAllocated$(Self);
      }
   }
   /// constructor TAllocation.Create(const ByteSize: Integer)
   ///  [line: 179, column: 25, file: System.Memory.Allocation]
   ,Create$38:function(Self, ByteSize) {
      TDataTypeConverter.Create$15$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   /// constructor TAllocation.Create()
   ///  [line: 173, column: 25, file: System.Memory.Allocation]
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$1 = TAllocationOptions.Create$36($New(TAllocationOptions),Self);
      return Self
   }
   /// function TAllocation.DataGetSize() : Integer
   ///  [line: 230, column: 22, file: System.Memory.Allocation]
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self);
   }
   /// function TAllocation.DataOffset() : Integer
   ///  [line: 224, column: 22, file: System.Memory.Allocation]
   ,DataOffset$1:function(Self) {
      return 0;
   }
   /// function TAllocation.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 236, column: 22, file: System.Memory.Allocation]
   ,DataRead$1:function(Self, Offset$3, ByteCount) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$3,ByteCount));
   }
   /// procedure TAllocation.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 243, column: 23, file: System.Memory.Allocation]
   ,DataWrite$1:function(Self, Offset$4, Bytes$2) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$4,TDataTypeConverter.BytesToTypedArray(Self,Bytes$2));
   }
   /// destructor TAllocation.Destroy()
   ///  [line: 186, column: 24, file: System.Memory.Allocation]
   ,Destroy:function(Self) {
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TAllocation.GetBufferHandle() : TBufferHandle
   ///  [line: 430, column: 22, file: System.Memory.Allocation]
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle) {
         Result = Self.FHandle.buffer;
      }
      return Result
   }
   /// function TAllocation.GetHandle() : TMemoryHandle
   ///  [line: 425, column: 22, file: System.Memory.Allocation]
   ,GetHandle:function(Self) {
      return Self.FHandle;
   }
   /// function TAllocation.GetSize() : Integer
   ///  [line: 420, column: 22, file: System.Memory.Allocation]
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   /// function TAllocation.GetTotalSize() : Integer
   ///  [line: 414, column: 22, file: System.Memory.Allocation]
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if (Self.FHandle) {
         Result = parseInt(Self.FHandle.length,10);
      }
      return Result
   }
   /// function TAllocation.GetTransport() : IBinaryTransport
   ///  [line: 195, column: 22, file: System.Memory.Allocation]
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   /// procedure TAllocation.Grow(const NumberOfBytes: Integer)
   ///  [line: 300, column: 23, file: System.Memory.Allocation]
   ,Grow:function(Self, NumberOfBytes$1) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle) {
         ExactNewSize = Self.FSize+NumberOfBytes$1;
         if (Self.FOptions$1.FUseCache) {
            if (NumberOfBytes$1<TAllocationOptions.GetCacheFree(Self.FOptions$1)) {
               (Self.FSize+= NumberOfBytes$1);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$1.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$1);
      }
   }
   /// procedure TAllocation.HandleAllocated()
   ///  [line: 248, column: 23, file: System.Memory.Allocation]
   ,HandleAllocated:function(Self) {
      /* null */
   }
   /// procedure TAllocation.HandleReleased()
   ///  [line: 253, column: 23, file: System.Memory.Allocation]
   ,HandleReleased:function(Self) {
      /* null */
   }
   /// procedure TAllocation.ReAllocate(const NewSize: Integer)
   ///  [line: 379, column: 23, file: System.Memory.Allocation]
   ,ReAllocate:function(Self, NewSize$1) {
      var LSizeToSet = 0;
      if (NewSize$1>0) {
         if (Self.FHandle) {
            if (NewSize$1!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$1.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,Self.FOptions$1.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,16);
               }
               Self.FHandle = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle,LSizeToSet);
               Self.FSize = NewSize$1;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$1);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   /// procedure TAllocation.Release()
   ///  [line: 287, column: 23, file: System.Memory.Allocation]
   ,Release$1:function(Self) {
      if (Self.FHandle) {
         try {
            Self.FHandle = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   /// procedure TAllocation.Shrink(const NumberOfBytes: Integer)
   ///  [line: 332, column: 23, file: System.Memory.Allocation]
   ,Shrink:function(Self, NumberOfBytes$2) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$2),0,2147483647);
         if (Self.FOptions$1.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$1.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$1.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   /// procedure TAllocation.Transport(const Target: IBinaryTransport)
   ///  [line: 200, column: 23, file: System.Memory.Allocation]
   ,Transport:function(Self, Target$2) {
      var Data$18 = [];
      if (Target$2===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$28(Self)) {
            try {
               Data$18 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle(Self));
               Target$2[3](Target$2[0](),Data$18);
            } catch ($e) {
               var e$1 = $W($e);
               throw EW3Exception.CreateFmt($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$1.ClassType), e$1.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
}
/// TBinaryData = class (TAllocation)
///  [line: 158, column: 3, file: System.Memory.Buffer]
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   /// procedure TBinaryData.AppendBuffer(const Raw: TMemoryHandle)
   ///  [line: 1201, column: 23, file: System.Memory.Buffer]
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   /// procedure TBinaryData.AppendBytes(const Bytes: TByteArray)
   ///  [line: 1257, column: 23, file: System.Memory.Buffer]
   ,AppendBytes:function(Self, Bytes$3) {
      var LLen$6 = 0,
         LOffset$1 = 0;
      LLen$6 = Bytes$3.length;
      if (LLen$6>0) {
         LOffset$1 = TAllocation.GetSize$3(Self);
         TAllocation.Grow(Self,LLen$6);
         TAllocation.GetHandle(Self).set(Bytes$3,LOffset$1);
      }
   }
   /// procedure TBinaryData.AppendFloat32(const Value: float32)
   ///  [line: 1187, column: 23, file: System.Memory.Buffer]
   ,AppendFloat32:function(Self, Value$26) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$26);
   }
   /// procedure TBinaryData.AppendFloat64(const Value: float64)
   ///  [line: 1194, column: 23, file: System.Memory.Buffer]
   ,AppendFloat64:function(Self, Value$27) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$27);
   }
   /// procedure TBinaryData.AppendMemory(const Buffer: TBinaryData; const ReleaseBufferOnExit: Boolean)
   ///  [line: 1215, column: 23, file: System.Memory.Buffer]
   ,AppendMemory:function(Self, Buffer$1, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$1!==null) {
         try {
            if (TAllocation.GetSize$3(Buffer$1)>0) {
               LOffset$4 = TAllocation.GetSize$3(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3(Buffer$1));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$1);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$1);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   /// procedure TBinaryData.AppendStr(const Text: String)
   ///  [line: 1235, column: 23, file: System.Memory.Buffer]
   ,AppendStr:function(Self, Text$2) {
      var LLen$7 = 0,
         LOffset$5 = 0,
         LTemp$3 = [],
         x$7 = 0;
      LLen$7 = Text$2.length;
      if (LLen$7>0) {
         LOffset$5 = TAllocation.GetSize$3(Self);
         LTemp$3 = TString.EncodeUTF8(TString,Text$2);
         TAllocation.Grow(Self,LTemp$3.length);
         var $temp11;
         for(x$7=0,$temp11=LTemp$3.length;x$7<$temp11;x$7++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$3[x$7]);
            ++LOffset$5;
         }
      }
   }
   /// function TBinaryData.Clone() : TBinaryData
   ///  [line: 1160, column: 22, file: System.Memory.Buffer]
   ,Clone:function(Self) {
      return TBinaryData.Create$42($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   /// function TBinaryData.Copy(const Offset: Integer; const ByteLen: Integer) : TByteArray
   ///  [line: 480, column: 22, file: System.Memory.Buffer]
   ,Copy$1:function(Self, Offset$5, ByteLen) {
      var Result = [];
      var LSize = 0,
         LTemp$4 = null;
      LSize = TAllocation.GetSize$3(Self);
      if (LSize>0) {
         if (Offset$5>=0&&Offset$5<LSize) {
            if (Offset$5+ByteLen<=LSize) {
               LTemp$4 = new Uint8Array(Self.FDataView.buffer,Offset$5,ByteLen);
               Result = Array.prototype.slice.call(LTemp$4);
               LTemp$4.buffer = null;
               LTemp$4 = null;
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.CopyFrom(const Buffer: TBinaryData; const Offset: Integer; const ByteLen: Integer)
   ///  [line: 1165, column: 23, file: System.Memory.Buffer]
   ,CopyFrom$2:function(Self, Buffer$2, Offset$6, ByteLen$1) {
      if (Buffer$2!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle(Buffer$2),Offset$6,ByteLen$1);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   /// procedure TBinaryData.CopyFromMemory(const Raw: TMemoryHandle; Offset: Integer; ByteLen: Integer)
   ///  [line: 1173, column: 23, file: System.Memory.Buffer]
   ,CopyFromMemory:function(Self, Raw$1, Offset$7, ByteLen$2) {
      if (TMemoryHandleHelper$Valid$3(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$7)) {
            if (ByteLen$2>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle(Self),Offset$7,ByteLen$2);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$7]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   /// constructor TBinaryData.Create(aHandle: TMemoryHandle)
   ///  [line: 283, column: 25, file: System.Memory.Buffer]
   ,Create$42:function(Self, aHandle) {
      var LSignature;
      TDataTypeConverter.Create$15$(Self);
      if (TMemoryHandleHelper$Defined(aHandle)&&TMemoryHandleHelper$Valid$3(aHandle)) {
         if (aHandle.toString) {
            LSignature = aHandle.toString();
            if (SameText(String(LSignature),"[object Uint8Array]")||SameText(String(LSignature),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   /// function TBinaryData.CutBinaryData(const Offset: Integer; const ByteLen: Integer) : TBinaryData
   ///  [line: 1139, column: 22, file: System.Memory.Buffer]
   ,CutBinaryData:function(Self, Offset$8, ByteLen$3) {
      var Result = null;
      var LSize$1 = 0,
         LNewBuffer = null;
      if (ByteLen$3>0) {
         LSize$1 = TAllocation.GetSize$3(Self);
         if (LSize$1>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$8)) {
               LNewBuffer = TAllocation.GetHandle(Self).subarray(Offset$8,Offset$8+ByteLen$3-1);
               Result = TBinaryData.Create$42($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$8]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$42($New(TBinaryData),null);
      }
      return Result
   }
   /// function TBinaryData.CutStream(const Offset: Integer; const ByteLen: Integer) : TStream
   ///  [line: 1117, column: 22, file: System.Memory.Buffer]
   ,CutStream:function(Self, Offset$9, ByteLen$4) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$9,ByteLen$4));
   }
   /// function TBinaryData.CutTypedArray(Offset: Integer; ByteLen: Integer) : TMemoryHandle
   ///  [line: 1122, column: 22, file: System.Memory.Buffer]
   ,CutTypedArray:function(Self, Offset$10, ByteLen$5) {
      var Result = undefined;
      var LTemp$5 = null;
      if (ByteLen$5>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$10)) {
            if (TAllocation.GetSize$3(Self)-Offset$10>0) {
               LTemp$5 = Self.FDataView.buffer.slice(Offset$10,Offset$10+ByteLen$5);
               Result = new Uint8Array(LTemp$5);
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.FromBase64(FileData: String)
   ///  [line: 639, column: 23, file: System.Memory.Buffer]
   ,FromBase64:function(Self, FileData) {
      var LBytes$2 = [];
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LBytes$2 = TBase64EncDec.Base64ToBytes$2(TBase64EncDec,FileData);
         if (LBytes$2.length>0) {
            TBinaryData.AppendBytes(Self,LBytes$2);
         }
      }
   }
   /// procedure TBinaryData.FromNodeBuffer(const NodeBuffer: JNodeBuffer)
   ///  [line: 93, column: 23, file: SmartNJ.Streams]
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$28(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   /// function TBinaryData.GetBit(const BitIndex: Integer) : Boolean
   ///  [line: 357, column: 22, file: System.Memory.Buffer]
   ,GetBit$1:function(Self, BitIndex) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex]);
      }
      return Result
   }
   /// function TBinaryData.GetBitCount() : Integer
   ///  [line: 317, column: 22, file: System.Memory.Buffer]
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3(Self)<<3;
   }
   /// function TBinaryData.GetByte(const Index: Integer) : Byte
   ///  [line: 677, column: 22, file: System.Memory.Buffer]
   ,GetByte:function(Self, Index) {
      var Result = 0;
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index)) {
            Result = Self.FDataView.getUint8(Index);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index]);
         }
      }
      return Result
   }
   /// procedure TBinaryData.HandleAllocated()
   ///  [line: 309, column: 23, file: System.Memory.Buffer]
   ,HandleAllocated:function(Self) {
      var LRef$2 = undefined;
      LRef$2 = TAllocation.GetBufferHandle(Self);
      (Self.FDataView) = new DataView(LRef$2);
   }
   /// procedure TBinaryData.HandleReleased()
   ///  [line: 322, column: 23, file: System.Memory.Buffer]
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   /// function TBinaryData.OffsetInRange(const Offset: Integer) : Boolean
   ///  [line: 850, column: 22, file: System.Memory.Buffer]
   ,OffsetInRange:function(Self, Offset$11) {
      var Result = false;
      var LSize$2 = 0;
      LSize$2 = TAllocation.GetSize$3(Self);
      if (LSize$2>0) {
         Result = Offset$11>=0&&Offset$11<=LSize$2;
      } else {
         Result = (Offset$11==0);
      }
      return Result
   }
   /// function TBinaryData.ReadBool(Offset: Integer) : Boolean
   ///  [line: 842, column: 22, file: System.Memory.Buffer]
   ,ReadBool:function(Self, Offset$12) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$12)) {
         Result = Self.FDataView.getUint8(Offset$12)>0;
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$12, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadBytes(Offset: Integer; ByteLen: Integer) : TByteArray
   ///  [line: 825, column: 22, file: System.Memory.Buffer]
   ,ReadBytes:function(Self, Offset$13, ByteLen$6) {
      var Result = [];
      var LSize$3 = 0,
         x$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$13)) {
         LSize$3 = TAllocation.GetSize$3(Self);
         if (Offset$13+ByteLen$6<=LSize$3) {
            var $temp12;
            for(x$8=0,$temp12=ByteLen$6;x$8<$temp12;x$8++) {
               Result.push(Self.FDataView.getUint8(Offset$13+x$8));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$13, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat32(Offset: Integer) : float32
   ///  [line: 719, column: 22, file: System.Memory.Buffer]
   ,ReadFloat32:function(Self, Offset$14) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$14)) {
         if (Offset$14+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$14);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$14,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$14,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$14, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat64(Offset: Integer) : float64
   ///  [line: 701, column: 22, file: System.Memory.Buffer]
   ,ReadFloat64:function(Self, Offset$15) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$15)) {
         if (Offset$15+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$15);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$15,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$15,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$15, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt(Offset: Integer) : Integer
   ///  [line: 787, column: 22, file: System.Memory.Buffer]
   ,ReadInt:function(Self, Offset$16) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$16)) {
         if (Offset$16+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$16);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$16,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$16,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$16, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt16(Offset: Integer) : SmallInt
   ///  [line: 736, column: 22, file: System.Memory.Buffer]
   ,ReadInt16:function(Self, Offset$17) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$17)) {
         if (Offset$17+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$17);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$17,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$17,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$17, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadLong(Offset: Integer) : Longword
   ///  [line: 770, column: 22, file: System.Memory.Buffer]
   ,ReadLong$1:function(Self, Offset$18) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$18)) {
         if (Offset$18+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$18);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$18,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$18,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$18, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadStr(Offset: Integer; ByteLen: Integer) : String
   ///  [line: 804, column: 22, file: System.Memory.Buffer]
   ,ReadStr$1:function(Self, Offset$19, ByteLen$7) {
      var Result = "";
      var LSize$4 = 0,
         LFetch = [],
         x$9 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$19)) {
         LSize$4 = TAllocation.GetSize$3(Self);
         if (Offset$19+ByteLen$7<=LSize$4) {
            var $temp13;
            for(x$9=0,$temp13=ByteLen$7;x$9<$temp13;x$9++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$19+x$9)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$19, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadWord(Offset: Integer) : Word
   ///  [line: 753, column: 22, file: System.Memory.Buffer]
   ,ReadWord$1:function(Self, Offset$20) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         if (Offset$20+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$20);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$20,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$20,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[17],[Offset$20, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// procedure TBinaryData.SetBit(const BitIndex: Integer; const value: Boolean)
   ///  [line: 352, column: 23, file: System.Memory.Buffer]
   ,SetBit$1:function(Self, BitIndex$1, value) {
      TBinaryData.SetByte(Self,(BitIndex$1>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$1%8),TBinaryData.GetByte(Self,(BitIndex$1>>>3)),value));
   }
   /// procedure TBinaryData.SetByte(const Index: Integer; const Value: Byte)
   ///  [line: 689, column: 23, file: System.Memory.Buffer]
   ,SetByte:function(Self, Index$1, Value$28) {
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$1)) {
            Self.FDataView.setUint8(Index$1,Value$28);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$1]);
         }
      }
   }
   /// function TBinaryData.ToBase64() : String
   ///  [line: 655, column: 22, file: System.Memory.Buffer]
   ,ToBase64:function(Self) {
      return TDataTypeConverter.BytesToBase64(Self.ClassType,TBinaryData.ToBytes(Self));
   }
   /// function TBinaryData.ToBytes() : TByteArray
   ///  [line: 510, column: 22, file: System.Memory.Buffer]
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$5 = 0,
         LTemp$6 = null;
      LSize$5 = TAllocation.GetSize$3(Self);
      if (LSize$5>0) {
         LTemp$6 = new Uint8Array(Self.FDataView.buffer,0,LSize$5);
         Result = Array.prototype.slice.call(LTemp$6);
         LTemp$6.buffer = null;
         LTemp$6 = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TBinaryData.ToHexDump(BytesPerRow: Integer; Options: TBufferHexDumpOptions) : String
   ///  [line: 367, column: 22, file: System.Memory.Buffer]
   ,ToHexDump:function(Self, BytesPerRow, Options$4) {
      var Result = "";
      var mDump = [],
         mCount = 0,
         x$10 = 0;
      var LByte = 0,
         mPad = 0,
         z = 0;
      var mPad$1 = 0,
         z$1 = 0;
      function SliceToText(DataSlice) {
         var Result = "";
         var y = 0;
         var LChar = "",
            LOff = 0;
         if (DataSlice.length>0) {
            var $temp14;
            for(y=0,$temp14=DataSlice.length;y<$temp14;y++) {
               LChar = TDataTypeConverter.ByteToChar(TDataTypeConverter,DataSlice[y]);
               if ((((LChar>="A")&&(LChar<="Z"))||((LChar>="a")&&(LChar<="z"))||((LChar>="0")&&(LChar<="9"))||(LChar==",")||(LChar==";")||(LChar=="<")||(LChar==">")||(LChar=="{")||(LChar=="}")||(LChar=="[")||(LChar=="]")||(LChar=="-")||(LChar=="_")||(LChar=="#")||(LChar=="$")||(LChar=="%")||(LChar=="&")||(LChar=="\/")||(LChar=="(")||(LChar==")")||(LChar=="!")||(LChar=="§")||(LChar=="^")||(LChar==":")||(LChar==",")||(LChar=="?"))) {
                  Result+=LChar;
               } else {
                  Result+="_";
               }
            }
         }
         LOff = BytesPerRow-DataSlice.length;
         while (LOff>0) {
            Result+="_";
            --LOff;
         }
         Result+="\r\n";
         return Result
      };
      if (TAllocation.GetHandle(Self)) {
         mCount = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp15;
         for(x$10=0,$temp15=TAllocation.GetSize$3(Self);x$10<$temp15;x$10++) {
            LByte = TBinaryData.GetByte(Self,x$10);
            mDump.push(LByte);
            if ($SetIn(Options$4,0,0,2)) {
               Result+="$"+(IntToHex2(LByte)).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(LByte)).toLocaleUpperCase();
            }
            ++mCount;
            if (mCount>=BytesPerRow) {
               if ($SetIn(Options$4,1,0,2)&&mCount>0) {
                  Result+=" ";
                  mPad = BytesPerRow-mCount;
                  var $temp16;
                  for(z=1,$temp16=mPad;z<=$temp16;z++) {
                     if ($SetIn(Options$4,0,0,2)) {
                        Result+="$";
                     }
                     Result+="00";
                     ++mCount;
                     if (mCount>=BytesPerRow) {
                        mCount = 0;
                        break;
                     } else {
                        Result+=" ";
                     }
                  }
               }
               if (mDump.length>0) {
                  Result+=SliceToText(mDump);
                  mDump.length=0;
                  mCount = 0;
               }
            } else {
               Result+=" ";
            }
         }
         if (mDump.length>0) {
            if ($SetIn(Options$4,1,0,2)&&mCount>0) {
               mPad$1 = BytesPerRow-mCount;
               var $temp17;
               for(z$1=1,$temp17=mPad$1;z$1<=$temp17;z$1++) {
                  if ($SetIn(Options$4,0,0,2)) {
                     Result+="$";
                  }
                  Result+="00";
                  ++mCount;
                  if (mCount>=BytesPerRow) {
                     break;
                  } else {
                     Result+=" ";
                  }
               }
            }
            Result+=" "+SliceToText(mDump);
            mCount = 0;
            mDump.length=0;
         }
      }
      return Result
   }
   /// function TBinaryData.ToStream() : TStream
   ///  [line: 583, column: 22, file: System.Memory.Buffer]
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$6 = 0,
         LCache = [],
         LTemp$7 = null;
      Result = TDataTypeConverter.Create$15$($New(TMemoryStream));
      LSize$6 = TAllocation.GetSize$3(Self);
      if (LSize$6>0) {
         LTemp$7 = new Uint8Array(Self.FDataView.buffer,0,LSize$6);
         LCache = Array.prototype.slice.call(LTemp$7);
         LTemp$7 = null;
         if (LCache.length>0) {
            try {
               TStream.Write$1(Result,LCache);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$2 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToString() : String
   ///  [line: 660, column: 22, file: System.Memory.Buffer]
   ,ToString$2:function(Self) {
      var Result = "";
      var CHUNK_SIZE = 32768;
      var LRef$3 = undefined;
      if (TAllocation.GetHandle(Self)) {
         LRef$3 = TAllocation.GetHandle(Self);
         var c = [];
    for (var i=0; i < (LRef$3).length; i += CHUNK_SIZE) {
      c.push(String.fromCharCode.apply(null, (LRef$3).subarray(i, i + CHUNK_SIZE)));
    }
    Result = c.join("");
      }
      return Result
   }
   /// function TBinaryData.ToTypedArray() : TMemoryHandle
   ///  [line: 619, column: 22, file: System.Memory.Buffer]
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$8 = 0,
         LTemp$8 = null;
      LLen$8 = TAllocation.GetSize$3(Self);
      if (LLen$8>0) {
         LTemp$8 = Self.FDataView.buffer.slice(0,LLen$8);
         Result = new Uint8Array(LTemp$8);
      }
      return Result
   }
   /// procedure TBinaryData.WriteBinaryData(const Offset: Integer; const Data: TBinaryData)
   ///  [line: 882, column: 23, file: System.Memory.Buffer]
   ,WriteBinaryData:function(Self, Offset$21, Data$19) {
      var LGrowth = 0;
      if (Data$19!==null) {
         if (TAllocation.GetSize$3(Data$19)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$21)) {
               LGrowth = 0;
               if (Offset$21+TAllocation.GetSize$3(Data$19)>TAllocation.GetSize$3(Self)-1) {
                  LGrowth = Offset$21+TAllocation.GetSize$3(Data$19)-TAllocation.GetSize$3(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Data$19),0,TAllocation.GetHandle(Self),0,TAllocation.GetSize$3(Data$19));
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$21]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   /// procedure TBinaryData.WriteBool(const Offset: Integer; const Data: Boolean)
   ///  [line: 969, column: 23, file: System.Memory.Buffer]
   ,WriteBool:function(Self, Offset$22, Data$20) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$22)) {
         LGrowth$1 = 0;
         if (Offset$22+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$1 = Offset$22+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$20) {
            Self.FDataView.setUint8(Offset$22,1);
         } else {
            Self.FDataView.setUint8(Offset$22,0);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$22]);
      }
   }
   /// procedure TBinaryData.WriteBuffer(const Offset: Integer; const Data: TMemoryHandle)
   ///  [line: 906, column: 23, file: System.Memory.Buffer]
   ,WriteBuffer$2:function(Self, Offset$23, Data$21) {
      var LBuffer$1 = null,
         LGrowth$2 = 0;
      if (Data$21) {
         if (Data$21.buffer) {
            LBuffer$1 = Data$21.buffer;
            if (LBuffer$1.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$23)) {
                  LGrowth$2 = 0;
                  if (Offset$23+Data$21.length>TAllocation.GetSize$3(Self)-1) {
                     LGrowth$2 = Offset$23+Data$21.length-TAllocation.GetSize$3(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$21,0,TAllocation.GetHandle(Self),Offset$23,parseInt(TAllocation.GetHandle(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$23]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   /// procedure TBinaryData.WriteBytes(const Offset: Integer; const Data: TByteArray)
   ///  [line: 863, column: 23, file: System.Memory.Buffer]
   ,WriteBytes:function(Self, Offset$24, Data$22) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$24)) {
         if (Data$22.length>0) {
            LGrowth$3 = 0;
            if (Offset$24+Data$22.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$3 = Offset$24+Data$22.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle(Self).set(Data$22,Offset$24);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$24]);
      }
   }
   /// procedure TBinaryData.WriteFloat32(const Offset: Integer; const Data: float32)
   ///  [line: 989, column: 23, file: System.Memory.Buffer]
   ,WriteFloat32:function(Self, Offset$25, Data$23) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         LGrowth$4 = 0;
         if (Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$4 = Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$25,Data$23);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$25,Data$23,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$25,Data$23,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$25]);
      }
   }
   /// procedure TBinaryData.WriteFloat64(const Offset: Integer; const Data: float64)
   ///  [line: 1012, column: 23, file: System.Memory.Buffer]
   ,WriteFloat64:function(Self, Offset$26, Data$24) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$26)) {
         LGrowth$5 = 0;
         if (Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$5 = Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$26,Number(Data$24));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$26,Number(Data$24),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$26,Number(Data$24),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$26]);
      }
   }
   /// procedure TBinaryData.WriteInt16(const Offset: Integer; const Data: SmallInt)
   ///  [line: 1033, column: 23, file: System.Memory.Buffer]
   ,WriteInt16:function(Self, Offset$27, Data$25) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         LGrowth$6 = 0;
         if (Offset$27+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$6 = Offset$27+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$27,Data$25);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$27,Data$25,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$27,Data$25,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$27]);
      }
   }
   /// procedure TBinaryData.WriteInt32(const Offset: Integer; const Data: Integer)
   ///  [line: 1096, column: 23, file: System.Memory.Buffer]
   ,WriteInt32:function(Self, Offset$28, Data$26) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         LGrowth$7 = 0;
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$7 = Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$28,Data$26);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$28,Data$26,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$28,Data$26,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$28]);
      }
   }
   /// procedure TBinaryData.WriteLong(const Offset: Integer; const Data: Longword)
   ///  [line: 1075, column: 23, file: System.Memory.Buffer]
   ,WriteLong:function(Self, Offset$29, Data$27) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$29)) {
         LGrowth$8 = 0;
         if (Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$8 = Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$29,Data$27);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$29,Data$27,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$29,Data$27,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$29]);
      }
   }
   /// procedure TBinaryData.WriteStr(const Offset: Integer; const Data: String)
   ///  [line: 943, column: 23, file: System.Memory.Buffer]
   ,WriteStr$1:function(Self, Offset$30, Data$28) {
      var LTemp$9 = [],
         LGrowth$9 = 0,
         x$11 = 0;
      if (Data$28.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$30)) {
            LTemp$9 = TString.EncodeUTF8(TString,Data$28);
            LGrowth$9 = 0;
            if (Offset$30+LTemp$9.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$9 = Offset$30+LTemp$9.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp18;
            for(x$11=0,$temp18=LTemp$9.length;x$11<$temp18;x$11++) {
               Self.FDataView.setUint8(Offset$30+x$11,LTemp$9[x$11]);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$30]);
         }
      }
   }
   /// procedure TBinaryData.WriteWord(const Offset: Integer; const Data: Word)
   ///  [line: 1054, column: 23, file: System.Memory.Buffer]
   ,WriteWord$1:function(Self, Offset$31, Data$29) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$31)) {
         LGrowth$10 = 0;
         if (Offset$31+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$10 = Offset$31+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$31,Data$29);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$31,Data$29,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$31,Data$29,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$31]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$15:TAllocation.Create$15
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TBinaryData.$Intf={
   IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$2,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
}
/// EBinaryData = class (EW3Exception)
///  [line: 157, column: 3, file: System.Memory.Buffer]
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TBitAccess = class (TObject)
///  [line: 20, column: 3, file: System.Types.Bits]
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBitAccess.Get(const index: Integer; const Value: Byte) : Boolean
   ///  [line: 114, column: 27, file: System.Types.Bits]
   ,Get:function(Self, index$1, Value$29) {
      var Result = false;
      var mMask = 0;
      if (index$1>=0&&index$1<8) {
         mMask = 1<<index$1;
         Result = ((Value$29&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$1]);
      }
      return Result
   }
   /// function TBitAccess.Set(const Index: Integer; const Value: Byte; const Data: Boolean) : Byte
   ///  [line: 127, column: 27, file: System.Types.Bits]
   ,Set$4:function(Self, Index$2, Value$30, Data$30) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$30;
      if (Index$2>=0&&Index$2<8) {
         mMask$1 = 1<<Index$2;
         mSet = ((Value$30&mMask$1)!=0);
         if (mSet!=Data$30) {
            if (Data$30) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
/// TStream = class (TDataTypeConverter)
///  [line: 80, column: 3, file: System.Streams]
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   /// function TStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 473, column: 18, file: System.Streams]
   ,CopyFrom:function(Self, Source$2, Count$2) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.DataGetSize() : Integer
   ///  [line: 439, column: 18, file: System.Streams]
   ,DataGetSize:function(Self) {
      return TStream.GetSize$(Self);
   }
   /// function TStream.DataOffset() : Integer
   ///  [line: 433, column: 18, file: System.Streams]
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   /// function TStream.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 445, column: 19, file: System.Streams]
   ,DataRead:function(Self, Offset$32, ByteCount$1) {
      return TStream.ReadBuffer$(Self,Offset$32,ByteCount$1);
   }
   /// procedure TStream.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 451, column: 19, file: System.Streams]
   ,DataWrite:function(Self, Offset$33, Bytes$4) {
      TStream.WriteBuffer$(Self,Bytes$4,Offset$33);
   }
   /// function TStream.GetPosition() : Integer
   ///  [line: 478, column: 18, file: System.Streams]
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.GetSize() : Integer
   ///  [line: 498, column: 18, file: System.Streams]
   ,GetSize:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Read(const Count: Integer) : TByteArray
   ///  [line: 456, column: 18, file: System.Streams]
   ,Read:function(Self, Count$3) {
      return TStream.ReadBuffer$(Self,TStream.GetPosition$(Self),Count$3);
   }
   /// function TStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 508, column: 18, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$34, Count$4) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 493, column: 18, file: System.Streams]
   ,Seek:function(Self, Offset$35, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// procedure TStream.SetPosition(NewPosition: Integer)
   ///  [line: 483, column: 19, file: System.Streams]
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// procedure TStream.SetSize(NewSize: Integer)
   ///  [line: 488, column: 19, file: System.Streams]
   ,SetSize:function(Self, NewSize$2) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// function TStream.Skip(Amount: Integer) : Integer
   ///  [line: 503, column: 18, file: System.Streams]
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Write(const Buffer: TByteArray) : Integer
   ///  [line: 461, column: 18, file: System.Streams]
   ,Write$1:function(Self, Buffer$3) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$3,TStream.GetPosition$(Self));
      Result = Buffer$3.length;
      return Result
   }
   /// procedure TStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 513, column: 19, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$4, Offset$36) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TMemoryStream = class (TStream)
///  [line: 122, column: 3, file: System.Streams]
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$1 = null;
      $.FPos = 0;
   }
   /// function TMemoryStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 258, column: 24, file: System.Streams]
   ,CopyFrom:function(Self, Source$3, Count$5) {
      var Result = 0;
      var LData = [];
      LData = TStream.ReadBuffer$(Source$3,TStream.GetPosition$(Source$3),Count$5);
      TStream.WriteBuffer$(Self,LData,TStream.GetPosition$(Self));
      Result = LData.length;
      return Result
   }
   /// constructor TMemoryStream.Create()
   ///  [line: 246, column: 27, file: System.Streams]
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FBuffer$1 = TDataTypeConverter.Create$15$($New(TAllocation));
      return Self
   }
   /// destructor TMemoryStream.Destroy()
   ///  [line: 252, column: 26, file: System.Streams]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TMemoryStream.GetPosition() : Integer
   ///  [line: 265, column: 24, file: System.Streams]
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   /// function TMemoryStream.GetSize() : Integer
   ///  [line: 340, column: 24, file: System.Streams]
   ,GetSize:function(Self) {
      return TAllocation.GetSize$3(Self.FBuffer$1);
   }
   /// function TMemoryStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 358, column: 24, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$37, Count$6) {
      var Result = [];
      var LLen$9 = 0,
         LBytesToMove = 0,
         LTemp$10 = null;
      LLen$9 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$(Self)) {
         LLen$9 = TStream.GetSize$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$9>0) {
         try {
            LBytesToMove = Count$6;
            if (LBytesToMove>LLen$9) {
               LBytesToMove = LLen$9;
            }
            LTemp$10 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Self.FBuffer$1),Offset$37,LTemp$10,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$10);
            TStream.SetPosition$(Self,Offset$37+Result.length);
         } catch ($e) {
            var e$3 = $W($e);
            throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$3.FMessage]);
         }
      }
      return Result
   }
   /// function TMemoryStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 312, column: 24, file: System.Streams]
   ,Seek:function(Self, Offset$38, Origin$1) {
      var Result = 0;
      var LSize$7 = 0;
      LSize$7 = TStream.GetSize$(Self);
      if (LSize$7>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$38>-1) {
                  TStream.SetPosition$(Self,Offset$38);
                  Result = Offset$38;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$38),0,LSize$7);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$38)),0,LSize$7);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   /// procedure TMemoryStream.SetPosition(NewPosition: Integer)
   ///  [line: 270, column: 25, file: System.Streams]
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$8 = 0;
      LSize$8 = TStream.GetSize$(Self);
      if (LSize$8>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$8);
      }
   }
   /// procedure TMemoryStream.SetSize(NewSize: Integer)
   ///  [line: 277, column: 25, file: System.Streams]
   ,SetSize:function(Self, NewSize$3) {
      var LSize$9 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$9 = TStream.GetSize$(Self);
      if (NewSize$3>0) {
         if (NewSize$3==LSize$9) {
            return;
         }
         if (NewSize$3>LSize$9) {
            LDiff = NewSize$3-LSize$9;
            if (TAllocation.GetSize$3(Self.FBuffer$1)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$1,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         } else {
            LDiff$1 = LSize$9-NewSize$3;
            if (TAllocation.GetSize$3(Self.FBuffer$1)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$1,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$1);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$(Self));
   }
   /// function TMemoryStream.Skip(Amount: Integer) : Integer
   ///  [line: 345, column: 24, file: System.Streams]
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$10 = 0,
         LTotal$1 = 0;
      LSize$10 = TStream.GetSize$(Self);
      if (LSize$10>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$10) {
            LTotal$1 = LSize$10-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   /// procedure TMemoryStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 386, column: 25, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$5, Offset$39) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$28(Self.FBuffer$1)&&Offset$39<1) {
            TAllocation.Allocate(Self.FBuffer$1,Buffer$5.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$5);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle(Self.FBuffer$1),0,Buffer$5.length);
            TStream.SetPosition$(Self,Buffer$5.length);
            return;
         }
         if (TStream.GetPosition$(Self)==TStream.GetSize$(Self)) {
            TAllocation.Grow(Self.FBuffer$1,Buffer$5.length);
            mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$5);
            TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle(Self.FBuffer$1),Offset$39,Buffer$5.length);
         } else {
            TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$5),0,TAllocation.GetHandle(Self.FBuffer$1),Offset$39,Buffer$5.length);
         }
         TStream.SetPosition$(Self,Offset$39+Buffer$5.length);
      } catch ($e) {
         var e$4 = $W($e);
         throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$4.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStreamSeekOrigin enumeration
///  [line: 48, column: 3, file: System.Streams]
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
/// TCustomFileStream = class (TStream)
///  [line: 151, column: 3, file: System.Streams]
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$1 = 0;
      $.FFilename = "";
   }
   /// procedure TCustomFileStream.SetAccessMode(const Value: TFileAccessMode)
   ///  [line: 214, column: 29, file: System.Streams]
   ,SetAccessMode:function(Self, Value$31) {
      Self.FAccess$1 = Value$31;
   }
   /// procedure TCustomFileStream.SetFilename(const Value: String)
   ///  [line: 219, column: 29, file: System.Streams]
   ,SetFilename:function(Self, Value$32) {
      Self.FFilename = Value$32;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom:TStream.CopyFrom
   ,GetPosition:TStream.GetPosition
   ,GetSize:TStream.GetSize
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// EStream = class (EW3Exception)
///  [line: 56, column: 3, file: System.Streams]
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamWriteError = class (EStream)
///  [line: 58, column: 3, file: System.Streams]
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamReadError = class (EStream)
///  [line: 57, column: 3, file: System.Streams]
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamNotImplemented = class (EStream)
///  [line: 59, column: 3, file: System.Streams]
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TAllocationOptions = class (TW3OwnedObject)
///  [line: 100, column: 3, file: System.Memory.Allocation]
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 113, column: 41, file: System.Memory.Allocation]
   ,a$27:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TAllocation);
   }
   /// function TAllocationOptions.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 134, column: 29, file: System.Memory.Allocation]
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   /// constructor TAllocationOptions.Create(const AOwner: TAllocation)
   ///  [line: 127, column: 32, file: System.Memory.Allocation]
   ,Create$36:function(Self, AOwner$1) {
      TW3OwnedObject.Create$16(Self,AOwner$1);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   /// function TAllocationOptions.GetCacheFree() : Integer
   ///  [line: 140, column: 29, file: System.Memory.Allocation]
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed(Self);
   }
   /// function TAllocationOptions.GetCacheUsed() : Integer
   ///  [line: 145, column: 29, file: System.Memory.Allocation]
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle(TAllocationOptions.a$27(Self)).length-TAllocation.GetSize$3(TAllocationOptions.a$27(Self)))),10);
      }
      return Result
   }
   /// procedure TAllocationOptions.SetCacheSize(const NewCacheSize: Integer)
   ///  [line: 156, column: 30, file: System.Memory.Allocation]
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   /// procedure TAllocationOptions.SetUseCache(const NewUseValue: Boolean)
   ///  [line: 151, column: 30, file: System.Memory.Allocation]
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
function a$91(Self) {
   return ((!Self[0]())?true:false);
}/// EAllocation = class (EW3Exception)
///  [line: 22, column: 3, file: System.Memory.Allocation]
var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomReader = class (TDataTypeConverter)
///  [line: 38, column: 3, file: System.Reader]
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$2 = null;
      $.FOffset$2 = $.FTotalSize$1 = 0;
      $.FOptions$2 = [0];
   }
   /// constructor TW3CustomReader.Create(const Access: IBinaryTransport)
   ///  [line: 113, column: 29, file: System.Reader]
   ,Create$39:function(Self, Access) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$2 = [1];
      Self.FAccess$2 = Access;
      Self.FOffset$2 = Self.FAccess$2[0]();
      Self.FTotalSize$1 = Self.FAccess$2[1]();
      return Self
   }
   /// function TW3CustomReader.GetReadOffset() : Integer
   ///  [line: 156, column: 26, file: System.Reader]
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FOffset$2;
      } else {
         Result = Self.FAccess$2[0]();
      }
      return Result
   }
   /// function TW3CustomReader.GetTotalSize() : Integer
   ///  [line: 148, column: 26, file: System.Reader]
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$2[1]();
      }
      return Result
   }
   /// function TW3CustomReader.Read(const BytesToRead: Integer) : TByteArray
   ///  [line: 169, column: 26, file: System.Reader]
   ,Read$1:function(Self, BytesToRead) {
      var Result = [];
      if (BytesToRead>0) {
         Result = Self.FAccess$2[2](TW3CustomReader.GetReadOffset(Self),BytesToRead);
         if ($SetIn(Self.FOptions$2,0,0,1)) {
            (Self.FOffset$2+= Result.length);
         }
      } else {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead.toString()+")"));
      }
      return Result
   }
   /// procedure TW3CustomReader.SetUpdateOption(const NewUpdateMode: TW3ReaderProviderOptions)
   ///  [line: 122, column: 27, file: System.Reader]
   ,SetUpdateOption:function(Self, NewUpdateMode) {
      Self.FOptions$2 = NewUpdateMode.slice(0);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TReader = class (TW3CustomReader)
///  [line: 82, column: 3, file: System.Reader]
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TStreamReader = class (TReader)
///  [line: 86, column: 3, file: System.Reader]
var TStreamReader = {
   $ClassName:"TStreamReader",$Parent:TReader
   ,$Init:function ($) {
      TReader.$Init($);
   }
   /// constructor TStreamReader.Create(const Stream: TStream)
   ///  [line: 99, column: 27, file: System.Reader]
   ,Create$40:function(Self, Stream$1) {
      TW3CustomReader.Create$39(Self,$AsIntf(Stream$1,"IBinaryTransport"));
      if (!$SetIn(Self.FOptions$2,0,0,1)) {
         TW3CustomReader.SetUpdateOption(Self,[1]);
      }
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// EW3ReadError = class (EW3Exception)
///  [line: 34, column: 3, file: System.Reader]
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomWriter = class (TDataTypeConverter)
///  [line: 39, column: 3, file: System.Writer]
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions = [0];
   }
   /// constructor TW3CustomWriter.Create(const Access: IBinaryTransport)
   ///  [line: 102, column: 29, file: System.Writer]
   ,Create$29:function(Self, Access$1) {
      TDataTypeConverter.Create$15(Self);
      Self.FAccess = Access$1;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions = [3];
      return Self
   }
   /// function TW3CustomWriter.GetOffset() : Integer
   ///  [line: 125, column: 26, file: System.Writer]
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   /// function TW3CustomWriter.GetTotalFree() : Integer
   ///  [line: 146, column: 26, file: System.Writer]
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   /// function TW3CustomWriter.GetTotalSize() : Integer
   ///  [line: 116, column: 26, file: System.Writer]
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   /// function TW3CustomWriter.QueryBreachOfBoundary(const BytesToFit: Integer) : Boolean
   ///  [line: 133, column: 26, file: System.Writer]
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   /// procedure TW3CustomWriter.SetAccessOptions(const NewOptions: TW3WriterOptions)
   ///  [line: 111, column: 27, file: System.Writer]
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions = NewOptions.slice(0);
   }
   /// function TW3CustomWriter.Write(const Data: TByteArray) : Integer
   ///  [line: 151, column: 26, file: System.Writer]
   ,Write:function(Self, Data$31) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$31.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$31);
            if ($SetIn(Self.FOptions,0,0,2)) {
               (Self.FOffset+= Data$31.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$31,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$31);
               if ($SetIn(Self.FOptions,0,0,2)) {
                  (Self.FOffset+= Data$31.length);
               }
            } else {
               throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[12],[Data$31.length]));
            }
         }
         Result = Data$31.length;
      } else {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[14],[LBytesToWrite]));
      }
      return Result
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TWriter = class (TW3CustomWriter)
///  [line: 76, column: 3, file: System.Writer]
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TStreamWriter = class (TW3CustomWriter)
///  [line: 79, column: 3, file: System.Writer]
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   /// constructor TStreamWriter.Create(const Stream: TStream)
   ///  [line: 92, column: 27, file: System.Writer]
   ,Create$30:function(Self, Stream$2) {
      TW3CustomWriter.Create$29(Self,$AsIntf(Stream$2,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// EW3WriteError = class (EW3Exception)
///  [line: 31, column: 3, file: System.Writer]
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TFileStream = class (TCustomFileStream)
///  [line: 29, column: 3, file: SmartNJ.Streams]
var TFileStream = {
   $ClassName:"TFileStream",$Parent:TCustomFileStream
   ,$Init:function ($) {
      TCustomFileStream.$Init($);
      $.FHandle$3 = undefined;
      $.FPosition = 0;
   }
   /// function TFileStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 348, column: 22, file: SmartNJ.Streams]
   ,CopyFrom:function(Self, Source$4, Count$7) {
      var Result = 0;
      var DataRead$2 = [];
      if (Self.FHandle$3) {
         if (Source$4!==null) {
            if (Count$7>0) {
               DataRead$2 = TStream.ReadBuffer$(Source$4,TStream.GetPosition$(Source$4),Count$7);
               TStream.WriteBuffer$(Self,DataRead$2,Self.FPosition);
               Result = DataRead$2.length;
            }
         } else {
            throw Exception.Create($New(EStream),"Invalid source stream error");
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// constructor TFileStream.Create(const FullFilePath: String; const AccessMode: TFileAccessMode)
   ///  [line: 119, column: 25, file: SmartNJ.Streams]
   ,Create$32:function(Self, FullFilePath, AccessMode$1) {
      var Filesystem,
         OpenFlags = "";
      switch (AccessMode$1) {
         case 0 :
            OpenFlags = "rs";
            break;
         case 1 :
            OpenFlags = "w";
            break;
         case 2 :
            OpenFlags = "w+";
            break;
      }
      Filesystem = NodeFsAPI();
      try {
         Self.FHandle$3 = Filesystem.openSync(FullFilePath,OpenFlags);
      } catch ($e) {
         var e$5 = $W($e);
         throw $e;
      }
      TCustomFileStream.SetAccessMode(Self,AccessMode$1);
      TCustomFileStream.SetFilename(Self,FullFilePath);
      return Self
   }
   /// destructor TFileStream.Destroy()
   ///  [line: 147, column: 24, file: SmartNJ.Streams]
   ,Destroy:function(Self) {
      var FileSystem = null;
      if (Self.FHandle$3) {
         FileSystem = NodeFsAPI();
         try {
            FileSystem.closeSync(Self.FHandle$3);
         } finally {
            Self.FHandle$3 = undefined;
         }
      }
      TDataTypeConverter.Destroy(Self);
   }
   /// function TFileStream.GetPosition() : Integer
   ///  [line: 161, column: 22, file: SmartNJ.Streams]
   ,GetPosition:function(Self) {
      var Result = 0;
      if (Self.FHandle$3) {
         Result = Self.FPosition;
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.GetSize() : Integer
   ///  [line: 198, column: 22, file: SmartNJ.Streams]
   ,GetSize:function(Self) {
      var Result = 0;
      if (Self.FHandle$3) {
         Result = NodeFsAPI().statSync(Self.FFilename).size;
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 272, column: 22, file: SmartNJ.Streams]
   ,ReadBuffer:function(Self, Offset$40, Count$8) {
      var Result = [];
      var LTypedChunk = null;
      var FileSystem$1,
         LCache$1,
         LChunk;
      if (Self.FHandle$3) {
         if (Count$8>0) {
            FileSystem$1 = NodeFsAPI();
            LCache$1 = new Buffer(Count$8);
            try {
               LChunk = FileSystem$1.readSync(Self.FHandle$3,LCache$1,0,Count$8,Self.FPosition);
            } catch ($e) {
               var e$6 = $W($e);
               throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$6.FMessage]);
            }
            LTypedChunk = new Uint8Array(LCache$1);
            Result = TDatatype.TypedArrayToBytes$1(TDatatype,LTypedChunk);
            TStream.SetPosition$(Self,Offset$40+Result.length);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 239, column: 22, file: SmartNJ.Streams]
   ,Seek:function(Self, Offset$41, Origin$2) {
      var Result = 0;
      var LSize$11 = 0;
      if (Self.FHandle$3) {
         LSize$11 = TStream.GetSize$(Self);
         if (LSize$11>0) {
            switch (Origin$2) {
               case 0 :
                  if (Offset$41>-1) {
                     TStream.SetPosition$(Self,Offset$41);
                     Result = Offset$41;
                  }
                  break;
               case 1 :
                  Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$41),0,LSize$11);
                  TStream.SetPosition$(Self,Result);
                  break;
               case 2 :
                  Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$41)),0,LSize$11);
                  TStream.SetPosition$(Self,Result);
                  break;
            }
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// procedure TFileStream.SetPosition(NewPosition: Integer)
   ///  [line: 169, column: 23, file: SmartNJ.Streams]
   ,SetPosition:function(Self, NewPosition$2) {
      var LSize$12 = 0;
      if (Self.FHandle$3) {
         LSize$12 = TStream.GetSize$(Self);
         if (LSize$12>0) {
            Self.FPosition = TInteger.EnsureRange(NewPosition$2,0,LSize$12);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   /// procedure TFileStream.SetSize(NewSize: Integer)
   ///  [line: 207, column: 23, file: SmartNJ.Streams]
   ,SetSize:function(Self, NewSize$4) {
      var LFileSystem,
         LData$1 = [],
         LSize$13 = 0,
         LDiff$2 = 0;
      if (Self.FHandle$3) {
         LSize$13 = TStream.GetSize$(Self);
         LFileSystem = NodeFsAPI();
         if (NewSize$4<0) {
            NewSize$4 = 0;
         }
         if (NewSize$4>LSize$13) {
            LDiff$2 = NewSize$4-LSize$13;
            $ArraySetLenC(LData$1,NewSize$4,function (){return 0});
            TStream.WriteBuffer$(Self,LData$1,LSize$13);
         } else {
            try {
               LFileSystem.ftruncateSync(Self.FHandle$3,NewSize$4);
            } catch ($e) {
               var e$7 = $W($e);
               throw EW3Exception.CreateFmt($New(EStreamReadError),$R[6],[e$7.FMessage]);
            }
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   /// function TFileStream.Skip(Amount: Integer) : Integer
   ///  [line: 180, column: 22, file: SmartNJ.Streams]
   ,Skip:function(Self, Amount$2) {
      var Result = 0;
      var LSize$14 = 0,
         LTotal$2 = 0;
      if (Self.FHandle$3) {
         LSize$14 = TStream.GetSize$(Self);
         if (LSize$14>0) {
            LTotal$2 = TStream.GetPosition$(Self)+Amount$2;
            if (LTotal$2>LSize$14) {
               LTotal$2 = LSize$14-LTotal$2;
            }
            (Self.FPosition+= LTotal$2);
            Result = LTotal$2;
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// procedure TFileStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 315, column: 23, file: SmartNJ.Streams]
   ,WriteBuffer:function(Self, Buffer$6, Offset$42) {
      var FileSystem$2,
         LChunk$1;
      if (Self.FHandle$3) {
         if (Buffer$6.length>0) {
            FileSystem$2 = NodeFsAPI();
            LChunk$1 = new Buffer( new Uint8Array(Buffer$6).buffer );
            try {
               FileSystem$2.writeSync(Self.FHandle$3,LChunk$1,0,Buffer$6.length,Self.FPosition,"binary");
            } catch ($e) {
               var e$8 = $W($e);
               throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$8.FMessage]);
            }
            TStream.SetPosition$(Self,Offset$42+Buffer$6.length);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
};
TFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TW3StorageObjectType enumeration
///  [line: 36, column: 3, file: System.Device.Storage]
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
/// TW3OwnedErrorObject = class (TW3OwnedObject)
///  [line: 78, column: 3, file: system.objects]
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$5 = null;
   }
   /// constructor TW3OwnedErrorObject.Create(const AOwner: TObject)
   ///  [line: 227, column: 33, file: system.objects]
   ,Create$16:function(Self, AOwner$2) {
      TW3OwnedObject.Create$16(Self,AOwner$2);
      Self.FOptions$5 = TW3ErrorObjectOptions.Create$53($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3OwnedErrorObject.Destroy()
   ///  [line: 233, column: 32, file: system.objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$5);
      TObject.Destroy(Self);
   }
   /// procedure TW3OwnedErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 273, column: 31, file: system.objects]
   ,SetLastErrorF$1:function(Self, ErrorText$1, Values$3) {
      Self.FLastError$1 = Format(ErrorText$1,Values$3.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedErrorObject = class (TW3OwnedErrorObject)
///  [line: 96, column: 3, file: system.objects]
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   /// procedure TW3OwnedLockedErrorObject.DisableAlteration()
   ///  [line: 189, column: 37, file: system.objects]
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.EnableAlteration()
   ///  [line: 196, column: 37, file: system.objects]
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2(Self);
         }
      }
   }
   /// function TW3OwnedLockedErrorObject.GetLockState() : Boolean
   ///  [line: 206, column: 36, file: system.objects]
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectLocked()
   ///  [line: 211, column: 37, file: system.objects]
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectUnLocked()
   ///  [line: 217, column: 37, file: system.objects]
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3Component = class (TW3OwnedLockedErrorObject)
///  [line: 19, column: 3, file: system.widget]
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.FInitialized = false;
   }
   /// constructor TW3Component.Create(AOwner: TW3Component)
   ///  [line: 73, column: 26, file: system.widget]
   ,Create$49:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$3);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   /// constructor TW3Component.CreateEx(AOwner: TObject)
   ///  [line: 88, column: 26, file: system.widget]
   ,CreateEx:function(Self, AOwner$4) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$4);
      Self.FInitialized = false;
      return Self
   }
   /// destructor TW3Component.Destroy()
   ///  [line: 99, column: 25, file: system.widget]
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TW3Component.FinalizeObject()
   ///  [line: 129, column: 24, file: system.widget]
   ,FinalizeObject:function(Self) {
      /* null */
   }
   /// procedure TW3Component.InitializeObject()
   ///  [line: 119, column: 24, file: system.widget]
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$49$:function($){return $.ClassType.Create$49.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3StorageDevice = class (TW3Component)
///  [line: 145, column: 3, file: System.Device.Storage]
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FActive$1 = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$6 = [0];
   }
   /// procedure TW3StorageDevice.FinalizeObject()
   ///  [line: 211, column: 28, file: System.Device.Storage]
   ,FinalizeObject:function(Self) {
      if (Self.FActive$1) {
         TW3StorageDevice.UnMount(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3StorageDevice.GetActive() : Boolean
   ///  [line: 307, column: 27, file: System.Device.Storage]
   ,GetActive:function(Self) {
      return Self.FActive$1;
   }
   /// procedure TW3StorageDevice.GetDeviceId(CB: TW3DeviceIdCallback)
   ///  [line: 261, column: 28, file: System.Device.Storage]
   ,GetDeviceId:function(Self, CB) {
      if (CB) {
         CB(Self.FIdentifier,true);
      }
   }
   /// procedure TW3StorageDevice.GetDeviceOptions(CB: TW3DeviceOptionsCallback)
   ///  [line: 218, column: 28, file: System.Device.Storage]
   ,GetDeviceOptions:function(Self, CB$1) {
      if (CB$1) {
         CB$1(Self.FOptions$6.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.GetName(CB: TW3DeviceNameCallback)
   ///  [line: 255, column: 28, file: System.Device.Storage]
   ,GetName:function(Self, CB$2) {
      if (CB$2) {
         CB$2(Self.FName,true);
      }
   }
   /// procedure TW3StorageDevice.InitializeObject()
   ///  [line: 203, column: 28, file: System.Device.Storage]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$6 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$49:TW3Component.Create$49
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath$1,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists$1,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine$1,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DeviceAuthenticationData = class (TObject)
///  [line: 67, column: 3, file: System.Device.Storage]
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJFileItemList = class (JObject)
///  [line: 62, column: 3, file: System.Device.Storage]
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

/// TNJFileItem = class (JObject)
///  [line: 52, column: 3, file: System.Device.Storage]
function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

/// TW3ErrorObjectOptions = class (TObject)
///  [line: 27, column: 3, file: system.objects]
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   /// constructor TW3ErrorObjectOptions.Create()
   ///  [line: 139, column: 35, file: system.objects]
   ,Create$53:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObject = class (TObject)
///  [line: 56, column: 3, file: system.objects]
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError = "";
      $.FOptions$4 = null;
   }
   /// procedure TW3ErrorObject.ClearLastError()
   ///  [line: 351, column: 26, file: system.objects]
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   /// constructor TW3ErrorObject.Create()
   ///  [line: 288, column: 28, file: system.objects]
   ,Create$47:function(Self) {
      TObject.Create(Self);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$53($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3ErrorObject.Destroy()
   ///  [line: 294, column: 27, file: system.objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   /// function TW3ErrorObject.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 356, column: 25, file: system.objects]
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   /// function TW3ErrorObject.GetFailed() : Boolean
   ///  [line: 305, column: 25, file: system.objects]
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   /// function TW3ErrorObject.GetLastError() : String
   ///  [line: 300, column: 25, file: system.objects]
   ,GetLastError:function(Self) {
      return Self.FLastError;
   }
   /// procedure TW3ErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 310, column: 26, file: system.objects]
   ,SetLastError:function(Self, ErrorText$2) {
      var ErrClass = null;
      Self.FLastError = Trim$_String_(ErrorText$2);
      if (Self.FLastError.length>0) {
         if (Self.FOptions$4.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
       }
         }
         if (Self.FOptions$4.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass,""),Self.FLastError);
         }
      }
   }
   /// procedure TW3ErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 345, column: 26, file: system.objects]
   ,SetLastErrorF:function(Self, ErrorText$3, Values$4) {
      TW3ErrorObject.SetLastError(Self,Format(ErrorText$3,Values$4.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
TW3ErrorObject.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// EW3ErrorObject = class (EW3Exception)
///  [line: 21, column: 3, file: system.objects]
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WriteLnF(Text$3, Data$32) {
   util().log(Format(String(Text$3),Data$32.slice(0)));
};
function WriteLn(Text$4) {
   util().log(String(Text$4));
};
/// TW3DirectoryParser = class (TW3ErrorObject)
///  [line: 49, column: 3, file: System.IOUtils]
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   /// function TW3DirectoryParser.GetErrorObject() : IW3ErrorAccess
   ///  [line: 176, column: 29, file: System.IOUtils]
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   /// function TW3DirectoryParser.IsPathRooted(FilePath: String) : Boolean
   ///  [line: 181, column: 29, file: System.IOUtils]
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   /// function TW3DirectoryParser.IsRelativePath(FilePath: String) : Boolean
   ///  [line: 194, column: 29, file: System.IOUtils]
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3UnixDirectoryParser = class (TW3DirectoryParser)
///  [line: 81, column: 3, file: System.IOUtils]
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3UnixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 670, column: 33, file: System.IOUtils]
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator = "",
         x$12 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Separator = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator)) {
         TW3ErrorObject.SetLastError(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$12=FilePath$2.length;x$12>=1;x$12--) {
         {var $temp19 = FilePath$2.charAt(x$12-1);
            if ($temp19==".") {
               Result = FilePath$2.substr(0,(x$12-1))+NewExt.v;
               break;
            }
             else if ($temp19==Separator) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 723, column: 33, file: System.IOUtils]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 739, column: 33, file: System.IOUtils]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 517, column: 33, file: System.IOUtils]
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 604, column: 33, file: System.IOUtils]
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$1 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$6.length>0) {
         Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$1)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$1);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$1)+Separator$1;
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 627, column: 33, file: System.IOUtils]
   ,GetExtension:function(Self, Filename$1) {
      var Result = "";
      var Separator$2 = "",
         x$13 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$1.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$1,Separator$2)) {
            TW3ErrorObject.SetLastError(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$13=Filename$1.length;x$13>=1;x$13--) {
               {var $temp20 = Filename$1.charAt(x$13-1);
                  if ($temp20==".") {
                     dx = Filename$1.length;
                     (dx-= x$13);
                     ++dx;
                     Result = RightStr(Filename$1,dx);
                     break;
                  }
                   else if ($temp20==Separator$2) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 585, column: 33, file: System.IOUtils]
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$3 = "",
         Parts = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$3)) {
            TW3ErrorObject.SetLastError(Self,"No filename part in path error");
         } else {
            Parts = (Temp$1).split(Separator$3);
            Result = Parts[(Parts.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 575, column: 33, file: System.IOUtils]
   ,GetFileNameWithoutExtension:function(Self, Filename$2) {
      var Result = "";
      var temp$11 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      temp$11 = TW3DirectoryParser.GetFileName$(Self,Filename$2);
      if (!TW3ErrorObject.GetFailed(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$11,"");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 532, column: 33, file: System.IOUtils]
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$1 = [],
         Separator$4 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$4)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$1 = (Temp$2).split(Separator$4);
            Result = Parts$1[(Parts$1.length-1)];
            return Result;
         }
         Parts$1 = (Temp$2).split(Separator$4);
         if (Parts$1.length>1) {
            Result = Parts$1[(Parts$1.length-1)-1];
         } else {
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 398, column: 33, file: System.IOUtils]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3UnixDirectoryParser.GetRootMoniker() : String
   ///  [line: 403, column: 33, file: System.IOUtils]
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   /// function TW3UnixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 408, column: 33, file: System.IOUtils]
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp21=0;$temp21<FileName.length;$temp21++) {
               el=$uniCharAt(FileName,$temp21);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 437, column: 33, file: System.IOUtils]
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp22=0;$temp22<FolderName.length;$temp22++) {
               el$1=$uniCharAt(FolderName,$temp22);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 714, column: 33, file: System.IOUtils]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$5 = "";
      Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$5)) {
         Result = FilePath$9;
      } else {
         Result = Separator$5+FilePath$9;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 731, column: 33, file: System.IOUtils]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 466, column: 33, file: System.IOUtils]
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$6 = "",
         PathParts = [],
         Index$3 = 0,
         a$92 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$6);
            Index$3 = 0;
            var $temp23;
            for(a$92=0,$temp23=PathParts.length;a$92<$temp23;a$92++) {
               part = PathParts[a$92];
               {var $temp24 = part;
                  if ($temp24=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp24=="~") {
                     if (Index$3>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index$3==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index$3+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3Win32DirectoryParser = class (TW3UnixDirectoryParser)
///  [line: 62, column: 3, file: SmartNJ.System]
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   /// function TW3Win32DirectoryParser.GetPathSeparator() : Char
   ///  [line: 609, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   /// function TW3Win32DirectoryParser.GetRootMoniker() : String
   ///  [line: 614, column: 34, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos$1 = 0;
         xpos$1 = (ThisPath.indexOf(":\\")+1);
         if (xpos$1>=2) {
            ++xpos$1;
            Result = ThisPath.substr(0,xpos$1);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3PosixDirectoryParser = class (TW3DirectoryParser)
///  [line: 68, column: 3, file: SmartNJ.System]
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3PosixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 479, column: 34, file: SmartNJ.System]
   ,ChangeFileExt:function(Self, FilePath$12, NewExt$1) {
      var Result = "";
      var LName$2 = "";
      var Separator$7 = "",
         x$14 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$12.length>0) {
         if ((FilePath$12.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$12]);
         } else {
            if (StrEndsWith(FilePath$12," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$12]);
            } else {
               Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$2,Separator$7)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[FilePath$12]);
               } else {
                  if ((FilePath$12.indexOf(Separator$7)+1)>0) {
                     LName$2 = TW3DirectoryParser.GetFileName$(Self,FilePath$12);
                     if (TW3ErrorObject.GetFailed(Self)) {
                        return Result;
                     }
                  } else {
                     LName$2 = FilePath$12;
                  }
                  if (LName$2.length>0) {
                     if ((LName$2.indexOf(".")+1)>0) {
                        for(x$14=LName$2.length;x$14>=1;x$14--) {
                           if (LName$2.charAt(x$14-1)==".") {
                              Result = LName$2.substr(0,(x$14-1))+NewExt$1;
                              break;
                           }
                        }
                     } else {
                        Result = LName$2+NewExt$1;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$12]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 596, column: 34, file: SmartNJ.System]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$13) {
      var Result = "";
      var Separator$8 = "";
      Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$13,Separator$8)) {
         Result = FilePath$13.substr((1+Separator$8.length)-1);
      } else {
         Result = FilePath$13;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 577, column: 34, file: SmartNJ.System]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$14) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$14,Separator$9)) {
         Result = FilePath$14.substr(0,(FilePath$14.length-Separator$9.length));
      } else {
         Result = FilePath$14;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 327, column: 34, file: SmartNJ.System]
   ,GetDevice:function(Self, FilePath$15) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$15.length>0) {
         if (StrBeginsWith(FilePath$15,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 534, column: 34, file: SmartNJ.System]
   ,GetDirectoryName:function(Self, FilePath$16) {
      var Result = "";
      var Separator$10 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$16.length>0) {
         if ((FilePath$16.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$16]);
         } else {
            if (StrEndsWith(FilePath$16," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$16]);
            } else {
               Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$16,Separator$10)) {
                  Result = FilePath$16;
                  return Result;
               }
               NameParts$1 = (FilePath$16).split(Separator$10);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$10)+Separator$10;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$16]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 419, column: 34, file: SmartNJ.System]
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var LName$3 = "";
      var Separator$11 = "",
         x$15 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$3.length>0) {
         if ((Filename$3.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$3]);
         } else {
            if (StrEndsWith(Filename$3," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$3]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$3,Separator$11)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[Filename$3]);
               } else {
                  if ((Filename$3.indexOf(Separator$11)+1)>0) {
                     LName$3 = TW3DirectoryParser.GetFileName$(Self,Filename$3);
                  } else {
                     LName$3 = Filename$3;
                  }
                  if (!TW3ErrorObject.GetFailed(Self)) {
                     for(x$15=Filename$3.length;x$15>=1;x$15--) {
                        {var $temp25 = Filename$3.charAt(x$15-1);
                           if ($temp25==".") {
                              dx$1 = Filename$3.length;
                              (dx$1-= x$15);
                              ++dx$1;
                              Result = RightStr(Filename$3,dx$1);
                              break;
                           }
                            else if ($temp25==Separator$11) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$3)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$3]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 342, column: 34, file: SmartNJ.System]
   ,GetFileName:function(Self, FilePath$17) {
      var Result = "";
      var Separator$12 = "",
         x$16 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$17.length>0) {
         if ((FilePath$17.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$17]);
         } else {
            if (StrEndsWith(FilePath$17," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$17]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$17,Separator$12)) {
                  for(x$16=FilePath$17.length;x$16>=1;x$16--) {
                     if (FilePath$17.charAt(x$16-1)!=Separator$12) {
                        Result = FilePath$17.charAt(x$16-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$17]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 372, column: 34, file: SmartNJ.System]
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var Separator$13 = "",
         LName$4 = "";
      var x$17 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$4.length>0) {
         if ((Filename$4.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$4]);
         } else {
            if (StrEndsWith(Filename$4," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$4]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$4.indexOf(Separator$13)+1)>0) {
                  LName$4 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
               } else {
                  LName$4 = Filename$4;
               }
               if (!TW3ErrorObject.GetFailed(Self)) {
                  if (LName$4.length>0) {
                     if ((LName$4.indexOf(".")+1)>0) {
                        for(x$17=LName$4.length;x$17>=1;x$17--) {
                           if (LName$4.charAt(x$17-1)==".") {
                              Result = LName$4.substr(0,(x$17-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$4;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$4]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 274, column: 34, file: SmartNJ.System]
   ,GetPathName:function(Self, FilePath$18) {
      var Result = "";
      var LParts = [],
         LTemp$11 = "";
      var Separator$14 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$18.length>0) {
         if ((FilePath$18.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$18]);
         } else {
            if (StrEndsWith(FilePath$18," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$18]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$18,Separator$14)) {
                  if (FilePath$18==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$11 = (FilePath$18).substr(0,(FilePath$18.length-Separator$14.length));
                  LParts = (LTemp$11).split(Separator$14);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$18).split(Separator$14);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$18]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 155, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.GetRootMoniker() : String
   ///  [line: 160, column: 35, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 165, column: 34, file: SmartNJ.System]
   ,HasValidFileNameChars:function(Self, FileName$1) {
      var Result = false;
      var el$2 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName$1.length>0) {
         if ((FileName$1.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$1]);
         } else {
            for (var $temp26=0;$temp26<FileName$1.length;$temp26++) {
               el$2=$uniCharAt(FileName$1,$temp26);
               if (!el$2) continue;
               Result = (((el$2>="A")&&(el$2<="Z"))||((el$2>="a")&&(el$2<="z"))||((el$2>="0")&&(el$2<="9"))||(el$2=="-")||(el$2=="_")||(el$2==".")||(el$2==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$2, FileName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 194, column: 34, file: SmartNJ.System]
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp27=0;$temp27<FolderName$1.length;$temp27++) {
               el$3=$uniCharAt(FolderName$1,$temp27);
               if (!el$3) continue;
               Result = (((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="-")||(el$3=="_")||(el$3==".")||(el$3==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$3, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 587, column: 34, file: SmartNJ.System]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$19) {
      var Result = "";
      var Separator$15 = "";
      Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$19,Separator$15)) {
         Result = FilePath$19;
      } else {
         Result = Separator$15+FilePath$19;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 567, column: 34, file: SmartNJ.System]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$20) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$20;
      if (!StrEndsWith(Result,Separator$16)) {
         Result+=Separator$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 223, column: 34, file: SmartNJ.System]
   ,IsValidPath:function(Self, FilePath$21) {
      var Result = false;
      var Separator$17 = "",
         PathParts$1 = [],
         Index$4 = 0,
         a$93 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$21.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$21]);
      } else {
         if (FilePath$21.length>0) {
            Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$21).split(Separator$17);
            Index$4 = 0;
            var $temp28;
            for(a$93=0,$temp28=PathParts$1.length;a$93<$temp28;a$93++) {
               part$1 = PathParts$1[a$93];
               {var $temp29 = part$1;
                  if ($temp29=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$21]);
                     return false;
                  }
                   else if ($temp29=="~") {
                     if (Index$4>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$21]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$4==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$4+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TRunPlatform enumeration
///  [line: 35, column: 3, file: SmartNJ.System]
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function ParamStr$1(index$2) {
   var Result = "";
   Result = process.argv[index$2];
   return Result
};
function ParamCount$1() {
   var Result = 0;
   Result = process.argv.length;
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp30 = token;
      if ($temp30=="darwin") {
         Result = 3;
      }
       else if ($temp30=="win32") {
         Result = 1;
      }
       else if ($temp30=="linux") {
         Result = 2;
      }
       else if ($temp30=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
/// TPath = class (TObject)
///  [line: 107, column: 3, file: System.IOUtils]
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TPath.GetDevice(const FilePath: String) : String
   ///  [line: 268, column: 22, file: System.IOUtils]
   ,GetDevice$3:function(FilePath$22) {
      var Result = "";
      var Access$2 = null,
         Error$1 = null;
      Access$2 = GetDirectoryParser();
      Error$1 = Access$2[2]();
      Result = Access$2[10](FilePath$22);
      if (Error$1[0]()) {
         throw Exception.Create($New(EPathError),Error$1[1]());
      }
      return Result
   }
   /// function TPath.IsPathRooted(const FilePath: String) : Boolean
   ///  [line: 257, column: 22, file: System.IOUtils]
   ,IsPathRooted$1:function(FilePath$23) {
      var Result = false;
      var Access$3 = null,
         Error$2 = null;
      Access$3 = GetDirectoryParser();
      Error$2 = Access$3[2]();
      Result = Access$3[7](FilePath$23);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$47($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
/// EPathError = class (EW3Exception)
///  [line: 105, column: 3, file: System.IOUtils]
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3RepeatResult enumeration
///  [line: 55, column: 3, file: System.Time]
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
/// TW3CustomRepeater = class (TObject)
///  [line: 71, column: 3, file: System.Time]
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive = false;
      $.FDelay$1 = 0;
      $.FHandle$1 = undefined;
   }
   /// procedure TW3CustomRepeater.AllocTimer()
   ///  [line: 446, column: 29, file: System.Time]
   ,AllocTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3CustomRepeater.FreeTimer(Self);
      }
      Self.FHandle$1 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   /// destructor TW3CustomRepeater.Destroy()
   ///  [line: 400, column: 30, file: System.Time]
   ,Destroy:function(Self) {
      if (Self.FActive) {
         TW3CustomRepeater.SetActive(Self,false);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomRepeater.FreeTimer()
   ///  [line: 455, column: 29, file: System.Time]
   ,FreeTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$1);
         Self.FHandle$1 = undefined;
      }
   }
   /// procedure TW3CustomRepeater.SetActive(const NewActive: Boolean)
   ///  [line: 414, column: 29, file: System.Time]
   ,SetActive:function(Self, NewActive) {
      if (NewActive!=Self.FActive) {
         try {
            if (Self.FActive) {
               TW3CustomRepeater.FreeTimer(Self);
            } else {
               TW3CustomRepeater.AllocTimer(Self);
            }
         } finally {
            Self.FActive = NewActive;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
};
/// TW3Dispatch = class (TObject)
///  [line: 135, column: 3, file: System.Time]
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TW3Dispatch.ClearInterval(const Handle: TW3DispatchHandle)
   ///  [line: 234, column: 29, file: System.Time]
   ,ClearInterval:function(Self, Handle$4) {
      clearInterval(Handle$4);
   }
   /// function TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ///  [line: 264, column: 28, file: System.Time]
   ,Execute$1:function(Self, EntryPoint$1, WaitForInMs) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs);
      return Result
   }
   /// procedure TW3Dispatch.RepeatExecute(const Entrypoint: TProcedureRef; const RepeatCount: Integer; const IntervalInMs: Integer)
   ///  [line: 272, column: 29, file: System.Time]
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute$1(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute$1(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   /// function TW3Dispatch.SetInterval(const Entrypoint: TProcedureRef; const IntervalDelayInMS: Integer) : TW3DispatchHandle
   ///  [line: 226, column: 28, file: System.Time]
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   ,Destroy:TObject.Destroy
};
function DateTimeToJDate(Present) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present-25569)*86400000));
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
/// TWriteFileOptions = record
///  [line: 71, column: 3, file: NodeJS.fs]
function Copy$TWriteFileOptions(s,d) {
   return d;
}
function Clone$TWriteFileOptions($) {
   return {

   }
}
/// TWatchOptions = record
///  [line: 93, column: 3, file: NodeJS.fs]
function Copy$TWatchOptions(s,d) {
   return d;
}
function Clone$TWatchOptions($) {
   return {

   }
}
/// TWatchFileOptions = record
///  [line: 88, column: 3, file: NodeJS.fs]
function Copy$TWatchFileOptions(s,d) {
   return d;
}
function Clone$TWatchFileOptions($) {
   return {

   }
}
/// TWatchFileListenerObject = record
///  [line: 83, column: 3, file: NodeJS.fs]
function Copy$TWatchFileListenerObject(s,d) {
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {

   }
}
/// TReadFileSyncOptions = record
///  [line: 66, column: 3, file: NodeJS.fs]
function Copy$TReadFileSyncOptions(s,d) {
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {

   }
}
/// TReadFileOptions = record
///  [line: 61, column: 3, file: NodeJS.fs]
function Copy$TReadFileOptions(s,d) {
   return d;
}
function Clone$TReadFileOptions($) {
   return {

   }
}
/// TCreateWriteStreamOptions = class (TObject)
///  [line: 122, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateWriteStreamOptionsEx = class (TCreateWriteStreamOptions)
///  [line: 131, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptions = class (TObject)
///  [line: 102, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptionsEx = class (TCreateReadStreamOptions)
///  [line: 112, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TAppendFileOptions = record
///  [line: 77, column: 3, file: NodeJS.fs]
function Copy$TAppendFileOptions(s,d) {
   return d;
}
function Clone$TAppendFileOptions($) {
   return {

   }
}
function NodeFsAPI() {
   return require("fs");
};
function NodePathAPI() {
   return require("path");
};
/// JPathParseData = record
///  [line: 20, column: 3, file: NodeJS.Path]
function Copy$JPathParseData(s,d) {
   return d;
}
function Clone$JPathParseData($) {
   return {

   }
}
function util() {
   return require("util");
};
/// TCustomCodec = class (TObject)
///  [line: 134, column: 3, file: System.Codec]
var TCustomCodec = {
   $ClassName:"TCustomCodec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBindings = [];
      $.FCodecInfo = null;
   }
   /// constructor TCustomCodec.Create()
   ///  [line: 290, column: 26, file: System.Codec]
   ,Create$28:function(Self) {
      TObject.Create(Self);
      Self.FCodecInfo = TCustomCodec.MakeCodecInfo$(Self);
      if (Self.FCodecInfo===null) {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.Create",Self,"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   /// destructor TCustomCodec.Destroy()
   ///  [line: 299, column: 25, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo);
      TObject.Destroy(Self);
   }
   /// function TCustomCodec.MakeCodecInfo() : TCodecInfo
   ///  [line: 305, column: 23, file: System.Codec]
   ,MakeCodecInfo:function(Self) {
      return null;
   }
   /// procedure TCustomCodec.RegisterBinding(const Binding: TCodecBinding)
   ///  [line: 310, column: 24, file: System.Codec]
   ,RegisterBinding:function(Self, Binding) {
      if (Self.FBindings.indexOf(Binding)<0) {
         Self.FBindings.push(Binding);
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.RegisterBinding",Self,"Binding already connected to codec error");
      }
   }
   /// procedure TCustomCodec.UnRegisterBinding(const Binding: TCodecBinding)
   ///  [line: 319, column: 24, file: System.Codec]
   ,UnRegisterBinding:function(Self, Binding$1) {
      var LIndex = 0;
      LIndex = Self.FBindings.indexOf(Binding$1);
      if (LIndex>=0) {
         Self.FBindings.splice(LIndex,1)
         ;
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.UnRegisterBinding",Self,"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TCustomCodec.$Intf={
   ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
   ,ICodecProcess:[TCustomCodec.EncodeData,TCustomCodec.DecodeData]
}
/// TUTF8Codec = class (TCustomCodec)
///  [line: 23, column: 3, file: System.codec.utf8]
var TUTF8Codec = {
   $ClassName:"TUTF8Codec",$Parent:TCustomCodec
   ,$Init:function ($) {
      TCustomCodec.$Init($);
   }
   /// function TUTF8Codec.CanUseClampedArray() : Boolean
   ///  [line: 67, column: 27, file: System.codec.utf8]
   ,CanUseClampedArray:function() {
      var Result = {v:false};
      try {
         var LTemp$12 = undefined;
         try {
            LTemp$12 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$9 = $W($e);
            return Result.v;
         }
         if (LTemp$12) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   /// procedure TUTF8Codec.EncodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 211, column: 22, file: System.codec.utf8]
   ,EncodeData:function(Self, Source$5, Target$3) {
      var LReader = null,
         LWriter = null,
         BytesToRead$1 = 0,
         LCache$2 = [],
         Text$5 = "";
      LReader = TW3CustomReader.Create$39($New(TStreamReader),Source$5);
      try {
         LWriter = TW3CustomWriter.Create$29($New(TStreamWriter),Target$3);
         try {
            BytesToRead$1 = TW3CustomReader.GetTotalSize$2(LReader)-TW3CustomReader.GetReadOffset(LReader);
            if (BytesToRead$1>0) {
               LCache$2 = TW3CustomReader.Read$1(LReader,BytesToRead$1);
               Text$5 = TDatatype.BytesToString$1(TDatatype,LCache$2);
               LCache$2.length=0;
               LCache$2 = TUTF8Codec.Encode(Self,Text$5);
               TW3CustomWriter.Write(LWriter,LCache$2);
            }
         } finally {
            TObject.Free(LWriter);
         }
      } finally {
         TObject.Free(LReader);
      }
   }
   /// procedure TUTF8Codec.DecodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 241, column: 22, file: System.codec.utf8]
   ,DecodeData:function(Self, Source$6, Target$4) {
      var LReader$1 = null,
         LWriter$1 = null,
         BytesToRead$2 = 0,
         LCache$3 = [],
         Text$6 = "";
      LReader$1 = TW3CustomReader.Create$39($New(TReader),Source$6);
      try {
         LWriter$1 = TW3CustomWriter.Create$29($New(TWriter),Target$4);
         try {
            BytesToRead$2 = TW3CustomReader.GetTotalSize$2(LReader$1)-TW3CustomReader.GetReadOffset(LReader$1);
            if (BytesToRead$2>0) {
               LCache$3 = TW3CustomReader.Read$1(LReader$1,BytesToRead$2);
               Text$6 = TUTF8Codec.Decode(Self,LCache$3);
               LCache$3.length=0;
               LCache$3 = TDatatype.StringToBytes$1(TDatatype,Text$6);
               TW3CustomWriter.Write(LWriter$1,LCache$3);
            }
         } finally {
            TObject.Free(LWriter$1);
         }
      } finally {
         TObject.Free(LReader$1);
      }
   }
   /// function TUTF8Codec.MakeCodecInfo() : TCodecInfo
   ///  [line: 46, column: 21, file: System.codec.utf8]
   ,MakeCodecInfo:function(Self) {
      var Result = null;
      var LAccess = null;
      var LVersion = {viMajor:0,viMinor:0,viRevision:0};
      Result = TObject.Create($New(TCodecInfo));
      LVersion.viMajor = 0;
      LVersion.viMinor = 2;
      LVersion.viRevision = 0;
      LAccess = $AsIntf(Result,"ICodecInfo");
      LAccess[0]("UTF8Codec");
      LAccess[1]("text\/utf8");
      LAccess[2](LVersion);
      LAccess[3]([6]);
      LAccess[5](1);
      LAccess[6](0);
      return Result
   }
   /// function TUTF8Codec.Encode(TextToEncode: String) : TByteArray
   ///  [line: 80, column: 21, file: System.codec.utf8]
   ,Encode:function(Self, TextToEncode$2) {
      var Result = {v:[]};
      try {
         var LEncoder = null;
         var LClip = null;
         var LTemp$13 = null;
         var n = 0;
         var LTyped = null;
         try {
            LEncoder = new TextEncoder();
         } catch ($e) {
            if (TextToEncode$2.length>0) {
               if (TUTF8Codec.CanUseClampedArray()) {
                  LClip = new Uint8ClampedArray(1);
                  LTemp$13 = new Uint8ClampedArray(1);
               } else {
                  LClip = new Uint8Array(1);
                  LTemp$13 = new Uint8Array(1);
               }
               var $temp31;
               for(n=1,$temp31=TextToEncode$2.length;n<=$temp31;n++) {
                  LClip[0]=TString.CharCodeFor(TString,TextToEncode$2.charAt(n-1));
                  if (LClip[0]<128) {
                     Result.v.push(LClip[0]);
                  } else if (LClip[0]>127&&LClip[0]<2048) {
                     LTemp$13[0]=((LClip[0]>>>6)|192);
                     Result.v.push(LTemp$13[0]);
                     LTemp$13[0]=((LClip[0]&63)|128);
                     Result.v.push(LTemp$13[0]);
                  } else {
                     LTemp$13[0]=((LClip[0]>>>12)|224);
                     Result.v.push(LTemp$13[0]);
                     LTemp$13[0]=(((LClip[0]>>>6)&63)|128);
                     Result.v.push(LTemp$13[0]);
                     Result.v.push((LClip[0]&63)|128);
                     Result.v.push(LTemp$13[0]);
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped = LEncoder.encode(TextToEncode$2);
            Result.v = TDatatype.TypedArrayToBytes$1(TDatatype,LTyped);
         } finally {
            LEncoder = null;
         }
      } finally {return Result.v}
   }
   /// function TUTF8Codec.Decode(const BytesToDecode: TByteArray) : String
   ///  [line: 145, column: 21, file: System.codec.utf8]
   ,Decode:function(Self, BytesToDecode$1) {
      var Result = {v:""};
      try {
         var LDecoder = null;
         var bytelen = 0,
            i$2 = 0,
            c = 0,
            c2 = 0,
            c2$1 = 0,
            c3 = 0,
            c4 = 0,
            u = 0,
            c2$2 = 0,
            c3$1 = 0,
            LTyped$1 = undefined;
         try {
            LDecoder = new TextDecoder();
         } catch ($e) {
            bytelen = BytesToDecode$1.length;
            if (bytelen>0) {
               i$2 = 0;
               while (i$2<bytelen) {
                  c = BytesToDecode$1[i$2];
                  ++i$2;
                  if (c<128) {
                     Result.v+=TString.FromCharCode(TString,c);
                  } else if (c>191&&c<224) {
                     c2 = BytesToDecode$1[i$2];
                     ++i$2;
                     Result.v+=TString.FromCharCode(TString,(((c&31)<<6)|(c2&63)));
                  } else if (c>239&&c<365) {
                     c2$1 = BytesToDecode$1[i$2];
                     ++i$2;
                     c3 = BytesToDecode$1[i$2];
                     ++i$2;
                     c4 = BytesToDecode$1[i$2];
                     ++i$2;
                     u = (((((c&7)<<18)|((c2$1&63)<<12))|((c3&63)<<6))|(c4&63))-65536;
                     Result.v+=TString.FromCharCode(TString,(55296+(u>>>10)));
                     Result.v+=TString.FromCharCode(TString,(56320+(u&1023)));
                  } else {
                     c2$2 = BytesToDecode$1[i$2];
                     ++i$2;
                     c3$1 = BytesToDecode$1[i$2];
                     ++i$2;
                     Result.v+=TString.FromCharCode(TString,(((c&15)<<12)|(((c2$2&63)<<6)|(c3$1&63))));
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$1 = TDatatype.BytesToTypedArray$1(TDatatype,BytesToDecode$1);
            Result.v = LDecoder.decode(LTyped$1);
         } finally {
            LDecoder = null;
         }
      } finally {return Result.v}
   }
   ,Destroy:TCustomCodec.Destroy
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TUTF8Codec.$Intf={
   ICodecProcess:[TUTF8Codec.EncodeData,TUTF8Codec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
/// TCodecVersionInfo = record
///  [line: 38, column: 3, file: System.Codec]
function Copy$TCodecVersionInfo(s,d) {
   d.viMajor=s.viMajor;
   d.viMinor=s.viMinor;
   d.viRevision=s.viRevision;
   return d;
}
function Clone$TCodecVersionInfo($) {
   return {
      viMajor:$.viMajor,
      viMinor:$.viMinor,
      viRevision:$.viRevision
   }
}
/// TCodecManager = class (TObject)
///  [line: 159, column: 3, file: System.Codec]
var TCodecManager = {
   $ClassName:"TCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs = [];
   }
   /// function TCodecManager.CodecByClass(const ClsType: TCodecClass) : TCustomCodec
   ///  [line: 249, column: 24, file: System.Codec]
   ,CodecByClass:function(Self, ClsType) {
      var Result = null;
      var a$94 = 0;
      var LItem$3 = null;
      var a$95 = [];
      Result = null;
      a$95 = Self.FCodecs;
      var $temp32;
      for(a$94=0,$temp32=a$95.length;a$94<$temp32;a$94++) {
         LItem$3 = a$95[a$94];
         if (TObject.ClassType(LItem$3.ClassType)==ClsType) {
            Result = LItem$3;
            break;
         }
      }
      return Result
   }
   /// destructor TCodecManager.Destroy()
   ///  [line: 193, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   /// procedure TCodecManager.RegisterCodec(const CodecClass: TCodecClass)
   ///  [line: 262, column: 25, file: System.Codec]
   ,RegisterCodec:function(Self, CodecClass) {
      var LItem$4 = null;
      LItem$4 = TCodecManager.CodecByClass(Self,CodecClass);
      if (LItem$4===null) {
         LItem$4 = TCustomCodec.Create$28($NewDyn(CodecClass,""));
         Self.FCodecs.push(LItem$4);
      } else {
         throw EW3Exception.Create$27($New(ECodecManager),"TCodecManager.RegisterCodec",Self,"Codec already registered error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TCodecInfo = class (TObject)
///  [line: 71, column: 3, file: System.Codec]
var TCodecInfo = {
   $ClassName:"TCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout = $.ciMime = $.ciName = "";
      $.ciDataFlow = [0];
      $.ciInput = 0;
      $.ciOutput = 0;
      $.ciVersion = {viMajor:0,viMinor:0,viRevision:0};
   }
   /// procedure TCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow)
   ///  [line: 494, column: 22, file: System.Codec]
   ,SetDataFlow:function(Self, coFlow) {
      Self.ciDataFlow = coFlow.slice(0);
   }
   /// procedure TCodecInfo.SetDescription(const coInfo: String)
   ///  [line: 489, column: 22, file: System.Codec]
   ,SetDescription:function(Self, coInfo) {
      Self.ciAbout = coInfo;
   }
   /// procedure TCodecInfo.SetInput(const InputType: TCodecDataFormat)
   ///  [line: 479, column: 22, file: System.Codec]
   ,SetInput:function(Self, InputType) {
      Self.ciInput = InputType;
   }
   /// procedure TCodecInfo.SetMime(const coMime: String)
   ///  [line: 467, column: 22, file: System.Codec]
   ,SetMime:function(Self, coMime) {
      Self.ciMime = coMime;
   }
   /// procedure TCodecInfo.SetName(const coName: String)
   ///  [line: 462, column: 22, file: System.Codec]
   ,SetName:function(Self, coName) {
      Self.ciName = coName;
   }
   /// procedure TCodecInfo.SetOutput(const OutputType: TCodecDataFormat)
   ///  [line: 484, column: 22, file: System.Codec]
   ,SetOutput:function(Self, OutputType) {
      Self.ciOutput = OutputType;
   }
   /// procedure TCodecInfo.SetVersion(const coVersion: TCodecVersionInfo)
   ///  [line: 472, column: 22, file: System.Codec]
   ,SetVersion:function(Self, coVersion) {
      Self.ciVersion.viMajor = coVersion.viMajor;
      Self.ciVersion.viMinor = coVersion.viMinor;
      Self.ciVersion.viRevision = coVersion.viRevision;
   }
   ,Destroy:TObject.Destroy
};
TCodecInfo.$Intf={
   ICodecInfo:[TCodecInfo.SetName,TCodecInfo.SetMime,TCodecInfo.SetVersion,TCodecInfo.SetDataFlow,TCodecInfo.SetDescription,TCodecInfo.SetInput,TCodecInfo.SetOutput]
}
/// TCodecDataFormat enumeration
///  [line: 52, column: 3, file: System.Codec]
var TCodecDataFormat = [ "cdBinary", "cdText" ];
/// function TCodecDataFlowHelper.Ordinal(const Self: TCodecDataFlow) : Integer
///  [line: 426, column: 31, file: System.Codec]
function TCodecDataFlowHelper$Ordinal(Self$11) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$11,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$11,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
/// TCodecDataDirection enumeration
///  [line: 47, column: 3, file: System.Codec]
var TCodecDataDirection = { 1:"cdRead", 2:"cdWrite" };
/// TCodecBinding = class (TObject)
///  [line: 102, column: 3, file: System.Codec]
var TCodecBinding = {
   $ClassName:"TCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec = null;
   }
   /// constructor TCodecBinding.Create(const EndPoint: TCustomCodec)
   ///  [line: 334, column: 27, file: System.Codec]
   ,Create$41:function(Self, EndPoint) {
      var LAccess$1 = null;
      TObject.Create(Self);
      if (EndPoint!==null) {
         Self.FCodec = EndPoint;
         LAccess$1 = $AsIntf(Self.FCodec,"ICodecBinding");
         LAccess$1[0](Self);
      } else {
         throw EW3Exception.Create$27($New(ECodecBinding),"TCodecBinding.Create",Self,"Binding failed, invalid endpoint error");
      }
      return Self
   }
   /// destructor TCodecBinding.Destroy()
   ///  [line: 349, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      var LAccess$2 = null;
      LAccess$2 = $AsIntf(Self.FCodec,"ICodecBinding");
      LAccess$2[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// ECodecManager = class (EW3Exception)
///  [line: 29, column: 3, file: System.Codec]
var ECodecManager = {
   $ClassName:"ECodecManager",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecError = class (EW3Exception)
///  [line: 30, column: 3, file: System.Codec]
var ECodecError = {
   $ClassName:"ECodecError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecBinding = class (EW3Exception)
///  [line: 28, column: 3, file: System.Codec]
var ECodecBinding = {
   $ClassName:"ECodecBinding",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager() {
   var Result = null;
   if (!__Manager) {
      __Manager = TObject.Create($New(TCodecManager));
   }
   Result = __Manager;
   return Result
};
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$7 = 0;
var a$10 = 0;
var a$9 = 0;
var a$8 = 0;
var a$11 = 0;
var a$12 = 0;
var a$30 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var NodeProgram = null;
var __Parser = null;
var __EventsRef = undefined;
var __RESERVED = [];
var __RESERVED = ["$ClassName", "$Parent", "$Init", "toString", "toLocaleString", "valueOf", "indexOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor", "destructor"].slice();
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
var __Manager = null;
var __Def_Converter = TDataTypeConverter.Create$15$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
SetupTypeLUT();
TCodecManager.RegisterCodec(CodecManager(),TUTF8Codec);
var __EventsRef = require("events");
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$47($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$47($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var $Application = function() {
   NodeProgram = TLinkApp.Create$3($New(TLinkApp));
   TLinkApp.Execute(NodeProgram);
}
$Application();

